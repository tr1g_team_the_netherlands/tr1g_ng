
PROGRAM _INIT
	MpPackMLMode_0.ModeID := 1;

	EM[0].Name	:= 'Rehanger';
	EM[1].Name	:= 'Rejector';
	
END_PROGRAM

PROGRAM _CYCLIC
	MpPackMLMode_0.Start		:= FALSE;	//Bridged in StateMachine below
	MpPackMLMode_0.Stop			:= gMmiCmd.Cmd.xStop;
	MpPackMLMode_0.Reset		:= gMmiCmd.Cmd.xReset;
	MpPackMLMode_0.Clear		:= gMmiCmd.Cmd.xClear;
	MpPackMLMode_0.Hold			:= FALSE;
	MpPackMLMode_0.Unhold		:= FALSE;
	MpPackMLMode_0.Suspend		:= gMmiCmd.Cmd.xPauze;
	MpPackMLMode_0.Unsuspend	:= gMmiCmd.Cmd.xStart;  //misused START to give a UN-suspend cmd
	MpPackMLMode_0.Abort		:= FALSE;

	xSC := (EM[0].xSC AND EM[1].xSC);
	
	//PACK ML
	CASE MpPackMLCore_0.ModeCurrent OF
		Production: // #1 Mode Production
			CASE MpPackMLCore_0.StateCurrent OF
				mpPACKML_STATE_STOPPED:		
					PilotLight(Color := Green);	
				
				mpPACKML_STATE_RESETTING:
					PilotLight(Color := Blue);
					MpPackMLMode_0.StateComplete := xSC;

					
				mpPACKML_STATE_IDLE:
					PilotLight(Color := Blue);
					MpPackMLMode_0.Start	:= TRUE;
				
				mpPACKML_STATE_STARTING:
					PilotLight(Color := Blue);
					MpPackMLMode_0.StateComplete := xSC;
				
				mpPACKML_STATE_EXECUTE:
					PilotLight(Color := White);
					
				mpPACKML_STATE_SUSPENDING:
					PilotLight(Color := Blue);
				
					IF xSC THEN
						StateTimer;
						IF timState >= T#5s THEN
							MpPackMLMode_0.StateComplete := TRUE;		
						END_IF;
					END_IF;
					
				
				mpPACKML_STATE_SUSPENDED:
					PilotLight(Color := Blue_Flash);
				
				mpPACKML_STATE_UNSUSPENDING:
					PilotLight(Color := Blue);
					MpPackMLMode_0.StateComplete := xSC;
				
				mpPACKML_STATE_STOPPING:
					PilotLight(Color := Blue);
					IF xSC THEN
						StateTimer;
						IF timState >= T#2s THEN
							MpPackMLMode_0.StateComplete := TRUE;		
						END_IF;
					END_IF;
				
				mpPACKML_STATE_ABORTING:
					PilotLight(Color := Yellow);
					IF gFbAirPres.rAirPres2 < 0.5 AND gFeedBackFricDisc[0] = 0 THEN
						MpPackMLMode_0.StateComplete := TRUE;
					END_IF
				
				mpPACKML_STATE_ABORTED:
					PilotLight(Color := Red);
								
				mpPACKML_STATE_CLEARING:
					StateTimer;
					PilotLight(Color := Yellow);
					xEnableAir		:= TRUE;	
					
					IF timState > T#3s THEN								//Give some time to reach stable airpressure.
						MpPackMLMode_0.StateComplete := TRUE;	
					END_IF;
					
					
				
			END_CASE;
	END_CASE;
	
	hw.Do_.xLightBlue	:= PilotLight.Wire.BlueWire;
	hw.Do_.xLightGreen	:= PilotLight.Wire.GreenWire;
	hw.Do_.xLightRed	:= PilotLight.Wire.RedWire;
	hw.Do_.xLightWhite	:= PilotLight.Wire.WhiteWire;
	hw.Do_.xLightYellow	:= PilotLight.Wire.YellowWire;
	
	MpPackMLMode_0.Abort := (gFbAirPres.rAirPres1 < Cfg.Config.General.rMinAirPres OR  gMmiCmd.xReleaseAir);// AND 
						//NOT (MpPackMLCore_0.StateCurrent = mpPACKML_STATE_STOPPED OR MpPackMLCore_0.StateCurrent = mpPACKML_STATE_STOPPING OR MpPackMLCore_0.StateCurrent = mpPACKML_STATE_CLEARING);
	
	
	MpPackMLCore_0(MpLink := ADR(gPackMLCore), Enable := TRUE);
	MpPackMLMode_0(MpLink := ADR(gPackMLCore), Enable := TRUE, Activate := TRUE);
	MpPackMLMode_0.StateComplete := FALSE;
	
	IF MpPackMLCore_0.StateCurrent = mpPACKML_STATE_ABORTING OR MpPackMLCore_0.StateCurrent = mpPACKML_STATE_ABORTED THEN
		xEnableAir := FALSE; 
	END_IF;
	

	RTInfo_0(enable := TRUE );
	IF MpPackMLMode_0.StateCurrent <> preState THEN
		timState	:= 0;
		preState	:= MpPackMLMode_0.StateCurrent;
	END_IF;
	
				
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

