
TYPE
	MMI_PilotLight_Colors_typ : 
		(
		OFF := 0,
		ON := 1,
		Test := 2,
		Green := 3,
		Red := 4,
		Yellow := 5,
		Blue := 6,
		White := 7,
		Green_Flash := 8,
		Red_Flash := 9,
		Yellow_Flash := 10,
		Blue_Flash := 11,
		White_Flash := 12,
		Green_Red_Flash := 13,
		Green_Yellow_Flash := 14,
		Green_Blue_Flash := 15,
		Green_White_Flash := 16,
		Red_Yellow_Flash := 17,
		Red_Blue_Flash := 18,
		Red_White_Flash := 19,
		Yellow_Blue_Flash := 20,
		Yellow_White_Flash := 21,
		Blue_White_Flash := 22,
		Green_Red_Yellow_Flash := 23,
		Green_Red_Blue_Flash := 24,
		Green_Red_White_Flash := 25,
		Green_Yellow_Blue_Flash := 26,
		Green_Yellow_White_Flash := 27,
		Green_Blue_White_Flash := 28,
		Red_Yellow_Blue_Flash := 29,
		Red_Yellow_White_Flash := 30,
		Red_Blue_White_Flash := 31,
		Yellow_Blue_White_Flash := 32,
		GRYBW_Flash := 33
		);
	MMI_PilotLight_Wires_typ : 	STRUCT 
		GreenWire : BOOL;
		RedWire : BOOL;
		YellowWire : BOOL;
		BlueWire : BOOL;
		WhiteWire : BOOL;
	END_STRUCT;
END_TYPE
