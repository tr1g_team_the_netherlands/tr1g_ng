
FUNCTION_BLOCK EncoderCalculations
	VAR_INPUT
		diRawEnc : DINT;
		EncSet : EncSet_typ;
	END_VAR
	VAR_OUTPUT
		Enc : Encoder_typ;
	END_VAR
	VAR
		udiEncPos : UDINT;
		rEncRpm : REAL;
		udiEncExt : EncoderExtender;
		rAvgRpm : MTBasicsPT1 := (Enable:=TRUE,Gain:=1.0,TimeConstant:=1.0);
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderExtender
	VAR_INPUT
		diEncVal : DINT;
		diEncResolution : DINT;
	END_VAR
	VAR_OUTPUT
		udiEncoder : UDINT;
	END_VAR
	VAR
		diCurrEncVal : DINT;
		diDiffEncVal : DINT;
		diOldEncVal : DINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION encOverUnderFlow : DINT
	VAR_INPUT
		diDiffEncoder : DINT;
		diResolution : DINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK Settings
	VAR_INPUT
		SetRehanger_in : SetRehanger_typ;
		SetCarDisc_in : SetCarDisc_typ;
		SetRejetor_in : SetRejector_typ;
		SetConfig_in : Configuration_typ;
		rEncSupPulsesMeter : REAL;
		rEncDisPulsesMeter : REAL;
	END_VAR
	VAR_OUTPUT
		SetRehanger_Out : SetRehanger_typ;
		SetCarDisc_Out : SetCarDisc_typ;
		SetRejector_Out : SetRejector_typ;
		SetConfig_Out : Configuration_typ;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK Rejector
	VAR_INPUT
		PackMLMode : DINT;
		PackMLState : DINT;
		Enc : Encoder_typ;
		Set : SetRejector_typ;
		Cfg : SetRejectorConfig_typ;
		xSensorLeft : BOOL;
		xSensorRight : BOOL;
	END_VAR
	VAR_OUTPUT
		xStateComplete : BOOL;
		xCylOpen : BOOL;
		xCylClose : BOOL;
	END_VAR
	VAR
		rOpenCor : REAL;
		rCloseCor : REAL;
		xReLeft : R_TRIG;
		xReRight : R_TRIG;
		xDetWindowEna : BOOL;
		udiDetWindowEncPos : UDINT;
		xRejProd : BOOL;
		xTpCylOut : TP;
		xTpCylIn : TP;
		xEnaRejector : BOOL;
		xInit : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK ControlCPX_FB40
	VAR_INPUT
		aValve : ARRAY[0..7] OF FestoValve;
		uiPresIn : ARRAY[0..3] OF UINT;
	END_VAR
	VAR_OUTPUT
		usiValve_1to4 : USINT;
		usiValve_5to8 : USINT;
		rPresOut : ARRAY[0..3] OF REAL;
	END_VAR
	VAR
		rHistAir0 : ARRAY[0..49999] OF REAL;
		uiIndex0 : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK ShakleTable
	VAR_INPUT
		udiEncPos : UDINT;
		udiEncPosRes : UDINT;
		aPulsDis : KickersPulsDistances_typ;
		xAddNewRow : BOOL;
		xProdDet : BOOL;
		xPdsSig : BOOL;
		xResetTable : BOOL;
		xCarLeft : BOOL;
		usiPdsValue : USINT;
	END_VAR
	VAR_OUTPUT
		audiKickerPos : KickersPositions;
		xDataLogTrigger : BOOL;
		xOverFlowTable : BOOL;
		xCarGo : BOOL;
		ShakleTab : ARRAY[0..99] OF ShakleRow_typ;
		xFbCarLeft : BOOL;
		xFbReject : BOOL;
	END_VAR
	VAR
		xReAddNewRow : R_TRIG;
		exportShakleRow : ShakleRow_typ;
		siIndexTab : SINT;
		udiShkNr : UDINT;
		udiProdDet : UDINT;
		usiProdType : USINT;
		udiPdsDet : UDINT;
		usiPdsVal : USINT;
		usiArrSize : USINT;
		usiIndexProdDet : USINT;
		usiIndexPdsSigDet : USINT;
		usiIndexKickOut : USINT;
		usiIndexKickIn : USINT;
		xReProdDet : R_TRIG;
		xRePdsSig : R_TRIG;
		usiResetTabLoop : USINT;
		usiIndexCarGo : USINT;
		xFeCarLeft : F_TRIG;
		udiActWindow : UDINT;
		xTestOn : BOOL;
		sProdIndication : SINT;
		iProdIndicationWindow : INT;
		sPdsIndication : USINT;
		iPdsIndicationWindow : INT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK Rehanger
	VAR_INPUT
		PackMLMode : DINT;
		PackMLState : MpPackMLStateEnum;
		Di_ : Di_Typ;
		Enc : Encoder_typ;
		SetRehang : SetRehanger_typ;
		xPdsReject : BOOL;
		xRejCar : BOOL;
		SetCarDisc : SetCarDisc_typ;
		CfgCarDisc : SetCarDiscConfig_typ;
		xSupLineRun : BOOL;
		xDisLineRun : BOOL;
		uiSpdDrive : UINT;
		IndicationLight : IndicatorLight_typ;
	END_VAR
	VAR_OUTPUT
		xSperPdsOpen : BOOL;
		xSperPdsClose : BOOL;
		xSperMxOpen : BOOL;
		xSperMxClose : BOOL;
		xKicker1OUT : BOOL;
		xKicker2OUT : BOOL;
		xKicker1IN : BOOL;
		xKicker2IN : BOOL;
		xStateComplete : BOOL;
		xSpdCarDisc : CarDiscCtrl_typ;
		xdataLogTrigger : BOOL;
		xOverFlowTable : BOOL;
		ShakleTable : ShakleTable;
		xIndicationLight : BOOL;
		usiPdsValue : USINT;
	END_VAR
	VAR
		xDetCarPds : BOOL;
		xDetNulCarPds : BOOL;
		xDetCarMx : BOOL;
		xDetNulCarMx : BOOL;
		xReDetNulCarPds : R_TRIG;
		xFeDetNulCarPds : F_TRIG;
		xReDetNulCarMx : R_TRIG;
		xFeDetNulCarMx : F_TRIG;
		rK1OutCor : REAL;
		rK2OutCor : REAL;
		rK1InCor : REAL;
		rK2InCor : REAL;
		udiProdMin : UDINT;
		udiProdMax : UDINT;
		udiPdsMin : UDINT;
		udiPdsMax : UDINT;
		udiCarGo : UDINT;
		udiKick1Out : UDINT;
		udiKick2Out : UDINT;
		udiKick1In : UDINT;
		udiKick2In : UDINT;
		xShkTableCarGo : BOOL;
		Kick : KickerStates_typ;
		uiNrCarPres : UINT;
		usiCarCntMxPds : CTUD;
		usiCarCntPdsMx : CTUD;
		xPdsMxFull : BOOL;
		usiSetPdsMxFull : USINT := 9;
		xMxPdsFull : BOOL;
		usiSetMxPdsFull : USINT := 9;
		SperPDS : SperPDS;
		xInitSperStart : BOOL;
		xSelectDetCarMx : BOOL;
		SperMX : SperMX;
		xInitCarDone : BOOL;
		xEnaFricDiscInit : BOOL;
		xEnaFricDisc : BOOL;
		rFricDiscSpeed : REAL;
		xResetInitKickers : BOOL;
		ResetState : RehangResetState;
		xTpInitReset : TP;
		xFeInitReset : F_TRIG;
		usiInitCarCnt : CTU;
		xInitRoundPds : BOOL;
		xInitRoundMx : BOOL;
		xRsRejOneCar : RS;
		timState : TIME;
		RTInfo_0 : RTInfo;
		MS_TO_RPM_0 : MS_TO_RPM;
		RPM_to_MoviGearSpd_0 : RPM_to_MoviGearSpd;
		aDis : KickersPulsDistances_typ;
		CTDSusLoop : CTD;
		aPdsCarVal : ARRAY[0..14] OF USINT;
		usiCarIndexDis : USINT;
		xFeDetCarMx : F_TRIG;
		xFeDetCarPds : F_TRIG;
		usiCarIndexSup : USINT;
		xTestOn : BOOL;
		usiSimPdsVal : USINT;
		xReShkDet : R_TRIG;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK SperMX
	VAR_INPUT
		Set : SetCarDisc_typ;
		Cfg : SetCarDiscConfig_typ;
		xInitCarDet : BOOL;
		xDetHook : BOOL;
		xCarDet : BOOL;
		xInitRound : BOOL;
		rSpdRpmFricDisc : REAL;
		xDiAfterSlot : BOOL;
		xAfterSlotFull : BOOL;
	END_VAR
	VAR_OUTPUT
		xSperOpen : BOOL;
		xSperClose : BOOL;
	END_VAR
	VAR
		timWaitBeforeOpen : TIME;
		xTpWaitTime : TP;
		xFeWaitTime : F_TRIG;
		xFeMxPdsFull : F_TRIG;
		timWaitOpen : TIME;
		xReleaseSper : BOOL;
		xTpSperOpen : TP;
		xTpSperClose : TP;
		RPM_TO_TimeDistance_0 : RPM_TO_TimeDistance;
		CylActTime_Dis_0 : CylActTime_Dis;
		rMsFricDisc : REAL;
		xFeCloseAction : F_TRIG;
		xTpWaitTillPosition : TP;
		timWaitAfterClose : TIME;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK SperPDS
	VAR_INPUT
		Cfg : SetCarDiscConfig_typ;
		Set : SetCarDisc_typ;
		xDetHook : BOOL;
		Enc : Encoder_typ;
		xInitRound : BOOL;
		rSpdRpmFricDisc : REAL;
		xAfterSlotFull : BOOL;
		xReqRelCar : BOOL;
	END_VAR
	VAR_OUTPUT
		xSperOpen : BOOL;
		xSperClose : BOOL;
	END_VAR
	VAR
		timWaitBeforeOpen : TIME;
		xTpWaitTime : TP;
		xFeWait : F_TRIG;
		timWaitOpen : TIME;
		xReleasesper : BOOL;
		xTpSperOpen : TP;
		xTpSperClose : TP;
		timWaitSynchro : TIME;
		timWaitFricDisc : TIME;
		rRpsPickup : REAL;
		rRpsSynchro : REAL;
		rMsSynchro : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK calcCircumference
	VAR_INPUT
		rRadius : {REDUND_UNREPLICABLE} REAL;
	END_VAR
	VAR_OUTPUT
		rCalcCircumference : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RPM_to_MoviGearSpd
	VAR_INPUT
		rRPM : {REDUND_UNREPLICABLE} REAL;
		uiMaxCtrlLvl : {REDUND_UNREPLICABLE} UINT;
		uiMinCtrlLvl : {REDUND_UNREPLICABLE} UINT;
		rMaxRPM : {REDUND_UNREPLICABLE} REAL;
		rMinRPM : {REDUND_UNREPLICABLE} REAL;
		xReverse : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		iRPM_to_MoviGearSpd : INT;
	END_VAR
	VAR
		rRequestSpeed : {REDUND_UNREPLICABLE} REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RPM_TO_TimeDistance
	VAR_INPUT
		rRPM : {REDUND_UNREPLICABLE} REAL;
		rRadius : {REDUND_UNREPLICABLE} REAL;
		rDistance : {REDUND_UNREPLICABLE} REAL;
	END_VAR
	VAR_OUTPUT
		timRPM_TO_TimeDistance : TIME;
	END_VAR
	VAR
		rRpmMsec : {REDUND_UNREPLICABLE} REAL;
		RPM_TO_M_MSec_0 : RPM_TO_M_MSec;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RPM_TO_M_MSec
	VAR_INPUT
		rRPM : {REDUND_UNREPLICABLE} REAL;
		rRadius : {REDUND_UNREPLICABLE} REAL;
	END_VAR
	VAR_OUTPUT
		rRPM_TO_M_MSec : REAL;
	END_VAR
	VAR
		rCircumference : {REDUND_UNREPLICABLE} REAL;
		calcCircumference_0 : calcCircumference;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK synchro_MS_To_RPM_TO_TIME
	VAR_INPUT
		rMeterSecond : {REDUND_UNREPLICABLE} REAL;
		rRadiusSynchroCar : {REDUND_UNREPLICABLE} REAL;
		rRadiusSynchro : {REDUND_UNREPLICABLE} REAL;
		rDistance : {REDUND_UNREPLICABLE} REAL;
		timMinTime : {REDUND_UNREPLICABLE} TIME;
	END_VAR
	VAR_OUTPUT
		timSynchro_MS_To_RPM_TO_TIME : TIME;
	END_VAR
	VAR
		rCircumference : {REDUND_UNREPLICABLE} REAL;
		timWaitTime : {REDUND_UNREPLICABLE} TIME;
		rRpmSpeed : {REDUND_UNREPLICABLE} REAL;
		calcCircumference_0 : calcCircumference;
		RPM_TO_TimeDistance_0 : RPM_TO_TimeDistance;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK CylActTime_Dis
	VAR_INPUT
		rDistance : {REDUND_UNREPLICABLE} REAL; (*distance in [m]*)
		rRPM : {REDUND_UNREPLICABLE} REAL;
		rRadius : {REDUND_UNREPLICABLE} REAL;
		timMinTime : {REDUND_UNREPLICABLE} TIME;
	END_VAR
	VAR_OUTPUT
		timCylActTime_Dis : TIME;
	END_VAR
	VAR
		timWaitTime : {REDUND_UNREPLICABLE} TIME;
		RPM_TO_TimeDistance_0 : RPM_TO_TimeDistance;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK MS_TO_RPM
	VAR_INPUT
		rMeterSecond : {REDUND_UNREPLICABLE} REAL;
		rRadius : {REDUND_UNREPLICABLE} REAL;
	END_VAR
	VAR_OUTPUT
		iMS_TO_RPM : INT;
	END_VAR
	VAR
		rCircumference : {REDUND_UNREPLICABLE} REAL;
		calcCircumference_0 : calcCircumference;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK SimulateEncoder
	VAR_INPUT
		ticks : INT;
		encValPeriode : UDINT := 0;
		rCircumference : REAL;
	END_VAR
	VAR_OUTPUT
		diRawEnc : DINT;
	END_VAR
	VAR
		udiEncCounter : UDINT;
		udiEncVal : UDINT;
		RTInfo_0 : RTInfo;
		rMs : REAL;
		rRPM : REAL;
		udiEncPosTEST : UDINT;
		rEncRpmTEST : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION MoviGearSpd_to_RPM : REAL
	VAR_INPUT
		rMoviGearSpd : REAL;
		uiMaxCtrlLvl : UINT;
		uiMinCtrlLvl : UINT;
		rMaxRPM : REAL;
		rMinRPM : REAL;
	END_VAR
END_FUNCTION
