
FUNCTION_BLOCK MS_TO_RPM
	//convert the speed in M/s into RPM
	calcCircumference_0(rRadius := rRadius );
	
	rCircumference:= calcCircumference_0.rCalcCircumference;
		
	iMS_TO_RPM :=  REAL_TO_INT((60.0 * rMeterSecond) / rCircumference);

END_FUNCTION_BLOCK

FUNCTION_BLOCK RPM_TO_M_MSec
	//Convert the Speed in RPM into travelled distance within 1 millisec
	
	calcCircumference_0(rRadius := rRadius );
	rCircumference := calcCircumference_0.rCalcCircumference;
	
	rRPM_TO_M_MSec := rRPM * rCircumference  * (1.0/60.0) / 1000.0; 
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK CylActTime_Dis
	//linked to the speed the open time of the cil with respect to the traveld distance.
	//check for minimum time. If 0ms the make 1
	RPM_TO_TimeDistance_0(rRPM:= rRPM, rRadius:= rRadius, rDistance:= rDistance);
	
	timWaitTime:= RPM_TO_TimeDistance_0.timRPM_TO_TimeDistance;
	
	IF timWaitTime < timMinTime THEN
		timWaitTime := timMinTime; //minimum Time
	END_IF;
	
	timCylActTime_Dis := timWaitTime;

END_FUNCTION_BLOCK

FUNCTION_BLOCK synchro_MS_To_RPM_TO_TIME
	(* Synchrowheel speed 
	first caculate the synchroweel speed in RPM based on PDS line speed [m/s] (as circumference take synchrowheel + carrier)
	second recalulate the speed [m/s] with the synchrowheel circumference
	thirth calculate the time needed TO travel the given (param) distance
	last check IF the time > 1ms.
	*)
	calcCircumference_0(rRadius := rRadiusSynchroCar );
	rCircumference := calcCircumference_0.rCalcCircumference;			//calc circumference (synchrowheel + carrier)
	rRpmSpeed := (rMeterSecond * 60.0)/ rCircumference; 						//calc speed [m/s] into RPM

	RPM_TO_TimeDistance_0(rRPM:= rRpmSpeed, rRadius:= rRadiusSynchro, rDistance:= rDistance);
	
	timWaitTime:= RPM_TO_TimeDistance_0.timRPM_TO_TimeDistance;	//calculate travel time
	
	IF timWaitTime < timMinTime THEN
		timWaitTime := timMinTime; //minimum Time
	END_IF;
	
	timSynchro_MS_To_RPM_TO_TIME := timWaitTime;
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK RPM_TO_TimeDistance
	//calculate the traveled distance in one millisecond
	RPM_TO_M_MSec_0(rRPM := rRPM, rRadius := rRadius);
	
	rRpmMsec := RPM_TO_M_MSec_0.rRPM_TO_M_MSec;				
		
	timRPM_TO_TimeDistance := REAL_TO_TIME((ABS(rDistance / rRpmMsec))); 
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK calcCircumference 
	//Calculate the Circumference
	
	rCalcCircumference := PI * 2.0 * rRadius; 
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK RPM_to_MoviGearSpd 
	// Movigear has a minimum and maximum RPM (i.e. 11.5 - 115.4)
	// is controlled by a minimum value and maximum value (i.e. 1000 - 10000)
	// so RPM has to be converted into the correct CtrlLevel.
		
	
	//take minimum control speed into account if this requested speed is lower, then take the min speed.
	rRequestSpeed 		:= (rRPM * (UINT_TO_REAL(uiMaxCtrlLvl - uiMinCtrlLvl) / (rMaxRPM - rMinRPM)));	
	IF ABS(rRequestSpeed) < uiMinCtrlLvl THEN
		rRequestSpeed := uiMinCtrlLvl;
	END_IF
	
	IF xReverse THEN
		rRequestSpeed := rRequestSpeed * -1.0;
	END_IF
	
	iRPM_to_MoviGearSpd 	:= REAL_TO_INT(rRequestSpeed);
	
END_FUNCTION_BLOCK

FUNCTION MoviGearSpd_to_RPM
	
	MoviGearSpd_to_RPM	:= rMoviGearSpd / (UINT_TO_REAL(uiMaxCtrlLvl - uiMinCtrlLvl) / (rMaxRPM - rMinRPM));
	
		
	
END_FUNCTION
