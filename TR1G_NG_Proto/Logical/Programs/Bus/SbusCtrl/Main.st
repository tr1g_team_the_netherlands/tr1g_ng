
PROGRAM _INIT
	fb_SbusMasterDriver(
		xEnable:=TRUE,
		strNetId:='IF7', //strNetId:='SS1.IF1',
		usiBaudRate:=50,
		uiMessages:=5);
	 
END_PROGRAM

PROGRAM _CYCLIC
		//start stop
	IF gFricDisc.Run THEN
		aiPoUser1[0] := 6;
	ELSE
		aiPoUser1[0] := 0;
	END_IF;
	
	//Speed
	aiPoUser1[1] := REAL_TO_INT(gFricDisc.SpeedDrive);
	aiPoUser1[2] := REAL_TO_INT(gFricDisc.Ramp);
	
		
	// ----------- Call and intputs-------------------------------- 
	fb_SbusInfoMaster_1();
	fb_SbusInfoMaster_1.xEnable:=TRUE;
	fb_SbusInfoMaster_1.udiHandle:=fb_SbusMasterDriver.udiHandle;
	
	// ------------outputs ----------------------------------------
	structBusInfoMaster_1 := fb_SbusInfoMaster_1.structInfo;
	
	// ------------Call and intputs--------------------------------
	fb_SbusController_drive_1();
	fb_SbusController_drive_1.aiPo:=aiPoUser1;
	fb_SbusController_drive_1.udiHandle:=fb_SbusMasterDriver.udiHandle;
	fb_SbusController_drive_1.uiMotorNumber:=2;		//LET OP STAAT NU NOG OP 2, DIT MOET NOG AANGEPAST WORDEN!!!
	
	// -------------outputs----------------------------------------
	fb_SbusController_drive_1.uiMasterStatus:= fb_SbusMasterDriver.uiStatus;
	aiPiUser1 := fb_SbusController_drive_1.aiPi;
	
	gFeedBackFricDisc[0] := fb_SbusController_drive_1.aiPi[0];
	gFeedBackFricDisc[1] := fb_SbusController_drive_1.aiPi[1];
	gFeedBackFricDisc[2] := fb_SbusController_drive_1.aiPi[2];
	
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

