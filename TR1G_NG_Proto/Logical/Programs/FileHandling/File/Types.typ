
TYPE
	folderCheck_enum_typ : 
		(
		STATE_FOLDER_INIT := 0,
		STATE_FOLDER_CHECK := 1,
		STATE_CREATE_MAREL_FOLDER := 2,
		STATE_CREATE_RECIPE_FOLDER := 3,
		STATE_CREATE_DATALOG_FOLDER := 4,
		STATE_FOLDER_IDLE := 5
		);
END_TYPE
