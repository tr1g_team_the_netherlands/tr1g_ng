
(* this functionblock is used to control the drive.*)
(* the input contains an array of 3 PO int and the output contains an array of 3 PI ints*)
FUNCTION_BLOCK sbus_controller
	splitTheInt(aiPo[0],internal.byLowPo[0],internal.byHighPo[0]);
	splitTheInt(aiPo[1],internal.byLowPo[1],internal.byHighPo[1]);
	splitTheInt(aiPo[2],internal.byLowPo[2],internal.byHighPo[2]);
	
	aiPi[0] := jointTheBytes(internal.byLowPi[0],internal.byHighPi[0]);
	aiPi[1] := jointTheBytes(internal.byLowPi[1],internal.byHighPi[1]);
	aiPi[2] := jointTheBytes(internal.byLowPi[2],internal.byHighPi[2]);
	
	internal.abDataPo[0] := internal.byHighPo[0];
	internal.abDataPo[1] := internal.byLowPo[0];
	internal.abDataPo[2] := internal.byHighPo[1];
	internal.abDataPo[3] := internal.byLowPo[1];
	internal.abDataPo[4] := internal.byHighPo[2];
	internal.abDataPo[5] := internal.byLowPo[2];
	internal.abDataPo[6] := internal.byHighPo[3];
	internal.abDataPo[7] := internal.byLowPo[3];
	
	IF uiMasterStatus = 0 THEN 
		internal.CANwrite_0
			(
			enable:=1,
			us_ident:=udiHandle,
			can_id:=Sbus_IdPo(uiMotorNumber),
		data_adr:=ADR(internal.abDataPo),
		data_lng:=6 (*control word commands are only 6 bytes messages*)
		);		
		IF internal.CANwrite_0.status = ERR_OK THEN
			RandDmessage := 'Write actions without errors';
		ELSE
			RandDmessage := 'There went somting wrong with the write action';		
		END_IF
		
		uiStatus := internal.CANwrite_0.status;
		// CANread
		internal.CANread_0
			(
			enable :=1,
			us_ident :=udiHandle,
			can_id :=Sbus_IdPi(uiMotorNumber),
		data_adr :=ADR(internal.abDataPi)
		);
		IF internal.CANread_0.status = ERR_OK THEN
			RandDmessage := 'Read actions without errors';
			internal.byHighPi[0] := internal.abDataPi[0];
			internal.byLowPi[0]	:=  internal.abDataPi[1];
			internal.byHighPi[1] := internal.abDataPi[2];
			internal.byLowPi[1]	:=  internal.abDataPi[3];
			internal.byHighPi[2] := internal.abDataPi[4];
			internal.byLowPi[2]	:=  internal.abDataPi[5];
			internal.byHighPi[3] := internal.abDataPi[6];
			internal.byLowPi[3]	:=  internal.abDataPi[7];
		ELSE
			RandDmessage := 'There went somting wrong with the read action';	
			uiStatus := internal.CANread_0.status;
		END_IF
	END_IF;			
END_FUNCTION_BLOCK
