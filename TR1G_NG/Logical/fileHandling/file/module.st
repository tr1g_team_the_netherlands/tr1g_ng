PROGRAM _INIT
	
	cm.setup.parent.name := 'PackMLTR';
	
END_PROGRAM

PROGRAM _CYCLIC
	
	CASE cm.state OF

		0:	cm.description := 'config handling';
			MpRecipeXml_Config.MpLink := ADR(mpConfig);
			MpRecipeXml_Config.Enable := TRUE;
			//MpRecipeXml_Config.DeviceName := ADR('CONFIG');
			MpRecipeXml_Config.DeviceName := ADR('FileDev');
			MpRecipeXml_Config.FileName := ADR('config.xml');
			MpRecipeXml_Config.Category := ADR('config');
			MpRecipeXml_Config();
	
	END_CASE
	
	IF test THEN
		MpDataRecorder_0.MpLink := ADR(gDataRecorder);
		MpDataRecorder_0.DeviceName := ADR('Log');
		MpDataRecorder_0.RecordMode := mpDATA_RECORD_MODE_TRIGGER;
		MpDataRecorder_0.Trigger := xOverFlowTable OR trigger;
		MpDataRecorder_0.Enable := TRUE;
	
	
		MpDataRegPar_0.MpLink := ADR(gDataRecorder);
		MpDataRegPar_0.PVName := ADR('PackMLTR:rehanger.HookTable.HookTab[0].hookNr');
		MpDataRegPar_0.Description := ADR('Hook Number'); 
		
		IF MpDataRecorder_0.StatusID = ERR_OK THEN
			MpDataRegPar_0.Enable := TRUE; 
		END_IF;
	END_IF;


	cm();								// call the mocule
	MpDataRecorder_0();
	MpDataRegPar_0();
END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM
