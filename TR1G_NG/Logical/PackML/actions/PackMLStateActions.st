
ACTION StateClearing: 

END_ACTION

ACTION StateStopped: 
	PackMlMode.Reset := TRUE;
	xStateComplete := TRUE;
END_ACTION

ACTION StateStarting:
	xStateComplete := TRUE;
END_ACTION

ACTION StateIdle:
	PackMlMode.Reset:=FALSE;
	xStateComplete := TRUE;
	
END_ACTION

ACTION StateSuspended:
	xStateComplete := TRUE;
END_ACTION

ACTION StateExecute:
	xStateComplete := TRUE;
END_ACTION

ACTION StateStopping:
	xStateComplete := TRUE;
END_ACTION

ACTION StateAborting:
	xStateComplete := TRUE;
END_ACTION

ACTION StateAborted:
	xStateComplete := TRUE;
END_ACTION

ACTION StateHolding:
	xStateComplete := TRUE;
END_ACTION

ACTION StateHeld:
	xStateComplete := TRUE;
END_ACTION

ACTION StateUnholding:
	xStateComplete := TRUE;
END_ACTION

ACTION StateSuspending:
	xStateComplete := TRUE;
END_ACTION

ACTION StateResetting:
	xStateComplete := TRUE;
END_ACTION

ACTION StateCompleting:
	xStateComplete := TRUE;
END_ACTION

ACTION StateCompleted:
	xStateComplete := TRUE;
END_ACTION

ACTION StateUnsuspending:
	xStateComplete := TRUE;
END_ACTION

ACTION StateUndefined:
	xStateComplete := TRUE;
END_ACTION