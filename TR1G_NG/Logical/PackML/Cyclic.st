
PROGRAM _CYCLIC
 
	PackMlCore();
	PackMlMode();
	
	
	PackMlCore.MpLink:=ADR(gPackMLCoreLink);
	PackMlMode.MpLink:=ADR(gPackMLCoreLink);
	
	PackMlCore.Enable 			:= TRUE;
		
	PackMlMode.Enable 			:= TRUE;	// enable the PackMlmode dfb
	PackMlMode.ModeID 			:= 1;		// set current mode to production
	PackMlMode.Activate			:= TRUE;	// activate the set mode
	PackMlMode.StateComplete	:= xStateComplete;
	
	xStateComplete := FALSE;
	CASE PackMlCore.StateCurrent OF
		
		mpPACKML_STATE_CLEARING:
			StateClearing;
			
		
		mpPACKML_STATE_STOPPED:	
			StateStopped;
			
		
		mpPACKML_STATE_STARTING:
			StateStarting;
			
		
		mpPACKML_STATE_IDLE:
			StateIdle;
			
		
		mpPACKML_STATE_SUSPENDED:
			StateSuspended;
			
		
		mpPACKML_STATE_EXECUTE:
			StateExecute;
			
			
		mpPACKML_STATE_STOPPING:
			StateStopping;
			
		
		mpPACKML_STATE_ABORTING:
			StateAborting;
		
		
		mpPACKML_STATE_ABORTED:
			StateAborted;
			
		
		mpPACKML_STATE_HOLDING:
			StateHolding;
		
		
		mpPACKML_STATE_HELD:
			StateHeld;
			
		
		mpPACKML_STATE_UNHOLDING:
			StateUnholding;
			
		
		mpPACKML_STATE_SUSPENDING:
			StateSuspending;
		
		
		mpPACKML_STATE_UNSUSPENDING:
			StateUnsuspending;
			
		
		mpPACKML_STATE_RESETTING:
			StateResetting;
			
		
		mpPACKML_STATE_COMPLETING:
			StateCompleting;
			
		
		mpPACKML_STATE_COMPLETE:
			StateCompleted;
		
		
		mpPACKML_STATE_UNDEFINED:
			StateUndefined;
			
		
		ELSE
			StateUndefined;
			
		
	END_CASE;
	
		
		
	
	 
END_PROGRAM
