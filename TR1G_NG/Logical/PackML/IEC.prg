﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.2.6.110 SP?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Objects>
    <Object Type="File" Description="Cyclic code">Cyclic.st</Object>
    <Object Type="File" Description="Init code">Init.st</Object>
    <Object Type="File" Description="Exit code">Exit.st</Object>
    <Object Type="File" Description="Local data types" Private="true">Types.typ</Object>
    <Object Type="File" Description="Local variables" Private="true">Variables.var</Object>
    <Object Type="Package">actions</Object>
  </Objects>
</Program>