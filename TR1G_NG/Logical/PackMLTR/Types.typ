(*Settings*)

TYPE
	config_typ : 	STRUCT 
		minimumAirPressure : INT;
		suspendingHookCount : UINT;
		uiEncDisRes : UINT := 5000; (*Encoder resolution Discharge line*)
		uiEncSupRes : UINT := 5000; (*Encoder resolution Supply line*)
		rEncSupCircumference : REAL := 1.522; (*Circumference of the wheel where the encoder is mounted on *)
		rEncDisCircumference : REAL := 1.44; (*Circumference of the wheel where the encoder is mounted on *)
		usEncSupHookAmount : USINT := 8; (*Total number of hooks transported by 1 revolution of the wheel where the encoder is mounted on.*)
		usEncDisHookAmount : USINT := 6; (*Total number of hooks transported by 1 revolution of the wheel where the encoder is mounted on.*)
		New_Member2 : USINT;
		New_Member1 : USINT;
		New_Member : USINT;
	END_STRUCT;
END_TYPE

(*Hardware*)

TYPE
	hw_typ : 	STRUCT 
		ai : hw_ai;
		ao : hw_ao;
		di : hw_di;
		do : hw_do;
		io : hw_io;
	END_STRUCT;
	hw_io : 	STRUCT 
		diEncPds : DINT;
		diEncMx : DINT;
	END_STRUCT;
	hw_ai : 	STRUCT 
		ai_airPressure : INT;
	END_STRUCT;
	hw_do : 	STRUCT 
		xRejectExt : BOOL;
		xRejectRet : BOOL;
		xRehangKick1Ext : BOOL;
		xRehangKick1Ret : BOOL;
		xRehangKick2Ext : BOOL;
		xRehangKick2Ret : BOOL;
		xRehangSupSperExt : BOOL;
		xRehangSupSperRet : BOOL;
		xRehangDisSperExt : BOOL;
		xRehangDisSperRet : BOOL;
		xLightGreen : BOOL;
		xLightRed : BOOL;
		xLightYellow : BOOL;
		xLightBlue : BOOL;
		xLightWhite : BOOL;
	END_STRUCT;
	hw_di : 	STRUCT 
		EmergencyStopOK : BOOL;
		xS01_Hook : BOOL;
		xS01 : BOOL; (*Sensor Hook Detection Supply line*)
		xS02 : BOOL; (*Sensor Hook Detection Discharge line*)
		xS03 : BOOL;
		xS04 : BOOL;
		xS05 : BOOL;
		xS06 : BOOL;
		xS07 : BOOL;
		xS08 : BOOL; (*Sensor (Rejector) Product detected left*)
		xS09 : BOOL; (*Sensor (Rejector) Product detected right*)
		xS10 : BOOL;
		K1Out : BOOL;
		K1In : BOOL;
		K2Out : BOOL;
		K2In : BOOL;
		PdsOpen : BOOL;
		PdsClosed : BOOL;
		MxOpen : BOOL;
		MxClose : BOOL;
		Pds_Reject : BOOL;
	END_STRUCT;
	hw_ao : 	STRUCT 
	END_STRUCT;
END_TYPE

(*MachineControl*)

TYPE
	MachineControl : 	STRUCT 
		xDisLineRun : BOOL;
		xSupLineRun : BOOL;
		xAirPresOk : BOOL;
	END_STRUCT;
END_TYPE
