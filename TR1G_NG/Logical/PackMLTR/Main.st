
PROGRAM _INIT
	// gets the task name and setup names for config and receipe types
	ST_name(0,ADR(configName),0);
	brdkStrCpy(ADR(configName),ADR(configName));
	brdkStrCat(ADR(configName),ADR(':config'));
END_PROGRAM

PROGRAM _CYCLIC
(* ---------- Simulate PDS input -------*)
	
	
(* ---------- Calculations ---------- *)
	//INPUTS
	//encoders data (supply & discharge line)
	calcTR1G.iEncoderDataPDS 		:= hw.io.diEncPds;
	calcTR1G.iEncPDSNTResolution	:= config.uiEncSupRes;
	calcTR1G.iEncPDSNTCircumference := config.rEncSupCircumference;
	calcTR1G.iPdsNrOfHooks			:= config.usEncSupHookAmount;
	calcTR1G.iSensHookPDS			:= hw.di.xS01;
	
	calcTR1G.iEncoderDataMX			:= hw.io.diEncMx;
	calcTR1G.iEncMXResolution		:= config.uiEncDisRes;
	calcTR1G.iEncMXCircumference	:= config.rEncDisCircumference;
	calcTR1G.iMxNrOfHooks			:= config.usEncDisHookAmount;
	calcTR1G.iSensHookMX			:= hw.di.xS02;
	
	//call function
	calcTR1G();
	
	//OUTPUTS
	MachCtrl.xSupLineRun 			:= calcTR1G.xSupLineRun;
	MachCtrl.xDisLineRun			:= calcTR1G.xDisLineRun;
	
	
(* ---------- Settings ---------- *)
	//INPUTS	
	Settings.iSettingsRehanger		:= aSetRehanger;
	Settings.iSettingsCarrierDisk 	:= aSetCarCtrl;
	Settings.iSettingsRejector		:= aSetRejector;
	Settings.iPdsEnc				:= calcTR1G.oEncoderPDSNT;
	Settings.iMxEnc					:= calcTR1G.oEncoderMX;
	
	//Call Function
	Settings();
	
	//OUTPUTS
	
	
	
(* ---------- PackML ---------- *)
	CASE em.mode OF

		MODE_PRODUCING:	// #1			
			CASE em.state OF
					
				STATE_STOPPED:
					//em.cmd.sc := TRUE;
					PilotLight(Color := Green);
				

				STATE_RESETTING:
					
					IF rehanger.xInitResetStateReady THEN
						em.cmd.sc := TRUE;
					END_IF
				
					PilotLight(Color := Red_Flash);
				
				STATE_IDLE:
					em.cmd.start := TRUE;
					em.cmd.sc := TRUE;
					PilotLight(Color := Red_Flash);
		
				STATE_STARTING:
					resetEmptyHook;
					em.cmd.sc 		:= TRUE;
					PilotLight(Color := Red_Flash);
				
				STATE_EXECUTE:
					//IF emptyHookCount >= config.suspendingHookCount THEN
					//	em.cmd.suspend := TRUE;
					//END_IF

					// maybe check for mx in suspended

					//em.cmd.stop := NOT pdsLineRunning OR NOT mxLineRunning;
					PilotLight(Color := Red);
					em.cmd.sc 	:= TRUE;

				STATE_SUSPENDING:
					PilotLight(Color := White);
					em.cmd.sc := TRUE;

				STATE_SUSPENDED:
					em.description := 'wait for product';
					PilotLight(Color := White);
					//IF hw.di.xS03 THEN
					//	em.cmd.start := TRUE;
					//END_IF
					em.cmd.sc := TRUE;

				STATE_UNSUSPENDING:
					resetEmptyHook;
					PilotLight(Color := White);
					em.cmd.sc 		:= TRUE;

				STATE_STOPPING:
					em.description := 'wait for drive to stop (speed = 0)';
					// stop sbus drive
					// check spped of drive
					PilotLight(Color := Red_Flash);
					em.cmd.sc := TRUE;

				STATE_ABORTING:
					// maybe send stop command to drive
					PilotLight(Color := Blue_Flash);
					em.cmd.sc := TRUE;

				STATE_ABORTED:
					PilotLight(Color := Blue);
					em.cmd.sc := TRUE;

				STATE_CLEARING:
					// TODO: reset / clear alarms
					em.cmd.sc := TRUE;

				ELSE
					em.cmd.sc := TRUE;

			END_CASE
	
		MODE_MANUAL:
	
		MODE_MAINTENANCE:

		MODE_CLEAN:
		
	END_CASE

(*

	IF hw.ai.ai_airPressure < config.minimumAirPressure THEN
		// TODO: make airpressure low alarm
		em.cmd.abort := TRUE;
	ELSIF NOT hw.di.EmergencyStopOK THEN
		// TODo: make emergency stop pressed alarm
		em.cmd.abort := TRUE;
	END_IF
*)
	//If Sup line is not running!
	//em.cmd.stop := NOT calcTR1G.xSupLineRun;
	


	em();								// call the mocule

	config;
	MpRecipeRegPar_Config(MpLink := ADR(mpConfig), Enable := TRUE, PVName := ADR(configName), Category := ADR('config'));
	
	
	// hello
	
(* ---------- Rejector ---------- *)
	//INPUTS
	productRejector.xEnable 		:= FALSE;
	productRejector.Mode			:= em.mode;
	productRejector.State			:= em.state;
	productRejector.iEncoder 		:= calcTR1G.oEncoderMX;
	productRejector.Settings		:= Settings.oSettingsRejector;
	productRejector.iSensorLeft		:= hw.di.xS08;
	productRejector.iSensorRight	:= hw.di.xS09;
	productRejector.iResetTable		:= FALSE;
	
	//call the function
	productRejector();
		
	
(* ---------- Rehanger & CarrierDisc ---------- *)
	rehanger.Mode					:= em.mode;
	rehanger.State					:= em.state;
	rehanger.EncSup					:= calcTR1G.oEncoderPDSNT;
	rehanger.SetRehang				:= Settings.oSettingsRehanger;
	rehanger.SetCarDisc				:= Settings.oSettingsCarrierDisk;	
	rehanger.xS01_Hook				:= hw.di.xS01_Hook;
	rehanger.xS01					:= hw.di.xS01;
	rehanger.xS02					:= hw.di.xS02;
	rehanger.xS03					:= hw.di.xS03;
	rehanger.xS04					:= hw.di.xS04;
	rehanger.xS05					:= hw.di.xS05;
	rehanger.xS06					:= hw.di.xS06;
	rehanger.xS07					:= hw.di.xS07;
	rehanger.PdsReject				:= hw.di.Pds_Reject OR myRejectSignal;
	
	rehanger();
	xOverFlowTable := rehanger.xOverFlowTable;
	
	hw.do.xRehangKick1Ext			:= rehanger.xKicker1IN;
	hw.do.xRehangKick1Ret			:= rehanger.xKicker1OUT;
	hw.do.xRehangKick2Ext			:= rehanger.xKicker2IN;
	hw.do.xRehangKick2Ret			:= rehanger.xKicker2OUT;
	FrictionDisc					:= rehanger.xSpdFrictionDisc;
	hw.do.xRehangSupSperExt 		:= rehanger.xSperPdsOpen; 
	hw.do.xRehangSupSperRet			:= rehanger.xSperPdsClose;
	hw.do.xRehangDisSperExt 		:= rehanger.xSperMxClose;
	hw.do.xRehangDisSperRet			:= rehanger.xSperMxOpen;
	
	
	
(* ---------- OUTPUTS ---------- *)
	updateHardware; //action 

	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

