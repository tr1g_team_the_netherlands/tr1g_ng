
(* TODO: Add your comment here *)
FUNCTION_BLOCK calcTransitionTime
	
	xReActSignal(CLK:= xActivationSignal);
	xReFb1(CLK := xFF1);
	xReFb2(CLK := xFF2);
	
	
	IF xReActSignal.Q THEN
		xCalculationActive := TRUE; 
		diStartSignal := AsIOTimeStamp();
				
	END_IF
	
	IF xCalculationActive AND (xReFb1.Q OR xReFb2.Q)	 THEN
		diStopSigal := AsIOTimeStamp();
		timSignalDuration := DINT_TO_TIME((diStopSigal - diStartSignal)/1000);
		xCalculationActive := FALSE;
	END_IF
	
	
	
	
			
		
	
	
	
END_FUNCTION_BLOCK