﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.2.5.388?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Cyclic code">Cyclic.cpp</File>
    <File Description="Initialization code">Init.cpp</File>
    <File Description="Exit code">Exit.cpp</File>
    <File Description="Local data types" Private="true">Types.typ</File>
    <File Description="Local variables" Private="true">Variables.var</File>
    <File>interface.h</File>
    <File>Calculator.h</File>
    <File>Subtractor.h</File>
    <File>Adder.h</File>
  </Files>
</Program>