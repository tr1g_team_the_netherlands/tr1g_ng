_IEC_INTERFACE class Calculator
{
	public:
       
	_IEC_IN REAL input1;
	_IEC_IN REAL input2;
	_IEC_OUT REAL output;
       
	_IEC_FUNCTION virtual REAL Calculate() = 0;
	_IEC_CYCLIC void Cyclic();
};
