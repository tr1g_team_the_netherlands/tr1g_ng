
(* This tool scans the CAN bus for sBus devices *)
FUNCTION_BLOCK sBus_scanTool
	internal.fb_TONscanDelay(IN:=TRUE,PT:=T#10ms);
	//setup the send telegram
	splitTheInt(16#265F,internal.byRequestIndexLow,internal.byRequestIndexHigh);
	internal.abSendData[0]:= DINT_TO_BYTE(READ_PARAMETER);		// management byte
	internal.abSendData[1]:= 0;									// sub inbdex
	internal.abSendData[2]:= internal.byRequestIndexHigh;		// index high byte
	internal.abSendData[3]:= internal.byRequestIndexLow;		// index low byte
	internal.abSendData[4]:= 0;									// data
	internal.abSendData[5]:= 0;									// data
	internal.abSendData[6]:= 0;									// data
	internal.abSendData[7]:= 0;									// data
	
	IF EDGEPOS( xStartScanPulse )THEN 
		internal.xScanStarted := TRUE;
		internal.iArrayIndexer := 1;
	END_IF;
	
	
	IF internal.xScanStarted AND (udiHandle > 0 ) THEN  
		IF (internal.iArrayIndexer < MAX_DRIVES) AND internal.fb_TONscanDelay.Q THEN
					
			internal.COBidParameterRequestCh1	:= Sbus_IdRequestCh1(internal.iArrayIndexer);
			internal.COBidParameterResponseCh1	:= Sbus_IdResponseCh1(internal.iArrayIndexer);
		
			//send the request to the drive
			internal.fb_CANwrite_0
			(
				enable:=1,
				us_ident:=udiHandle,
				can_id:=internal.COBidParameterRequestCh1,
				data_adr:=ADR(internal.abSendData),
				data_lng:=SIZEOF(internal.abSendData)
			);
			
			uiStatus := internal.fb_CANwrite_0.status;
			
			
			
			internal.iArrayIndexer := internal.iArrayIndexer+1;
			internal.fb_TONscanDelay(IN:=FALSE);
		ELSIF (internal.iArrayIndexer >= MAX_DRIVES)THEN
			internal.xScanStarted:=FALSE;
		END_IF;
	END_IF;
	
	
END_FUNCTION_BLOCK
