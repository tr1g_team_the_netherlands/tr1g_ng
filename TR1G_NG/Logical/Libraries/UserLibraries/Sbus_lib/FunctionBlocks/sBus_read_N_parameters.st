
(* functionblock te read an array of parameters from a drive *)
FUNCTION_BLOCK sBus_read_N_parameters
	internal.fb_tonUpdate(IN:=TRUE,PT:=tUpdateTime);
	internal.COBidParameterRequestCh1	:= Sbus_IdRequestCh1(uiMotorNumber);
	internal.COBidParameterResponseCh1	:= Sbus_IdResponseCh1(uiMotorNumber);
	
	IF internal.fb_tonUpdate.Q THEN 
		IF udiHandle >0 THEN
			// clean the request array
			FOR internal.iIndex := 0 TO 7 DO 
				internal.abSendData[internal.iIndex] := 0;
			END_FOR;
			
			// set up the telegram for sending
			// management byte
			// sub index
			// index
			internal.abSendData[0]:= DINT_TO_BYTE(structParmeterListIn[internal.iArrayIndexer].eManagement);
			internal.abSendData[1]:= structParmeterListIn[internal.iArrayIndexer].bySubIndex;
			
			// convert the the int(index) to 2 seperate bytes
			splitTheInt(structParmeterListIn[internal.iArrayIndexer].iIndex,
			internal.byRequestIndexLow,internal.byRequestIndexHigh);
			internal.abSendData[2]:= internal.byRequestIndexHigh;
			internal.abSendData[3]:= internal.byRequestIndexLow;
			
			//send the request to the drive
			internal.fb_CANwrite_0
				(
				enable:=1,
				us_ident:=udiHandle,
				can_id:=internal.COBidParameterRequestCh1,
				data_adr:=ADR(internal.abSendData),
				data_lng:=SIZEOF(internal.abSendData)
				);
			
			IF internal.fb_CANwrite_0.status = ERR_OK THEN
				RandDmessage := 'Write actions without errors';
			ELSE
				RandDmessage := 'There went somting wrong with the write action';		
			END_IF
			
			//read back the response of the send request
			internal.fb_CANread_0
				(
				enable:=1,
				us_ident:=udiHandle,
				can_id:=internal.COBidParameterResponseCh1,
				data_adr:=ADR(internal.abReceiveData)
				);
			
			IF internal.fb_CANread_0.status = ERR_OK THEN
				RandDmessage := 'Read actions without errors';
			ELSE
				RandDmessage := 'There went somting wrong with the read action';		
			END_IF
			
		
			// check by the respond message if the transfer was oke. then increase the indexer
			IF ((internal.abReceiveData[0] = DINT_TO_BYTE(READ_PARAMETER)) AND
				(internal.abReceiveData[2] 	= internal.abSendData[2]) AND
				(internal.abReceiveData[3]	= internal.abSendData[3]) AND
				(internal.abReceiveData[1] 	= internal.abSendData[1]))THEN
				
					//populate the output struct
					structParameterListOut[internal.iArrayIndexer].eManagement 		:= BYTE_TO_DINT(internal.abReceiveData[0]);
					structParameterListOut[internal.iArrayIndexer].bySubIndex 		:= internal.abReceiveData[1];
					structParameterListOut[internal.iArrayIndexer].iIndex 			:= jointTheBytes(internal.abReceiveData[3], internal.abReceiveData[2]);
					structParameterListOut[internal.iArrayIndexer].strParameterName := structParmeterListIn[internal.iArrayIndexer].strParameterName;
					structParameterListOut[internal.iArrayIndexer].strUnit 			:= structParmeterListIn[internal.iArrayIndexer].strUnit;
					
					internal.abTempReceiveData[0] := internal.abReceiveData[4];
					internal.abTempReceiveData[1] := internal.abReceiveData[5];
					internal.abTempReceiveData[2] := internal.abReceiveData[6];
					internal.abTempReceiveData[3] := internal.abReceiveData[7];
					
					join_ab4_to_Di(internal.abTempReceiveData,structParameterListOut[internal.iArrayIndexer].diData );
					
					IF (internal.iArrayIndexer >= iAmountOfParameters) THEN 
						internal.iArrayIndexer := 0;
						xReadyWrite := TRUE;
						internal.fb_tonUpdate(IN:=FALSE);
					ELSE
						internal.iArrayIndexer := internal.iArrayIndexer +1;
						xReadyWrite := FALSE;
					END_IF;
				
			END_IF;
		END_IF;	
	END_IF;
	
	
END_FUNCTION_BLOCK
