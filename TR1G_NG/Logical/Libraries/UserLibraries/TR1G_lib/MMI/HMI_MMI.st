
(* Contains functionblocks for Light(s) *)
FUNCTION_BLOCK MMI_PilotLight
//VERSION HISTORY:
//	-----------------------------------------------------
//	Version:	Date:		Author:		Description:
//	V0.0		25-11-2016	X.Thissen	Initial Version	
//
	
	(*RESET all wires to zero*)
	Wire.GreenWire := Wire.RedWire := Wire.YellowWire := Wire.BlueWire := Wire.WhiteWire := FALSE;
		
	CASE Color OF 
		OFF							:
		ON							:
		Test				 	 	: Wire.GreenWire := Wire.RedWire := Wire.YellowWire := Wire.BlueWire := Wire.WhiteWire := TRUE;
		Green				 	 	: Wire.GreenWire := TRUE;		
		Green_Flash				 	: Wire.GreenWire := Wire.RedWire := TRUE;
		Red				 	 	 	: Wire.RedWire := TRUE;
		Red_Flash				 	: Wire.RedWire := Wire.YellowWire := TRUE;
		Blue				 	 	: Wire.BlueWire := TRUE;
		Blue_Flash				 	: Wire.BlueWire := Wire.WhiteWire := TRUE;
		Yellow				 	 	: Wire.YellowWire := TRUE;
		Yellow_Flash				: Wire.YellowWire := Wire.BlueWire := TRUE;
		White				 	 	: Wire.WhiteWire := TRUE;
		White_Flash				 	: Wire.GreenWire := Wire.WhiteWire := TRUE;
		Green_Red_Flash				: Wire.GreenWire := Wire.YellowWire := TRUE;
		Green_Yellow_Flash			: Wire.GreenWire := Wire.BlueWire := TRUE;
		Green_Blue_Flash			: Wire.RedWire := Wire.BlueWire := TRUE;
		Green_White_Flash			: Wire.RedWire := Wire.WhiteWire := TRUE;
		Red_Yellow_Flash			: Wire.YellowWire := Wire.WhiteWire := TRUE;
		Red_Blue_Flash				: Wire.GreenWire := Wire.RedWire := Wire.WhiteWire := TRUE;
		Red_White_Flash				: Wire.GreenWire := Wire.RedWire := Wire.BlueWire := TRUE;
		Yellow_Blue_Flash			: Wire.GreenWire := Wire.RedWire := Wire.YellowWire := TRUE;
		Yellow_White_Flash			: Wire.RedWire := Wire.YellowWire := Wire.WhiteWire := TRUE;
		Blue_White_Flash			: Wire.GreenWire := Wire.RedWire := Wire.BlueWire :=  TRUE;
		Green_Red_Yellow_Flash		: Wire.GreenWire := Wire.YellowWire := Wire.WhiteWire := TRUE;
		Green_Red_Blue_Flash		: Wire.RedWire := Wire.BlueWire := Wire.WhiteWire := TRUE;
		Green_Red_White_Flash		: Wire.GreenWire := Wire.BlueWire := Wire.WhiteWire := TRUE;
		Green_Yellow_Blue_Flash		: Wire.YellowWire := Wire.BlueWire := Wire.WhiteWire := TRUE;
		Green_Yellow_White_Flash	: Wire.GreenWire :=  Wire.YellowWire := Wire.BlueWire := TRUE;
		Green_Blue_White_Flash		: Wire.GreenWire :=  Wire.YellowWire := Wire.BlueWire := Wire.WhiteWire := TRUE;
		Red_Yellow_Blue_Flash		: Wire.GreenWire := Wire.RedWire := Wire.YellowWire := Wire.BlueWire :=  TRUE;
		Red_Yellow_White_Flash		: Wire.GreenWire := Wire.RedWire := Wire.YellowWire := Wire.WhiteWire := TRUE;
		Red_Blue_White_Flash		: Wire.GreenWire := Wire.RedWire :=  Wire.BlueWire := Wire.WhiteWire := TRUE;
		Yellow_Blue_White_Flash		: Wire.RedWire := Wire.YellowWire := Wire.BlueWire := Wire.WhiteWire := TRUE;
		GRYBW_Flash					: Wire.GreenWire := Wire.RedWire := Wire.YellowWire := Wire.BlueWire := Wire.WhiteWire := TRUE;
	END_CASE; 
	
END_FUNCTION_BLOCK
