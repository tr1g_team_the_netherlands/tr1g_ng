(*Rejector*)

TYPE
	Config : 	STRUCT 
		New_Member12 : USINT;
		New_Member11 : USINT;
		New_Member10 : USINT;
		New_Member9 : USINT;
		New_Member8 : USINT;
		New_Member7 : USINT;
		New_Member6 : USINT;
		New_Member5 : USINT;
		New_Member4 : USINT;
		New_Member3 : USINT;
		New_Member2 : USINT;
		New_Member1 : USINT;
		New_Member : USINT;
		minimumAirPressure : INT;
		suspendingHookCount : UINT;
		uiEncDisRes : UINT := 5000; (*Encoder resolution Discharge line*)
		uiEncSupRes : UINT := 5000; (*Encoder resolution Supply line*)
		rEncSupCircumference : REAL := 1.522; (*Circumference of the wheel where the encoder is mounted on *)
		rEncDisCircumference : REAL := 1.44; (*Circumference of the wheel where the encoder is mounted on *)
		usEncSupHookAmount : USINT := 8; (*Total number of hooks transported by 1 revolution of the wheel where the encoder is mounted on.*)
		usEncDisHookAmount : USINT := 6; (*Total number of hooks transported by 1 revolution of the wheel where the encoder is mounted on.*)
	END_STRUCT;
	CarToHMI : 	STRUCT 
		TotalNrCar : UINT;
		nrCarMxPds : INT;
		nrCarPdsMX : INT;
	END_STRUCT;
	RejectorSettings : 	STRUCT 
		StartDelay : {REDUND_UNREPLICABLE} REAL := 0.11;
		RejectWindow : {REDUND_UNREPLICABLE} REAL := 0.10;
		SensDetWindow : {REDUND_UNREPLICABLE} REAL := 0.02;
		ActivateTimeCylinder : {REDUND_UNREPLICABLE} TIME := T#80ms;
		CylResponceTime_Open : TIME := T#70ms;
		CylResponceTime_Close : TIME := T#60ms;
	END_STRUCT;
	RejectedData : 	STRUCT 
		DetectEncPos : DINT;
		DetectWind : DINT;
		RejectOpen : DINT;
		RejectClose : DINT;
	END_STRUCT;
END_TYPE

(*Rehanger*)

TYPE
	KickersPositions : 	STRUCT 
		xRejectProduct : BOOL;
		xRejectProdNextProd : BOOL;
		udiK1Out : UDINT;
		udiK1In : UDINT;
		udiK2Out : UDINT;
		udiK2In : UDINT;
	END_STRUCT;
	Setting_Rehanger : 	STRUCT 
		KickerSettings : {REDUND_UNREPLICABLE} KickersSettings;
		disProdDetection : {REDUND_UNREPLICABLE} REAL := 0.62;
		disPdsSigDetection : {REDUND_UNREPLICABLE} REAL := 0.80;
		disCarrierGo : REAL := 1.04;
		ProdDetectionWindow : {REDUND_UNREPLICABLE} REAL := 0.14;
		PdsSigWindow : {REDUND_UNREPLICABLE} REAL := 0.15;
		CarGoWindow : REAL := 0.10;
	END_STRUCT;
	Cyltiming : 	STRUCT 
		CylActivationTime : {REDUND_UNREPLICABLE} TIME := T#25ms;
		CylOpenTime : TIME := T#90ms;
		CylCloseTime : TIME := T#90ms;
	END_STRUCT;
	hookValues : 	STRUCT 
		hookNr : {REDUND_UNREPLICABLE} UDINT;
		Det_Hook : {REDUND_UNREPLICABLE} UDINT;
		Det_ProductMin : {REDUND_UNREPLICABLE} UDINT;
		Det_Product : {REDUND_UNREPLICABLE} UDINT;
		Det_ProductMax : {REDUND_UNREPLICABLE} UDINT;
		ProdDetType : {REDUND_UNREPLICABLE} ProdDetTypes;
		Det_PdsMin : {REDUND_UNREPLICABLE} UDINT;
		Det_Pds : {REDUND_UNREPLICABLE} UDINT;
		Det_PdsMax : {REDUND_UNREPLICABLE} UDINT;
		PdsValue : USINT;
		CarrierGo : {REDUND_UNREPLICABLE} UDINT;
		KickersCycleGo : {REDUND_UNREPLICABLE} BOOL;
		kicker1Out : {REDUND_UNREPLICABLE} UDINT;
		kicker2Out : {REDUND_UNREPLICABLE} UDINT;
		kicker1In : {REDUND_UNREPLICABLE} UDINT;
		kicker2In : {REDUND_UNREPLICABLE} UDINT;
	END_STRUCT;
	HookTable : 	STRUCT 
		indexNewHook : {REDUND_UNREPLICABLE} USINT := 0;
		indexProdDetecPos : {REDUND_UNREPLICABLE} USINT;
		indexPdsSignalPos : {REDUND_UNREPLICABLE} USINT;
		indexKickerOutPos : {REDUND_UNREPLICABLE} USINT;
		indexKickerInPos : {REDUND_UNREPLICABLE} USINT;
		HookNumber : {REDUND_UNREPLICABLE} UDINT := 0;
		HookValues : {REDUND_UNREPLICABLE} ARRAY[0..49]OF hookValues;
		maxSizeHookTable : {REDUND_UNREPLICABLE} USINT := 49; (*HookTable size. MUST BE the same as MAX index hookValues[0..49]*)
	END_STRUCT;
	KickersSettings : 	STRUCT 
		disKicker1Out : {REDUND_UNREPLICABLE} REAL := 1.10;
		disKicker2Out : {REDUND_UNREPLICABLE} REAL := 1.13;
		disKicker1In : {REDUND_UNREPLICABLE} REAL := 1.27;
		disKicker2In : {REDUND_UNREPLICABLE} REAL := 1.31;
		CylActivationTime : TIME := T#80ms;
		disKickActWindow : {REDUND_UNREPLICABLE} REAL := 0.05;
		CylResponceTime_Open : TIME := T#70ms;
		CylResponceTime_Close : TIME := T#60ms;
		ValveResponceTime_Open : TIME := T#9ms;
		ValveResponceTime_Close : TIME := T#24ms;
		LengthAirHose : REAL := 1; (*airhose lenght [m]*)
		AirHoseTimeDelay : TIME := T#6ms; (*Time delay (airhose) [sec / meter]*)
	END_STRUCT;
	KickersTimings : 	STRUCT 
		TP_Kicker1_Out : {REDUND_UNREPLICABLE} TP;
		TP_Kicker2_Out : {REDUND_UNREPLICABLE} TP;
		TP_Kicker1_In : {REDUND_UNREPLICABLE} TP;
		TP_Kicker2_In : {REDUND_UNREPLICABLE} TP;
		Kicker1_Out : {REDUND_UNREPLICABLE} BOOL := FALSE;
		Kicker2_Out : {REDUND_UNREPLICABLE} BOOL := FALSE;
		Kicker1_In : {REDUND_UNREPLICABLE} BOOL := TRUE;
		Kicker2_In : {REDUND_UNREPLICABLE} BOOL := TRUE;
		Kicker1isOut : {REDUND_UNREPLICABLE} BOOL := FALSE;
		Kicker2isOut : {REDUND_UNREPLICABLE} BOOL := FALSE;
		Kicker1isIn : {REDUND_UNREPLICABLE} BOOL := TRUE;
		Kicker2isIn : {REDUND_UNREPLICABLE} BOOL := TRUE;
	END_STRUCT;
END_TYPE

(*CarControl*)

TYPE
	MoviGearSettings_typ : 	STRUCT 
		uiMinCtrlLvl : UINT := 1000;
		uiMaxCtrlLvl : UINT := 10000;
		rMinRPM : REAL := 11.5;
		rMaxRPM : REAL := 115.4;
		iRamp : INT := 2500;
	END_STRUCT;
	Settings_CarControl : 	STRUCT 
		RadiusFrictionDisc : REAL := 0.24;
		CarrierAmount : USINT := 13;
		RadiusFricDiscCar : REAL := 0.53;
		StartDelayCar : TIME := T#20ms;
		SLIP : USINT := 5;
		SpeedFactor : REAL := 1.25;
		PdsSlotTiming : Cyltiming := (CylActivationTime:=T#80ms);
		MxSlotTiming : Cyltiming := (CylActivationTime:=T#80ms);
		MoviGear : MoviGearSettings_typ;
		rRadiusSynchroCar : REAL := 0.33;
		rRadiusSynchro : REAL := 0.11;
		timMinSwitchTime : TIME := T#1ms;
		rDisBeforOpenPds : REAL := 0.005;
		rDisOpenPds : REAL := 0.03;
		rDisBeforOpenMx : REAL := 0.03;
		rDisOpenMx : REAL := 0.03;
	END_STRUCT;
	FricDisc : 	STRUCT 
		Run : BOOL := 0;
		SpeedRPM : REAL := 0.0;
		SpeedDrive : INT;
		Ramp : INT := 2500;
		DriveNumber : USINT := 1;
		MinRPM : REAL := 11.5;
		MaxRPM : REAL := 115.4;
		MinCtrlLevel : UINT := 1000;
		MaxCtrlLevel : UINT := 10000;
	END_STRUCT;
END_TYPE

(*PackML*)

TYPE
	PackMLCommands : 	STRUCT 
		xEnable : {REDUND_UNREPLICABLE} BOOL;
		xErrorReset : {REDUND_UNREPLICABLE} BOOL;
		ModelID : {REDUND_UNREPLICABLE} INT;
		xActivate : {REDUND_UNREPLICABLE} BOOL;
		xStart : {REDUND_UNREPLICABLE} BOOL;
		xStop : {REDUND_UNREPLICABLE} BOOL;
		xReset : {REDUND_UNREPLICABLE} BOOL;
		xHold : {REDUND_UNREPLICABLE} BOOL;
		xUnhold : {REDUND_UNREPLICABLE} BOOL;
		xSuspend : {REDUND_UNREPLICABLE} BOOL;
		xUnSuspend : {REDUND_UNREPLICABLE} BOOL;
		xAbort : {REDUND_UNREPLICABLE} BOOL;
		xClear : {REDUND_UNREPLICABLE} BOOL;
		xStateComplete : {REDUND_UNREPLICABLE} BOOL;
	END_STRUCT;
	PackMLModes : 
		(
		Automatic,
		Manual,
		Maintenance,
		Cleaning,
		SemiAutomatic
		);
	PackMLStates : 
		(
		St_Starting,
		St_Completing,
		St_Resetting,
		St_Holding,
		St_UnHolding,
		St_Suspending,
		St_UnSuspending,
		St_Clearing,
		St_Stopping,
		St_Aborting,
		St_Execute,
		St_Idle,
		St_Held,
		St_Suspended,
		St_Stoppded,
		St_Aborted
		);
	ProdDetTypes : 
		(
		noProduct,
		bothLegsDetected,
		oneLegDetected,
		twoLegDetected,
		moreLegDetected
		);
END_TYPE

(*Settings*)

TYPE
	MachineSettings : 	STRUCT 
		EncoderPDSNT : {REDUND_UNREPLICABLE} Encoder;
		EncoderMX : {REDUND_UNREPLICABLE} Encoder;
		ProdRejector : {REDUND_UNREPLICABLE} RejectorSettings;
	END_STRUCT;
END_TYPE

(*Other*)

TYPE
	Encoder : 	STRUCT 
		EncoderPosition : {REDUND_UNREPLICABLE} UDINT;
		PulsesRevolution : {REDUND_UNREPLICABLE} INT;
		Circumference : {REDUND_UNREPLICABLE} REAL;
		PulsesMeter : {REDUND_UNREPLICABLE} REAL;
		RPM : REAL;
		MeterSec : REAL;
	END_STRUCT;
	SignalData : {REDUND_UNREPLICABLE} 	STRUCT 
		uTransitions : {REDUND_UNREPLICABLE} UDINT;
		tData : {REDUND_UNREPLICABLE} ARRAY[0..49]OF tstTimingData;
		iStructIndexRE : {REDUND_UNREPLICABLE} INT;
		iStructIndexFE : {REDUND_UNREPLICABLE} INT;
	END_STRUCT;
	SignalDataPilotLight : 	STRUCT 
		uTransitionsGreen : USINT;
		uTransitionsRed : USINT;
		uTransitionsYellow : USINT;
		uTransitionsBlue : USINT;
		uTransitionsWhite : USINT;
		onTimeGreen : TIME;
		onTimeRed : TIME;
		onTimeYellow : TIME;
		onTimeBlue : TIME;
		onTimeWhite : TIME;
	END_STRUCT;
END_TYPE

(*HMI_MMI*)

TYPE
	MMI_PilotLight_Colors : 
		(
		OFF,
		ON,
		Test,
		Green,
		Red,
		Yellow,
		Blue,
		White,
		Green_Flash,
		Red_Flash,
		Yellow_Flash,
		Blue_Flash,
		White_Flash,
		Green_Red_Flash,
		Green_Yellow_Flash,
		Green_Blue_Flash,
		Green_White_Flash,
		Red_Yellow_Flash,
		Red_Blue_Flash,
		Red_White_Flash,
		Yellow_Blue_Flash,
		Yellow_White_Flash,
		Blue_White_Flash,
		Green_Red_Yellow_Flash,
		Green_Red_Blue_Flash,
		Green_Red_White_Flash,
		Green_Yellow_Blue_Flash,
		Green_Yellow_White_Flash,
		Green_Blue_White_Flash,
		Red_Yellow_Blue_Flash,
		Red_Yellow_White_Flash,
		Red_Blue_White_Flash,
		Yellow_Blue_White_Flash,
		GRYBW_Flash
		);
	MMI_PilotLight_Wires : 	STRUCT 
		GreenWire : BOOL;
		RedWire : BOOL;
		YellowWire : BOOL;
		BlueWire : BOOL;
		WhiteWire : BOOL;
	END_STRUCT;
	SignalDataPilotLight1 : 	STRUCT 
		uTransitionsGreen : USINT;
		uTransitionsRed : USINT;
		uTransitionsYellow : USINT;
		uTransitionsBlue : USINT;
		uTransitionsWhite : USINT;
		onTimeGreen : TIME;
		onTimeRed : TIME;
		onTimeYellow : TIME;
		onTimeBlue : TIME;
		onTimeWhite : TIME;
	END_STRUCT;
	tstTimingData : 	STRUCT 
		tOnTime : TIME;
		tOffTime : TIME;
		tTimeDifferencePulse : TIME;
		tTimeStamp : DATE_AND_TIME;
	END_STRUCT;
	Fb_FrictionDisc : 	STRUCT 
		Running : INT;
		word1 : INT;
		word2 : INT;
	END_STRUCT;
END_TYPE
