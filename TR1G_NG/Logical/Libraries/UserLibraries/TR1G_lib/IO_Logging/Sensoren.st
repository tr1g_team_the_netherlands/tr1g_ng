
(* TODO: Add your comment here *)
FUNCTION_BLOCK digitalSignalDataCollector
	cpuTimeMS := clock_ms();
	cpuTimeStamp.enable :=TRUE;
	cpuTimeStamp();
	//calculate the maximum size of the used dataArray for Logging.
	dataArraySize := SIZEOF(outSignalData.tData) / SIZEOF(outSignalData.tData[0]);

	(*---- Copy signal to internal bool ----*)
	internSignalSignal :=  (NOT i_xSignalInvert AND i_xSignal) OR
						   (    i_xSignalInvert AND NOT i_xSignal);
		
	(*---- OUTPUTS ----*)	
	//invert Signal output	
	o_xSignal := (NOT enableForcing AND internSignalSignal ) OR 
				 (    enableForcing AND (NOT i_xForceLow AND (internSignalSignal OR i_xForceHigh)));
	
	//when the block is used as CFG input then LOG input actions. ELSE as CFG output then log output actions
	IF NOT cfg_IN_or_OUT THEN 	
		RE_xSignal(CLK := i_xSignal );
		FE_xSignal(CLK := i_xSignal );
	ELSE
		//Rising AND Falling 
		RE_xSignal(CLK := o_xSignal );
		FE_xSignal(CLK := o_xSignal );
	END_IF;
	
	o_xSignal_RE := RE_xSignal.Q;	
	o_xSignal_FE := FE_xSignal.Q;
			
	
	(*---- LOGGING ----*)	
	// equialize the diffence between RE and FE counters when the count is started with a FE instead of an RE
	IF (outSignalData.iStructIndexRE - outSignalData.iStructIndexFE) < 0  THEN
		outSignalData.iStructIndexRE := outSignalData.iStructIndexFE ;
	END_IF;
	
	
	//Rising Edge 
	IF RE_xSignal.Q  THEN 
		//increase Transition Counter
		outSignalData.uTransitions := outSignalData.uTransitions + 1;
		
		//get the systemTimestamp
		outSignalData.tData[outSignalData.iStructIndexRE].tOnTime := cpuTimeMS;
		outSignalData.tData[outSignalData.iStructIndexRE].tTimeStamp := cpuTimeStamp.DT1;
		//increase index number (at end of array, reset index)
		IF outSignalData.iStructIndexRE >= 49 THEN outSignalData.iStructIndexRE :=0; ELSE outSignalData.iStructIndexRE := outSignalData.iStructIndexRE + 1; END_IF;		
		
	END_IF;
	//Falling EDGE
	IF FE_xSignal.Q THEN 
		//increase Transition Counter
		outSignalData.uTransitions := outSignalData.uTransitions + 1;
		
		//get the systemTimestamp
		outSignalData.tData[outSignalData.iStructIndexFE].tOffTime := cpuTimeMS;
		outSignalData.tData[outSignalData.iStructIndexFE].tTimeDifferencePulse := outSignalData.tData[outSignalData.iStructIndexFE].tOffTime - outSignalData.tData[outSignalData.iStructIndexFE].tOnTime;
		//increase index number (at end of array, reset index)
		IF outSignalData.iStructIndexFE >= 49 THEN outSignalData.iStructIndexFE :=0; ELSE outSignalData.iStructIndexFE := outSignalData.iStructIndexFE + 1; END_IF;		
	END_IF;
	
	(*---- Reset Log valus ----*)
	IF i_ResetLogValues THEN
		outSignalData.uTransitions := 0;	
		outSignalData.iStructIndexFE:=0;
		outSignalData.iStructIndexRE:=0;
		
		FOR i := 0 TO dataArraySize-1 DO
	 		outSignalData.tData[i].tOffTime := 0;
			outSignalData.tData[i].tOnTime := 0;
			outSignalData.tData[i].tTimeDifferencePulse := 0;
		END_FOR;
	END_IF;
		
END_FUNCTION_BLOCK




















