
FUNCTION MS_TO_RPM
	//convert the speed in M/s into RPM
	
	circumference:= calcCircumference(radius:= radius);
	
	MS_TO_RPM :=  REAL_TO_INT((60.0 * meterSecond) / circumference);

END_FUNCTION

FUNCTION RPM_TO_M_MSec
	//Convert the Speed in RPM into travelled distance within 1 millisec
	
	circumference := calcCircumference(radius:= radius);
	
	RPM_TO_M_MSec := RPM * circumference  * (1.0/60.0) / 1000.0; 
	
END_FUNCTION

FUNCTION CylActTime_Dis
	//linked to the speed the open time of the cil with respect to the traveld distance.
	//check for minimum time. If 0ms the make 1
	
	waitTime:= RPM_TO_TimeDistance(RPM:= RPM, radius:= radius, Distance:= distance);
	
	IF waitTime < minTime THEN
		waitTime := minTime; //minimum Time
	END_IF;
	
	CylActTime_Dis := waitTime;

END_FUNCTION

FUNCTION synchro_MS_To_RPM_TO_TIME
	(* Synchrowheel speed 
	first caculate the synchroweel speed in RPM based on PDS line speed [m/s] (as circumference take synchrowheel + carrier)
	second recalulate the speed [m/s] with the synchrowheel circumference
	thirth calculate the time needed to travel the given (param) distance
	last check if the time > 1ms.
	*)
	
	circumference := calcCircumference(radius:= radiusSynchroCar);			//calc circumference (synchrowheel + carrier)
	rpmSpeed := (meterSecond * 60.0)/ circumference; 						//calc speed [m/s] into RPM

	waitTime:= RPM_TO_TimeDistance(RPM:= rpmSpeed, radius:= radiusSynchro, Distance:= distance);	//calculate travel time
	
	IF waitTime < minTime THEN
		waitTime := minTime; //minimum Time
	END_IF;
	
	synchro_MS_To_RPM_TO_TIME := waitTime;
	
END_FUNCTION

FUNCTION RPM_TO_TimeDistance
	//calculate the traveled distance in one millisecond
	
	rpmMsec := RPM_TO_M_MSec(RPM:= RPM, radius:= radius);
		
	RPM_TO_TimeDistance := REAL_TO_TIME((ABS(Distance / rpmMsec))); 
	
END_FUNCTION

FUNCTION calcCircumference 
	//Calculate the Circumference
	
	calcCircumference := PI * 2.0 * radius; 
	
END_FUNCTION

FUNCTION RPM_to_MoviGearSpd 
	// Movigear has a minimum and maximum RPM (i.e. 11.5 - 115.4)
	// is controlled by a minimum value and maximum value (i.e. 1000 - 10000)
	// so RPM has to be converted into the correct CtrlLevel.
		
	
	//take minimum control speed into account if this requested speed is lower, then take the min speed.
	rRequestSpeed 		:= (RPM * (UINT_TO_REAL(maxCtrlLvl - minCtrlLvl) / (maxRPM - minRPM)));	
	IF ABS(rRequestSpeed) < minCtrlLvl THEN
		rRequestSpeed := minCtrlLvl;
	END_IF
	
	IF reverse THEN
		rRequestSpeed := rRequestSpeed * -1.0;
	END_IF
	
	RPM_to_MoviGearSpd 	:= REAL_TO_INT(rRequestSpeed);
	
END_FUNCTION

