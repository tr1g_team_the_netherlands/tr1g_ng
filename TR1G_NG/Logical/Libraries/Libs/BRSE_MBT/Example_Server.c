/*! 

 \page Example_server Example Server
 \section Example_server Example Server
 
 \par Description
	This program set up an TCP modbus server on the ethernet port of the system

 \par Example
  Example \ref tasks can be found here.

\par MBserv.var
\code
VAR
	FB_BRSE_MBT_Server : BRSE_MBT_Server := (0);
	FB_MBT_ServerExit : MBT_ServerExit := (0);
	discreteInput : ARRAY[0..9] OF BOOL := [9(FALSE)];
	coil : ARRAY[0..10] OF BOOL := [10(FALSE)];
	inputRegister : ARRAY[0..11] OF INT := [11(0)];
	holdingRegister : ARRAY[0..9] OF INT := [9(0)];
	realValue : ARRAY[0..5] OF REAL :=[5(0.0)];
	i : USINT := 0;
END_VAR
\endcode

\par MBserv.st
\code
PROGRAM _INIT
	
	(* Create a Modbus server with unit identity = 1, Coil = 10, HoldingRegister = 11, DiscreteInputs = 12, InputRegister = 13 *)
	BRSE_MB_Alloc(1,10,11,12,13);
	(* Create a Modbus server with unit identity = 2, Coil = 10, HoldingRegister = 11, DiscreteInputs = 12, InputRegister = 13 *)
	BRSE_MB_Alloc(2,10,11,12,13);

	(* Bind the coils *)
	FOR i:=0 TO 9 DO
		BRSE_MB_Coil(1,i,ADR(coil1[i]));
		BRSE_MB_Coil(2,i,ADR(coil2[i]));
	END_FOR;
	(* Bind the discrete inputs *)
	FOR i:=0 TO 10 DO
		BRSE_MB_DiscreteInput(1,i,ADR(discreteInput1[i]));
		BRSE_MB_DiscreteInput(2,i,ADR(discreteInput2[i]));
	END_FOR;
	(* Bind the holdning registers *)
	FOR i:=0 TO 11 DO
		BRSE_MB_HoldingRegister(1,i,ADR(holdingRegister1[i]));
		BRSE_MB_HoldingRegister(2,i,ADR(holdingRegister2[i]));
	END_FOR;
	(* Bind the input registers *)
	FOR i:=0 TO 12 DO
		BRSE_MB_InputRegister(1,i,ADR(inputRegister1[i]));
		BRSE_MB_InputRegister(2,i,ADR(inputRegister2[i]));
	END_FOR;
	

END_PROGRAM
	
PROGRAM _CYCLIC
	
		(* Enable the server *)
	FB_BRSE_MBT_Server.Options			:= 	BRSE_MBT_OPT_MULT_CLIENT_SAME_IP + 
											BRSE_MBT_OPT_dummy_0 + 
											BRSE_MBT_OPT_dummy_1 + 
											BRSE_MBT_OPT_dummy_2 + 
											BRSE_MBT_OPT_dummy_3 +
											BRSE_MBT_OPT_dummy_4;
	FB_BRSE_MBT_Server.SocketTimeout	:= 0;
	FB_BRSE_MBT_Server.ClientFormat		:= BRSE_MB_INTEL;
	FB_BRSE_MBT_Server.Enable 			:= 1;
	FB_BRSE_MBT_Server();
	
END_PROGRAM
	
PROGRAM _EXIT
	
	(* Close the open tcp port *)
	FB_MBT_ServerExit();
	
END_PROGRAM

\endcode
*/
 
 
 
/*
 
 Example on how to use the BRSE_MBT Library as a modbus server in C

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif

void _INIT MBRTestINIT( void )
{
	UINT i;
	
	BRSE_MB_Alloc(1, NbrCoils, NbrHoldingRegisters, NbrDiscreteInputs, NbrInputRegisters);
	
	for (i=0; i<NbrCoils; i++)
	{
		BRSE_MB_Coil(1, i, (UDINT)&Coils[i]);
	}
	for (i=0; i<NbrHoldingRegisters; i++)
	{
		BRSE_MB_HoldingRegister(1, i, (UDINT)&HoldingRegisters[i]);
	}
	for (i=0; i<NbrDiscreteInputs; i++)
	{
		BRSE_MB_DiscreteInput(1, i, (UDINT)&DiscreteInputs[i]);
	}
	for (i=0; i<NbrInputRegisters; i++)
	{
		BRSE_MB_InputRegister(1, i, (UDINT)&InputRegisters[i]);
	}
	
	FB_BRSE_MBT_Server.Enable 			= 1;
	FB_BRSE_MBT_Server.SocketTimeout	= 0;
	FB_BRSE_MBT_Server.ClientFormat		= BRSE_MB_INTEL;
}

void _CYCLIC MBRTestCYCLIC( void )
{		
	FB_BRSE_MBT_Server.Options			= 	BRSE_MBT_OPT_MULT_CLIENT_SAME_IP + 
											BRSE_MBT_OPT_dummy_0 + 
											BRSE_MBT_OPT_dummy_1 + 
											BRSE_MBT_OPT_dummy_2 + 
											BRSE_MBT_OPT_dummy_3 +
											BRSE_MBT_OPT_dummy_4;
	BRSE_MBT_Server(&FB_BRSE_MBR_Server);
}

*/

