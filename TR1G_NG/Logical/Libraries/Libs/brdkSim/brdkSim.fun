
FUNCTION_BLOCK brdkSimMapIO (*Maps a pv to an IO channel.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		enable : BOOL; (*Enables the function block. If the fb is called on a simulated target the function block will automatically enable.*)
		pChannelName : UDINT; (*Pointer to the channel name.*)
		handle : {REDUND_UNREPLICABLE} brdksim_handle_typ; (*Handle from a simulation value generator function block.*)
	END_VAR
	VAR_OUTPUT
		status : UINT; (*Status of the function block.*)
	END_VAR
	VAR
		internal : brdkSimMapIO_internal_typ; (*Internal variables.*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK brdkSimMapVar (*Maps a pv to a variable using PV_xgetadr(). Must be called and setup in INIT program.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		enable : BOOL; (*Enables the function block. If the fb is called on a simulated target the function block will automatically enable.*)
		pVariableName : UDINT; (*Pointer to the variable name. Make sure to write it in the right syntax.*)
		handle : brdksim_handle_typ; (*Handle from a simulation value generator function block.*)
	END_VAR
	VAR_OUTPUT
		status : UINT; (*Status of the function block.*)
	END_VAR
	VAR
		internal : brdkSimMapVar_internal_typ; (*Internal variables.*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK brdkSimGenStepVal (*Generates a stepped value.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		trigger : BOOL; (*Triggers the output.*)
		setup : brdkSimGenStepVal_setup_typ; (*Setup parameters for function block.*)
		reset : BOOL; (*Resets the function block to it's initialization value and state.*)
	END_VAR
	VAR_OUTPUT
		handle : brdksim_handle_typ; (*Handle needed for map function block. (DINT type)*)
		output : DINT; (*Indicates if the sensor value is activated.*)
	END_VAR
	VAR
		internal : brdkSimGenStepVal_internal_typ; (*Internal variables.*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK brdkSimGenRandVal (* *) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		trigger : BOOL;
		setup : brdkSimGenRandVal_setup_typ; (*Setup parameters for function block.*)
		reset : BOOL; (*Resets the function block to it's initialization value and state.*)
	END_VAR
	VAR_OUTPUT
		handle : brdksim_handle_typ; (*Handle needed for map function block. (DINT type)*)
		output : DINT; (*Indicates if the sensor value is activated.*)
	END_VAR
	VAR
		internal : brdkSimGenRandVal_internal_typ; (*Internal variables.*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK brdkSimTimeDelay (*Activates a trigger after a time delay.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		enable : BOOL; (*Enables the function block.*)
		delay : TIME; (*Time delay.*)
		reset : BOOL; (*Resets the function block.*)
	END_VAR
	VAR_OUTPUT
		trigger : BOOL; (*Trigger output.*)
	END_VAR
	VAR
		internal : brdkSimTimeDelay_internal_typ; (*Internal variables.*)
	END_VAR
END_FUNCTION_BLOCK
