#include <brdkSim_func.h>

void brdkSimMapIO(struct brdkSimMapIO* this) {
	switch(this->internal.state) {

		case 0:	/* init state */ 
			this->internal.cpuSimulated = DiagCpuIsSimulated();
			if(this->enable || this->internal.cpuSimulated) {
				this->internal.AsIOEnableForcing_0.enable = true;
				this->internal.AsIOEnableForcing_0.pDatapoint = this->pChannelName;
				AsIOEnableForcing(&this->internal.AsIOEnableForcing_0);
				this->status = this->internal.AsIOEnableForcing_0.status;
				if(this->internal.AsIOEnableForcing_0.status == ERR_OK) {
					this->internal.AsIOSetForceValue_0.enable = true;
					this->internal.AsIOSetForceValue_0.pDatapoint = this->internal.AsIOEnableForcing_0.pDatapoint;
					this->internal.AsIOSetForceValue_0.value = *((unsigned long*)this->handle);
					AsIOSetForceValue(&this->internal.AsIOSetForceValue_0);
					this->internal.state = 10;
				}
			}
			break;

		case 10:
			AsIOSetForceValue(&this->internal.AsIOSetForceValue_0);
			this->status = this->internal.AsIOSetForceValue_0.status;
			if(this->internal.AsIOSetForceValue_0.status == ERR_OK) this->internal.state = 20;
			break;

		case 20:
			if(this->enable || this->internal.cpuSimulated) {
				if(*((unsigned long*)this->handle) != this->internal.oldValue) {
					this->internal.AsIOSetForceValue_0.value = *((unsigned long*)this->handle);
					AsIOSetForceValue(&this->internal.AsIOSetForceValue_0);
					this->internal.state = 10;
				}
			}
			else {
				this->internal.AsIOSetForceValue_0.enable = false;
				AsIOSetForceValue(&this->internal.AsIOSetForceValue_0);
				this->internal.AsIODisableForcing_0.enable = true;
				this->internal.AsIODisableForcing_0.pDatapoint = this->internal.AsIOEnableForcing_0.pDatapoint;
				AsIODisableForcing(&this->internal.AsIODisableForcing_0);
				this->internal.state = 20;
			}
			break;
				
		case 30:
			AsIODisableForcing(&this->internal.AsIODisableForcing_0);
			this->status = this->internal.AsIODisableForcing_0.status;
			if(this->internal.AsIODisableForcing_0.status == ERR_OK) this->internal.state = 0;
			break;

	}
	if(this->handle)this->internal.oldValue = *((unsigned long*)this->handle);
}
