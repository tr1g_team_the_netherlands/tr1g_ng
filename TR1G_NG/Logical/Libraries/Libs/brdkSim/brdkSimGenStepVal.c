#include <brdkSimGenStepVal_func.h>

void brdkSimGenStepVal(struct brdkSimGenStepVal* this) {
	
	switch(this->internal.state) {

		case 0:	/* wait for trigger */
			this->internal.value = this->setup.initValue;
			this->internal.stepDirection = this->setup.stepDirection == BRDK_SIM_STEP_TO_MAX_MIN;
			if(this->trigger) {
				this->trigger = false;
				this->internal.state = 10;
			}
			break;

		case 10: /* do in/de-crement */
			switch(this->setup.stepTrigger) {
				case BRDK_SIM_STEP_BY_CYCLE: brdkSimGenDoStep(this); break;
				case BRDK_SIM_STEP_BY_TRIGGER: 
					if(this->trigger) brdkSimGenDoStep(this); 
					this->trigger = false;
					break;
			}
			if(!this->reset) {
				this->internal.value = this->setup.initValue;
				this->internal.state = 0;
			}
			break;

	}
	this->handle = (unsigned long)&this->internal.value;
}
