
TYPE
	brdksim_handle_typ :UDINT := 0;
 (*brdkSim handle type.*)	brdkSimGenStepVal_internal_typ : 	STRUCT  (*Internal structure.*)
		state : DINT := 0; (*State of the function block.*)
		value : UDINT := 0; (*Stored value.*)
		stepDirection : BOOL := 0; (*Step direction. 1 = Increment, 0 = Decrement.*)
	END_STRUCT;
	brdkSimTimeDelay_internal_typ : 	STRUCT  (*Internal structure for time delay.*)
		state : DINT := 0; (*State of the function block.*)
		cycleTime : UDINT := 0; (*Current task cycle time.*)
		timeElapsed : UDINT := 0; (*Time elapsed for delay.*)
	END_STRUCT;
	brdkSimGenRandVal_internal_typ : 	STRUCT  (*Internal structure.*)
		state : DINT := 0; (*State of the function block.*)
		value : UDINT := 0; (*Stored value.*)
	END_STRUCT;
	brdkSimGenDI_internal_typ : 	STRUCT  (*Internal structure.*)
		state : DINT := 0; (*State of the function block.*)
		cycleTime : UDINT := 0; (*Current task cycle time.*)
		timeElapsed : UDINT := 0; (*Time elapsed for delay.*)
		value : UDINT := 0; (*Stored value.*)
	END_STRUCT;
	brdkSimMapIO_internal_typ : 	STRUCT  (*Internal structure.*)
		state : DINT := 0; (*State of the function block.*)
		AsIOEnableForcing_0 : AsIOEnableForcing; (*To enable forcing of an IO channel.*)
		AsIOSetForceValue_0 : AsIOSetForceValue; (*To forcing of an IO channel.*)
		AsIODisableForcing_0 : AsIODisableForcing; (*To disable forcing of an IO channel.*)
		oldValue : UDINT := 0; (*Old value.*)
		cpuSimulated : BOOL := FALSE; (*Is the target besing simulated?*)
	END_STRUCT;
	brdkSimMapVar_internal_typ : 	STRUCT  (*Internal structure.*)
		state : DINT := 0; (*State of the function block.*)
		oldValue : UDINT := 0; (*Old value.*)
		cpuSimulated : BOOL := FALSE; (*Is the target besing simulated?*)
		pVariable : UDINT := 0; (*Pointer to the variable.*)
		size : UDINT := 0; (*Size of the variable.*)
	END_STRUCT;
	brdkSimGenStepVal_setup_typ : 	STRUCT  (*Setup parameters for function block.*)
		min : DINT := -2147483648; (*Minimum value.*)
		max : DINT := 2147483647; (*Maximum value.*)
		step : DINT := 1; (*Each cyclic the output value is in/de-cremented with the step value.*)
		stepDirection : brdkSimGenStepVal_setup_dir_typ; (*Direction of the steps.*)
		stepTrigger : brdkSimGenStepVal_setup_trig_typ; (*Step trigger.*)
		initValue : DINT := 0; (*Initialization value.*)
	END_STRUCT;
	brdkSimGenStepVal_setup_dir_typ : 
		( (*Direction of the steps.*)
		BRDK_SIM_STEP_TO_MAX := 0, (*Steps to maximum value.*)
		BRDK_SIM_STEP_TO_MIN := 1, (*Steps to minimum value.*)
		BRDK_SIM_STEP_TO_MAX_MIN := 2, (*Steps to maximum value and minimum value continuously.*)
		BRDK_SIM_STEP_TO_MIN_MAX := 3 (*Steps to minimum value and maximum value continuously.*)
		) := BRDK_SIM_STEP_TO_MAX;
	brdkSimGenStepVal_setup_trig_typ : 
		( (*Stepper trigger.*)
		BRDK_SIM_STEP_BY_CYCLE := 0, (*Step is triggered automatically every cycle. Trigger input is still needed once to start the function block.*)
		BRDK_SIM_STEP_BY_TRIGGER := 1 (*Step is only triggered by an edge on the trigger input. Trigger input is needed at least once to start the function block. Trigger input is reset by the function block.*)
		) := BRDK_SIM_STEP_BY_CYCLE;
	brdkSimGenRandVal_setup_typ : 	STRUCT  (*Setup parameters for function block.*)
		min : DINT := -2147483648; (*Minimum value. Must be smaller and not equal to maximum value.*)
		max : DINT := 2147483647; (*Maximum value. Must be greater and not equal to minimum value.*)
		initValue : USINT := 0; (*Initialization value.*)
	END_STRUCT;
END_TYPE
