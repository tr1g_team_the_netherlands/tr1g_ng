#include <brdkSIM_func.h>

void brdkSimTimeDelay(struct brdkSimTimeDelay* this) {
	switch(this->internal.state) {

		case 0:	/* init state */
			if(this->internal.cycleTime == 0) this->internal.cycleTime = getCycleTime(); /* get the modules cycle time */
			this->trigger = false;
			this->internal.state = 10;
			break;	

		case 10: /* wait for enable */
			if(this->enable) {
				this->internal.timeElapsed = 0;
				if(this->delay != 0) this->internal.state = 20;
				else {
					this->trigger = true;
					this->internal.state = 30;
				}
			}
			break;

		case 20: /* wait for elapsed time */
			this->internal.timeElapsed += this->internal.cycleTime/1000;
			if(this->internal.timeElapsed >= this->delay) {
				this->trigger = true;
				this->internal.state = 30;
			}
			if(!this->enable) {
				this->trigger = false;
				this->internal.state = 10;
			}
			else if(this->reset) {
				this->reset = false;
				this->internal.timeElapsed = 0;
				this->trigger = false;
				this->internal.state = (this->delay != 0) ? 20 : 30;
			}
			break;

		case 30: /* wait for enable off */
			if(!this->enable) {
				this->trigger = false;
				this->internal.state = 10;
			}
			else if(this->reset) {
				this->reset = false;
				this->internal.timeElapsed = 0;
				this->trigger = false;
				this->internal.state = (this->delay != 0) ? 20 : 30;
			}
			break;

	}
}
