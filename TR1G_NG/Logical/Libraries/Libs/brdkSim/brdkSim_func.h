#ifndef BRDK_SIM_FUNCS
#define BRDK_SIM_FUNCS 1

#include <brdkSim.h>
#include <bur/plctypes.h>

#ifdef __cplusplus
	extern "C"
	{
#endif

#ifdef __cplusplus
	};
#endif

#define false 0
#define true 1

unsigned long getCycleTime();

#endif /* !BRDK_SIM_FUNCS */
