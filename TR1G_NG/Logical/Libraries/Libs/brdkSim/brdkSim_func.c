#include <brdkSIM_func.h>

/* gets the cycle time of the task the em structure is called inside */
unsigned long getCycleTime() {
	RTInfo_typ RTInfo_0;
	RTInfo_0.enable = 1;
	RTInfo(&RTInfo_0);
	return RTInfo_0.cycle_time;
}

