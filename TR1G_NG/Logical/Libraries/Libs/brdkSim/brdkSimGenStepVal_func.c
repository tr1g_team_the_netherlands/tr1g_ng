#include <brdkSimGenStepVal_func.h>

/* make sure that the value don't exceed the maximum value */
signed long getIncrementValue(signed long max, signed long step, signed long val) {
	return (max-step >= val) ? step : max-val;
}

/* make sure that the value don't exceed the minimum value */
signed long getDecrementValue(signed long min, signed long step, signed long val) {
	return (min+step <= val) ? step : val-min;
}

void brdkSimGenDoStep(brdkSimGenStepVal_typ* this) {
	if(this->setup.step != 0) {
		switch(this->setup.stepDirection) {
	
			case BRDK_SIM_STEP_TO_MAX:
				if(this->internal.value < this->setup.max) this->internal.value += getIncrementValue(this->setup.max,this->setup.step,this->internal.value);
				break;

			case BRDK_SIM_STEP_TO_MIN:
				if(this->internal.value > this->setup.min) this->internal.value -= getDecrementValue(this->setup.min,this->setup.step,this->internal.value);
				break;

			case BRDK_SIM_STEP_TO_MAX_MIN: case BRDK_SIM_STEP_TO_MIN_MAX:
				if(this->internal.stepDirection) {	/* Increment */
					if(this->internal.value < this->setup.max) this->internal.value += getIncrementValue(this->setup.max,this->setup.step,this->internal.value);
					else {
						this->internal.value -= getDecrementValue(this->setup.min,this->setup.step,this->internal.value);
						this->internal.stepDirection = 0;
					}
				}
				else { /* Decrement */
					if(this->internal.value > this->setup.min) this->internal.value -= getDecrementValue(this->setup.min,this->setup.step,this->internal.value);
					else {
						this->internal.value += getIncrementValue(this->setup.max,this->setup.step,this->internal.value);
						this->internal.stepDirection = 1;
					}
				}
				break;
	
		}
	}
}


