#ifndef BRDK_GENSTEPVAL_FUNCS
#define BRDK_GENSTEPVAL_FUNCS 1

#include <brdkSim_func.h>

void brdkSimGenDoStep(brdkSimGenStepVal_typ* this);

#endif /* !BRDK_SIM_GENSTEPVAL_FUNCS */
