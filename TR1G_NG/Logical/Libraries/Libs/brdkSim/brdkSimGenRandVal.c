#include <brdkSim_func.h>
#include <stdlib.h>

void brdkSimGenRandVal(struct brdkSimGenRandVal* this) {

	switch(this->internal.state) {

		case 0:	/* wait for trigger */
			this->internal.value = this->setup.initValue;
			if(this->trigger) {
				this->trigger = false;
				this->internal.state = 10;
			}
			break;

		case 10: /* generate random number */
			if(this->setup.max > this->setup.min) {
				// min is negative
				srand(AsIOTimeStamp());
				if(this->setup.min < 0) {
					/* could be improved. Lowest min is not with */
					unsigned long tmpMax = this->setup.max-this->setup.min;
					unsigned long tmpOutput = rand() % tmpMax+1;
					this->output = (signed long)(tmpOutput + this->setup.min);
				}
				else this->output = this->setup.min + (rand() % (this->setup.max - this->setup.min + 1));
			}
			else this->output = this->setup.initValue;
			if(!this->reset) {
				this->internal.value = this->setup.initValue;
				this->internal.state = 0;
			}
			break;

	}
	this->handle = (unsigned long)&this->internal.value;
}
