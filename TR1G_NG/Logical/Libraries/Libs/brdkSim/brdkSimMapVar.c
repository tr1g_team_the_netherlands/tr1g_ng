#include <brdkSim_func.h>

void brdkSimMapVar(struct brdkSimMapVar* this) {
	switch(this->internal.state) {

		case 0:	/* read pv address - must always be done in INIT program */ 
			this->internal.cpuSimulated = DiagCpuIsSimulated();
			this->status 				= PV_xgetadr((char*)this->pVariableName, &this->internal.pVariable, &this->internal.size);	
			this->internal.state 		= 10;
			break;

		case 10: /* wait for enable */
			if((this->enable || this->internal.cpuSimulated) && this->internal.pVariable != 0 && this->handle != 0) {
				brdkStrMemCpy(this->internal.pVariable,this->handle,this->internal.size);
				this->internal.state = 20;
			}
			break;

		case 20: /* wait for disable */
			if(this->enable || this->internal.cpuSimulated) {
				if(brdkStrMemCmp(this->internal.pVariable,this->handle,this->internal.size) != 0) {
					brdkStrMemCpy(this->internal.pVariable,this->handle,this->internal.size);
				}
			}
			break;

	}
}
