
/*! 

 \page Example
 \anchor Example

 \section Example Example - Binding multiple variables to Modbus server
 
 \par Description
	 
	You normally use the BRSE_MB together with another BRSE_MBx library (Depending on protocol - BRSE_MBT, BRSE_MBR, BRSE_MBU, BRSE_MBS)
	For more examples on how to use this library. Please take a look in the help of the corresponding library.

\par Example
 	This is an example that shows a modbus server/client application where four lamps and buttons are located on the modbus server.
	The array Lamps[] corresponds to lamps (discrete inputs, read-only). 
	The array LampButtons[] corresponds to the buttons (coils, read-write) to switch on and off the lamps.
	
	\arg Alt 1 is the common way to bind variables to the server one by one. 
	\arg Alt 2 shows the use of MB_BindCoils and MB_BindDiscreteInputs, which handles multiple bindings at the same time.

\par Code on server side:

	As the server side functions only configure the modbus registers, all configuration of the BRSE_MB functionality 
	should be done in the _INIT part of the program, or within one scan of the CYCLIC part.

	The physically dependent functions (from the other libraries), like BRSE_MBR_Server or BRSE_MBT_Server are the only functions that should run in the CYCLIC code.

\par	Alt 1 (Structured Text) - Use of the standard function blocks to bind variables one by one 
	
\code
PROGRAM _INIT

	BRSE_MB_Alloc(Unit, MaxCoils, MaxHoldingRegisters,DiscreteInputs,MaxInputRegisters);
	
	BRSE_MB_Coil(Unit,0,ADR(LampButtons[0]));
	BRSE_MB_Coil(Unit,1,ADR(LampButtons[1]));
	BRSE_MB_Coil(Unit,2,ADR(LampButtons[2]));
	BRSE_MB_Coil(Unit,3,ADR(LampButtons[3]));
	
	BRSE_MB_DiscreteInput(Unit, 15, ADR(Lamps[0]));
	BRSE_MB_DiscreteInput(Unit, 16, ADR(Lamps[1]));
	BRSE_MB_DiscreteInput(Unit, 17, ADR(Lamps[2]));
	BRSE_MB_DiscreteInput(Unit, 18, ADR(Lamps[3]));

END_PROGRAM
\endcode

	
	Alt 2 (FBD) - Use of the multiple bind function blocks 
	
	\image html y:\Application\Libraries\BRSE_MB\Documentation\Example_MB_Bind_ServerSide.jpg


\par Code on Client side:

The client requests changes of variables on the server or reads the variables from the server.
This can be done in a cyclic manner, as shown below with the Push / Poll functions, or within one execution, with the Read / Write functions.

\code
PROGRAM _INIT

	(* A Modbus client is created with id 1 - for more info, see MBR/MBT/MBS/MBU help *)
	MB_AllocClient(1);

END_PROGRAM

PROGRAM _CYCLIC
	
	MB_PushCoils_0.ClientID                   := 1;				
	MB_PushCoils_0.Enable                     := 0;           (* No need to enable yet*)
	MB_PushCoils_0.Quantity                   := 4;				
	MB_PushCoils_0.PushInterval               := 200;         (* Number of cycles between write requests*)
	MB_PushCoils_0.VariablesAddress           := ADR(LampButtons);
	MB_PushCoils_0.StartAddress               := 0;
	MB_PushCoils_0.Unit                       := 1;
	MB_PushCoils(&MB_PushCoils_0); 
	
	MB_PollDiscreteInputs_0.ClientID          := 1;
	MB_PollDiscreteInputs_0.Enable            := 0;
	MB_PollDiscreteInputs_0.Quantity          := 4;
	MB_PollDiscreteInputs_0.PollInterval      := 200;         (* Number of cycles between read requests*)
	MB_PollDiscreteInputs_0.VariablesAddress  := ADR(Lamps);
	MB_PollDiscreteInputs_0.StartAddress      := 15;
	MB_PollDiscreteInputs_0.Unit              := 1;
	MB_PollDiscreteInputs(&MB_PollDiscreteInputs_0);

END_PROGRAM
\endcode

*/


