/*! \file MB_BindingsToServer.c */
 
#include <bur/plctypes.h>
#include "BRSE_MB.h"
#include "BRSE_MB_Main.h"


/*! \interface MB_BindCoils
	\brief Function for binding multiple coils (read-write boolean values) to variables on server. Uses BRSE_MB_Coils.
	
	Note that this function uses relative modbus adressing. Reference 0 corresponds to modicon adress 1.
	
	\param[in]	Unit				Unit identifier (Unit on server: 0-255)
	\param[in]	Reference			Coil index within unit (0-2000)
	
	\param[in]	VariableAddress		Address to element in array
	\param[in]	Quantity			Number of elements in array
	\param[out] Status				Returns 0 or an error code.
*/

/* Bind the coils - BOOL */
void MB_BindCoils(struct MB_BindCoils* inst)
{
	UINT i;
	for (i=0; i < inst->Quantity; i++)
	{
		inst->Status = BRSE_MB_Coil(inst->Unit,inst->Reference+i,inst->VariableAddress+i);
	}
}


/*! \interface MB_BindDiscreteInputs
	\brief Function for binding multiple discrete inputs (read only boolean values) to variables on server. Uses BRSE_MB_DiscreteInput.
	
	Note that this function uses relative modbus adressing. Reference 0 corresponds to modicon adress 10001.
	
	\param[in]	Unit				Unit identifier (Unit on server: 0-255)
	\param[in]	Reference			Discrete input index within unit (0-2000)
	
	\param[in]	VariableAddress		Address to element in array
	\param[in]	Quantity			Number of elements in array
	\param[out] Status				Returns 0 or an error code.
*/

/* Bind discrete inputs - BOOL */
void MB_BindDiscreteInputs(struct MB_BindDiscreteInputs* inst)
{
	UINT i;
	for (i=0; i < inst->Quantity; i++)
	{
		inst->Status = BRSE_MB_DiscreteInput(inst->Unit,inst->Reference+i,inst->VariableAddress+i);
	}
}


/*! \interface MB_BindInputRegisters
	\brief Function for binding multiple input registers (read only 16 bit integer values) to variables on server. Uses BRSE_MB_InputRegister.
	
	Note that this function uses relative modbus adressing. Reference 0 corresponds to modicon adress 30001.
	
	\param[in]	Unit				Unit identifier (Unit on server: 0-255)
	\param[in]	Reference			Input register index within unit (0-125)
	
	\param[in]	VariableAddress		Address to element in array
	\param[in]	Quantity			Number of elements in array
	\param[out] Status				Returns 0 or an error code.
*/

/* Bind input registers - read only 16 bit INT values */
void MB_BindInputRegisters(struct MB_BindInputRegisters* inst)
{
	UINT i;
	for (i=0; i < inst->Quantity; i++)
	{
		inst->Status = BRSE_MB_InputRegister(inst->Unit,inst->Reference+i,inst->VariableAddress+i*2);
	}
}

/*! \interface MB_BindHoldingRegisters
	\brief Function for binding multiple input registers (read-write 16 bit integer values) to variables on server. Uses BRSE_MB_HoldingRegister.
	
	Note that this function uses relative modbus adressing. Reference 0 corresponds to modicon adress 40001.
	
	\param[in]	Unit				Unit identifier (Unit on server: 0-255)
	\param[in]	Reference			Holding register index within unit (0-125)
	
	\param[in]	VariableAddress		Address to element in array
	\param[in]	Quantity			Number of elements in array
	\param[out] Status				Returns 0 or an error code.
*/

/* Holding registers - read-write 16 bit INT values */
void MB_BindHoldingRegisters(struct MB_BindHoldingRegisters* inst)
{
	UINT i;
	for (i=0; i < inst->Quantity; i++)
	{
		inst->Status = BRSE_MB_HoldingRegister(inst->Unit,inst->Reference+i,inst->VariableAddress+i*2);
	}
}

/* 4 byte holding registers - read-write REAL values */
void MB_Bind4ByteHoldingRegisters(struct MB_Bind4ByteHoldingRegisters* inst)
{
	UINT i;
	for (i=0; i < inst->Quantity; i++)
	{
		inst->Status = BRSE_MB_4ByteHoldingRegister(inst->Unit,inst->Reference+i*2,inst->VariableAddress+i*4);
	}
}


