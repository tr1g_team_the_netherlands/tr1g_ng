/**
 * Prosys OPC UA Java SDK
 *
 * Copyright (c) Prosys PMS Ltd., <http://www.prosysopc.com>.
 * All rights reserved.
 */
package opc_client;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.opcfoundation.ua.application.Client;
import org.opcfoundation.ua.builtintypes.DataValue;
import org.opcfoundation.ua.builtintypes.ExpandedNodeId;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.core.ApplicationType;
import org.opcfoundation.ua.core.Identifiers;
import org.opcfoundation.ua.core.ReferenceDescription;
import org.opcfoundation.ua.transport.security.SecurityMode;

import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.ConnectException;
import com.prosysopc.ua.client.ServerConnectionException;
import com.prosysopc.ua.client.UaClient;

/**
 * A very minimal client application. Connects to the server and reads one
 * variable. Works with a non-secure connection.
 */



public class SimpleClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		@SuppressWarnings("unused")
		ExpandedNodeId nodeId = null;
		Timer updateTimer = new Timer();

		UaClient client = new UaClient("opc.tcp://10.1.25.56:4840/BR/UA/EmbeddedServer");
		client.setTimeout(30000);

		System.out.println("OPC UA server name :" + client.getServerName());



		client.getAddressSpace().setMaxReferencesPerNode(1000);
		client.getAddressSpace().setReferenceTypeId(Identifiers.HierarchicalReferences);
		String eenWaarde = null;


		//client.getAddressSpace().browse(nodeId);

		//client.getNamespaceTable();
		//List<ReferenceDescription> references = client.getAddressSpace().browse(nodeId);

		client.setSecurityMode(SecurityMode.NONE);
		initialize(client);

		updateTimer.schedule(new TimerTask()
		{

			@Override
			public void run() {
				try
				{
				client.connect();
				}
				catch ( Exception ex)
				{
					ex.printStackTrace();
				}
				if (client.isConnected())
				{
					System.out.println("Client is connected to server on " + client.getHost());
					try {
						System.out.println("OPC UA server status : " + client.getServerStatus().toString());
					} catch (ServerConnectionException | StatusException e)
					{
						e.printStackTrace();
					}
					System.out.println("OPC UA server state : " + client.getServerState());

				}
				//DataValue value = client.readValue(Identifiers.Server_ServerStatus_State);
				DataValue value = null;
				try {
					value = client.readValue(Identifiers.Server_ServerStatus_CurrentTime);
					//value = client.r
				} catch (ServiceException | StatusException e) {

					e.printStackTrace();
				}
				//DataValue variable = client.readAttribute(arg0, arg1)
				System.out.println(value);
				client.disconnect();
				if (!client.isConnected())
				{
					System.out.println("Client is disconnected from server" + "\n");
				}

			}



		},0,5000);


		//System.out.println("Program ended");
		//System.exit(0);
	}

	/**
	 * Define a minimal ApplicationIdentity. If you use secure connections, you
	 * will also need to define the application instance certificate and manage
	 * server certificates. See the SampleConsoleClient.initialize() for a full
	 * example of that.
	 */
	protected static void initialize(UaClient client)
			throws SecureIdentityException, IOException, UnknownHostException {
		// *** Application Description is sent to the server
		ApplicationDescription appDescription = new ApplicationDescription();
		appDescription.setApplicationName(new LocalizedText("SimpleClient", Locale.ENGLISH));
		// 'localhost' (all lower case) in the URI is converted to the actual
		// host name of the computer in which the application is run
		appDescription.setApplicationUri("urn:localhost:UA:SimpleClient");
		appDescription.setProductUri("urn:prosysopc.com:UA:SimpleClient");
		appDescription.setApplicationType(ApplicationType.Client);

		final ApplicationIdentity identity = new ApplicationIdentity();
		identity.setApplicationDescription(appDescription);
		client.setApplicationIdentity(identity);
	}

}
