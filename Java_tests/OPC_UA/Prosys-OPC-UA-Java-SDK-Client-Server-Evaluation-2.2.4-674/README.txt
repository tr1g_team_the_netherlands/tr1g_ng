This is the distribution of Prosys OPC UA Java SDK.

See the LICENSE document for license information.

SYSTEM REQUIREMENTS

The Java stack requires Java SE 6 (Java Runtime Environment 1.6) or later. 
You also need to install JDK 6 (jdk1.6) or later to be able to compile your applications. 
The latest available version is usually recommended.
The downloads are available from 
http://www.oracle.com/technetwork/java/javase/downloads/index.html

INSTALL

Extract the SDK Package on your computer. 
Add the libraries in the 'lib'-directory to the Java classpath in your environment. You
can then use them in your applications. 
See below (DEPLOYMENT), which libraries you actually need to use and deploy. 

The samples in the package assume that you have installed JDK and set up the
JAVA_HOME environment variable to the JDK directory 
(so that the Java compiler is located at %JAVA_HOME%\bin\javac.exe (Windows) 
or $JAVA_HOME/bin/javac (Linux))  

If your setup works, you can run the samples using the batch or shell script 
in the samples directory.

INSTALL IN ECLIPSE

If you are running Eclipse, you can install the SDK in it as follows:

- Go to the Package Explorer on the left.
- Select 'New-Java Project'
- In Eclipse 4.x, uncheck 'Use default location' and find the directory
  where you extracted the package for the 'Location'
- In Eclipse 3.x, select 'Create project from existing source' and find the directory
  where you extracted the package
- Press Finish

=> You should have the project in your workspace.

Note that Eclipse adds all libraries that are found to the Build Path by default. 
See below (DEPLOYMENT), which libraries you actually need to use and deploy. 

To use the SDK in your own projects, you can simply copy all the .jar:s from the 
lib directory and add them to your project's Build Path. They should appear in your
project's Referenced Libraries, after that. 

JAVADOC INSTALL 

The Javadoc of the SDK is zipped in the doc-directory. You can install it to Eclipse 
as zipped or extracted.

Find the SDK.jar from your project's Referenced Libraries (see above) and open
Properties for it. There you can locate the javadoc and link it for the SDK library.

You can do the same with the Opc.Ua.Stack.jar.

TUTORIALS

The tutorial documents in the doc-directory help you getting started with the SDK
development.

SOURCE INSTALL 

If you have the source version of the SDK, you can find it zipped in the src directory.

If you did not unzip the source before you installed it to Eclipse, 
you can install the source as follows:

Unzip the SDK and Stack source packages in the src directory.

In Eclipse, define that src folder is a source folder:

- In Package Explorer select 'src' folder
- From the popup menu (right click), select 'Build Path->Use as Source Folder'
- You may also need to refresh the Package Explorer to see the extracted source files: Press F5.

To test that everything works, go to the samples-folder in Eclipse and 
locate the client/SampleConsoleClient and/or server/SampleConsoleServer. 
Find 'Run As-Java Application' from the popup menu to start it up. 

DEPLOYMENT

When you deploy your applications, you will also need to deploy the respective libraries. 
But you won't necessarily need them all.

Prosys*              SDK classes             Necessary for all applications
Opc.Ua.Stack*.jar    UA communication        Necessary for all applications
slf4j-api-*.jar      SLF4J Logging Facade	   Necessary for all applications
slf4j-log4j12-*.jar  SLF4J-to-Log4J bridge   If you wish to use Log4J version 1.2 for logging (see below for more)
log4j-1.2.17.jar     Apache Log4J            If you wish to use Log4J version 1.2 for logging
http-*.jar           HTTP Core components    Necessary for all applications that support HTTPS
commons-logging*.jar Commons Logging         Necessary for all client applications that support HTTPS
bc*.jar              Bouncy Castle security  Optional, but recommend in Java SE for UA security support: 
                                             if not included SunJCE libraries, will be used. 
                                             Note that the SunJCE based security have not been tested as much as BC.
sc*.jar              Spongy Castle security  Necessary for Android applications to support UA security

It is also recommended that the SDK classes are bundled in your own application, instead of deploying it
as is. Obfuscation techniques are also recommended to guard it against illegal usage.

NOTE: You must not repackage the Bouncy Castle library into your application application. 
It is a signed security library and must be linked to your application via the original bc*.jar
files in the classpath.

Also note the respective licenses for each library, as mentioned in LICENSE.txt

USAGE OF SECURITY LIBRARIES

OPC UA Security is implemented in the Java Stack component, provided by the OPC Foundation. 
The stack will pick an available security library automatically, but if necessary even a custom
library can be plugged in.

The following is copied from the 'readme' section of the stack and describes the library usage 
in detail:

"Security libraries are used as they are available. If no extra libraries are available, 
the Sun JCE implementation will be used.  Testing has so far based on the Bouncy Castle 
library and it is therefore recommended in normal applications.

The current implementation, since version 1.02.337.0, is based on a flexible CryptoProvider
model. There are several implementations of this interface available in the stack and you 
can also implement your own, if you have a custom security framework that you need to use.

The stack will pick a default CryptoProvider automatically as it finds them from the class
path. Alternatively, you can define the provider that you wish to use by setting it with 
CryptoUtil.setCryptoProvider() or with CryptoUtil.setSecurityPoviderName(). 

In the same manner, custom certificate framework can be used by implementing interface CertificateProvider 
and setting it with CertificateUtils.setCertificateProvider().

Current CryptoProvider implementations:

BcCryptoProvider (default, if Bouncy Castle is in the class path: uses Bouncy Castle directly)
ScCryptoProvider (default in Android, if Spongy Castle is in the class path)
SunJceCryptoProvider (default if Bouncy Castle or Spongy Castle are not available)
BcJceCryptoProvider (uses Bouncy Castle via the JCE crypto framework)
ScJceCryptoProvider (uses Spongy Castle via the JCE crypto framework)

If any of the ...JceCryptoProvider is used, you will have to install the JCE Unlimited Strength 
Jurisdiction Policy Files, from Oracle (for Java 6, 7 or 8, respectively), to enable support for 
256 bit security policies:

JRE6: http://www.oracle.com/technetwork/java/javase/downloads/jce-6-download-429243.html
JRE7: http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html
JRE8: http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html

Android includes a limited version of Bouncy Castle and the standard Bouncy Castle cannot 
be installed there. It also does not include the Sun classes. However, the Spongy Castle
libraries will provide the same functionality as Bouncy Castle in Android, so these 
libraries should be used in Android, unless the application can do without security altogether.

Current CertificateProvider implementations:

BcCertificateProvider (default, if Bouncy Castle is in the class path)
ScCertificateProvider (default in Android, if Spongy Castle is in the class path)
SunJceCertificateProvider (default if Bouncy Castle or Spongy Castle are not available)"

LOGGING LIBRARY SELECTION

The SDK in (since version 2.1) using SLF4J logging facade for message logging. This enables each application
to use any specific logging library, since SLF4J is just used to direct logging to a selected library.

slf4j-api must always be included in the application build path.

The actual library selection is done by picking one of the SLF4J bridges and the actual library. By default,
the SDK is shipped with Log4J version 1.2 - and therefore the slf4j-log4j12 bridge is also included. For other
logging alternatives, see http://slf4j.org

ANDROID DEVELOPMENT

The SDK can be used for Android application development as well as for normal Java development. The libraries 
that are usable on Android are

Prosys*  
Opc.Ua.Stack*.jar
slf4j-api-*.jar
slf4j-android-*.jar
sc*.jar

It is not possible to use HTTPS on Android due to library conflicts in HTTP Core components.

CODE GENERATION

The SDK contains a code generator, which can be used to create Java classes corresponding to
various OPC UA type definitions. The code generator is in the codegen directory. See the Readme.md in there
for more details on how to use it. 

KNOWN ISSUES

The following issues are known problems with the OPC Foundation OPC UA Java Stack:

- TLS 1.2 policy required by OPC UA does not work (required ciphers not supported by JSSE) 
- HTTPS testing is not finished yet with the other stacks
- Apache HTTP Core http*.jar libraries deprecated lot of things when moving from version 4.2.x to 4.3.x.
  Problems with new HTTP libraries and certain message sizes came up in testing and backwards compatibility
  with previous HTTP libraries was conserved in this stack version.
- .NET Client requires that the server has a certificate signed by a trusted CA, if HTTPS is used.

MIGRATION FROM 1.x

If you are migrating an existing application that is using version 1.x of the SDK, you can check the Migration Guide at
http://www.prosysopc.com/opcua/Prosys_OPC_UA_Java_SDK_Migration_To_2_2.pdf (link available on the download page), which
explains the differences in 2.x and helps you to modify your application respectively.


Copyright (c) 2010-2015
Prosys PMS Ltd
http://www.prosysopc.com
uajava-support@prosysopc.com
