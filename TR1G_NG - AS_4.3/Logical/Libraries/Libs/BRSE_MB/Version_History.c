/*!
\page History

\section History

\par V0.00
	\arg First draft version

\par V1.00
	\arg First functional version. Added inline comments.

\par V1.01
	\arg Added the input Unit in the client requests. Make it possible to communicate to many units in the same server in a simple way.
	\arg Added documentation for the client.

\par V1.02
	\arg Updated documentation.

\par V1.03
	\arg Added functionality for handling 4 byte variables.
	
\par V1.10
	\arg Release version for handling 4 byte holding registers

\par V1.11
	\arg Bugfix for writing multiple holding registers 4 byte. the last INT was copied in the wrong way.

\par V1.12
	\arg Bugfixes:
	\arg Allocation functions, if allocation failed, write to logbook and don't memset.
	\arg WriteHoldingRegister, 4WriteByteHoldingRegisters, WriteCoils buffer lengths were 1 byte too long.
	\arg Client functions check if the queue is alive while waiting for answers, returning error codes if the queue died.

\par V1.13
	\arg Minor change in the poll macro (client). Previously the condition to execute was (cycleCounter%pollCycles==0), now it's cycleCounter>=pollCycles (cycleCounter is reset when command is executed).

\par V1.14
	\arg Pagefault when writing to a single holding register when this was mapped as a 4-byte value has been fixed. Now
	an ILLEGAL_DATA_ADDRESS is returned if a 4byte value is mapped on that modbus reference.
	\arg All function blocks with an \b Execute input have been changed so that the execute input is edge sensitive (previously,
	the input was handled rather as an \b Enable input)
	\arg MB_QUEUE_SIZE has been set up to 50, it seems unlikely to have more simultaneous function calls than this at the moment.
	\arg Added dependency to the brsystem library, which reads out the cycle time of the task. The inputs of the functions have therefore changed from \b Poll/PushCycles to \b Poll/PushInterval, which you specify in ms.

\par V1.15
	\arg Corrected status bits from push and poll functions.

\par V1.16
	\arg MB_AllocClient : Changed return value to always return the internal address, even though allocation has already been performed.
	\arg Added extra information on the Error_Number page
	\arg Added the ModbusUDP adapter library BRSE_MBU to the documentation

\par V1.17
	\arg Five FUBs added tp server side, to bind multiple values at the same time:
	\arg MB_BindCoils
	\arg MB_BindDiscreteInputs
	\arg MB_BindInputRegisters
	\arg MB_BindHoldingRegisters
	\arg MB_Bind4ByteHoldingRegisters
	\arg Added an example 

\par V1.18
	\arg AsString changed to AsBrStr
	\arg CONVERT changed to AsIecCon
	\arg New feature in BRSE_MB_Main.h:
	It is possible to write single or multiple coils/registers/4ByteRegister. 
	BRSE_MB always use the functioncode(FC) for single write if the input quantity is set to 1.
	\n These defines locks the function codes to multiple operation even if input quantity is 1:
 	\n #define \b LOCK_FUNCTION_CODE_MULTIPLE_COIL		0 (FC for single coil 				--> 16#05, multiple coil 		--> 16#0F)
 	\n #define \b LOCK_FUNCTION_CODE_MULTIPLE_REGISTER	0 (FC for single register 			--> 16#06, multiple register 	--> 16#10)
	\n #define \b LOCK_FUNCTION_CODE_MULTIPLE_4B_REGISTER	0 (FC for single 4byte register	--> n.u., multiple 4byte register	--> 16#31)
	
\par V1.19
	\arg New features:
	\n It is possible to set the node of a unit.
	\n The following functions have been added in order to handle the node number:
	\n unsigned short BRSE_MB_SetNode(signed short Unit, unsigned char Node)
 	\n unsigned short BRSE_MB_GetNode(signed short Unit)
	\n signed short BRSE_MB_GetUnit(unsigned char Node)
	\n static UINT BRSE_MB_CheckUnit(INT Unit) (Internal function)
	\n
	\arg New Error codes:
	\n BRSE_MB_ERR_UNIT_NOT_ALLOCATED (20003);
	\n BRSE_MB_ERR_UNIT_NOT_EXISTING (20004);
	\n BRSE_MB_ERR_UNIT_INVALID_INDEX (20005);
	
\par V1.20
	\arg Removed the concept of node of a station introduced in V1.19.
	\n A server station will have two key values: Unit and Id.
    \n  1. Unit: value that identifies the server station in the network and can be changed dinamycally.
    \n          At the moment of the allocation, the unit value will define the Id number;
    \n  2. Id: value that identifies the server station and cannot be changed dinamycally;

	\arg The functions introduced in V1.19 in order to handle the node and unit numbers
	\n   have been substituted by the following:
	\n unsigned short BRSE_MB_SetUnit(signed short Id, unsigned char Unit);
 	\n unsigned short BRSE_MB_GetUnit(signed short Id, unsigned char* Unit);
	\n signed short BRSE_MB_GetId(unsigned char Unit);
	\n UINT BRSE_MB_CheckId(INT Id) (Internal function);
	\n
	\arg New Error codes:
	\n BRSE_MB_ERR_NULL_POINTER : UINT := 20006;
	\n BRSE_MB_ERR_UNIT_DUPLICATE : UINT := 20007;
*/
