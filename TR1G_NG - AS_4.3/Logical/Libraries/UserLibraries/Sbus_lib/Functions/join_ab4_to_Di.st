
(* This function is used to combine a 4 byte array to a DINT *)
(* Typical use of this funtion is to combine the data array of a CAN message to a DINT format *)
(* Unions are not allowed to use in structured text			*)
FUNCTION join_ab4_to_Di
	diData := BYTE_TO_DINT(abDataIn[0]);
	diData := SHL(diData,8);
	diData := diData OR BYTE_TO_DINT(abDataIn[1]);
	diData := SHL(diData,8);
	diData := diData OR BYTE_TO_DINT(abDataIn[2]);
	diData := SHL(diData,8);
	diData := diData OR BYTE_TO_DINT(abDataIn[3]);

	join_ab4_to_Di := 1;
END_FUNCTION
