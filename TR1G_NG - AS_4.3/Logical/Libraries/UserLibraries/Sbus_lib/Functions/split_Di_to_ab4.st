
(* This function can be used to split a double in to an 4 byte array.	*)
(* This fuction is used TO create 4 data field in a CAN message  		*)
FUNCTION split_Di_to_ab4
	abTelegramData[0] := DINT_TO_BYTE(SHR(diData AND 16#ff000000,24));
	abTelegramData[1] := DINT_TO_BYTE(SHR(diData AND 16#00ff0000,16));
	abTelegramData[2] := DINT_TO_BYTE(SHR(diData AND 16#0000ff00,8));
	abTelegramData[3] := DINT_TO_BYTE(diData AND 16#000000ff);
	
split_Di_to_ab4 := 1;
END_FUNCTION
