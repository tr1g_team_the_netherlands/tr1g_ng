
(* receive information of the bus *)
FUNCTION_BLOCK sBus_info
	CANinfo_0(enable :=xEnable , us_ident :=udiHandle , info_adr :=ADR(infoStruct) );
	uiStatus := CANinfo_0.status;
	structInfo := infoStruct;
	
	
END_FUNCTION_BLOCK
