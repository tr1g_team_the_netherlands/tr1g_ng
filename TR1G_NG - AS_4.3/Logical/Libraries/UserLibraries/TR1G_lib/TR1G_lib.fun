(*Main*)

FUNCTION_BLOCK controlCPX_FB40
	VAR_INPUT
		xValve1_A : BOOL;
		xValve1_B : BOOL;
		xValve2_A : BOOL;
		xValve2_B : BOOL;
		xValve3_A : BOOL;
		xValve3_B : BOOL;
		xValve4_A : BOOL;
		xValve4_B : BOOL;
		xValve5_A : BOOL;
		xValve5_B : BOOL;
		xValve6_A : BOOL;
		xValve6_B : BOOL;
		xValve7_A : BOOL;
		xValve7_B : BOOL;
		xValve8_A : BOOL;
		xValve8_B : BOOL;
	END_VAR
	VAR_OUTPUT
		usiValve_1To4 : USINT := 0;
		usiValve_5To8 : USINT := 0;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK movingAverageFilterEncoder
	VAR_INPUT
		iEnc : Encoder;
		trigger : BOOL;
	END_VAR
	VAR_OUTPUT
		oAvg : DINT;
		oDisLastShakle : DINT;
	END_VAR
	VAR
		RE_encoderVal : R_TRIG;
		encVals : ARRAY[0..9] OF DINT;
		index : SINT;
		lastTriggerVal : DINT;
		actTriggerVal : DINT;
		myMovingAverage : LCMovAvFilter := (enable:=TRUE,base:=10);
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK CalculationsAndMeasurements (*Speed measurements and Speed calculations*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iEncoderDataPDS : DINT;
		iEncoderDataMX : DINT;
		iEncPDSNTCircumference : REAL;
		iEncPDSNTResolution : UINT;
		iPdsNrOfHooks : REAL;
		iEncMXCircumference : REAL;
		iEncMXResolution : UINT;
		iMxNrOfHooks : REAL;
		iSensHookPDS : BOOL;
		iSensHookMX : BOOL;
	END_VAR
	VAR_OUTPUT
		oEncoderPDSNT : Encoder;
		oPDSHookSpeed : REAL;
		oLineSpeedPDSNT : INT;
		oEncoderMX : Encoder;
		oMXHookSpeed : REAL;
		oLineSpeedMX : INT;
		oSpeedMxLine : REAL;
		oSpeedPDSline : REAL;
		xSupLineRun : BOOL;
		xDisLineRun : BOOL;
	END_VAR
	VAR
		movingAveragePds : LCRMovAvgFlt := (enable:=TRUE,base:=500);
		movingAverageMx : LCRMovAvgFlt := (enable:=TRUE,base:=500);
		movAvgPDSNTms : LCRMovAvgFlt := (enable:=TRUE,base:=500);
		movAvgMSms : LCRMovAvgFlt := (enable:=TRUE,base:=500);
		iEncoderPDSNT : UDINT;
		iEncoderMX : UDINT;
		iEncoderPDSNTrev : REAL;
		iEncoderMXrev : REAL;
		EncExtenderPDS : {REDUND_UNREPLICABLE} EncoderToLargerRange;
		EncExtenderMX : {REDUND_UNREPLICABLE} EncoderToLargerRange;
		reSensHookPDS : R_TRIG;
		prevTimePdsHook : TIME;
		reSensHookMX : R_TRIG;
		prevTimeMxHook : TIME;
		reSensHookMx : R_TRIG;
		AveragePds : MTBasicsPT1 := (Enable:=TRUE,Gain:=1.0,TimeConstant:=5.0);
		AverageMx : MTBasicsPT1 := (Enable:=TRUE,Gain:=1.0,TimeConstant:=5.0);
		udiEncExtPds : EncoderExtender;
		udiEncExtMx : EncoderExtender;
		rAvgPdsNTms : MTBasicsPT1 := (Enable:=TRUE,Gain:=1.0,TimeConstant:=5.0);
		rAvgMXms : MTBasicsPT1 := (Enable:=TRUE,Gain:=1.0,TimeConstant:=5.0);
		xSimulation : BOOL := FALSE;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderToLargerRange (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iEncoderPosition : DINT;
		iEncoderResolution : INT;
	END_VAR
	VAR_OUTPUT
		oEncoderPosition : DINT;
	END_VAR
	VAR
		encValuePreviousCycle : DINT;
		encValueCurrentCycle : DINT;
		myCounter : UDINT;
		diffEncValue : DINT;
	END_VAR
END_FUNCTION_BLOCK
(*TR1G*)

FUNCTION encOverUnderFlow : DINT
	VAR_INPUT
		diDiffEncoder : DINT;
		diResolution : DINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK Rehanger
	VAR_INPUT
		Mode : brdk_um_mode_typ;
		State : brdk_em_states_typ;
		EncSup : Encoder;
		SetRehang : Setting_Rehanger;
		xS01_Hook : BOOL;
		xS01 : BOOL;
		xS02 : BOOL;
		xS03 : BOOL;
		xS04 : BOOL;
		xS05 : BOOL;
		xS06 : BOOL;
		xS07 : BOOL;
		SetCarDisc : Settings_CarControl;
	END_VAR
	VAR_OUTPUT
		xKicker1OUT : BOOL;
		xKicker2OUT : BOOL;
		xKicker1IN : BOOL;
		xKicker2IN : BOOL;
		xSpdFrictionDisc : FricDisc;
		xSperPdsOpen : BOOL;
		xSperPdsClose : BOOL;
		xSperMxOpen : BOOL;
		xSperMxClose : BOOL;
		xInitResetStateReady : BOOL;
	END_VAR
	VAR
		HookTable : hookTable;
		rOpenCorrection : REAL;
		rCloseCorrection : REAL;
		udiProdMin : UDINT;
		udiProdMax : UDINT;
		udiPdsMin : UDINT;
		udiPdsMax : UDINT;
		udiKick1Out : UDINT;
		udiKick2Out : UDINT;
		udiKick1In : UDINT;
		udiKick2In : UDINT;
		xResetTable : BOOL;
		KickTiming : KickersTimings;
		udiHookTabKick1Out : UDINT;
		udiHookTabKick2Out : UDINT;
		udiHookTabKick1In : UDINT;
		udiHookTabKick2In : UDINT;
		xInitCarDone : BOOL;
		xDetNulCarPds : BOOL;
		xDetNulCarMx : BOOL;
		xDetCarPds : BOOL;
		xDetCarMx : BOOL;
		xInitRound : BOOL;
		xInitRoundPds : BOOL;
		xInitRoundMx : BOOL;
		xFeDetNulCarPds : R_TRIG;
		xFeDetNulCarMx : R_TRIG;
		uiNrCarPres : UINT;
		usiNrCarPdsMx : USINT;
		usiNrCarMxPds : USINT;
		usiInitCarCount : CTU;
		usiCarCountPdsMx : CTUD;
		usiCarCountMxPds : CTUD;
		timWaitPdsBeforeOpen : TIME;
		xTpS01 : TP;
		xFeWaitPdsS01 : F_TRIG;
		timWaitPdsOpen : TIME;
		xTpSperPdsOpen : TP;
		xEnaFricDisc : BOOL := TRUE;
		xSlotMxFull : BOOL;
		xReleaseCarPds : BOOL;
		xTpSperPdsClose : TP;
		timWaitMxBeforeOpen : TIME;
		tonGateMxWaitOpen : TON;
		xTpGateMxOpen : TP;
		xReleaseCarMx : BOOL;
		xFeS01 : F_TRIG;
		SlotPdsFull : BOOL;
		xTPgateMxClose : TP;
		ToHmi : CarToHMI;
		iResetState : INT;
		ResetState : STRING[80];
		xTpInitTimer : TP;
		xEnaFricDiscInit : BOOL;
		rFricDiscSpeed : REAL;
		xEnaRunByState : BOOL;
		xResetInitKickers : BOOL;
		xInitResetSpers : BOOL;
		xReDetNulCarPds : R_TRIG;
		xReDetNulCarMx : R_TRIG;
		SperPDS : SperPDS;
		SperMX : SperMX;
		xPdsMxFull : BOOL;
		xMxPdsFull : BOOL;
		xFeInit : F_TRIG;
		xInitSperStart : BOOL;
		xSelectDetCarMx : BOOL;
		myOwnstate : BOOL;
		myEnaKick : BOOL;
		xHookTableCarGo : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK Rehanger_Old
	VAR_INPUT
		xEnable : BOOL;
		Mode : PackMLModes;
		State : PackMLStates;
		iEncoder : Encoder;
		Set : Setting_Rehanger;
		iSensHook : BOOL;
		iProd : BOOL;
		iPDSsignal : BOOL;
		iCarrierGo : BOOL;
		itableMaxSize : USINT;
	END_VAR
	VAR_OUTPUT
		oMode : BOOL;
		oState : BOOL;
		oStateComplete : BOOL;
		oKicker1OUT : BOOL;
		oKicker1IN : BOOL;
		oKicker2OUT : BOOL;
		oKicker2IN : BOOL;
		pdsTable : HookTable;
		sendKickersOut : BOOL;
		sendKickersIn : BOOL;
		oCarrierGo : BOOL;
	END_VAR
	VAR
		Kickers_Timing : KickersTimings;
		RE_checkInSens : R_TRIG;
		PDSHookTable : {REDUND_UNREPLICABLE} HookTable;
		ActiveHook : INT;
		ActiveHookPDS : INT;
		RE_checkInPDSPuls : R_TRIG;
		iLoop : SINT;
		offProdDet : DINT;
		myResetTable : BOOL;
		RE_checkInProdPuls : R_TRIG;
		myRejectCounter : USINT;
		sendKicker1Out : BOOL;
		sendKicker2Out : BOOL;
		sendKicker1In : BOOL;
		sendKicker2In : BOOL;
		openCorrection : REAL;
		closeCorrection : REAL;
		arrSizeCalc : UDINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK ProductRejector (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		xEnable : BOOL;
		Mode : DINT;
		State : brdk_em_states_typ;
		iEncoder : Encoder;
		Settings : RejectorSettings;
		iSensorLeft : BOOL;
		iSensorRight : BOOL;
		iResetTable : BOOL;
	END_VAR
	VAR_OUTPUT
		oMode : BOOL;
		oState : BOOL;
		oStateComplete : {REDUND_UNREPLICABLE} BOOL;
		oRejectorOUT : BOOL;
		oRejectorIN : BOOL;
		oTable : ARRAY[0..49] OF RejectedData;
	END_VAR
	VAR
		ReSensRight : R_TRIG;
		ReSensLeft : R_TRIG;
		rejectProduct : BOOL;
		detWindowEna : BOOL;
		detWindowEncPos : UDINT;
		Tp_Cyl_Out : TP;
		Tp_Cyl_In : TP;
		pulsStartDelay : REAL;
		pulsRejectWindow : REAL;
		pulsDetWindow : REAL;
		openCorrection : REAL;
		closeCorrection : REAL;
		RejectTable : ARRAY[0..49] OF RejectedData;
		i : UINT;
		RE_resetMem : R_TRIG;
		iResetLoop : INT;
		arrTableSize : INT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK SperPDS
	VAR_INPUT
		xInitRound : BOOL;
		Enc : Encoder;
		xDetHook : BOOL;
		rSpdRpmFricDisc : REAL;
		Set : Settings_CarControl;
		xAfterSlotFull : BOOL;
		xReqRelCar : BOOL;
	END_VAR
	VAR_OUTPUT
		xSperOpen : BOOL;
		xSperClose : BOOL;
	END_VAR
	VAR
		timWaitBeforeOpen : TIME;
		xTpWaitTime : TP;
		xFeWait : F_TRIG;
		timWaitOpen : TIME;
		xTpSperOpen : TP;
		xTpSperClose : TP;
		xReleasesper : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK SperMX
	VAR_INPUT
		xDetHook : BOOL;
		xCarDet : BOOL;
		xInitCarDet : BOOL;
		rSpdRpmFricDisc : REAL;
		Set : Settings_CarControl;
		xAfterSlotFull : BOOL;
		xInitRound : BOOL;
	END_VAR
	VAR_OUTPUT
		xSperOpen : BOOL;
		xSperClose : BOOL;
	END_VAR
	VAR
		timWaitBeforeOpen : TIME;
		xTpWaitTime : TP;
		xFeWaitTime : F_TRIG;
		timWaitOpen : TIME;
		xTpSperOpen : TP;
		xTpSperClose : TP;
		xFeMxPdsFull : F_TRIG;
		xReleaseSper : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK CarrierControl
	VAR_INPUT
		xEnable : BOOL;
		iEnaFricDisc : BOOL;
		Settings : Settings_CarControl := (PdsSlotTiming:=(CylActivationTime:=T#200ms),MxSlotTiming:=(CylActivationTime:=T#200ms));
		Mode : PackMLModes;
		State : PackMLStates;
		iCarDetPds : BOOL;
		iCarNulDetPds : BOOL;
		iEncPds : Encoder;
		iCarDetMx : BOOL;
		iCarNulDetMx : BOOL;
		iEncMx : Encoder;
		iRehangerGoCarrier : BOOL;
		iDetCarAfterMx : BOOL;
		iHookDetect : BOOL;
		iSpdFrictionDisc : ARRAY[0..2] OF INT;
	END_VAR
	VAR_OUTPUT
		oMode : BOOL;
		oState : BOOL;
		oSpdFrictionDisc : FricDisc;
		oStateComplete : {REDUND_UNREPLICABLE} BOOL;
		oSperPdsOpen : BOOL;
		oSperMxOpen : BOOL;
		oSperPdsClose : BOOL;
		oSperMxClose : BOOL;
		oSlotMxFull : BOOL;
		oSlotPdsReady : BOOL;
		oToHmi : CarToHMI;
		oCarrierGo : BOOL;
	END_VAR
	VAR
		totalNrOfCarriers : USINT;
		initCarDone : BOOL;
		initCarrierDetectedNul : BOOL;
		Fe_CarDetPds : F_TRIG;
		Re_CarDetPds : R_TRIG;
		Re_CarDetMx : R_TRIG;
		Fe_CarNulDetPds : F_TRIG;
		Fe_CarNulDetMx : F_TRIG;
		nrCarriersPdsGate : USINT;
		nrCarriersMxGate : USINT;
		releaseCarMx : BOOL;
		gateMxOpen_TP : TP;
		gateMxClose_TP : TP;
		sperMxOpen : BOOL;
		tonGateMxWaitClose : TON;
		tonGateMxWaitOpen : TON;
		runCarMX : BOOL;
		initCountRound : BOOL;
		tWaitMxBeforeOpen : TIME := T#25ms;
		tWaitMxOpen : TIME := T#25ms;
		carMxLeft : BOOL;
		releaseCarPds : BOOL;
		waitForHook : TIME := T#20ms;
		ton_HookDetec : TON;
		re_S01 : R_TRIG;
		fe_S01 : F_TRIG;
		tpSperPdsOpen : TP;
		tpSperPdsClose : TP;
		carBetweenPdsMx : SINT;
		carBetweenMxPds : USINT;
		SlotPdsFull : BOOL;
		myTime : TIME := T#300ms;
		myWaitS01 : TIME;
		tp_S01 : TP;
		feWaitPdsS01 : F_TRIG;
		nrCarPdsMx : CTUD;
		Fe_CarDetMx : F_TRIG;
		nrCarMxPds : CTUD;
		myTEST : REAL;
		tWaitPdsBeforeOpen : TIME;
		tWaitPdsOpen : TIME;
		myDis : REAL := 0.025;
		myDis1 : REAL := 0.03;
		myWait2 : TIME;
		myPDScount : INT;
		myMXcount : INT;
	END_VAR
END_FUNCTION_BLOCK
(*HMI_MMI*)

FUNCTION_BLOCK MMI_PilotLight (*Marel PilotLight (5colors)*)
	VAR_INPUT
		Color : DINT;
	END_VAR
	VAR_OUTPUT
		Wire : MMI_PilotLight_Wires;
	END_VAR
END_FUNCTION_BLOCK
(*IO_Logging*)

FUNCTION_BLOCK digitalSignalDataCollector (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		i_xSignal : BOOL;
		i_xSignalInvert : BOOL;
		i_xForceHigh : BOOL;
		i_xForceLow : BOOL;
		i_ResetLogValues : BOOL;
	END_VAR
	VAR_OUTPUT
		o_xSignal : BOOL;
		o_xSignal_FE : BOOL;
		o_xSignal_RE : BOOL;
		outSignalData : SignalData;
	END_VAR
	VAR
		enableForcing : BOOL;
		FE_xSignal : f_trig;
		RE_xSignal : r_trig;
		cpuTimeStamp : DTGetTime;
		cpuTimeMS : TIME;
		i : INT;
		internSignalSignal : BOOL;
		cfg_IN_or_OUT : BOOL; (*Configuration. Default (false) = Input. "HIGH" = Output*)
		dataArraySize : INT;
	END_VAR
END_FUNCTION_BLOCK
(*Settings and Conversions*)

FUNCTION_BLOCK SetAndConvert
	VAR_INPUT
		iPdsEnc : Encoder;
		iMxEnc : Encoder;
		iSettingsRehanger : Setting_Rehanger;
		iSettingsCarrierDisk : Settings_CarControl;
		iSettingsRejector : RejectorSettings;
	END_VAR
	VAR_OUTPUT
		oSettingsRehanger : Setting_Rehanger;
		oSettingsCarrierDisk : Settings_CarControl;
		oSettingsRejector : RejectorSettings;
	END_VAR
END_FUNCTION_BLOCK
(*Functions*)

FUNCTION calcCircumference : REAL
	VAR_INPUT
		radius : REAL;
	END_VAR
END_FUNCTION

FUNCTION RPM_to_MoviGearSpd : INT
	VAR_INPUT
		RPM : REAL;
		maxCtrlLvl : UINT;
		minCtrlLvl : UINT;
		maxRPM : REAL;
		minRPM : REAL;
		reverse : BOOL;
	END_VAR
	VAR
		rRequestSpeed : REAL;
	END_VAR
END_FUNCTION

FUNCTION RPM_TO_TimeDistance : TIME
	VAR_INPUT
		RPM : REAL;
		radius : REAL;
		Distance : REAL;
	END_VAR
	VAR
		rpmMsec : REAL;
	END_VAR
END_FUNCTION

FUNCTION RPM_TO_M_MSec : REAL
	VAR_INPUT
		RPM : REAL;
		radius : REAL;
	END_VAR
	VAR
		circumference : REAL;
	END_VAR
END_FUNCTION

FUNCTION synchro_MS_To_RPM_TO_TIME : TIME
	VAR_INPUT
		meterSecond : REAL;
		radiusSynchroCar : REAL;
		radiusSynchro : REAL;
		distance : REAL;
		minTime : TIME;
	END_VAR
	VAR
		circumference : REAL;
		waitTime : TIME;
		rpmSpeed : REAL;
	END_VAR
END_FUNCTION

FUNCTION CylActTime_Dis : TIME
	VAR_INPUT
		distance : REAL; (*distance in [m]*)
		RPM : REAL;
		radius : REAL;
		minTime : TIME;
	END_VAR
	VAR
		waitTime : TIME;
	END_VAR
END_FUNCTION

FUNCTION MS_TO_RPM : INT
	VAR_INPUT
		meterSecond : REAL;
		radius : REAL;
	END_VAR
	VAR
		circumference : REAL;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK hookTable
	VAR_INPUT
		EncPos : DINT;
		EncPosResolution : DINT;
		xAddNewRow : BOOL;
		xProdDet : BOOL;
		xPdsSig : BOOL;
		udiProdMin : UDINT;
		udiProdMax : UDINT;
		udiPdsMin : UDINT;
		udiPdsMax : UDINT;
		udiKick1In : UDINT;
		udiKick1Out : UDINT;
		udiKick2In : UDINT;
		udiKick2Out : UDINT;
		xResetTable : BOOL;
	END_VAR
	VAR_OUTPUT
		audiKickerPos : KickersPositions;
		xCarGo : BOOL;
		myTable : ARRAY[0..99] OF hookValues;
	END_VAR
	VAR
		xReAddNewRow : R_TRIG;
		HookTab : ARRAY[0..99] OF hookValues;
		siIndexTab : SINT;
		udiHookNr : UDINT;
		udiProdDet : UDINT;
		usiProdType : USINT;
		udiPdsDet : UDINT;
		usiPdsVal : USINT;
		udiCarGo : UDINT;
		xRejProd : BOOL;
		usiArrSize : USINT;
		usiLoop : USINT;
		usiIndexProdDet : USINT;
		usiIndexPdsSigDet : USINT;
		usiIndexKickOut : USINT;
		usiIndexKickIn : USINT;
		xReProdDet : R_TRIG;
		xRePdsSig : R_TRIG;
		usiResetTabLoop : USINT;
		myShow1 : UDINT;
		myShow2 : UDINT;
		myWindowCarGoStart : UDINT := 150;
		myWindowCarGoStop : UDINT := 150;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderExtender
	VAR_INPUT
		diEncVal : DINT;
		diEncResolution : DINT;
	END_VAR
	VAR_OUTPUT
		udiEncoder : UDINT;
	END_VAR
	VAR
		diCurrEncVal : DINT;
		diDiffEncVal : DINT;
		diOldEncVal : DINT;
	END_VAR
END_FUNCTION_BLOCK
