FUNCTION_BLOCK controlCPX_FB40
	//Convert booleans into controlwords
	
	usiValve_1To4.0 := xValve1_A;
	usiValve_1To4.1 := xValve1_B;
	usiValve_1To4.2 := xValve2_A;
	usiValve_1To4.3 := xValve2_B;
	usiValve_1To4.4 := xValve3_A;
	usiValve_1To4.5 := xValve3_B;
	usiValve_1To4.6 := xValve4_A;
	usiValve_1To4.7 := xValve4_B;
	
	usiValve_5To8.0 := xValve5_A;
	usiValve_5To8.1 := xValve5_B;
	usiValve_5To8.2 := xValve6_A;
	usiValve_5To8.3 := xValve6_B;
	usiValve_5To8.4 := xValve7_A;
	usiValve_5To8.5 := xValve7_B;
	usiValve_5To8.6 := xValve8_A;
	usiValve_5To8.7 := xValve8_B;
	
END_FUNCTION_BLOCK	
	
FUNCTION_BLOCK EncoderToLargerRange
	//STILL TODO what if end of larger range is detected!
	encValueCurrentCycle := iEncoderPosition;
		
	//make intern encoder with larger range based on incoder input 
	IF encValuePreviousCycle <> encValueCurrentCycle THEN
		diffEncValue := (encValueCurrentCycle - encValuePreviousCycle);
		IF diffEncValue < (-0.5 * iEncoderResolution ) THEN diffEncValue := diffEncValue + iEncoderResolution; END_IF;
		myCounter := myCounter + diffEncValue;
	END_IF;
		
	//POST
	oEncoderPosition := myCounter;
	encValuePreviousCycle := encValueCurrentCycle;
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderExtender
	diCurrEncVal 	:= diEncVal;
	
	IF diCurrEncVal <> diOldEncVal THEN
		diDiffEncVal	:= encOverUnderFlow( diCurrEncVal- diOldEncVal, diEncResolution);
		diOldEncVal		:= diCurrEncVal;
	ELSE
		diDiffEncVal 	:= 0;
	END_IF
	
	udiEncoder := udiEncoder + diDiffEncVal;
		
END_FUNCTION_BLOCK


FUNCTION encOverUnderFlow
	IF 	diDiffEncoder >  diResolution / 3 THEN 
		diDiffEncoder := diDiffEncoder - diResolution; 
	END_IF
	IF 	diDiffEncoder < -diResolution / 3 THEN 
		diDiffEncoder := diDiffEncoder + diResolution; 
	END_IF
	
	encOverUnderFlow := diDiffEncoder;
END_FUNCTION  


(* Speed measurements and Speed calculations *)
FUNCTION_BLOCK CalculationsAndMeasurements
	
	//Extract data from ENCODER DATA
	iEncoderPDSNT 	:= (SHR(iEncoderDataPDS, 2) AND 16380);
	iEncoderMX 		:= (SHR(iEncoderDataMX,  2) AND 16380);
	iEncoderPDSNTrev:=  SHR(iEncoderDataPDS, 16);
	iEncoderMXrev	:=  SHR(iEncoderDataMX,  16);
	
	
	//Encoder position to Larger range.
	EncExtenderPDS(iEncoderPosition:= iEncoderPDSNT, iEncoderResolution:= iEncPDSNTResolution);
	EncExtenderMX(iEncoderPosition:= iEncoderMX, iEncoderResolution:= iEncMXResolution);
	udiEncExtPds(diEncVal := iEncoderPDSNT, diEncResolution := iEncPDSNTResolution);
	udiEncExtMx(diEncVal := iEncoderMX, diEncResolution := iEncMXResolution);
	
	
	//combine settings etc, into one struct.
	oEncoderPDSNT.Circumference 	:= iEncPDSNTCircumference;
	oEncoderPDSNT.EncoderPosition 	:= EncExtenderPDS.oEncoderPosition;
	//oEncoderPDSNT.EncoderPosition := udiEncExtPds.udiEncoder;
	oEncoderPDSNT.PulsesRevolution 	:= iEncPDSNTResolution;
	oEncoderPDSNT.PulsesMeter 		:= oEncoderPDSNT.PulsesRevolution / oEncoderPDSNT.Circumference;
	
	oEncoderMX.Circumference 		:= iEncMXCircumference; 
	oEncoderMX.EncoderPosition 		:= EncExtenderMX.oEncoderPosition;
	//oEncoderMX.EncoderPosition 	:= udiEncExtMx.udiEncoder;
	oEncoderMX.PulsesRevolution 	:= iEncMXResolution;
	oEncoderMX.PulsesMeter 			:= oEncoderMX.PulsesRevolution / oEncoderMX.Circumference;
	
	//Avering encoder revolutions
	//movingAveragePds(x:= REAL_TO_INT(ABS(iEncoderPDSNTrev)));
	//movingAverageMx( x:= REAL_TO_INT(ABS(iEncoderMXrev)));	
	
	AveragePds(In:= iEncoderPDSNTrev);	//first Order filter
	AverageMx( In:= iEncoderMXrev);		//first Order filter
	
	//Set average Revolutions to output
	//oEncoderPDSNT.RPM 		:= movingAveragePds.y;
	oEncoderPDSNT.RPM 	:= ABS(AveragePds.Out);
	//oEncoderMX.RPM			:= movingAverageMx.y;
	oEncoderMX.RPM 		:= ABS(AverageMx.Out);
	
	
	//calculate hookspeed (RPM * NumberOfHooks)
	oPDSHookSpeed := oEncoderPDSNT.RPM * iPdsNrOfHooks;
	oMXHookSpeed  := oEncoderMX.RPM * iMxNrOfHooks;
	
	//calculate total line speed / Hour
	oLineSpeedPDSNT := REAL_TO_INT(oPDSHookSpeed * 60.0);
	oLineSpeedMX 	:= REAL_TO_INT(oMXHookSpeed * 60.0);
	
	//output if line is running
	xSupLineRun := oLineSpeedPDSNT 	> 0;
	xDisLineRun	:= oLineSpeedMX		> 0;
	
	//Moving Average
	//movAvgPDSNTms(x:= ((oEncoderPDSNT.RPM * oEncoderPDSNT.Circumference) / 60)*100);		//Moving AVG
	//movAvgMSms(x:= ((oEncoderMX.RPM * oEncoderMX.Circumference) / 60)*100);					//Moving AVG
	rAvgPdsNTms(In:= (oEncoderPDSNT.RPM * oEncoderPDSNT.Circumference) / 60.0);				//First Order
	rAvgMXms(In:= (oEncoderMX.RPM * oEncoderMX.Circumference) / 60.0);								//First Order
	
	
	//oEncoderPDSNT.MeterSec := movAvgPDSNTms.y / 100;
	//oEncoderMX.MeterSec :=  movAvgMSms.y / 100;
	oEncoderPDSNT.MeterSec 	:= ABS(rAvgPdsNTms.Out);
	oEncoderMX.MeterSec 	:= ABS(rAvgMXms.Out);
	
	reSensHookPDS(CLK:= iSensHookPDS);
	IF reSensHookPDS.Q THEN
		oSpeedPDSline := (60.0 * 60.0 * 1000.0) / TIME_TO_REAL(clock_ms() - prevTimePdsHook);
		prevTimePdsHook := clock_ms();
	END_IF
	
	reSensHookMx(CLK:= iSensHookMX);
	IF reSensHookMx.Q THEN
		oSpeedMxLine := (60.0 * 60.0 * 1000.0) / TIME_TO_REAL(clock_ms() - prevTimeMxHook);
		prevTimeMxHook := clock_ms();
	END_IF
	
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK movingAverageFilterEncoder
	RE_encoderVal(CLK:= trigger);
		
	IF RE_encoderVal.Q THEN
		
		//calculate distance between last 2 shakles
		lastTriggerVal := actTriggerVal;
		actTriggerVal := iEnc.EncoderPosition;
		oDisLastShakle := actTriggerVal - lastTriggerVal;
		
		myMovingAverage(x:= DINT_TO_INT(oDisLastShakle));
		oAvg := myMovingAverage.y;
		
	END_IF;

	
END_FUNCTION_BLOCK

FUNCTION_BLOCK SetAndConvert
	
	//convert settings from modules from SI -> encoderPulses
	
	//Rehanger
	oSettingsRehanger := iSettingsRehanger;
	oSettingsRehanger.disProdDetection		:= iSettingsRehanger.disProdDetection		* iPdsEnc.PulsesMeter;
	oSettingsRehanger.disPdsSigDetection	:= iSettingsRehanger.disPdsSigDetection		* iPdsEnc.PulsesMeter;
	oSettingsRehanger.disCarrierGo			:= iSettingsRehanger.disCarrierGo			* iPdsEnc.PulsesMeter;
	oSettingsRehanger.ProdDetectionWindow	:= iSettingsRehanger.ProdDetectionWindow	* iPdsEnc.PulsesMeter;
	oSettingsRehanger.PdsSigWindow			:= iSettingsRehanger.PdsSigWindow			* iPdsEnc.PulsesMeter;
	
	//window is depending on the update rate of the encoder as well as the (resolution & total nr of hooks)
	oSettingsRehanger.KickerSettings.disKickActWindow	:= iSettingsRehanger.KickerSettings.disKickActWindow * iPdsEnc.PulsesMeter;
	oSettingsRehanger.KickerSettings.disKicker1In		:= iSettingsRehanger.KickerSettings.disKicker1In	 * iPdsEnc.PulsesMeter;
	oSettingsRehanger.KickerSettings.disKicker1Out		:= iSettingsRehanger.KickerSettings.disKicker1Out	 * iPdsEnc.PulsesMeter;
	oSettingsRehanger.KickerSettings.disKicker2In		:= iSettingsRehanger.KickerSettings.disKicker2In	 * iPdsEnc.PulsesMeter;
	oSettingsRehanger.KickerSettings.disKicker2Out		:= iSettingsRehanger.KickerSettings.disKicker2Out	 * iPdsEnc.PulsesMeter;
	//oSettingsRehanger.KickerSettings.CylTransitionTime 
	//oSettingsRehanger.KickerSettings.ValveResponceTime_Open
	//oSettingsRehanger.KickerSettings.ValveResponceTime_Close
	oSettingsRehanger.KickerSettings.AirHoseTimeDelay := REAL_TO_TIME(oSettingsRehanger.KickerSettings.LengthAirHose * TIME_TO_REAL(TimeDelayAirHose));
	
	
	
	//Carriers / FrictionDisk
	oSettingsCarrierDisk := iSettingsCarrierDisk;
	oSettingsCarrierDisk.MoviGear.rMinRPM		:= 11.5;
	oSettingsCarrierDisk.MoviGear.rMaxRPM		:= 115.4;
	oSettingsCarrierDisk.MoviGear.uiMinCtrlLvl	:= 1000;
	oSettingsCarrierDisk.MoviGear.uiMaxCtrlLvl	:= 10000;
	oSettingsCarrierDisk.MoviGear.iRamp			:= 2500;
	
	
	//Rejector
	oSettingsRejector := iSettingsRejector;
	oSettingsRejector.RejectWindow	:= iSettingsRejector.RejectWindow	* iMxEnc.PulsesMeter;
	oSettingsRejector.SensDetWindow	:= iSettingsRejector.SensDetWindow	* iMxEnc.PulsesMeter;
	oSettingsRejector.StartDelay	:= iSettingsRejector.StartDelay		* iMxEnc.PulsesMeter;
	oSettingsRejector.CylResponceTime_Open := iSettingsRejector.CylResponceTime_Open;
	oSettingsRejector.CylResponceTime_Close := oSettingsRejector.CylResponceTime_Close;
	
	

END_FUNCTION_BLOCK

