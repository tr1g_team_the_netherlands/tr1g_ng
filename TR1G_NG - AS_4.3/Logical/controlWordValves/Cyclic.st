
PROGRAM _CYCLIC
	(* Insert code here *)
	
	usiPneumatic_0.0 := xActuatorSystemAirOff;
	usiPneumatic_0.1 := xActuatorSystemAirOn;
	usiPneumatic_0.2 := xActuatorPdsSperExtended;
	usiPneumatic_0.3 := xActuatorPdsSperRetracted;
	usiPneumatic_0.4 := xActuatorMxSperExtended OR mySperMxOpen;
	usiPneumatic_0.5 := xActuatorMxSperRetracted OR mySperMxClosed;
	usiPneumatic_0.6 := xActuatorKicker1extended;
	usiPneumatic_0.7 := xActuatorKicker1retracted;
	
	usiPneumatic_1.0 := xActuatorKicker2extended;
	usiPneumatic_1.1 := xActuatorKicker2retracted;
	usiPneumatic_1.2 := xActuatorLegRejectorExtended;
	usiPneumatic_1.3 := xActuatorLegRejectorRetracted;
	usiPneumatic_1.4 := FALSE;
	usiPneumatic_1.5 := FALSE;
	usiPneumatic_1.6 := FALSE;
	usiPneumatic_1.7 := FALSE;
	
	dwTeller := dwTeller + 1;
	
	 
END_PROGRAM
