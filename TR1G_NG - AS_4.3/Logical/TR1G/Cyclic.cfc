PROGRAM _CYCLIC
(* GRAPHIC_INFORMATION_27DEE8E8_593B_45A1_8A15_7A695968B886_BEGIN *)
1CFCFBD-EditorCfcChart5.004.031
CCFCDoc1

CBaseDoc0

CPageFormat100NO426
CProject6

CCfgEditObject1-1
CTRrefdatatypes24
CDataType0DINT18
CDataType0REAL24
CDataType0UINT21
CDataType0BOOL15
CDataType0Encoder40
CDataType0INT16
CDataType0Setting_Rehanger40
CDataType0Settings_CarControl40
CDataType0RejectorSettings40
CDataType0ANY1
CDataType0brdk_em_states_typ40
CDataType0ARRAY [ 0..49] OF RejectedData40
CDataType0PackMLModes40
CDataType0PackMLStates40
CDataType0ARRAY [ 0..2] OF INT40
CDataType0FricDisc40
CDataType0CarToHMI40
CDataType0PackMLModes40
CDataType0PackMLStates40
CDataType0Encoder40
CDataType0Setting_Rehanger40
CDataType0USINT20
CDataType0HookTable40
CDataType0MMI_PilotLight_Wires40
CTRrefblocktypes7
CFirmwareBlockType0

CBlockType4NONONO10YES

CCfgEditObject1CalculationsAndMeasurements-1
CTRinputtype10
CClass2NOYES0NODINTNO

CCfgEditObject1iEncoderDataPDS1
CClass2NOYES1NODINTNO

CCfgEditObject1iEncoderDataMX2
CClass2NOYES2NOREALNO

CCfgEditObject1iEncPDSNTCircumference3
CClass2NOYES3NOUINTNO

CCfgEditObject1iEncPDSNTResolution4
CClass2NOYES4NOREALNO

CCfgEditObject1iPdsNrOfHooks5
CClass2NOYES5NOREALNO

CCfgEditObject1iEncMXCircumference6
CClass2NOYES6NOUINTNO

CCfgEditObject1iEncMXResolution7
CClass2NOYES7NOREALNO

CCfgEditObject1iMxNrOfHooks8
CClass2NOYES8NOBOOLNO

CCfgEditObject1iSensHookPDS9
CClass2NOYES9NOBOOLNO

CCfgEditObject1iSensHookMX10
CTRoutputtype10
CClass2NONO0NOEncoderNO

CCfgEditObject1oEncoderPDSNT11
CClass2NONO1NOREALNO

CCfgEditObject1oPDSHookSpeed12
CClass2NONO2NOINTNO

CCfgEditObject1oLineSpeedPDSNT13
CClass2NONO3NOEncoderNO

CCfgEditObject1oEncoderMX14
CClass2NONO4NOREALNO

CCfgEditObject1oMXHookSpeed15
CClass2NONO5NOINTNO

CCfgEditObject1oLineSpeedMX16
CClass2NONO6NOREALNO

CCfgEditObject1oSpeedMxLine17
CClass2NONO7NOREALNO

CCfgEditObject1oSpeedPDSline18
CClass2NONO8NOBOOLNO

CCfgEditObject1xSupLineRun19
CClass2NONO9NOBOOLNO

CCfgEditObject1xDisLineRun20
CTRparamtype0
CFirmwareBlockType0

CBlockType4NONONO10YES

CCfgEditObject1SetAndConvert-1
CTRinputtype5
CClass2NOYES0NOEncoderNO

CCfgEditObject1iPdsEnc1
CClass2NOYES1NOEncoderNO

CCfgEditObject1iMxEnc2
CClass2NOYES2NOSetting_RehangerNO

CCfgEditObject1iSettingsRehanger3
CClass2NOYES3NOSettings_CarControlNO

CCfgEditObject1iSettingsCarrierDisk4
CClass2NOYES4NORejectorSettingsNO

CCfgEditObject1iSettingsRejector5
CTRoutputtype3
CClass2NONO0NOSetting_RehangerNO

CCfgEditObject1oSettingsRehanger6
CClass2NONO1NOSettings_CarControlNO

CCfgEditObject1oSettingsCarrierDisk7
CClass2NONO2NORejectorSettingsNO

CCfgEditObject1oSettingsRejector8
CTRparamtype0
CFirmwareBlockType0

CBlockType4NOYESYES20NO

CCfgEditObject1OR-1
CTRinputtype2
CClass2NOYES0NOANYNO

CCfgEditObject1IN11
CClass2NOYES1NOANYNO

CCfgEditObject1IN22
CTRoutputtype1
CClass2NONO0NOANYNO

CCfgEditObject1Output0
CTRparamtype0
CFirmwareBlockType0

CBlockType4NONONO10YES

CCfgEditObject1ProductRejector-1
CTRinputtype8
CClass2NOYES0NOBOOLNO

CCfgEditObject1xEnable1
CClass2NOYES1NODINTNO

CCfgEditObject1Mode2
CClass2NOYES2NObrdk_em_states_typNO

CCfgEditObject1State3
CClass2NOYES3NOEncoderNO

CCfgEditObject1iEncoder4
CClass2NOYES4NORejectorSettingsNO

CCfgEditObject1Settings5
CClass2NOYES5NOBOOLNO

CCfgEditObject1iSensorLeft6
CClass2NOYES6NOBOOLNO

CCfgEditObject1iSensorRight7
CClass2NOYES7NOBOOLNO

CCfgEditObject1iResetTable8
CTRoutputtype6
CClass2NONO0NOBOOLNO

CCfgEditObject1oMode9
CClass2NONO1NOBOOLNO

CCfgEditObject1oState10
CClass2NONO2NOBOOLNO

CCfgEditObject1oStateComplete11
CClass2NONO3NOBOOLNO

CCfgEditObject1oRejectorOUT12
CClass2NONO4NOBOOLNO

CCfgEditObject1oRejectorIN13
CClass2NONO5NOARRAY [ 0..49] OF RejectedDataNO

CCfgEditObject1oTable14
CTRparamtype0
CFirmwareBlockType0

CBlockType4NONONO10YES

CCfgEditObject1CarrierControl-1
CTRinputtype15
CClass2NOYES0NOBOOLNO

CCfgEditObject1xEnable1
CClass2NOYES1NOBOOLNO

CCfgEditObject1iEnaFricDisc2
CClass2NOYES2NOSettings_CarControlNO

CCfgEditObject1Settings3
CClass2NOYES3NOPackMLModesNO

CCfgEditObject1Mode4
CClass2NOYES4NOPackMLStatesNO

CCfgEditObject1State5
CClass2NOYES5NOBOOLNO

CCfgEditObject1iCarDetPds6
CClass2NOYES6NOBOOLNO

CCfgEditObject1iCarNulDetPds7
CClass2NOYES7NOEncoderNO

CCfgEditObject1iEncPds8
CClass2NOYES8NOBOOLNO

CCfgEditObject1iCarDetMx9
CClass2NOYES9NOBOOLNO

CCfgEditObject1iCarNulDetMx10
CClass2NOYES10NOEncoderNO

CCfgEditObject1iEncMx11
CClass2NOYES11NOBOOLNO

CCfgEditObject1iRehangerGoCarrier12
CClass2NOYES12NOBOOLNO

CCfgEditObject1iDetCarAfterMx13
CClass2NOYES13NOBOOLNO

CCfgEditObject1iHookDetect14
CClass2NOYES14NOARRAY [ 0..2] OF INTNO

CCfgEditObject1iSpdFrictionDisc15
CTRoutputtype12
CClass2NONO0NOBOOLNO

CCfgEditObject1oMode16
CClass2NONO1NOBOOLNO

CCfgEditObject1oState17
CClass2NONO2NOFricDiscNO

CCfgEditObject1oSpdFrictionDisc18
CClass2NONO3NOBOOLNO

CCfgEditObject1oStateComplete19
CClass2NONO4NOBOOLNO

CCfgEditObject1oSperPdsOpen20
CClass2NONO5NOBOOLNO

CCfgEditObject1oSperMxOpen21
CClass2NONO6NOBOOLNO

CCfgEditObject1oSperPdsClose22
CClass2NONO7NOBOOLNO

CCfgEditObject1oSperMxClose23
CClass2NONO8NOBOOLNO

CCfgEditObject1oSlotMxFull24
CClass2NONO9NOBOOLNO

CCfgEditObject1oSlotPdsReady25
CClass2NONO10NOCarToHMINO

CCfgEditObject1oToHmi26
CClass2NONO11NOBOOLNO

CCfgEditObject1oCarrierGo27
CTRparamtype0
CFirmwareBlockType0

CBlockType4NONONO10YES

CCfgEditObject1Rehanger_Old-1
CTRinputtype10
CClass2NOYES0NOBOOLNO

CCfgEditObject1xEnable1
CClass2NOYES1NOPackMLModesNO

CCfgEditObject1Mode2
CClass2NOYES2NOPackMLStatesNO

CCfgEditObject1State3
CClass2NOYES3NOEncoderNO

CCfgEditObject1iEncoder4
CClass2NOYES4NOSetting_RehangerNO

CCfgEditObject1Set5
CClass2NOYES5NOBOOLNO

CCfgEditObject1iSensHook6
CClass2NOYES6NOBOOLNO

CCfgEditObject1iProd7
CClass2NOYES7NOBOOLNO

CCfgEditObject1iPDSsignal8
CClass2NOYES8NOBOOLNO

CCfgEditObject1iCarrierGo9
CClass2NOYES9NOUSINTNO

CCfgEditObject1itableMaxSize10
CTRoutputtype11
CClass2NONO0NOBOOLNO

CCfgEditObject1oMode11
CClass2NONO1NOBOOLNO

CCfgEditObject1oState12
CClass2NONO2NOBOOLNO

CCfgEditObject1oStateComplete13
CClass2NONO3NOBOOLNO

CCfgEditObject1oKicker1OUT14
CClass2NONO4NOBOOLNO

CCfgEditObject1oKicker1IN15
CClass2NONO5NOBOOLNO

CCfgEditObject1oKicker2OUT16
CClass2NONO6NOBOOLNO

CCfgEditObject1oKicker2IN17
CClass2NONO7NOHookTableNO

CCfgEditObject1pdsTable18
CClass2NONO8NOBOOLNO

CCfgEditObject1sendKickersOut19
CClass2NONO9NOBOOLNO

CCfgEditObject1sendKickersIn20
CClass2NONO10NOBOOLNO

CCfgEditObject1oCarrierGo21
CTRparamtype0
CFirmwareBlockType0

CBlockType4NONONO10YES

CCfgEditObject1MMI_PilotLight-1
CTRinputtype1
CClass2NOYES0NODINTNO

CCfgEditObject1Color1
CTRoutputtype1
CClass2NONO0NOMMI_PilotLight_WiresNO

CCfgEditObject1Wire2
CTRparamtype0
CPageFormat100NO426
CExtend_IEC_Info00
CFunctionChart40NO

CCfgEditObject1-1
CPageFormat100NO426
CTRblocks1
6Top Level Chart
CFreeCompoundBlockInterfaceType0

CBlockType4NONONO30YES

CCfgEditObject1-1
CTRinputtype0
CTRoutputtype0
CTRparamtype0
CFreeCompoundBlock1NO

CUserBlock0

COrganisationBlock0

CBlock7000111NO

CCfgEditObject1Top Level ChartTop Level ChartTop Level Chart10
CTRoutputs0
CTRinputs0
CTRparams0
CFunctionChart41NO

CCfgEditObject1-1
CPageFormat100NO426
CTRblocks8
7
CTextBlock0

CBlock710082NO

CCfgEditObject1(* TR1G NG MAIN PROGRAM *)110
CTRoutputs0
CTRinputs0
CTRparams0
0CalculationsAndMeasurementsCalculationsAndMeasurements
CFirmwareBlock0

CBlock72461161NO

CCfgEditObject1CalculationsAndMeasurements2CalculationsAndMeasurements15
CTRoutputs10
11
CConnector4YESNO-1NO

CCfgEditObject1oEncoderPDSNToEncoderPDSNT11
CValue1
CValueItem1EncPdsNt40NO
CTRconnections0
12
CConnector4YESNO-1NO

CCfgEditObject1oPDSHookSpeedoPDSHookSpeed12
CValue1
CValueItem1hookSpeedPds24NO
CTRconnections0
13
CConnector4YESNO-1NO

CCfgEditObject1oLineSpeedPDSNToLineSpeedPDSNT13
CValue1
CValueItem1myPdsSpeed216NO
CTRconnections0
14
CConnector4YESNO-1NO

CCfgEditObject1oEncoderMXoEncoderMX14
CValue1
CValueItem1EncMX40NO
CTRconnections0
15
CConnector4YESNO-1NO

CCfgEditObject1oMXHookSpeedoMXHookSpeed15
CValue1
CValueItem1hookSpeedMx24NO
CTRconnections0
16
CConnector4YESNO-1NO

CCfgEditObject1oLineSpeedMXoLineSpeedMX16
CValue1
CValueItem1myMxSpeed216NO
CTRconnections0
17
CConnector4YESNO-1NO

CCfgEditObject1oSpeedMxLineoSpeedMxLine17
CValue1
CValueItem1MyMXspeed24NO
CTRconnections0
18
CConnector4YESNO-1NO

CCfgEditObject1oSpeedPDSlineoSpeedPDSline18
CValue1
CValueItem1MyPDSspeed24NO
CTRconnections0
19
CConnector4YESNO-1NO

CCfgEditObject1xSupLineRunxSupLineRun19
CValue1
CValueItem115NO
CTRconnections0
20
CConnector4YESNO-1NO

CCfgEditObject1xDisLineRunxDisLineRun20
CValue1
CValueItem115NO
CTRconnections0
CTRinputs10
1
CConnector4YESNO-1NO

CCfgEditObject1iEncoderDataPDSiEncoderDataPDS1
CValue1
CValueItem1diEncDataPDSNT18NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1iEncoderDataMXiEncoderDataMX2
CValue1
CValueItem1diEncDataMX18NO
CTRconnections0
3
CConnector4YESNO-1NO

CCfgEditObject1iEncPDSNTCircumferenceiEncPDSNTCircumference3
CValue1
CValueItem11.5724NO
CTRconnections0
4
CConnector4YESNO-1NO

CCfgEditObject1iEncPDSNTResolutioniEncPDSNTResolution4
CValue1
CValueItem1myPulses21NO
CTRconnections0
5
CConnector4YESNO-1NO

CCfgEditObject1iPdsNrOfHooksiPdsNrOfHooks5
CValue1
CValueItem1824NO
CTRconnections0
6
CConnector4YESNO-1NO

CCfgEditObject1iEncMXCircumferenceiEncMXCircumference6
CValue1
CValueItem11.4424NO
CTRconnections0
7
CConnector4YESNO-1NO

CCfgEditObject1iEncMXResolutioniEncMXResolution7
CValue1
CValueItem1myPulses21NO
CTRconnections0
8
CConnector4YESNO-1NO

CCfgEditObject1iMxNrOfHooksiMxNrOfHooks8
CValue1
CValueItem1624NO
CTRconnections0
9
CConnector4YESNO-1NO

CCfgEditObject1iSensHookPDSiSensHookPDS9
CValue1
CValueItem1S0115NO
CTRconnections0
10
CConnector4YESNO-1NO

CCfgEditObject1iSensHookMXiSensHookMX10
CValue1
CValueItem1S0215NO
CTRconnections0
CTRparams0
0SetAndConvertSetAndConvert
CFirmwareBlock0

CBlock71519932NO

CCfgEditObject1SetAndConvert1SetAndConvert20
CTRoutputs3
6
CConnector4YESNO-1NO

CCfgEditObject1oSettingsRehangeroSettingsRehanger6
CValue1
CValueItem140NO
CTRconnections1
CTempSimpleAddress054010
7
CConnector4YESNO-1NO

CCfgEditObject1oSettingsCarrierDiskoSettingsCarrierDisk7
CValue1
CValueItem140NO
CTRconnections1
CTempSimpleAddress033510
8
CConnector4YESNO-1NO

CCfgEditObject1oSettingsRejectoroSettingsRejector8
CValue1
CValueItem140NO
CTRconnections1
CTempSimpleAddress053010
CTRinputs5
1
CConnector4YESNO-1NO

CCfgEditObject1iPdsEnciPdsEnc1
CValue1
CValueItem1EncPdsNt40NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1iMxEnciMxEnc2
CValue1
CValueItem1EncMX40NO
CTRconnections0
3
CConnector4YESNO-1NO

CCfgEditObject1iSettingsRehangeriSettingsRehanger3
CValue1
CValueItem1mySet_Rehanger40NO
CTRconnections0
4
CConnector4YESNO-1NO

CCfgEditObject1iSettingsCarrierDiskiSettingsCarrierDisk4
CValue1
CValueItem1mySet_CarControl40NO
CTRconnections0
5
CConnector4YESNO-1NO

CCfgEditObject1iSettingsRejectoriSettingsRejector5
CValue1
CValueItem1mySet_Rejector40NO
CTRconnections0
CTRparams0
0OROR
CFirmwareBlock0

CBlock711023423NO

CCfgEditObject1OR1OR25
CTRoutputs1
0
CConnector4YESNO-1NO

CCfgEditObject1OutputOutput0
CValue1
CValueItem11NO
CTRconnections1
CTempSimpleAddress064010
CTRinputs2
1
CConnector4YESNO-1NO

CCfgEditObject1IN1IN11
CValue1
CValueItem1S011NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1IN2IN22
CValue1
CValueItem1MyHookDetect1NO
CTRconnections0
CTRparams0
0ProductRejectorProductRejector
CFirmwareBlock0

CBlock71284654NO

CCfgEditObject1ProductRejector1ProductRejector30
CTRoutputs6
9
CConnector4YESNO-1NO

CCfgEditObject1oModeoMode9
CValue1
CValueItem115NO
CTRconnections0
10
CConnector4YESNO-1NO

CCfgEditObject1oStateoState10
CValue1
CValueItem115NO
CTRconnections0
11
CConnector4YESNO-1NO

CCfgEditObject1oStateCompleteoStateComplete11
CValue1
CValueItem115NO
CTRconnections0
12
CConnector4YESNO-1NO

CCfgEditObject1oRejectorOUToRejectorOUT12
CValue1
CValueItem1xActuatorLegRejectorExtended15NO
CTRconnections0
13
CConnector4YESNO-1NO

CCfgEditObject1oRejectorINoRejectorIN13
CValue1
CValueItem1xActuatorLegRejectorRetracted15NO
CTRconnections0
14
CConnector4YESNO-1NO

CCfgEditObject1oTableoTable14
CValue1
CValueItem1DisplayRejectTable40NO
CTRconnections0
CTRinputs8
1
CConnector4YESNO-1NO

CCfgEditObject1xEnablexEnable1
CValue1
CValueItem1myRejectEnable15NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1ModeMode2
CValue1
CValueItem118NO
CTRconnections0
3
CConnector4YESNO-1NO

CCfgEditObject1StateState3
CValue1
CValueItem140NO
CTRconnections0
4
CConnector4YESNO-1NO

CCfgEditObject1iEncoderiEncoder4
CValue1
CValueItem1EncMX40NO
CTRconnections0
5
CConnector4YESNO-1NO

CCfgEditObject1SettingsSettings5
CValue1
CValueItem140NO
CTRconnections1
CTempSimpleAddress082010
6
CConnector4YESNO-1NO

CCfgEditObject1iSensorLeftiSensorLeft6
CValue1
CValueItem1xSensorLegDetectLeft15NO
CTRconnections0
7
CConnector4YESNO-1NO

CCfgEditObject1iSensorRightiSensorRight7
CValue1
CValueItem1xSensorLegDetectRight15NO
CTRconnections0
8
CConnector4YESNO-1NO

CCfgEditObject1iResetTableiResetTable8
CValue1
CValueItem1myResetProductRejector15NO
CTRconnections0
CTRparams0
0CarrierControlCarrierControl
CFirmwareBlock0

CBlock72289885NO

CCfgEditObject1CarrierControl2CarrierControl35
CTRoutputs12
16
CConnector4YESNO-1NO

CCfgEditObject1oModeoMode16
CValue1
CValueItem115NO
CTRconnections0
17
CConnector4YESNO-1NO

CCfgEditObject1oStateoState17
CValue1
CValueItem115NO
CTRconnections0
18
CConnector4YESNO-1NO

CCfgEditObject1oSpdFrictionDiscoSpdFrictionDisc18
CValue1
CValueItem1FrictionDisc40NO
CTRconnections0
19
CConnector4YESNO-1NO

CCfgEditObject1oStateCompleteoStateComplete19
CValue1
CValueItem115NO
CTRconnections0
20
CConnector4YESNO-1NO

CCfgEditObject1oSperPdsOpenoSperPdsOpen20
CValue1
CValueItem1xActuatorPdsSperRetracted15NO
CTRconnections0
21
CConnector4YESNO-1NO

CCfgEditObject1oSperMxOpenoSperMxOpen21
CValue1
CValueItem1xActuatorMxSperRetracted15NO
CTRconnections0
22
CConnector4YESNO-1NO

CCfgEditObject1oSperPdsCloseoSperPdsClose22
CValue1
CValueItem1xActuatorPdsSperExtended15NO
CTRconnections0
23
CConnector4YESNO-1NO

CCfgEditObject1oSperMxCloseoSperMxClose23
CValue1
CValueItem1xActuatorMxSperExtended15NO
CTRconnections0
24
CConnector4YESNO-1NO

CCfgEditObject1oSlotMxFulloSlotMxFull24
CValue1
CValueItem115NO
CTRconnections0
25
CConnector4YESNO-1NO

CCfgEditObject1oSlotPdsReadyoSlotPdsReady25
CValue1
CValueItem115NO
CTRconnections0
26
CConnector4YESNO-1NO

CCfgEditObject1oToHmioToHmi26
CValue1
CValueItem1CarToHmi40NO
CTRconnections0
27
CConnector4YESNO-1NO

CCfgEditObject1oCarrierGooCarrierGo27
CValue1
CValueItem115NO
CTRconnections0
CTRinputs15
1
CConnector4YESNO-1NO

CCfgEditObject1xEnablexEnable1
CValue1
CValueItem1myCarCtrlEnable15NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1iEnaFricDisciEnaFricDisc2
CValue1
CValueItem1myFricDiscEna15NO
CTRconnections0
3
CConnector4YESNO-1NO

CCfgEditObject1SettingsSettings3
CValue1
CValueItem140NO
CTRconnections1
CTempSimpleAddress072010
4
CConnector4YESNO-1NO

CCfgEditObject1ModeMode4
CValue1
CValueItem140NO
CTRconnections0
5
CConnector4YESNO-1NO

CCfgEditObject1StateState5
CValue1
CValueItem140NO
CTRconnections0
6
CConnector4YESNO-1NO

CCfgEditObject1iCarDetPdsiCarDetPds6
CValue1
CValueItem1S0415NO
CTRconnections0
7
CConnector4YESNO-1NO

CCfgEditObject1iCarNulDetPdsiCarNulDetPds7
CValue1
CValueItem1S0515NO
CTRconnections0
8
CConnector4YESNO-1NO

CCfgEditObject1iEncPdsiEncPds8
CValue1
CValueItem1EncPdsNt40NO
CTRconnections0
9
CConnector4YESNO-1NO

CCfgEditObject1iCarDetMxiCarDetMx9
CValue1
CValueItem1S0615NO
CTRconnections0
10
CConnector4YESNO-1NO

CCfgEditObject1iCarNulDetMxiCarNulDetMx10
CValue1
CValueItem1S0715NO
CTRconnections0
11
CConnector4YESNO-1NO

CCfgEditObject1iEncMxiEncMx11
CValue1
CValueItem1EncMX40NO
CTRconnections0
12
CConnector4YESNO-1NO

CCfgEditObject1iRehangerGoCarrieriRehangerGoCarrier12
CValue1
CValueItem115NO
CTRconnections0
13
CConnector4YESNO-1NO

CCfgEditObject1iDetCarAfterMxiDetCarAfterMx13
CValue1
CValueItem115NO
CTRconnections0
14
CConnector4YESNO-1NO

CCfgEditObject1iHookDetectiHookDetect14
CValue1
CValueItem1S0115NO
CTRconnections0
15
CConnector4YESNO-1NO

CCfgEditObject1iSpdFrictionDisciSpdFrictionDisc15
CValue1
CValueItem1FeedBackFrictionDisck40NO
CTRconnections0
CTRparams0
0Rehanger_OldRehanger_Old
CFirmwareBlock0

CBlock712819766NO

CCfgEditObject1Rehanger_Old1Rehanger_Old40
CTRoutputs11
11
CConnector4YESNO-1NO

CCfgEditObject1oModeoMode11
CValue1
CValueItem115NO
CTRconnections0
12
CConnector4YESNO-1NO

CCfgEditObject1oStateoState12
CValue1
CValueItem115NO
CTRconnections0
13
CConnector4YESNO-1NO

CCfgEditObject1oStateCompleteoStateComplete13
CValue1
CValueItem115NO
CTRconnections0
14
CConnector4YESNO-1NO

CCfgEditObject1oKicker1OUToKicker1OUT14
CValue1
CValueItem1xActuatorKicker1extended15NO
CTRconnections0
15
CConnector4YESNO-1NO

CCfgEditObject1oKicker1INoKicker1IN15
CValue1
CValueItem1xActuatorKicker1retracted15NO
CTRconnections0
16
CConnector4YESNO-1NO

CCfgEditObject1oKicker2OUToKicker2OUT16
CValue1
CValueItem1xActuatorKicker2extended15NO
CTRconnections0
17
CConnector4YESNO-1NO

CCfgEditObject1oKicker2INoKicker2IN17
CValue1
CValueItem1xActuatorKicker2retracted15NO
CTRconnections0
18
CConnector4YESNO-1NO

CCfgEditObject1pdsTablepdsTable18
CValue1
CValueItem1myPdsTable40NO
CTRconnections0
19
CConnector4YESNO-1NO

CCfgEditObject1sendKickersOutsendKickersOut19
CValue1
CValueItem1sendKickersOut15NO
CTRconnections0
20
CConnector4YESNO-1NO

CCfgEditObject1sendKickersInsendKickersIn20
CValue1
CValueItem1sendKickersIn15NO
CTRconnections0
21
CConnector4YESNO-1NO

CCfgEditObject1oCarrierGooCarrierGo21
CValue1
CValueItem115NO
CTRconnections0
CTRinputs10
1
CConnector4YESNO-1NO

CCfgEditObject1xEnablexEnable1
CValue1
CValueItem1myRehangerEnable15NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1ModeMode2
CValue1
CValueItem140NO
CTRconnections0
3
CConnector4YESNO-1NO

CCfgEditObject1StateState3
CValue1
CValueItem140NO
CTRconnections0
4
CConnector4YESNO-1NO

CCfgEditObject1iEncoderiEncoder4
CValue1
CValueItem1EncPdsNt40NO
CTRconnections0
5
CConnector4YESNO-1NO

CCfgEditObject1SetSet5
CValue1
CValueItem140NO
CTRconnections1
CTempSimpleAddress062010
6
CConnector4YESNO-1NO

CCfgEditObject1iSensHookiSensHook6
CValue1
CValueItem115NO
CTRconnections1
CTempSimpleAddress002510
7
CConnector4YESNO-1NO

CCfgEditObject1iProdiProd7
CValue1
CValueItem1S0315NO
CTRconnections0
8
CConnector4YESNO-1NO

CCfgEditObject1iPDSsignaliPDSsignal8
CValue1
CValueItem115NO
CTRconnections0
9
CConnector4YESNO-1NO

CCfgEditObject1iCarrierGoiCarrierGo9
CValue1
CValueItem115NO
CTRconnections0
10
CConnector4YESNO-1NO

CCfgEditObject1itableMaxSizeitableMaxSize10
CValue1
CValueItem1mmiMaxTableSize20NO
CTRconnections0
CTRparams0
0MMI_PilotLightMMI_PilotLight
CFirmwareBlock0

CBlock71311417NO

CCfgEditObject1MMI_PilotLight1MMI_PilotLight45
CTRoutputs1
2
CConnector4YESNO-1NO

CCfgEditObject1WireWire2
CValue1
CValueItem1PilotLightConnection40NO
CTRconnections0
CTRinputs1
1
CConnector4YESNO-1NO

CCfgEditObject1ColorColor1
CValue1
CValueItem118NO
CTRconnections0
CTRparams0
CTRmargblocks5
501
CMarginBlock0

CBlock700011NO

CCfgEditObject11
CTRoutputs0
CTRinputs0
CTRparams0
502
CMarginBlock0

CBlock700011NO

CCfgEditObject12
CTRoutputs0
CTRinputs0
CTRparams0
503
CMarginBlock0

CBlock700037NO

CCfgEditObject13
CTRoutputs0
CTRinputs0
CTRparams0
504
CMarginBlock0

CBlock700011NO

CCfgEditObject14
CTRoutputs0
CTRinputs0
CTRparams0
505
CMarginBlock0

CBlock700011NO

CCfgEditObject15
CTRoutputs0
CTRinputs0
CTRparams0
CTRmargblocks1
501
CMarginBlock0

CBlock700011NO

CCfgEditObject11
CTRoutputs0
CTRinputs0
CTRparams0NO
(* GRAPHIC_INFORMATION_27DEE8E8_593B_45A1_8A15_7A695968B886_END *)
VAR
	zz0000FB_10_2_CalculationsAnd234 : CalculationsAndMeasurements;
	zz0000FB_10_1_SetAndConvert : SetAndConvert;
	zz0000FCT_10_1_OR_Output : ANY;
	zz0000FB_10_1_ProductRejector : ProductRejector;
	zz0000FB_10_2_CarrierControl : CarrierControl;
	zz0000FB_10_1_Rehanger_Old : Rehanger_Old;
	zz0000FB_10_1_MMI_PilotLight : MMI_PilotLight;
END_VAR

LD diEncDataPDSNT
ST zz0000FB_10_2_CalculationsAnd234.iEncoderDataPDS
LD diEncDataMX
ST zz0000FB_10_2_CalculationsAnd234.iEncoderDataMX
LD 1.57
ST zz0000FB_10_2_CalculationsAnd234.iEncPDSNTCircumference
LD myPulses
ST zz0000FB_10_2_CalculationsAnd234.iEncPDSNTResolution
LD 8
ST zz0000FB_10_2_CalculationsAnd234.iPdsNrOfHooks
LD 1.44
ST zz0000FB_10_2_CalculationsAnd234.iEncMXCircumference
LD myPulses
ST zz0000FB_10_2_CalculationsAnd234.iEncMXResolution
LD 6
ST zz0000FB_10_2_CalculationsAnd234.iMxNrOfHooks
LD S01
ST zz0000FB_10_2_CalculationsAnd234.iSensHookPDS
LD S02
ST zz0000FB_10_2_CalculationsAnd234.iSensHookMX
CAL zz0000FB_10_2_CalculationsAnd234
LD zz0000FB_10_2_CalculationsAnd234.oEncoderPDSNT
ST EncPdsNt
LD zz0000FB_10_2_CalculationsAnd234.oPDSHookSpeed
ST hookSpeedPds
LD zz0000FB_10_2_CalculationsAnd234.oLineSpeedPDSNT
ST myPdsSpeed2
LD zz0000FB_10_2_CalculationsAnd234.oEncoderMX
ST EncMX
LD zz0000FB_10_2_CalculationsAnd234.oMXHookSpeed
ST hookSpeedMx
LD zz0000FB_10_2_CalculationsAnd234.oLineSpeedMX
ST myMxSpeed2
LD zz0000FB_10_2_CalculationsAnd234.oSpeedMxLine
ST MyMXspeed
LD zz0000FB_10_2_CalculationsAnd234.oSpeedPDSline
ST MyPDSspeed
LD EncPdsNt
ST zz0000FB_10_1_SetAndConvert.iPdsEnc
LD EncMX
ST zz0000FB_10_1_SetAndConvert.iMxEnc
LD mySet_Rehanger
ST zz0000FB_10_1_SetAndConvert.iSettingsRehanger
LD mySet_CarControl
ST zz0000FB_10_1_SetAndConvert.iSettingsCarrierDisk
LD mySet_Rejector
ST zz0000FB_10_1_SetAndConvert.iSettingsRejector
CAL zz0000FB_10_1_SetAndConvert
LD S01
OR MyHookDetect
ST zz0000FCT_10_1_OR_Output
LD myRejectEnable
ST zz0000FB_10_1_ProductRejector.xEnable
LD EncMX
ST zz0000FB_10_1_ProductRejector.iEncoder
LD zz0000FB_10_1_SetAndConvert.oSettingsRejector
ST zz0000FB_10_1_ProductRejector.Settings
LD xSensorLegDetectLeft
ST zz0000FB_10_1_ProductRejector.iSensorLeft
LD xSensorLegDetectRight
ST zz0000FB_10_1_ProductRejector.iSensorRight
LD myResetProductRejector
ST zz0000FB_10_1_ProductRejector.iResetTable
CAL zz0000FB_10_1_ProductRejector
LD zz0000FB_10_1_ProductRejector.oRejectorOUT
ST xActuatorLegRejectorExtended
LD zz0000FB_10_1_ProductRejector.oRejectorIN
ST xActuatorLegRejectorRetracted
LD zz0000FB_10_1_ProductRejector.oTable
ST DisplayRejectTable
LD myCarCtrlEnable
ST zz0000FB_10_2_CarrierControl.xEnable
LD myFricDiscEna
ST zz0000FB_10_2_CarrierControl.iEnaFricDisc
LD zz0000FB_10_1_SetAndConvert.oSettingsCarrierDisk
ST zz0000FB_10_2_CarrierControl.Settings
LD S04
ST zz0000FB_10_2_CarrierControl.iCarDetPds
LD S05
ST zz0000FB_10_2_CarrierControl.iCarNulDetPds
LD EncPdsNt
ST zz0000FB_10_2_CarrierControl.iEncPds
LD S06
ST zz0000FB_10_2_CarrierControl.iCarDetMx
LD S07
ST zz0000FB_10_2_CarrierControl.iCarNulDetMx
LD EncMX
ST zz0000FB_10_2_CarrierControl.iEncMx
LD S01
ST zz0000FB_10_2_CarrierControl.iHookDetect
LD FeedBackFrictionDisck
ST zz0000FB_10_2_CarrierControl.iSpdFrictionDisc
CAL zz0000FB_10_2_CarrierControl
LD zz0000FB_10_2_CarrierControl.oSpdFrictionDisc
ST FrictionDisc
LD zz0000FB_10_2_CarrierControl.oSperPdsOpen
ST xActuatorPdsSperRetracted
LD zz0000FB_10_2_CarrierControl.oSperMxOpen
ST xActuatorMxSperRetracted
LD zz0000FB_10_2_CarrierControl.oSperPdsClose
ST xActuatorPdsSperExtended
LD zz0000FB_10_2_CarrierControl.oSperMxClose
ST xActuatorMxSperExtended
LD zz0000FB_10_2_CarrierControl.oToHmi
ST CarToHmi
LD myRehangerEnable
ST zz0000FB_10_1_Rehanger_Old.xEnable
LD EncPdsNt
ST zz0000FB_10_1_Rehanger_Old.iEncoder
LD zz0000FB_10_1_SetAndConvert.oSettingsRehanger
ST zz0000FB_10_1_Rehanger_Old.Set
LD zz0000FCT_10_1_OR_Output
ST zz0000FB_10_1_Rehanger_Old.iSensHook
LD S03
ST zz0000FB_10_1_Rehanger_Old.iProd
LD mmiMaxTableSize
ST zz0000FB_10_1_Rehanger_Old.itableMaxSize
CAL zz0000FB_10_1_Rehanger_Old
LD zz0000FB_10_1_Rehanger_Old.oKicker1OUT
ST xActuatorKicker1extended
LD zz0000FB_10_1_Rehanger_Old.oKicker1IN
ST xActuatorKicker1retracted
LD zz0000FB_10_1_Rehanger_Old.oKicker2OUT
ST xActuatorKicker2extended
LD zz0000FB_10_1_Rehanger_Old.oKicker2IN
ST xActuatorKicker2retracted
LD zz0000FB_10_1_Rehanger_Old.pdsTable
ST myPdsTable
LD zz0000FB_10_1_Rehanger_Old.sendKickersOut
ST sendKickersOut
LD zz0000FB_10_1_Rehanger_Old.sendKickersIn
ST sendKickersIn
CAL zz0000FB_10_1_MMI_PilotLight
LD zz0000FB_10_1_MMI_PilotLight.Wire
ST PilotLightConnection


END_PROGRAM
