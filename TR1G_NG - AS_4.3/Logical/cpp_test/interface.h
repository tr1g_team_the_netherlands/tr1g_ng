class Probeer
{
	public:
	Probeer();
	Probeer(int in1, int in2);
	~Probeer();
	
	virtual int startMotor(int speed , int ramp)=0;
	virtual int stopmotor()=0;
	virtual int inhibitDrive()=0;
	 
		
	private:
	
	protected:
	
};