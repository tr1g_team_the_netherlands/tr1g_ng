PROGRAM _INIT
	
	cm.setup.parent.name := 'main';

END_PROGRAM

PROGRAM _CYCLIC
	
	CASE cm.state OF

		0:	cm.description := 'config handling';
			MpRecipeXml_Config.MpLink := ADR(mpConfig);
			MpRecipeXml_Config.Enable := TRUE;
			MpRecipeXml_Config.DeviceName := ADR('CONFIG');
			MpRecipeXml_Config.FileName := ADR('config.xml');
			MpRecipeXml_Config.Category := ADR('config');
			MpRecipeXml_Config();
	
	END_CASE

	cm();								// call the mocule

END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM
