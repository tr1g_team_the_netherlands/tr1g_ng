
ACTION resetEmptyHook: 

	emptyHookCount := 0;

END_ACTION


ACTION updateHardware:

	//Reject
	hw.do.xRejectExt				:= productRejector.oRejectorOUT;
	hw.do.xRejectRet				:= productRejector.oRejectorIN;
	
	//Airvalves
	FestoValves.xValve1_A 	:= FALSE;
	FestoValves.xValve1_B	:= FALSE;
	FestoValves.xValve2_A	:= hw.do.xRehangSupSperRet;
	FestoValves.xValve2_B	:= hw.do.xRehangSupSperExt;
	FestoValves.xValve3_A	:= hw.do.xRehangDisSperExt;
	FestoValves.xValve3_B	:= hw.do.xRehangDisSperRet;
	FestoValves.xValve4_A	:= hw.do.xRehangKick1Ret;
	FestoValves.xValve4_B	:= hw.do.xRehangKick1Ext;
	FestoValves.xValve5_A	:= hw.do.xRehangKick2Ret;
	FestoValves.xValve5_B	:= hw.do.xRehangKick2Ext;
	FestoValves.xValve6_A	:= hw.do.xRejectExt;
	FestoValves.xValve6_B	:= hw.do.xRejectRet;
	FestoValves.xValve7_A 	:= FALSE;
	FestoValves.xValve7_B 	:= FALSE;
	FestoValves.xValve8_A 	:= FALSE;
	FestoValves.xValve8_B 	:= FALSE;
	FestoValves();
	
	hw.do.xLightGreen 		:= PilotLight.Wire.GreenWire;
	hw.do.xLightRed			:= PilotLight.Wire.RedWire;
	hw.do.xLightYellow		:= PilotLight.Wire.YellowWire;
	hw.do.xLightBlue		:= PilotLight.Wire.BlueWire;
	hw.do.xLightWhite		:= PilotLight.Wire.WhiteWire;
	

END_ACTION


