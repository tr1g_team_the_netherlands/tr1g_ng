
PROGRAM _CYCLIC
	//start stop
	IF FrictionDisc.Run THEN
		aiPoUser1[0] := 6;
	ELSE
		aiPoUser1[0] := 0;
	END_IF;
	
	//Speed
	aiPoUser1[1] := REAL_TO_INT( FrictionDisc.SpeedDrive);
	aiPoUser1[2] := REAL_TO_INT(FrictionDisc.Ramp);
	
		
	// ----------- Call and intputs-------------------------------- 
	fb_SbusInfoMaster_1();
	fb_SbusInfoMaster_1.xEnable:=TRUE;
	fb_SbusInfoMaster_1.udiHandle:=fb_SbusMasterDriver.udiHandle;
	
	// ------------outputs ----------------------------------------
	structBusInfoMaster_1 := fb_SbusInfoMaster_1.structInfo;
	
	// ------------Call and intputs--------------------------------
	fb_SbusController_drive_1();
	fb_SbusController_drive_1.aiPo:=aiPoUser1;
	fb_SbusController_drive_1.udiHandle:=fb_SbusMasterDriver.udiHandle;
	fb_SbusController_drive_1.uiMotorNumber:=2;		//LET OP STAAT NU NOG OP 2, DIT MOET NOG AANGEPAST WORDEN!!!
	
	// -------------outputs----------------------------------------
	fb_SbusController_drive_1.uiMasterStatus:= fb_SbusMasterDriver.uiStatus;
	aiPiUser1 := fb_SbusController_drive_1.aiPi;
	
	FeedBackFrictionDisck[0] := fb_SbusController_drive_1.aiPi[0];
	FeedBackFrictionDisck[1] := fb_SbusController_drive_1.aiPi[1];
	FeedBackFrictionDisck[2] := fb_SbusController_drive_1.aiPi[2];
	
END_PROGRAM
