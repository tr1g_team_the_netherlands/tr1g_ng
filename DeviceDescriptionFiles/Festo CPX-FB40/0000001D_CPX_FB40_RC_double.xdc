<?xml version="1.0" encoding="UTF-8"?>
<ISO15745ProfileContainer  xmlns="http://www.ethernet-powerlink.org" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ethernet-powerlink.org Powerlink_Main.xsd">
  <ISO15745Profile>
    <ProfileHeader>
      <ProfileIdentification>EPL_Device_Profile</ProfileIdentification>
      <ProfileRevision>1</ProfileRevision>
      <ProfileName> </ProfileName>
      <ProfileSource/>
      <ProfileClassID>Device</ProfileClassID>
      <ISO15745Reference>
        <ISO15745Part>1</ISO15745Part>
        <ISO15745Edition>1</ISO15745Edition>
        <ProfileTechnology>Powerlink</ProfileTechnology>
      </ISO15745Reference>
    </ProfileHeader>
    <ProfileBody xsi:type="ProfileBody_Device_Powerlink" fileName="0000001D_CPX_FB40_RC-double.xdc" fileCreator="ruc" fileCreationDate="2014-05-03" fileModifiedBy="ruc" fileModificationDate="2014-10-30" fileVersion="1.0">
	  <DeviceIdentity>
		<vendorName readOnly="true">Festo AG &amp; Co. KG</vendorName>
		<vendorID readOnly="true">0x0000001D</vendorID>
		<vendorText readOnly="true">
			<label lang="de">FESTO Hotline-Tel.: 0711/347-3000</label>
			<label lang="en">FESTO Hotline-Tel.: 0711/347-3000</label>
		</vendorText>
		<deviceFamily readOnly="true">
			<label lang="en">valves</label>
			<label lang="de">Ventile</label>
		</deviceFamily>
		<productFamily>CPX</productFamily>
		<productName>CPX-FB40_RC-double</productName>
		<productID>2474896</productID>
		<productText>
			<label lang="en">CPX-terminal</label>
			<label lang="de">CPX-Terminal</label>
		</productText>
		<orderNumber>F40</orderNumber>
	  </DeviceIdentity>
      <DeviceManager>
      </DeviceManager>
      <DeviceFunction>
        <capabilities>
         <!-- The characteristicsList is a list of key-value pairs.  -->
         <characteristicsList>
            <characteristic>
	      <characteristicName>
		<label lang="en"/>
	      </characteristicName>
	      <characteristicContent>
		<label lang="en"/>
	      </characteristicContent>
	    </characteristic>
    	</characteristicsList>
        </capabilities>
      </DeviceFunction>      
      <ApplicationProcess>
			<dataTypeList>
<!-- begin data types -->
				<struct name="Inputs: 8 x 16 Bit" uniqueID="UID_DT_Index2000_Sub01">
					<varDeclaration name="slot_0_TechnologyInput_CH" uniqueID="ID_Index2000_Sub01" size="8">
						<UINT/>
					</varDeclaration>
				</struct>
				<struct name="Outputs: 8 x 16 Bit" uniqueID="UID_DT_Index2000_Sub02">
					<varDeclaration name="slot_0_TechnologyOutput_CH" uniqueID="ID_Index2000_Sub02" size="8">
						<UINT/>
					</varDeclaration>
				</struct>

<!-- end data types -->
			</dataTypeList>
			<parameterList>
<!-- begin parameters -->
				<parameter uniqueID="Index2000_Sub01" access="read">
					<label lang="en">MOD_InputImage</label>
					<dataTypeIDRef uniqueIDRef="UID_DT_Index2000_Sub01"/>
				</parameter>
				<parameter uniqueID="Index2000_Sub02" access="readWrite">
					<label lang="en">MOD_OutputImage</label>
					<dataTypeIDRef uniqueIDRef="UID_DT_Index2000_Sub02"/>
				</parameter>

<!-- end parameters -->
			</parameterList>
	</ApplicationProcess>
    </ProfileBody>
  </ISO15745Profile>
  <ISO15745Profile>
    <ProfileHeader>
      <ProfileIdentification>EPL comm net profile</ProfileIdentification>
      <ProfileRevision>1</ProfileRevision>
      <ProfileName/>
      <ProfileSource/>
      <ProfileClassID>CommunicationNetwork</ProfileClassID>
      <ISO15745Reference>
        <ISO15745Part>1</ISO15745Part>
        <ISO15745Edition>1</ISO15745Edition>
        <ProfileTechnology>Powerlink</ProfileTechnology>
      </ISO15745Reference>
    </ProfileHeader>
    <ProfileBody xsi:type="ProfileBody_CommunicationNetwork_Powerlink" fileName="0000001D_CPX_FB40_RC-double.xdc" fileCreator="ruc" fileCreationDate="2014-05-28" fileModifiedBy="ruc" fileModificationDate="2014-10-30" fileVersion="1.0">
      <ApplicationLayers>
        <identity>
          <vendorID>0x0000001D</vendorID>
        </identity>
        <DataTypeList>
          <defType dataType="0001"><Boolean/></defType>
          <defType dataType="0002"><Integer8/></defType>
          <defType dataType="0003"><Integer16/></defType>
          <defType dataType="0004"><Integer32/></defType>
          <defType dataType="0005"><Unsigned8/></defType>
          <defType dataType="0006"><Unsigned16/></defType>
          <defType dataType="0007"><Unsigned32/></defType>
          <defType dataType="0008"><Real32/></defType>
          <defType dataType="0009"><Visible_String/></defType>
          <defType dataType="0010"><Integer24/></defType>
          <defType dataType="0011"><Real64/></defType>
          <defType dataType="0012"><Integer40/></defType>
          <defType dataType="0013"><Integer48/></defType>
          <defType dataType="0014"><Integer56/></defType>
          <defType dataType="0015"><Integer64/></defType>
          <defType dataType="000A"><Octet_String/></defType>
          <defType dataType="000B"><Unicode_String/></defType>
          <defType dataType="000C"><Time_of_Day/></defType>
          <defType dataType="000D"><Time_Diff/></defType>
          <defType dataType="000F"><Domain/></defType>
          <defType dataType="0016"><Unsigned24/></defType>
          <defType dataType="0018"><Unsigned40/></defType>
          <defType dataType="0019"><Unsigned48/></defType>
          <defType dataType="001A"><Unsigned56/></defType>
          <defType dataType="001B"><Unsigned64/></defType>
          <defType dataType="0401"><MAC_ADDRESS/></defType>
          <defType dataType="0402"><IP_ADDRESS/></defType>
          <defType dataType="0403"><NETTIME/></defType>
        </DataTypeList>
        <ObjectList>
          <Object index="1000" name="NMT_DeviceType_U32" objectType="7" PDOmapping="no" accessType="const" dataType="0007" defaultValue="0x000F0191" />
          <Object index="1001" name="ERR_ErrorRegister_U8" objectType="7" accessType="ro" dataType="0005" defaultValue="0x0" />
          <Object index="1003" name="ERR_History_ADOM" objectType="8" subNumber="33">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="rw" dataType="0005" defaultValue="0x0" PDOmapping="optional" />
            <SubObject subIndex="01" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="02" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="03" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="04" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="05" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="06" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="07" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="08" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="09" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="0A" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="0B" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="0C" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="0D" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="0E" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="0F" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="10" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="11" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="12" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="13" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="14" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="15" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="16" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="17" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="18" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="19" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="1A" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="1B" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="1C" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="1D" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="1E" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="1F" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
            <SubObject subIndex="20" name="HistoryEntry_DOM" objectType="7" accessType="ro" dataType="000F" PDOmapping="no" objFlags="0007" />
          </Object>
          <Object index="1006" name="NMT_CycleLen_U32" objectType="7" PDOmapping="no" accessType="rw" dataType="0007" defaultValue="0x3E8" objFlags="0004" />
          <Object index="1008" name="NMT_ManufactDevName_VS" objectType="7" PDOmapping="no" accessType="const" dataType="0009" defaultValue="CPX-FB40_basic" />
          <Object index="1011" name="NMT_RestoreDefParam_REC" objectType="9" subNumber="5">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x4" PDOmapping="no" />
            <SubObject subIndex="01" name="AllParam_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0" PDOmapping="no" />
            <SubObject subIndex="02" name="CommunicationParam_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0" PDOmapping="no" />
            <SubObject subIndex="03" name="ApplicationParam_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0" PDOmapping="no" />
            <SubObject subIndex="04" name="ManufacturerParam_04h_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0" PDOmapping="no" />
          </Object>
          <Object index="1018" name="NMT_IdentityObject_REC" objectType="9" subNumber="5">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x4" PDOmapping="no" />
            <SubObject subIndex="01" name="VendorId_U32" objectType="7" accessType="const" dataType="0007" defaultValue="0x0000001D" PDOmapping="no" />
            <SubObject subIndex="02" name="ProductCode_U32" objectType="7" accessType="const" dataType="0007" defaultValue="2474896" PDOmapping="no" />
            <SubObject subIndex="03" name="RevisionNo_U32" objectType="7" accessType="const" dataType="0007" defaultValue="0x00010005" PDOmapping="no" />
            <SubObject subIndex="04" name="SerialNo_U32" objectType="7" accessType="const" dataType="0007" defaultValue="0x00000001" PDOmapping="no" />
          </Object>
          <Object index="1020" name="CFM_VerifyConfiguration_REC" objectType="9" subNumber="5">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x4" PDOmapping="no" />
            <SubObject subIndex="01" name="ConfDate_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="02" name="ConfTime_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="03" name="ConfId_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="04" name="VerifyConfInvalid_BOOL" objectType="7" accessType="ro" dataType="0001" PDOmapping="no" />
          </Object>
          <Object index="1030" name="NMT_InterfaceGroup_0h_REC" objectType="9" subNumber="10">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x9" PDOmapping="no" />
            <SubObject subIndex="01" name="InterfaceIndex_U16" objectType="7" accessType="ro" dataType="0006" defaultValue="1" PDOmapping="no" />
            <SubObject subIndex="02" name="InterfaceDescription_VSTR" objectType="7" accessType="const" dataType="0009" PDOmapping="no" />
            <SubObject subIndex="03" name="InterfaceType_U8" objectType="7" accessType="const" dataType="0005" defaultValue="0x6" PDOmapping="no" />
            <SubObject subIndex="04" name="InterfaceMtu_U16" objectType="7" accessType="const" dataType="0006" PDOmapping="no" />
            <SubObject subIndex="05" name="InterfacePhysAddress_OSTR" objectType="7" accessType="const" dataType="000A" PDOmapping="no" />
            <SubObject subIndex="06" name="InterfaceName_VSTR" objectType="7" accessType="ro" dataType="0009" PDOmapping="no" />
            <SubObject subIndex="07" name="InterfaceOperStatus_U8" objectType="7" accessType="ro" dataType="0005" PDOmapping="no" />
            <SubObject subIndex="08" name="InterfaceAdminState_U8" objectType="7" accessType="rw" dataType="0005" defaultValue="0x1" PDOmapping="no" />
            <SubObject subIndex="09" name="Valid_BOOL" objectType="7" accessType="rw" dataType="0001" defaultValue="true" PDOmapping="no" />
          </Object>
          <Object index="1300" name="SDO_SequLayerTimeout_U32" objectType="7" PDOmapping="no" accessType="rw" dataType="0007" defaultValue="0x7D0" objFlags="0004" />
          <Object index="1301" name="SDO_CmdLayerTimeout_U32" objectType="7" PDOmapping="no" accessType="rw" dataType="0007" defaultValue="0x3E8" objFlags="0004" />
          <Object index="1400" name="PDO_RxCommParam_00h_REC" objectType="9" subNumber="3">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x02" PDOmapping="no" />
            <SubObject subIndex="01" name="NodeID_U8" objectType="7" accessType="rw" dataType="0005" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="02" name="MappingVersion_U8" objectType="7" accessType="rw" dataType="0005" defaultValue="0x0" PDOmapping="no" />
          </Object>
          <Object index="1600" name="PDO_RxMappParam_00h_AU64" objectType="8" subNumber="65">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="rw" dataType="0005" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="01" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="02" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="03" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="04" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="05" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="06" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="07" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="08" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="09" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0A" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0B" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0C" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0D" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0E" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0F" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="10" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="11" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="12" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="13" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="14" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="15" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="16" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="17" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="18" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="19" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1A" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1B" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1C" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1D" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1E" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1F" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="20" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="21" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="22" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="23" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="24" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="25" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="26" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="27" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="28" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="29" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2A" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2B" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2C" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2D" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2E" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2F" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="30" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="31" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="32" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="33" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="34" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="35" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="36" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="37" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="38" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="39" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3A" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3B" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3C" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3D" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3E" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3F" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="40" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
          </Object>
          <Object index="1800" name="PDO_TxCommParam_00h_REC" objectType="9" subNumber="3">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x02" PDOmapping="no" />
            <SubObject subIndex="01" name="NodeID_U8" objectType="7" accessType="rw" dataType="0005" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="02" name="MappingVersion_U8" objectType="7" accessType="rw" dataType="0005" defaultValue="0x0" PDOmapping="no" />
          </Object>
          <Object index="1A00" name="PDO_TxMappParam_00h_AU64" objectType="8" subNumber="65">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="rw" dataType="0005" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="01" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="02" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="03" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="04" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="05" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="06" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="07" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="08" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="09" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0A" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0B" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0C" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0D" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0E" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="0F" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="10" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="11" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="12" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="13" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="14" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="15" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="16" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="17" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="18" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="19" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1A" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1B" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1C" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1D" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1E" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="1F" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="20" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="21" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="22" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="23" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="24" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="25" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="26" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="27" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="28" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="29" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2A" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2B" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2C" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2D" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2E" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="2F" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="30" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="31" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="32" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="33" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="34" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="35" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="36" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="37" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="38" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="39" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3A" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3B" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3C" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3D" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3E" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="3F" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
            <SubObject subIndex="40" name="ObjectMapping" objectType="7" accessType="rw" dataType="001B" defaultValue="0x0000000000000000" PDOmapping="no" />
          </Object>
          <Object index="1C0B" name="DLL_CNLossSoC_REC" objectType="9" subNumber="4">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x3" PDOmapping="no" />
            <SubObject subIndex="01" name="CumulativeCnt_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="02" name="ThresholdCnt_U32" objectType="7" accessType="ro" dataType="0007" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="03" name="Threshold_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0xF" PDOmapping="no" />
          </Object>
          <Object index="1C0F" name="DLL_CNCRCError_REC" objectType="9" subNumber="4">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x3" PDOmapping="no" />
            <SubObject subIndex="01" name="CumulativeCnt_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="02" name="ThresholdCnt_U32" objectType="7" accessType="ro" dataType="0007" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="03" name="Threshold_U32" objectType="7" accessType="rw" dataType="0007" defaultValue="0xF" PDOmapping="no" />
          </Object>
          <Object index="1C14" name="DLL_CNLossOfSocTolerance_U32" objectType="7" PDOmapping="no" accessType="rw" dataType="0007" defaultValue="0x5F5E100" />
          <Object index="1E40" name="NWL_IpAddrTable_0h_REC" objectType="9" subNumber="6">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x5" PDOmapping="no" />
            <SubObject subIndex="01" name="IfIndex_U16" objectType="7" accessType="ro" dataType="0006" defaultValue="1" PDOmapping="no" />
            <SubObject subIndex="02" name="Addr_IPAD" objectType="7" accessType="ro" dataType="0402" defaultValue="0" PDOmapping="no" />
            <SubObject subIndex="03" name="NetMask_IPAD" objectType="7" accessType="ro" dataType="0402" defaultValue="255.255.255.0" PDOmapping="no" />
            <SubObject subIndex="04" name="ReasmMaxSize_U16" objectType="7" accessType="ro" dataType="0006" defaultValue="1500" PDOmapping="no" />
            <SubObject subIndex="05" name="DefaultGateway_IPAD" objectType="7" accessType="rw" dataType="0402" defaultValue="0" PDOmapping="no" />
          </Object>
          <Object index="1E4A" name="NWL_IpGroup_REC" objectType="9" subNumber="4">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x3" PDOmapping="no" />
            <SubObject subIndex="01" name="Forwarding_BOOL" objectType="7" accessType="ro" dataType="0001" PDOmapping="no" />
            <SubObject subIndex="02" name="DefaultTTL_U16" objectType="7" accessType="rw" dataType="0006" defaultValue="0x40" PDOmapping="no" />
            <SubObject subIndex="03" name="ForwardDatagrams_U32" objectType="7" accessType="ro" dataType="0007" defaultValue="0" PDOmapping="no" />
          </Object>
          <Object index="1F50" name="PDL_DownloadProgData_ADOM" objectType="8" subNumber="2">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="rw" dataType="0005" defaultValue="0x1" PDOmapping="no" />
            <SubObject subIndex="01" name="Program" objectType="7" accessType="rw" dataType="000F" PDOmapping="no" />
          </Object>
          <Object index="1F51" name="PDL_ProgCtrl_AU8" objectType="8" subNumber="2">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="rw" dataType="0005" defaultValue="0x1" PDOmapping="no" />
            <SubObject subIndex="01" name="ProgCtrl" objectType="7" accessType="rw" dataType="0005" defaultValue="0x0" PDOmapping="no" />
          </Object>
          <Object index="1F52" name="PDL_LocVerApplSw_REC" objectType="9" subNumber="3">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x2" PDOmapping="no" />
            <SubObject subIndex="01" name="ApplSwDate_U32" objectType="7" accessType="rw" dataType="0007" PDOmapping="no" />
            <SubObject subIndex="02" name="ApplSwTime_U32" objectType="7" accessType="rw" dataType="0007" PDOmapping="no" />
          </Object>
          <Object index="1F82" name="NMT_FeatureFlags_U32" objectType="7" PDOmapping="no" accessType="const" dataType="0007" defaultValue="0x00000245" />
          <Object index="1F83" name="NMT_EPLVersion_U8" objectType="7" PDOmapping="no" accessType="const" dataType="0005" defaultValue="0x20" />
          <Object index="1F8C" name="NMT_CurrNMTState_U8" objectType="7" accessType="ro" dataType="0005" />
          <Object index="1F93" name="NMT_EPLNodeID_REC" objectType="9" subNumber="3">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x2" PDOmapping="no" />
            <SubObject subIndex="01" name="NodeID_U8" objectType="7" accessType="ro" dataType="0005" defaultValue="0x1" PDOmapping="no" />
            <SubObject subIndex="02" name="NodeIDByHW_BOOL" objectType="7" accessType="ro" dataType="0001" defaultValue="true" PDOmapping="no" />
          </Object>
          <Object index="1F98" name="NMT_CycleTiming_REC" objectType="9" subNumber="10">
            <SubObject subIndex="00" name="NumberOfEntries" objectType="7" accessType="const" dataType="0005" defaultValue="0x9" PDOmapping="no" />
            <SubObject subIndex="01" name="IsochrTxMaxPayload_U16" objectType="7" accessType="const" dataType="0006" defaultValue="64" PDOmapping="no" />
            <SubObject subIndex="02" name="IsochrRxMaxPayload_U16" objectType="7" accessType="const" dataType="0006" defaultValue="64" PDOmapping="no" />
            <SubObject subIndex="03" name="PResMaxLatency_U32" objectType="7" accessType="const" dataType="0007" defaultValue="10000" PDOmapping="no" />
            <SubObject subIndex="04" name="PReqActPayloadLimit_U16" objectType="7" accessType="rw" dataType="0006" defaultValue="36" PDOmapping="no" />
            <SubObject subIndex="05" name="PResActPayloadLimit_U16" objectType="7" accessType="rw" dataType="0006" defaultValue="36" PDOmapping="no" />
            <SubObject subIndex="06" name="ASndMaxLatency_U32" objectType="7" accessType="const" dataType="0007" defaultValue="10000" PDOmapping="no" />
            <SubObject subIndex="07" name="MultiplCycleCnt_U8" objectType="7" accessType="rw" dataType="0005" defaultValue="0x0" PDOmapping="no" />
            <SubObject subIndex="08" name="AsyncMTU_U16" objectType="7" accessType="rw" dataType="0006" defaultValue="0x5DC" PDOmapping="no" objFlags="0004" />
            <SubObject subIndex="09" name="Prescaler_U16" objectType="7" accessType="rw" dataType="0006" defaultValue="0x2" PDOmapping="no" />
          </Object>
          <Object index="1F99" name="NMT_CNBasicEthernetTimeout_U32" objectType="7" PDOmapping="no" accessType="rw" dataType="0007" defaultValue="0x4C4B40" objFlags="0004" />
          <Object index="1F9A" name="NMT_HostName_VSTR" objectType="7" PDOmapping="no" accessType="rw" dataType="0009" defaultValue="Festo-CPX-Terminal" />
          <Object index="1F9E" name="NMT_ResetCmd_U8" objectType="7" PDOmapping="no" accessType="rw" dataType="0005" defaultValue="0xFF" />
<!-- begin channels -->
		<Object index="2000" name="slot 0 - FB40-RC - Powerlink Remote-Controller" objectType="9" dataType="0003" PDOmapping="no" >
			<SubObject subIndex="00" name="NumberOfEntries" objectType="7" dataType="0005" accessType="const" defaultValue="2"/>
			<SubObject subIndex="01" name="TechnologyInput" objectType="7" PDOmapping="TPDO" uniqueIDRef="Index2000_Sub01" />
			<SubObject subIndex="02" name="TechnologyOutput" objectType="7" PDOmapping="RPDO" uniqueIDRef="Index2000_Sub02" />
		</Object>
<!-- end channels -->
<!-- begin device specific parameters -->
		<Object index="20F0" name="ModuleIdentification_AU64" objectType="8" dataType="001B" accessType="rw" PDOmapping="no" >
			<SubObject subIndex="00" name="number of entries" objectType="7" dataType="0005" accessType="const" defaultValue="1" PDOmapping="no"/>
			<SubObject subIndex="01" name="Slot0_IdentNumber_U64" objectType="7" dataType="001B" accessType="rw" PDOmapping="no" actualValue="0x100810080000AB04"/>
		</Object>
        <Object index="20F2" name="DiagnosisHandshake_U8" objectType="7"  dataType="0005" accessType="rw" defaultValue="0" />
		<Object index="2100" name="slot 0 - FB40-RC - Powerlink Remote-Controller" objectType="9">
			<SubObject subIndex="01" name="ModuleState" objectType="7" accessType="ro" dataType="0006" actualValue="0x0000" />
			<SubObject subIndex="02" name="IdentCode" objectType="7" accessType="ro" dataType="0006" actualValue="0xAB04" />
			<SubObject subIndex="03" name="Revision" objectType="7" accessType="ro" dataType="0005" actualValue="0" />
			<SubObject subIndex="04" name="SerialNumber" objectType="7" accessType="ro" dataType="0007" actualValue="0x00000000" />
			<SubObject subIndex="05" name="Reserved" objectType="7" accessType="ro" dataType="0005"/>
			<SubObject subIndex="06" name="Properties" objectType="7" accessType="rw" dataType="0007" actualValue="0x00000007" />
			<SubObject subIndex="07" name="Ueberwachung_Busverbindung" objectType="7" dataType="0001" accessType="rw" PDOmapping="no" actualValue="false"/>
			<SubObject subIndex="00" name="NumberOfEntries" objectType="7" dataType="0005" accessType="const" defaultValue="7"/>
		</Object>

<!-- end device specific parameters -->

		</ObjectList>
      </ApplicationLayers>
      <TransportLayers />
      <NetworkManagement>
      <GeneralFeatures CFMConfigManager="false" DLLErrBadPhysMode="false" DLLErrMacBuffer="false" NMTCycleTimeGranularity="1" NMTBootTimeNotActive="9000000" DLLFeatureCN="true" DLLFeatureMN="false" NMTCycleTimeMax="50000" NMTCycleTimeMin="600" NMTEmergencyQueueSize="0" NMTErrorEntries="2" NMTMaxCNNodeID="239" NMTMaxCNNumber="239" NMTMaxHeartbeats="254" NMTNodeIDByHW="true" NMTProductCode="2474896" NMTRevisionNo="65541" NMTPublishActiveNodes="false" NMTPublishConfigNodes="false" NMTPublishEmergencyNew="false" NMTPublishNodeState="false" NMTPublishOperational="false" NMTPublishPreOp1="false" NMTPublishPreOp2="false" NMTPublishReadyToOp="false" NMTPublishStopped="false" NMTPublishTime="false" NWLForward="false" NWLICMPSupport="false" NWLIPSupport="true" PDOGranularity="8" PDOMaxDescrMem="4294967295" PDORPDOChannelObjects="254" PDORPDOChannels="1" PDORPDOCycleDataLim="4294967295" PDORPDOOverallObjects="64" PDOSelfReceipt="false" PDOTPDOChannelObjects="254" PDOTPDOCycleDataLim="4294967295" PDOTPDOOverallObjects="64" PHYExtEPLPorts="1" SDOMaxConnections="1" PHYHubIntegrated="false" RT1RT1SecuritySupport="false" RT1RT1Support="false" RT2RT2Support="false" SDOClient="false" SDOCmdFileRead="false" SDOCmdFileWrite="false" SDOCmdLinkName="false" SDOCmdReadAllByIndex="false" SDOCmdReadByName="false" SDOCmdReadMultParam="false" SDOCmdWriteAllByIndex="false" SDOCmdWriteByName="false" SDOCmdWriteMultParam="false" SDOMaxParallelConnections="1" SDOSeqLayerTxHistorySize="5" SDOServer="true" />
      <CNFeatures DLLCNFeatureMultiplex="true" NMTCNSoC2PReq="10000" />
      <Diagnostic>
      </Diagnostic>
      </NetworkManagement>
    </ProfileBody>
  </ISO15745Profile>
</ISO15745ProfileContainer>
