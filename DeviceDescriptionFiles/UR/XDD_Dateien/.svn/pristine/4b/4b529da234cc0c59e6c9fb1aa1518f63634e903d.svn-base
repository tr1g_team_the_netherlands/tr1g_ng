<?xml version="1.0" encoding="UTF-8"?>
<ISO15745ProfileContainer xmlns="http://www.ethernet-powerlink.org" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ethernet-powerlink.org ../Powerlink_Main.xsd">
	<ISO15745Profile>
		<ProfileHeader>
			<ProfileIdentification>EPL_Device_Profile_UR20_2FCNT_100</ProfileIdentification>
			<ProfileRevision>1</ProfileRevision>
			<ProfileName>Ethernet POWERLINK UR20-2FCNT-100 device profile</ProfileName>
			<ProfileSource/>
			<ProfileClassID>Device</ProfileClassID>
			<ISO15745Reference>
				<ISO15745Part>4</ISO15745Part>
				<ISO15745Edition>1</ISO15745Edition>
				<ProfileTechnology>Powerlink</ProfileTechnology>
			</ISO15745Reference>
		</ProfileHeader>
		<ProfileBody xsi:type="ProfileBody_Device_Powerlink_Modular_Child" fileName="00000230_UR20_2FCNT_100.XDD" fileCreator="Weidmüller" fileCreationDate="2016-05-30" fileCreationTime="15:00:00+01:00" fileModificationDate="2015-10-20" fileModificationTime="16:23:37.341+02:00" fileModifiedBy="Weidmüller" fileVersion="V01.00" supportedLanguages="en de" specificationVersion="1.2.0">
			<DeviceIdentity>
				<vendorName>Weidmüller</vendorName>
				<vendorID>0x00000230</vendorID>
				<vendorText>
					<label lang="de">Bei Problemen kontaktieren sie unseren Support: ++49(..</label>
					<label lang="en">Experiencing problems - contact our support : ++1(..</label>
				</vendorText>
				<deviceFamily>
					<label lang="en">Modular I/O system</label>
					<label lang="de">Modulares I/O System</label>
				</deviceFamily>
				<productName>UR20-2FCNT-100 / 1508080000</productName>
				<productID>1508080000</productID>
				<productText>
					<label lang="en">IO-MODUL 2 FCNT 32Bit DC24V 100kHz</label>
					<label lang="de">IO-MODUL 2 FCNT 32Bit DC24V 100kHz</label>
				</productText>
				<orderNumber>1508080000</orderNumber>
				<buildDate>2016-05-30</buildDate>
			</DeviceIdentity>
			<DeviceManager>
				<moduleManagement>
					<moduleInterface childID="UR20_2FCNT_100" type="UR20" moduleAddressing="position">
						<label lang="en-us">UR20 interface</label>
						<fileList>
							<file URI="00000230_*.xdd"/>
						</fileList>
						<moduleTypeList>
							<moduleType uniqueID="UR20_2FCNT_100_out" type="UR20" />
						</moduleTypeList>
					</moduleInterface>
				</moduleManagement>
			</DeviceManager>
			<DeviceFunction>
				<capabilities>
					<characteristicsList>
						<characteristic>
							<characteristicName>
								<label lang="en">Input/Output</label>
								<label lang="de">Ein-/Ausgänge</label>
							</characteristicName>
							<characteristicContent>
								<label lang="en">UR20 IO System</label>
								<label lang="de">UR20 IO System</label>
							</characteristicContent>
						</characteristic>
					</characteristicsList>
				</capabilities>
				<picturesList>
					<picture URI="UR20_2FCNT_100.png" type="frontPicture">
						<label lang="en">FrontFoto</label>
						<label lang="de">Frontfoto</label>
					</picture>
					<picture URI="UR20.ico" type="icon">
						<label lang="en">Icon</label>
						<label lang="de">Pictogramm</label>
					</picture>
				</picturesList>
				<connectorList>
					<connector id="UR20_IF1" connectorType="UR20" interfaceIDRef="UR20_2FCNT_100" positioning="localLeft" >
						<label lang="en-us">Left UR20 interface</label>
					</connector>
					<connector id="UR20_IF2" connectorType="UR20" interfaceIDRef="UR20_2FCNT_100_out" positioning="localRight" >
						<label lang="en-us">Right UR20 interface</label>
					</connector>
				</connectorList>
				<classificationList>
					<classification>IO</classification>
					<classification>Digital</classification>
					<classification>Input</classification>
				</classificationList>
			</DeviceFunction>
			<ApplicationProcess>
				<templateList>
					<parameterTemplate uniqueID="TID_Par_Filter_Select_Type" access="readWrite">
						<description lang="en">Template</description>
						<description lang="de">Template</description>
						<STRING/>
						<defaultValue value="5 us"/>
						<allowedValues>
							<value value="5 us">
								<label lang="en">5 us (187 kHz)</label>
								<label lang="de">5 us (187 kHz)</label>
							</value>
							<value value="11 us">
								<label lang="en">11 us (94 kHz)</label>
								<label lang="de">11 us (94 kHz)</label>
							</value>
							<value value="21 us">
								<label lang="en">21 us (47 kHz)</label>
								<label lang="de">21 us (47 kHz)</label>
							</value>
							<value value="43 us">
								<label lang="en">43 us (23 kHz)</label>
								<label lang="de">43 us (23 kHz)</label>
							</value>
							<value value="83 us">
								<label lang="en">83 us (12 kHz)</label>
								<label lang="de">83 us (12 kHz)</label>
							</value>
							<value value="167 us">
								<label lang="en">167 us (6 kHz)</label>
								<label lang="de">167 us (6 kHz)</label>
							</value>
							<value value="333 us">
								<label lang="en">333 us (3 kHz)</label>
								<label lang="de">333 us (3 kHz)</label>
							</value>
							<value value="667 us">
								<label lang="en">667 us (1,5 kHz)</label>
								<label lang="de">667 us (1,5 kHz)</label>
							</value>
							<value value="1 ms">
								<label lang="en">1 ms (732 Hz)</label>
								<label lang="de">1 ms (732 Hz)</label>
							</value>
							<value value="3 ms">
								<label lang="en">3 ms (366 Hz)</label>
								<label lang="de">3 ms (366 Hz)</label>
							</value>
							<value value="5 ms">
								<label lang="en">5 ms (183 Hz)</label>
								<label lang="de">5 ms (183 Hz)</label>
							</value>
							<value value="11 ms">
								<label lang="en">11 ms (92 Hz)</label>
								<label lang="de">11 ms (92 Hz)</label>
							</value>
							<value value="22 ms">
								<label lang="en">22 ms (46 Hz)</label>
								<label lang="de">22 ms (46 Hz)</label>
							</value>
							<value value="43 ms">
								<label lang="en">43 ms (23 Hz)</label>
								<label lang="de">43 ms (23 Hz)</label>
							</value>
							<value value="91 ms">
								<label lang="en">91 ms (11 Hz)</label>
								<label lang="de">91 ms (11 Hz)</label>
							</value>
							<value value="167 ms">
								<label lang="en">167 ms (6 Hz)</label>
								<label lang="de">167 ms (6 Hz)</label>
							</value>
							<value value="333 ms">
								<label lang="en">333 ms (3 Hz)</label>
								<label lang="de">333 ms (3 Hz)</label>
							</value>
						</allowedValues>
					</parameterTemplate>
					<parameterTemplate uniqueID="TID_Par_Filter_Data_Type" access="readWrite">
						<description lang="en">Data Type</description>
						<description lang="de">Data Type</description>
						<DINT/>
						<allowedValues>
							<range>
								<minValue value="0"/>
								<maxValue value="16"/>
							</range>
						</allowedValues>
					</parameterTemplate>
				</templateList>
				<parameterList>
					<parameter uniqueID="UID_CH1_Current_Period" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_CH2_Current_Period" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_CH1_Rising_Edges" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_CH2_Rising_Edges" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_CH1_Status" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<UINT/>
					</parameter>
					<parameter uniqueID="UID_CH2_Status" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<UINT/>
					</parameter>
					<parameter uniqueID="UID_CH1_Preset_Period" access="readWrite">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<DINT/>	
					</parameter>
					<parameter uniqueID="UID_CH2_Preset_Period" access="readWrite">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<DINT/>	
					</parameter>
					<parameter uniqueID="UID_CH1_Control_Word" access="readWrite">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<UINT/>	
					</parameter>
					<parameter uniqueID="UID_CH2_Control_Word" access="readWrite">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<UINT/>	
					</parameter>
					<parameter uniqueID="UID_Par_1_Sel" templateIDRef="TID_Par_Filter_Select_Type">
						<label lang="en">Input filter</label>
						<label lang="de">Eingangsfilter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_2_Sel" templateIDRef="TID_Par_Filter_Select_Type">
						<label lang="en">Input filter</label>
						<label lang="de">Eingangsfilter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_1" templateIDRef="TID_Par_Filter_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_Par_2" templateIDRef="TID_Par_Filter_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
				</parameterList>
				<parameterGroupList>
					<parameterGroup uniqueID="UID_Channel1_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 01</label>
						<label lang="de">Kanal 01</label>
						<parameterRef uniqueIDRef="UID_Par_1_Sel" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel2_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 02</label>
						<label lang="de">Kanal 02</label>
						<parameterRef uniqueIDRef="UID_Par_2_Sel" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_1_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_1_Map1" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="5 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map2" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="11 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map3" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="21 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map4" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="43 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map5" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="83 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map6" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="167 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map7" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="333 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="6" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map8" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="667 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="7" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map9" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="1 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="8" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map10" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="9" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map11" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="5 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="10" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map12" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="11 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="11" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map13" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="22 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="12" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map14" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="43 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="13" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map15" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="91 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="14" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map16" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="167 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="15" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map17" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="333 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="16" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_2_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_2_Map1" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="5 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map2" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="11 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map3" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="21 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map4" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="43 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map5" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="83 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map6" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="167 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map7" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="333 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="6" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map8" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="667 us" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="7" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map9" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="1 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="8" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map10" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="9" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map11" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="5 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="10" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map12" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="11 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="11" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map13" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="22 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="12" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map14" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="43 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="13" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map15" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="91 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="14" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map16" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="167 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="15" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map17" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="333 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="16" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_InputImage_Map" bitOffset="0">
						<label lang="en">InputImages</label>
						<label lang="de">InputImages</label>
						<parameterRef uniqueIDRef="UID_CH1_Current_Period" visible="true" bitOffset="0"/>
						<parameterRef uniqueIDRef="UID_CH1_Rising_Edges" visible="true" bitOffset="32"/>
						<parameterRef uniqueIDRef="UID_CH2_Current_Period" visible="true" bitOffset="64"/>
						<parameterRef uniqueIDRef="UID_CH2_Rising_Edges" visible="true" bitOffset="96"/>
						<parameterRef uniqueIDRef="UID_CH1_Status" visible="true" bitOffset="128"/>
						<parameterRef uniqueIDRef="UID_CH2_Status" visible="true" bitOffset="144"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_OutputImage_Map" bitOffset="0">
						<label lang="en">OutputImages</label>
						<label lang="de">OutputImages</label>
						<parameterRef uniqueIDRef="UID_CH1_Preset_Period" visible="true" bitOffset="0"/>
						<parameterRef uniqueIDRef="UID_CH2_Preset_Period" visible="true" bitOffset="32"/>
						<parameterRef uniqueIDRef="UID_CH1_Control_Word" visible="true" bitOffset="64"/>
						<parameterRef uniqueIDRef="UID_CH2_Control_Word" visible="true" bitOffset="80"/>
					</parameterGroup>
				</parameterGroupList>
			</ApplicationProcess>
		</ProfileBody>
	</ISO15745Profile>
	<ISO15745Profile>
		<ProfileHeader>
			<ProfileIdentification>EPL_Device_Profile_UR20_2FCNT_100</ProfileIdentification>
			<ProfileRevision>1</ProfileRevision>
			<ProfileName>Ethernet POWERLINK UR20-2FCNT-100 device profile</ProfileName>
			<ProfileSource/>
			<ProfileClassID>CommunicationNetwork</ProfileClassID>
			<ISO15745Reference>
				<ISO15745Part>4</ISO15745Part>
				<ISO15745Edition>1</ISO15745Edition>
				<ProfileTechnology>Powerlink</ProfileTechnology>
			</ISO15745Reference>
		</ProfileHeader>
		<ProfileBody xsi:type="ProfileBody_CommunicationNetwork_Powerlink_Modular_Child" fileName="00000230_UR20_2FCNT_100.XDD" fileCreator="Weidmüller" fileCreationDate="2015-10-20" fileCreationTime="15:00:00+01:00" fileModificationDate="2015-10-20" fileModificationTime="16:23:37.341+02:00" fileModifiedBy="Weidmüller" fileVersion="V01.00" supportedLanguages="en de" specificationVersion="1.2.0">
			<ApplicationLayers>
				<identity>
					<vendorID>0x00000230</vendorID>
					<deviceFamily>
						<label lang="en">UR20</label>
						<label lang="de">UR20</label>
					</deviceFamily>
					<productID>1508080000</productID>
				</identity>
				<ObjectList>
					<Object index="4600" name="ModuleInputImages" objectType="9" rangeSelector="ModuleInputImages">
						<SubObject subIndex="00" name="InputImage" objectType="7" dataType="000F" accessType="ro" PDOmapping="TPDO" uniqueIDRef="UID_InputImage_Map"/>
					</Object>
					<Object index="4700" name="ModuleOutputImages" objectType="9" rangeSelector="ModuleOutputImages">
						<SubObject subIndex="00" name="OutputImage" objectType="7" dataType="000F" accessType="wo" PDOmapping="RPDO" uniqueIDRef="UID_OutputImage_Map"/>
					</Object>
					
					<Object index="3100" name="ModuleParameter1" objectType="9" rangeSelector="ModuleParameter1">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_1_Map"/>
					</Object>
					
					<Object index="3101" name="ModuleParameter2" objectType="9" rangeSelector="ModuleParameter2">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_2_Map"/>
					</Object>
				</ObjectList>
			</ApplicationLayers>
			<TransportLayers/>
		</ProfileBody>
	</ISO15745Profile>
</ISO15745ProfileContainer>
