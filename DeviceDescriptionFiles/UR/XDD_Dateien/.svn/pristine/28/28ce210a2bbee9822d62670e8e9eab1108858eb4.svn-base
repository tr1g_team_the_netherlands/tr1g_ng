<?xml version="1.0" encoding="UTF-8"?>
<ISO15745ProfileContainer xmlns="http://www.ethernet-powerlink.org" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ethernet-powerlink.org ../Powerlink_Main.xsd">
	<ISO15745Profile>
		<ProfileHeader>
			<ProfileIdentification>EPL_Device_Profile_UR20_8DI_P_3W</ProfileIdentification>
			<ProfileRevision>1</ProfileRevision>
			<ProfileName>Ethernet POWERLINK UR20-8DI-P-3W device profile</ProfileName>
			<ProfileSource/>
			<ProfileClassID>Device</ProfileClassID>
			<ISO15745Reference>
				<ISO15745Part>4</ISO15745Part>
				<ISO15745Edition>1</ISO15745Edition>
				<ProfileTechnology>Powerlink</ProfileTechnology>
			</ISO15745Reference>
		</ProfileHeader>
		<ProfileBody xsi:type="ProfileBody_Device_Powerlink_Modular_Child" 	fileName="00000230_UR20_8DI_P_3W.XDD" 	fileCreator="Weidmüller" 	fileCreationDate="2016-05-31" 	fileCreationTime="15:00:00+01:00" 	fileModificationDate="2015-10-20" 	fileModificationTime="16:23:37.341+02:00" 	fileModifiedBy="Weidmüller" 	fileVersion="V01.00" 	supportedLanguages="en de" specificationVersion="1.2.0">
			<DeviceIdentity>
				<vendorName>Weidmüller</vendorName>
				<vendorID>0x00000230</vendorID>
				<vendorText>
					<label lang="de">Bei Problemen kontaktieren sie unseren Support: ++49(..</label>
					<label lang="en">Experiencing problems - contact our support : ++1(..</label>
				</vendorText>
				<deviceFamily>
					<label lang="en">Modular I/O system</label>
					<label lang="de">Modulares I/O System</label>
				</deviceFamily>
				<productName>UR20-8DI-P-3W / 1394400000</productName>
				<productID>1394400000</productID>
				<productText>
					<label lang="en">IO-MODUL 8 DIGITAL IN SOURCE</label>
					<label lang="de">IO-MODUL 8 DIGITAL IN SOURCE</label>
				</productText>
				<orderNumber>1394400000</orderNumber>
				<buildDate>2016-05-31</buildDate>
			</DeviceIdentity>
			<DeviceManager>
				<moduleManagement>
					<moduleInterface childID="UR20_8DI_P_3W" type="UR20" moduleAddressing="position">
						<label lang="en-us">UR20 interface</label>
						<fileList>
							<file URI="00000230_*.xdd"/>
						</fileList>
						<moduleTypeList>
							<moduleType uniqueID="UR20_8DI_P_3W_out" type="UR20" />
						</moduleTypeList>
					</moduleInterface>
				</moduleManagement>
			</DeviceManager>
			<DeviceFunction>
				<capabilities>
					<characteristicsList>
						<characteristic>
							<characteristicName>
								<label lang="en">Input/Output</label>
								<label lang="de">Ein-/Ausgänge</label>
							</characteristicName>
							<characteristicContent>
								<label lang="en">UR20 IO System</label>
								<label lang="de">UR20 IO System</label>
							</characteristicContent>
						</characteristic>
					</characteristicsList>
				</capabilities>
				<picturesList>
					<picture URI="UR20_8DI_P_3W.png" type="frontPicture">
						<label lang="en">FrontFoto</label>
						<label lang="de">Frontfoto</label>
					</picture>
					<picture URI="UR20.ico" type="icon">
						<label lang="en">Icon</label>
						<label lang="de">Pictogramm</label>
					</picture>
				</picturesList>
				<connectorList>
					<connector id="UR20_IF1" connectorType="UR20" interfaceIDRef="UR20_8DI_P_3W" positioning="localLeft" >
						<label lang="en-us">Left UR20 interface</label>
					</connector>
					<connector id="UR20_IF2" connectorType="UR20" interfaceIDRef="UR20_8DI_P_3W_out" positioning="localRight" >
						<label lang="en-us">Right UR20 interface</label>
					</connector>
				</connectorList>
				<classificationList>
					<classification>IO</classification>
					<classification>Digital</classification>
					<classification>Input</classification>
				</classificationList>
			</DeviceFunction>
			<ApplicationProcess>
				<templateList>
				<parameterTemplate uniqueID="TID_Par_Delay_Select_Type" access="readWrite">
						<label lang="en">Input delay</label>
						<label lang="de">Eingangsverzögerung</label>
						<STRING/>
						<defaultValue value="3 ms"/>
						<allowedValues>
							<value value="Nein">
								<label lang="en">No</label>
								<label lang="de">Nein</label>
							</value>
							<value value="0,3 ms">
								<label lang="en">0,3 ms</label>
								<label lang="de">0,3 ms</label>
							</value>
							<value value="3 ms">
								<label lang="en">3 ms</label>
								<label lang="de">3 ms</label>
							</value>
							<value value="10 ms">
								<label lang="en">10 ms</label>
								<label lang="de">10 ms</label>
							</value>
							<value value="20 ms">
								<label lang="en">20 ms</label>
								<label lang="de">20 ms</label>
							</value>
							<value value="40 ms">
								<label lang="en">40 ms</label>
								<label lang="de">40 ms</label>
							</value>
						</allowedValues>
					</parameterTemplate>
					<parameterTemplate uniqueID="TID_Par_Delay_Data_Type" access="readWrite">
						<description lang="en">Data Type</description>
						<description lang="de">Data Type</description>
						<DINT/>
						<allowedValues>
							<range>
								<minValue value="0"/>
								<maxValue value="5"/>
							</range>
						</allowedValues>
					</parameterTemplate>
				</templateList>
				<parameterList>
					<parameter uniqueID="UID_DigitalInput01" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 11" value="DI-01"/>
					</parameter>
					<parameter uniqueID="UID_DigitalInput02" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 21" value="DI-02"/>
					</parameter>
					<parameter uniqueID="UID_DigitalInput03" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 12" value="DI-03"/>
					</parameter>
					<parameter uniqueID="UID_DigitalInput04" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 22" value="DI-04"/>
					</parameter>
					<parameter uniqueID="UID_DigitalInput05" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 13" value="DI-05"/>
					</parameter>
					<parameter uniqueID="UID_DigitalInput06" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 23" value="DI-06"/>
					</parameter>
					<parameter uniqueID="UID_DigitalInput07" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 14" value="DI-07"/>
					</parameter>
					<parameter uniqueID="UID_DigitalInput08" access="read">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 24" value="DI-08"/>
					</parameter>
					<parameter uniqueID="UID_DigitalInput" access="read">
						<description lang="en">Packed digital inputs; 24 VDC</description>
						<description lang="de">Gepackte digitale Eingänge; 24 VDC</description>
						<USINT/>
						<property name="pin 11" value="DI-01"/>
						<property name="pin 21" value="DI-02"/>
						<property name="pin 12" value="DI-03"/>
						<property name="pin 22" value="DI-04"/>
						<property name="pin 13" value="DI-05"/>
						<property name="pin 23" value="DI-06"/>
						<property name="pin 14" value="DI-07"/>
						<property name="pin 24" value="DI-08"/>
					</parameter>
					<parameter uniqueID="UID_DigitalInputsPacked" access="readWrite">
						<label lang="en">Packed inputs</label>
						<label lang="de">Gepackte Eingänge</label>
						<description lang="en">Packed I/O data instead of single digital inputs</description>
						<description lang="de">Gepackte I/O Daten statt einzelne digitale Eingänge</description>
						<STRING/>
						<defaultValue value="off"/>
						<allowedValues>
							<value value="off">
								<label lang="en">off</label>
								<label lang="de">Aus</label>
							</value>
							<value value="on">
								<label lang="en">on</label>
								<label lang="de">Ein</label>
							</value>
						</allowedValues>
					</parameter>
					<parameter uniqueID="UID_Par_1_Sel" templateIDRef="TID_Par_Delay_Select_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_2_Sel" templateIDRef="TID_Par_Delay_Select_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_3_Sel" templateIDRef="TID_Par_Delay_Select_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_4_Sel" templateIDRef="TID_Par_Delay_Select_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_5_Sel" templateIDRef="TID_Par_Delay_Select_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_6_Sel" templateIDRef="TID_Par_Delay_Select_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_7_Sel" templateIDRef="TID_Par_Delay_Select_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_8_Sel" templateIDRef="TID_Par_Delay_Select_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<STRING/>
					</parameter>
					<parameter uniqueID="UID_Par_1" templateIDRef="TID_Par_Delay_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_Par_2" templateIDRef="TID_Par_Delay_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_Par_3" templateIDRef="TID_Par_Delay_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_Par_4" templateIDRef="TID_Par_Delay_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_Par_5" templateIDRef="TID_Par_Delay_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_Par_6" templateIDRef="TID_Par_Delay_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_Par_7" templateIDRef="TID_Par_Delay_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
					<parameter uniqueID="UID_Par_8" templateIDRef="TID_Par_Delay_Data_Type">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<DINT/>
					</parameter>
				</parameterList>
				<parameterGroupList>
					<parameterGroup uniqueID="UID_PG_General" kindOfAccess="Properties" configParameter="true">
						<label lang="en">General</label>
						<label lang="de">Allgemein</label>
						<parameterRef uniqueIDRef="UID_DigitalInputsPacked" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel1_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 01</label>
						<label lang="de">Kanal 01</label>
						<parameterRef uniqueIDRef="UID_Par_1_Sel" actualValue="3 ms" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel2_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 02</label>
						<label lang="de">Kanal 02</label>
						<parameterRef uniqueIDRef="UID_Par_2_Sel" actualValue="3 ms" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel3_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 03</label>
						<label lang="de">Kanal 03</label>
						<parameterRef uniqueIDRef="UID_Par_3_Sel" actualValue="3 ms" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel4_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 04</label>
						<label lang="de">Kanal 04</label>
						<parameterRef uniqueIDRef="UID_Par_4_Sel" actualValue="3 ms" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel5_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 05</label>
						<label lang="de">Kanal 05</label>
						<parameterRef uniqueIDRef="UID_Par_5_Sel" actualValue="3 ms" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel6_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 06</label>
						<label lang="de">Kanal 06</label>
						<parameterRef uniqueIDRef="UID_Par_6_Sel" actualValue="3 ms" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel7_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 07</label>
						<label lang="de">Kanal 07</label>
						<parameterRef uniqueIDRef="UID_Par_7_Sel" actualValue="3 ms" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel8_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 08</label>
						<label lang="de">Kanal 08</label>
						<parameterRef uniqueIDRef="UID_Par_8_Sel" actualValue="3 ms" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_InputImage_Map" bitOffset="0">
						<label lang="en">InputImages</label>
						<label lang="de">InputImages</label>
						<parameterGroup uniqueID="UID_InputImage_PackedOff" conditionalUniqueIDRef="UID_DigitalInputsPacked" conditionalValue="off" bitOffset="0">
							<label lang="en">InputImage digital inputs loose</label>
							<label lang="de">InputImage Digitaleingang vereinzelt</label>
							<parameterRef uniqueIDRef="UID_DigitalInput01" visible="true" bitOffset="0"/>
							<parameterRef uniqueIDRef="UID_DigitalInput02" visible="true" bitOffset="1"/>
							<parameterRef uniqueIDRef="UID_DigitalInput03" visible="true" bitOffset="2"/>
							<parameterRef uniqueIDRef="UID_DigitalInput04" visible="true" bitOffset="3"/>
							<parameterRef uniqueIDRef="UID_DigitalInput05" visible="true" bitOffset="4"/>
							<parameterRef uniqueIDRef="UID_DigitalInput06" visible="true" bitOffset="5"/>
							<parameterRef uniqueIDRef="UID_DigitalInput07" visible="true" bitOffset="6"/>
							<parameterRef uniqueIDRef="UID_DigitalInput08" visible="true" bitOffset="7"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_InputImage_PackedOn" conditionalUniqueIDRef="UID_DigitalInputsPacked" conditionalValue="on" bitOffset="0">
							<label lang="en">InputImage digital inputs packed </label>
							<label lang="de">InputImage Digitaleingang gepackt</label>
							<parameterRef uniqueIDRef="UID_DigitalInput" visible="true" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_1_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_1_Map1" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="Nein" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map2" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="0,3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map3" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map4" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="10 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map5" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="20 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map6" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="40 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_2_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_2_Map1" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="Nein" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map2" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="0,3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map3" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map4" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="10 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map5" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="20 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map6" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="40 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_3_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_3_Map1" conditionalUniqueIDRef="UID_Par_3_Sel" conditionalValue="Nein" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_3" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_3_Map2" conditionalUniqueIDRef="UID_Par_3_Sel" conditionalValue="0,3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_3" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_3_Map3" conditionalUniqueIDRef="UID_Par_3_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_3" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_3_Map4" conditionalUniqueIDRef="UID_Par_3_Sel" conditionalValue="10 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_3" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_3_Map5" conditionalUniqueIDRef="UID_Par_3_Sel" conditionalValue="20 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_3" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_3_Map6" conditionalUniqueIDRef="UID_Par_3_Sel" conditionalValue="40 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_3" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_4_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_4_Map1" conditionalUniqueIDRef="UID_Par_4_Sel" conditionalValue="Nein" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_4" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_4_Map2" conditionalUniqueIDRef="UID_Par_4_Sel" conditionalValue="0,3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_4" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_4_Map3" conditionalUniqueIDRef="UID_Par_4_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_4" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_4_Map4" conditionalUniqueIDRef="UID_Par_4_Sel" conditionalValue="10 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_4" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_4_Map5" conditionalUniqueIDRef="UID_Par_4_Sel" conditionalValue="20 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_4" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_4_Map6" conditionalUniqueIDRef="UID_Par_4_Sel" conditionalValue="40 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_4" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_5_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_5_Map1" conditionalUniqueIDRef="UID_Par_5_Sel" conditionalValue="Nein" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_5" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_5_Map2" conditionalUniqueIDRef="UID_Par_5_Sel" conditionalValue="0,3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_5" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_5_Map3" conditionalUniqueIDRef="UID_Par_5_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_5" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_5_Map4" conditionalUniqueIDRef="UID_Par_5_Sel" conditionalValue="10 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_5" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_5_Map5" conditionalUniqueIDRef="UID_Par_5_Sel" conditionalValue="20 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_5" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_5_Map6" conditionalUniqueIDRef="UID_Par_5_Sel" conditionalValue="40 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_5" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_6_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_6_Map1" conditionalUniqueIDRef="UID_Par_6_Sel" conditionalValue="Nein" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_6" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_6_Map2" conditionalUniqueIDRef="UID_Par_6_Sel" conditionalValue="0,3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_6" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_6_Map3" conditionalUniqueIDRef="UID_Par_6_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_6" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_6_Map4" conditionalUniqueIDRef="UID_Par_6_Sel" conditionalValue="10 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_6" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_6_Map5" conditionalUniqueIDRef="UID_Par_6_Sel" conditionalValue="20 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_6" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_6_Map6" conditionalUniqueIDRef="UID_Par_6_Sel" conditionalValue="40 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_6" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_7_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_7_Map1" conditionalUniqueIDRef="UID_Par_7_Sel" conditionalValue="Nein" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_7" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_7_Map2" conditionalUniqueIDRef="UID_Par_7_Sel" conditionalValue="0,3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_7" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_7_Map3" conditionalUniqueIDRef="UID_Par_7_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_7" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_7_Map4" conditionalUniqueIDRef="UID_Par_7_Sel" conditionalValue="10 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_7" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_7_Map5" conditionalUniqueIDRef="UID_Par_7_Sel" conditionalValue="20 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_7" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_7_Map6" conditionalUniqueIDRef="UID_Par_7_Sel" conditionalValue="40 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_7" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_8_Map" bitOffset="0">
						<label lang="en">Parameter</label>
						<label lang="de">Parameter</label>
						<parameterGroup uniqueID="UID_Par_8_Map1" conditionalUniqueIDRef="UID_Par_8_Sel" conditionalValue="Nein" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_8" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_8_Map2" conditionalUniqueIDRef="UID_Par_8_Sel" conditionalValue="0,3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_8" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_8_Map3" conditionalUniqueIDRef="UID_Par_8_Sel" conditionalValue="3 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_8" actualValue="2" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_8_Map4" conditionalUniqueIDRef="UID_Par_8_Sel" conditionalValue="10 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_8" actualValue="3" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_8_Map5" conditionalUniqueIDRef="UID_Par_8_Sel" conditionalValue="20 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_8" actualValue="4" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_8_Map6" conditionalUniqueIDRef="UID_Par_8_Sel" conditionalValue="40 ms" groupLevelVisible="false" bitOffset="0">
							<label lang="en">Parameter</label>
							<label lang="de">Parameter</label>
							<parameterRef uniqueIDRef="UID_Par_8" actualValue="5" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
				</parameterGroupList>
			</ApplicationProcess>
		</ProfileBody>
	</ISO15745Profile>
	<ISO15745Profile>
		<ProfileHeader>
			<ProfileIdentification>EPL_Device_Profile_UR20_8DI_P_3W</ProfileIdentification>
			<ProfileRevision>1</ProfileRevision>
			<ProfileName>Ethernet POWERLINK UR20-8DI-P-3W device profile</ProfileName>
			<ProfileSource/>
			<ProfileClassID>CommunicationNetwork</ProfileClassID>
			<ISO15745Reference>
				<ISO15745Part>4</ISO15745Part>
				<ISO15745Edition>1</ISO15745Edition>
				<ProfileTechnology>Powerlink</ProfileTechnology>
			</ISO15745Reference>
		</ProfileHeader>
		<ProfileBody xsi:type="ProfileBody_CommunicationNetwork_Powerlink_Modular_Child" 	fileName="00000230_UR20_8DI_P_3W.XDD" 	fileCreator="Weidmüller" 	fileCreationDate="2015-10-20" 	fileCreationTime="15:00:00+01:00" 	fileModificationDate="2015-10-20" 	fileModificationTime="16:23:37.341+02:00" 	fileModifiedBy="Weidmüller" 	fileVersion="V01.00" 	supportedLanguages="en de" specificationVersion="1.2.0">
			<ApplicationLayers>
				<identity>
					<vendorID>0x00000230</vendorID>
					<deviceFamily>
						<label lang="en">UR20</label>
						<label lang="de">UR20</label>
					</deviceFamily>
					<productID>1394400000</productID>
				</identity>
				<ObjectList>
					<Object index="4600" name="ModuleInputImages" objectType="9" rangeSelector="ModuleInputImages">
						<SubObject subIndex="00" name="InputImage" objectType="7" dataType="000F" accessType="ro" PDOmapping="TPDO" uniqueIDRef="UID_InputImage_Map"/>
					</Object>
					
					<Object index="3100" name="ModuleParameter1" objectType="9" rangeSelector="ModuleParameter1">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_1_Map"/>
					</Object>
					
					<Object index="3101" name="ModuleParameter2" objectType="9" rangeSelector="ModuleParameter2">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_2_Map"/>
					</Object>
					
					<Object index="3102" name="ModuleParameter3" objectType="9" rangeSelector="ModuleParameter3">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_3_Map"/>
					</Object>
					
					<Object index="3103" name="ModuleParameter4" objectType="9" rangeSelector="ModuleParameter4">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_4_Map"/>
					</Object>
					
					<Object index="3104" name="ModuleParameter5" objectType="9" rangeSelector="ModuleParameter5">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_5_Map"/>
					</Object>
					
					<Object index="3105" name="ModuleParameter6" objectType="9" rangeSelector="ModuleParameter6">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_6_Map"/>
					</Object>
					
					<Object index="3106" name="ModuleParameter7" objectType="9" rangeSelector="ModuleParameter7">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_7_Map"/>
					</Object>
					
					<Object index="3107" name="ModuleParameter8" objectType="9" rangeSelector="ModuleParameter8">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_8_Map"/>
					</Object>
				</ObjectList>
			</ApplicationLayers>
			<TransportLayers/>
		</ProfileBody>
	</ISO15745Profile>
</ISO15745ProfileContainer>
