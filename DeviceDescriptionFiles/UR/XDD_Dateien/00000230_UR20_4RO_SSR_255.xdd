<?xml version="1.0" encoding="UTF-8"?>
<ISO15745ProfileContainer xmlns="http://www.ethernet-powerlink.org" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ethernet-powerlink.org ../Powerlink_Main.xsd">
	<ISO15745Profile>
		<ProfileHeader>
			<ProfileIdentification>EPL_Device_Profile_UR20_4RO_SSR_255</ProfileIdentification>
			<ProfileRevision>1</ProfileRevision>
			<ProfileName>Ethernet POWERLINK UR20-4RO-SSR-255 device profile</ProfileName>
			<ProfileSource/>
			<ProfileClassID>Device</ProfileClassID>
			<ISO15745Reference>
				<ISO15745Part>4</ISO15745Part>
				<ISO15745Edition>1</ISO15745Edition>
				<ProfileTechnology>Powerlink</ProfileTechnology>
			</ISO15745Reference>
		</ProfileHeader>
		<ProfileBody xsi:type="ProfileBody_Device_Powerlink_Modular_Child" fileName="00000230_UR20_4RO_SSR_255.XDD" fileCreator="Weidmüller" fileCreationDate="2015-05-31" fileCreationTime="15:00:00+01:00" fileModificationDate="2015-10-20" fileModificationTime="16:23:37.341+02:00" fileModifiedBy="Weidmüller" fileVersion="V01.00" supportedLanguages="en de" specificationVersion="1.0.4">
			<DeviceIdentity>
				<vendorName>Weidmüller</vendorName>
				<vendorID>0x00000230</vendorID>
				<vendorText>
					<label lang="de">Bei Problemen kontaktieren sie unseren Support: ++49(..</label>
					<label lang="en">Experiencing problems - contact our support : ++1(..</label>
				</vendorText>
				<deviceFamily>
					<label lang="en">Modular I/O system</label>
					<label lang="de">Modulares I/O System</label>
				</deviceFamily>
				<productName>UR20-4RO-SSR-255 / 1315540000</productName>
				<productID>1315540000</productID>
				<productText>
					<label lang="en">IO-MODUL 4 DIGITAL OUT OPTO COUPLER NO</label>
					<label lang="de">IO-MODUL 4 DIGITAL OUT OPTO COUPLER NO</label>
				</productText>
				<orderNumber>1315540000</orderNumber>
				<buildDate>2016-05-31</buildDate>
			</DeviceIdentity>
			<DeviceManager>
				<moduleManagement>
					<moduleInterface childID="UR20_4RO_SSR_255" type="UR20" moduleAddressing="position">
						<label lang="en-us">UR20 interface</label>
						<fileList>
							<file URI="00000230_*.xdd"/>
						</fileList>
						<moduleTypeList>
							<moduleType uniqueID="UR20_4RO_SSR_255_out" type="UR20" />
						</moduleTypeList>
					</moduleInterface>
				</moduleManagement>
			</DeviceManager>
			<DeviceFunction>
				<capabilities>
					<characteristicsList>
						<characteristic>
							<characteristicName>
								<label lang="en">Input/Output</label>
								<label lang="de">Ein-/Ausgänge</label>
							</characteristicName>
							<characteristicContent>
								<label lang="en">UR20 IO System</label>
								<label lang="de">UR20 IO System</label>
							</characteristicContent>
						</characteristic>
					</characteristicsList>
				</capabilities>
				<picturesList>
					<picture URI="UR20_4RO_SSR_255.png" type="frontPicture">
						<label lang="en">FrontFoto</label>
						<label lang="de">Frontfoto</label>
					</picture>
					<picture URI="UR20.ico" type="icon">
						<label lang="en">Icon</label>
						<label lang="de">Pictogramm</label>
					</picture>
				</picturesList>
				<connectorList>
					<connector id="UR20_IF1" connectorType="UR20" interfaceIDRef="UR20_4RO_SSR_255" positioning="localLeft" >
						<label lang="en-us">Left UR20 interface</label>
					</connector>
					<connector id="UR20_IF2" connectorType="UR20" interfaceIDRef="UR20_4RO_SSR_255_out" positioning="localRight" >
						<label lang="en-us">Right UR20 interface</label>
					</connector>
				</connectorList>
				<classificationList>
					<classification>IO</classification>
					<classification>Digital</classification>
					<classification>Output</classification>
				</classificationList>
			</DeviceFunction>
			<ApplicationProcess>
				<templateList>
				<parameterTemplate uniqueID="TID_Par_Substitude_Select_Type" access="readWrite">
						<label lang="en">Sustitude value</label>
						<label lang="de">Fehlerersatzwert</label>
						<STRING/>
						<defaultValue value="Aus"/>
						<allowedValues>
							<value value="Aus">
								<label lang="en">Off</label>
								<label lang="de">Aus</label>
							</value>
							<value value="Ein">
								<label lang="en">On</label>
								<label lang="de">Ein</label>
							</value>
						</allowedValues>
					</parameterTemplate>
					<parameterTemplate uniqueID="TID_Par_Substitude_Data_Type" access="readWrite">
						<label lang="en">Data Type</label>
						<label lang="de">Data Type</label>
						<description lang="en">Data Type</description>
						<description lang="de">Data Type</description>
						<DINT/>
						<allowedValues>
							<range>
								<minValue value="0"/>
								<maxValue value="1"/>
							</range>
						</allowedValues>
					</parameterTemplate>
				</templateList>
				<parameterList>
					<parameter uniqueID="UID_DigitalOutput01" access="readWrite">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 11" value="DO-01"/>
					</parameter>
					<parameter uniqueID="UID_DigitalOutput02" access="readWrite">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 21" value="DO-02"/>
					</parameter>
					<parameter uniqueID="UID_DigitalOutput03" access="readWrite">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 12" value="DO-03"/>
					</parameter>
					<parameter uniqueID="UID_DigitalOutput04" access="readWrite">
						<description lang="en">24 VDC</description>
						<description lang="de">24 VDC</description>
						<BOOL/>
						<property name="pin 22" value="DO-04"/>
					</parameter>
					<parameter uniqueID="UID_DigitalOutput" access="readWrite">
						<description lang="en">Packed digital outputs; 24 VDC</description>
						<description lang="de">Gepackte digitale Ausgänge; 24 VDC</description>
						<USINT/>
						<property name="pin 11" value="DO-01"/>
						<property name="pin 21" value="DO-02"/>
						<property name="pin 12" value="DO-03"/>
						<property name="pin 22" value="DO-04"/>
					</parameter>
					<parameter uniqueID="UID_DigitalOutputsPacked" access="readWrite">
						<label lang="en">Packed outputs</label>
						<label lang="de">Gepackte Ausgänge</label>
						<description lang="en">Packed I/O data instead of single digital inputs</description>
						<description lang="de">Gepackte I/O Daten statt einzelne digitale Eingänge</description>
						<STRING/>
						<defaultValue value="off"/>
						<allowedValues>
							<value value="off">
								<label lang="en">off</label>
								<label lang="de">Aus</label>
							</value>
							<value value="on">
								<label lang="en">on</label>
								<label lang="de">Ein</label>
							</value>
						</allowedValues>
					</parameter>
					<parameter uniqueID="UID_Par_1_Sel" templateIDRef="TID_Par_Substitude_Select_Type"/>
					<parameter uniqueID="UID_Par_2_Sel" templateIDRef="TID_Par_Substitude_Select_Type"/>
					<parameter uniqueID="UID_Par_3_Sel" templateIDRef="TID_Par_Substitude_Select_Type"/>
					<parameter uniqueID="UID_Par_4_Sel" templateIDRef="TID_Par_Substitude_Select_Type"/>
					<parameter uniqueID="UID_Par_1" templateIDRef="TID_Par_Substitude_Data_Type"/>
					<parameter uniqueID="UID_Par_2" templateIDRef="TID_Par_Substitude_Data_Type"/>
					<parameter uniqueID="UID_Par_3" templateIDRef="TID_Par_Substitude_Data_Type"/>
					<parameter uniqueID="UID_Par_4" templateIDRef="TID_Par_Substitude_Data_Type"/>
				</parameterList>
				<parameterGroupList>
					<parameterGroup uniqueID="UID_PG_General" kindOfAccess="Properties" configParameter="true">
						<label lang="en">General</label>
						<label lang="de">Allgemein</label>
						<parameterRef uniqueIDRef="UID_DigitalOutputsPacked" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel1_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 01</label>
						<label lang="de">Kanal 01</label>
						<parameterRef uniqueIDRef="UID_Par_1_Sel" actualValue="Aus" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel2_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 02</label>
						<label lang="de">Kanal 02</label>
						<parameterRef uniqueIDRef="UID_Par_2_Sel" actualValue="Aus" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel3_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 03</label>
						<label lang="de">Kanal 03</label>
						<parameterRef uniqueIDRef="UID_Par_3_Sel" actualValue="Aus" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Channel4_Par" kindOfAccess="Properties" configParameter="true" groupLevelVisible="true">
						<label lang="en">Channel 04</label>
						<label lang="de">Kanal 04</label>
						<parameterRef uniqueIDRef="UID_Par_4_Sel" actualValue="Aus" visible="true"/>
					</parameterGroup>
					<parameterGroup uniqueID="UID_OutputImage_Map" bitOffset="0">
						<label lang="en">OutputImages</label>
						<label lang="de">OutputImages</label>
						<parameterGroup uniqueID="UID_OutputImage_PackedOff" conditionalUniqueIDRef="UID_DigitalOutputsPacked" conditionalValue="off" bitOffset="0">
							<label lang="en">OutputImage digital outputs loose</label>
							<label lang="de">OutputImage Digitalausgang vereinzelt</label>
							<parameterRef uniqueIDRef="UID_DigitalOutput01" visible="true" bitOffset="0"/>
							<parameterRef uniqueIDRef="UID_DigitalOutput02" visible="true" bitOffset="1"/>
							<parameterRef uniqueIDRef="UID_DigitalOutput03" visible="true" bitOffset="2"/>
							<parameterRef uniqueIDRef="UID_DigitalOutput04" visible="true" bitOffset="3"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_OutputImage_PackedOn" conditionalUniqueIDRef="UID_DigitalOutputsPacked" conditionalValue="on" bitOffset="0">
							<label lang="en">OutputImage digital outputs packed </label>
							<label lang="de">OutputImage Digitalausgang gepackt</label>
							<parameterRef uniqueIDRef="UID_DigitalOutput" visible="true" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>	
					<parameterGroup uniqueID="UID_Par_1_Map" bitOffset="0">
						<parameterGroup uniqueID="UID_Par_1_Map1" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="Aus" groupLevelVisible="false" bitOffset="0">
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_1_Map2" conditionalUniqueIDRef="UID_Par_1_Sel" conditionalValue="Ein" groupLevelVisible="false" bitOffset="0">
							<parameterRef uniqueIDRef="UID_Par_1" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_2_Map" bitOffset="0">
						<parameterGroup uniqueID="UID_Par_2_Map1" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="Aus" groupLevelVisible="false" bitOffset="0">
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_2_Map2" conditionalUniqueIDRef="UID_Par_2_Sel" conditionalValue="Ein" groupLevelVisible="false" bitOffset="0">
							<parameterRef uniqueIDRef="UID_Par_2" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_3_Map" bitOffset="0">
						<parameterGroup uniqueID="UID_Par_3_Map1" conditionalUniqueIDRef="UID_Par_3_Sel" conditionalValue="Aus" groupLevelVisible="false" bitOffset="0">
							<parameterRef uniqueIDRef="UID_Par_3" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_3_Map2" conditionalUniqueIDRef="UID_Par_3_Sel" conditionalValue="Ein" groupLevelVisible="false" bitOffset="0">
							<parameterRef uniqueIDRef="UID_Par_3" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
					<parameterGroup uniqueID="UID_Par_4_Map" bitOffset="0">
						<parameterGroup uniqueID="UID_Par_4_Map1" conditionalUniqueIDRef="UID_Par_4_Sel" conditionalValue="Aus" groupLevelVisible="false" bitOffset="0">
							<parameterRef uniqueIDRef="UID_Par_4" actualValue="0" visible="false" bitOffset="0"/>
						</parameterGroup>
						<parameterGroup uniqueID="UID_Par_4_Map2" conditionalUniqueIDRef="UID_Par_4_Sel" conditionalValue="Ein" groupLevelVisible="false" bitOffset="0">
							<parameterRef uniqueIDRef="UID_Par_4" actualValue="1" visible="false" bitOffset="0"/>
						</parameterGroup>
					</parameterGroup>
				</parameterGroupList>
			</ApplicationProcess>
		</ProfileBody>
	</ISO15745Profile>
	<ISO15745Profile>
		<ProfileHeader>
			<ProfileIdentification>EPL_Device_Profile_UR20_4RO_SSR_255</ProfileIdentification>
			<ProfileRevision>1</ProfileRevision>
			<ProfileName>Ethernet POWERLINK UR20-4RO-SSR-255 device profile</ProfileName>
			<ProfileSource/>
			<ProfileClassID>CommunicationNetwork</ProfileClassID>
			<ISO15745Reference>
				<ISO15745Part>4</ISO15745Part>
				<ISO15745Edition>1</ISO15745Edition>
				<ProfileTechnology>Powerlink</ProfileTechnology>
			</ISO15745Reference>
		</ProfileHeader>
		<ProfileBody xsi:type="ProfileBody_CommunicationNetwork_Powerlink_Modular_Child" fileName="00000230_UR20_4RO_SSR_255.XDD" fileCreator="Weidmüller" fileCreationDate="2015-10-20" fileCreationTime="15:00:00+01:00" fileModificationDate="2015-10-20" fileModificationTime="16:23:37.341+02:00" fileModifiedBy="Weidmüller" fileVersion="V01.00" supportedLanguages="en de" specificationVersion="1.0.4">
			<ApplicationLayers>
				<identity>
					<vendorID>0x00000230</vendorID>
					<deviceFamily>
						<label lang="en">UR20</label>
						<label lang="de">UR20</label>
					</deviceFamily>
					<productID>1315540000</productID>
				</identity>
				<ObjectList>
					<Object index="4700" name="ModuleOutputImages" objectType="9" rangeSelector="ModuleOutputImages">
						<SubObject subIndex="00" name="OutputImage" objectType="7" dataType="000F" accessType="wo" PDOmapping="RPDO" uniqueIDRef="UID_OutputImage_Map"/>
					</Object>
					
					<Object index="3100" name="ModuleParameter1" objectType="9" rangeSelector="ModuleParameter1">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_1_Map"/>
					</Object>
					
					<Object index="3101" name="ModuleParameter2" objectType="9" rangeSelector="ModuleParameter2">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_2_Map"/>
					</Object>
					
					<Object index="3102" name="ModuleParameter3" objectType="9" rangeSelector="ModuleParameter3">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_3_Map"/>
					</Object>
					
					<Object index="3103" name="ModuleParameter4" objectType="9" rangeSelector="ModuleParameter4">
						<SubObject subIndex="00" name="Parameter" objectType="7" dataType="0004" accessType="rw" uniqueIDRef="UID_Par_4_Map"/>
					</Object>
				</ObjectList>
			</ApplicationLayers>
			<TransportLayers/>
		</ProfileBody>
	</ISO15745Profile>
</ISO15745ProfileContainer>
