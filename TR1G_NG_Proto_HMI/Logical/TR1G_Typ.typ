
TYPE
	From_FastTask_type : 	STRUCT 
		xSperSupOpen : BOOL;
		xSperSupClose : BOOL;
		xSperDisOpen : BOOL;
		xSperDisClose : BOOL;
		xShkTabCarGo : BOOL;
		TableActions : KickersPositions;
		xTrolFrictSupEnaDiscDry : BOOL;
		xTrolFrictDisEnaDiscDry : BOOL;
		xKicker1Out : BOOL;
	END_STRUCT;
	RehangResetState : 
		( (*Rehanger Reset State (rrs)*)
		rrsInitStart := 1,
		rrsWaitForRunSupLine := 2,
		rrsInitKickers := 3,
		rrsWaitForRunDisLine := 4,
		rrsInitRejector := 5,
		rrsStartDrive := 6,
		rrsInitSper := 7,
		rrsWaitZeroCar := 8,
		rrsInitSupStarted := 9,
		rrsInitSupEnd := 10,
		rrsInitDisStarted := 11,
		rrsInitDisEnd := 12,
		rrsInitFinished := 13
		);
	KickersPositions : 	STRUCT 
		xGoOut2 : BOOL;
		xGoIn2 : BOOL;
		xGoOut1 : BOOL;
		xGoIn1 : BOOL;
		udiK1Out : UDINT;
		udiK2Out : UDINT;
		udiK1In : UDINT;
		udiK2In : UDINT;
		usiPdsValPuls : USINT;
		usiPdsVal : USINT;
		diProdDet : DINT;
	END_STRUCT;
	From_Rejector_type : 	STRUCT 
		xCylRejOpen : BOOL;
		xCylRejClose : BOOL;
	END_STRUCT;
	From_Rehanger : 	STRUCT 
		aDis : DistanceRehangerConversion_type;
		xInitSperStart : BOOL;
		xRsRejOneCar : BOOL;
		xPdsMxFull : BOOL;
		xMxPdsFull : BOOL;
		ResetState : RehangResetState;
		xModeProd : BOOL;
		xModeMan : BOOL;
		xRejCar : BOOL;
		usiCarIndexDis : USINT;
		usiCarIndexSup : USINT;
		xResetInitKickers : BOOL;
		xDiscDrying : BOOL;
		xRetractKickers : BOOL;
	END_STRUCT;
	AlmReactions_type : 	STRUCT 
		xStopMach : BOOL;
		xAbortMach : BOOL;
	END_STRUCT;
END_TYPE
