
(* TODO: Add your comment here *)
FUNCTION_BLOCK sBus_write_N_parameters

	internal.COBidParameterRequestCh1 := Sbus_IdRequestCh1(uiMotorNumber);
	internal.COBidParameterResponseCh1:=Sbus_IdResponseCh1(uiMotorNumber);
	
	IF (xWriteParameterToDrive) THEN 
		IF (udiHandle > 0) THEN 
			IF (internal.iArrayIndexer >= iAmountOfParameters) THEN 
				internal.iArrayIndexer := 0;
				xReadyWrite := TRUE;
			ELSE
				xReadyWrite := FALSE;
			END_IF;
			// clean the respone array
			FOR internal.iIndex := 0 TO 7 DO 
				internal.abReceiveData[internal.iIndex] := 0;
			END_FOR;
			
			// set up the telegram for sending
			internal.abSendData[0]:= DINT_TO_BYTE(structParmeterListIn[internal.iArrayIndexer].eManagement);
			internal.abSendData[1]:= structParmeterListIn[internal.iArrayIndexer].bySubIndex;
			
			// convert the the int to 2 seperate bytes
			splitTheInt(structParmeterListIn[internal.iArrayIndexer].iIndex,
			internal.byRequestIndexLow,internal.byRequestIndexHigh);
			internal.abSendData[2]:= internal.byRequestIndexHigh;
			internal.abSendData[3]:= internal.byRequestIndexLow;
			
			// convert the Di data to an array of 4 bytes
			split_Di_to_ab4(diData:= structParmeterListIn[internal.iArrayIndexer].diData,
			abTelegramData:=internal.abTempSendData);
			internal.abSendData[4]:= internal.abTempSendData[0];
			internal.abSendData[5]:= internal.abTempSendData[1];
			internal.abSendData[6]:= internal.abTempSendData[2];
			internal.abSendData[7]:= internal.abTempSendData[3];
			
			internal.CANwrite_0
				(
				enable:=1,
				us_ident:=udiHandle,
				can_id:=internal.COBidParameterRequestCh1,
				data_adr:=ADR(internal.abSendData),
				data_lng:=SIZEOF(internal.abSendData)
				);
			
			IF internal.CANwrite_0.status = ERR_OK THEN
				RandDmessage := 'Write actions without errors';
			ELSE
				RandDmessage := 'There went somting wrong with the write action';		
			END_IF
			
			// read back the response telegram
			internal.CANread_0
				(
				enable:=1,
				us_ident:=udiHandle,
				can_id:=internal.COBidParameterResponseCh1,
				data_adr:=ADR(internal.abReceiveData)
				);
			
			IF internal.CANread_0.status = ERR_OK THEN
				RandDmessage := 'Read actions without errors';
			ELSE
				RandDmessage := 'There went somting wrong with the read action';		
			END_IF
			
			
			// check by the respond message if the transfer was oke. then increase the indexer
			IF (internal.abReceiveData[0] = DINT_TO_BYTE(READ_PARAMETER)) AND (internal.abReceiveData[2] > 0) THEN
				internal.iArrayIndexer := internal.iArrayIndexer +1;
			END_IF;
		END_IF;
END_IF;
END_FUNCTION_BLOCK