
TYPE
	MoviGearSettings_typ : 	STRUCT 
		uiMinCtrlLvl : UINT := 1000;
		uiMaxCtrlLvl : UINT := 10000;
		rMinRPM : REAL := 11.5;
		rMaxRPM : REAL := 115.4;
		iRamp : INT := 2500;
		xReverse : BOOL := TRUE;
	END_STRUCT;
END_TYPE
