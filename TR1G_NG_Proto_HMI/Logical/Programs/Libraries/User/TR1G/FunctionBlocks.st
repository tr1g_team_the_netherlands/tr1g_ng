FUNCTION_BLOCK avgCalcDyn
	
	//check if deviation between input and Average is to large. 
	//Yes => then decrease buffersize to get quicker a good average. 
	//When stable enlarge buffersize to get stabler readings.
	
	//limit windowsizes MTFilterMovingAverage > 0 AND < 10000
	uiStdBufsize := LIMIT(1, uiStdBufsize, 9999);
	uiChgBufsize := LIMIT(1, uiChgBufsize, 9999);	
	
	
	movAvg.Enable 	:= xEnable;
	movAvg.In		:= rIn;
	movAvg();
		
	IF (rIn - movAvg.Out) >= (movAvg.Out * rDevFactor)  THEN
		movAvg(WindowLength := uiChgBufsize, Update := TRUE);
	ELSIF movAvg.WindowLength <> uiStdBufsize THEN 
		movAvg(WindowLength := uiStdBufsize, Update := TRUE);
	END_IF;
	movAvg(Update := FALSE);

	rOut	:= movAvg.Out;
	uiOut	:= REAL_TO_UINT(rOut);
	
END_FUNCTION_BLOCK

FUNCTION rCalcCircumference 
	//Calculate the Circumference
	
	rCalcCircumference := PI * 2.0 * rRadius; 
	
END_FUNCTION

FUNCTION MoviGearSpd_to_RPM
	
	MoviGearSpd_to_RPM	:= rMoviGearSpd / (UINT_TO_REAL(uiMaxCtrlLvl - uiMinCtrlLvl) / (rMaxRPM - rMinRPM));

END_FUNCTION

FUNCTION_BLOCK MS_TO_RPM
	//convert the speed in M/s into RPM	
	rCircumference:= rCalcCircumference(rRadius);
	
	rMS_TO_RPM	:= (60.0 * rMeterSecond) / rCircumference;
	//Recalcalculate with speedfactor
	rMS_TO_RPM 	:= SEL(xEnaSpdFact, rMS_TO_RPM, rMS_TO_RPM * rSpdFact);
	
	iMS_TO_RPM 	:=  REAL_TO_INT(rMS_TO_RPM);
	
		
	movAvg.xEnable 		:= iMS_TO_RPM > 0; 
	movAvg.rIn 			:= rMS_TO_RPM; 
	movAvg.uiStdBufsize := 6000;
	movAvg.uiChgBufsize := 250;
	movAvg.rDevFactor 	:= 0.1;
	movAvg();
		
	rAvgRPM 			:= movAvg.rOut;
	

END_FUNCTION_BLOCK

FUNCTION calcSpeedRatio
	(*	
	SpeedRatio: 	SpdSupply / SpdDischarge
	Speedfactor:	3,75 -1,25 * SpeedRatio
	*)
	IF rLineSpdSupHour > 0 AND rLineSpdDisHour > 0 THEN
		rSpeedRatio	:= rLineSpdSupHour / rLineSpdDisHour;
		//rSpeedFactor	:= 3.75 - 1.25 * rSpeedRatio;
		
		rSpeedFactor := (10.5 * EXP(-1.75 * rSpeedRatio)) + 0.4;
	ELSE
		rSpeedRatio	:= 0;
		rSpeedFactor:= 0;	
	END_IF;
	
	calcSpeedRatio := LIMIT(0.4, rSpeedFactor, 3.0);
		
		
END_FUNCTION

FUNCTION_BLOCK RPM_to_MoviGearSpd 
	// Movigear has a minimum and maximum RPM (i.e. 11.5 - 115.4)
	// is controlled by a minimum value and maximum value (i.e. 1000 - 10000)
	// so RPM has to be converted into the correct CtrlLevel.
		
	
	//take minimum control speed into account if this requested speed is lower, then take the min speed.
	rRequestSpeed 	:= (rRPM * (UINT_TO_REAL(uiMaxCtrlLvl - uiMinCtrlLvl) / (rMaxRPM - rMinRPM)));		
	rRequestSpeed 	:= LIMIT(uiMinCtrlLvl, ABS(rRequestSpeed), uiMaxCtrlLvl);
	
	IF xReverse THEN
		rRequestSpeed := rRequestSpeed * -1.0;
	END_IF
	
	iRPM_to_MoviGearSpd 	:= REAL_TO_INT(rRequestSpeed);
	
END_FUNCTION_BLOCK

FUNCTION Percent_to_MoviGearSpd 
	
	IF DriveSet.xReverse THEN
		uiReverseFact := -1;
	ELSE
		uiReverseFact := 1;
	END_IF
		
	Percent_to_MoviGearSpd := UINT_TO_INT((REAL_TO_UINT((DriveSet.uiMaxCtrlLvl - DriveSet.uiMinCtrlLvl) / 100.0) * USINT_TO_UINT(runPercent)) + DriveSet.uiMinCtrlLvl) * uiReverseFact;
	
	
END_FUNCTION

FUNCTION_BLOCK RPM_TO_M_MSec
	//Convert the Speed in RPM into travelled distance within 1 millisec
	
	rCircumference := rCalcCircumference(rRadius);
	
	rRPM_TO_M_MSec := rRPM * rCircumference  * (1.0/60.0) / 1000.0; 
	
END_FUNCTION_BLOCK

FUNCTION RPM_to_MSec
	
	RPM_to_MSec := rRPM * rCalcCircumference(rRadius)  * (1.0/60.0); 
		
END_FUNCTION

FUNCTION_BLOCK synchro_MS_To_RPM_TO_TIME
	(* Synchrowheel speed 
	first caculate the synchroweel speed in RPM based on PDS line speed [m/s] (as circumference take synchrowheel + carrier)
	second recalulate the speed [m/s] with the synchrowheel circumference
	thirth calculate the time needed TO travel the given (param) distance
	last check IF the time > 1ms.
	*)
	rCircumference := rCalcCircumference(rRadiusSynchroCar); 	//calc circumference (synchrowheel + carrier)
	rRpmSpeed := (rMeterSecond * 60.0)/ rCircumference; 						//calc speed [m/s] into RPM

	RPM_TO_TimeDistance_0(rRPM:= rRpmSpeed, rRadius:= rRadiusSynchro, rDistance:= rDistance);
	
	timWaitTime:= RPM_TO_TimeDistance_0.timRPM_TO_TimeDistance;	//calculate travel time
	
	IF timWaitTime < timMinTime THEN
		timWaitTime := timMinTime; //minimum Time
	END_IF;
	
	timSynchro_MS_To_RPM_TO_TIME := timWaitTime;
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK RPM_TO_TimeDistance
	//calculate the traveled distance in one millisecond
	RPM_TO_M_MSec_0(rRPM := rRPM, rRadius := rRadius);
	
	rRpmMsec := RPM_TO_M_MSec_0.rRPM_TO_M_MSec;				
		
	timRPM_TO_TimeDistance := REAL_TO_TIME((ABS(rDistance / rRpmMsec))); 
	
END_FUNCTION_BLOCK

FUNCTION calcDisIntoTime
	rCircumference	:= rCalcCircumference(rRadius);
	
	
	calcDisIntoTime := REAL_TO_UINT(rDis / ((rRpm * rCircumference) / (60.0 * 1000.0 )));
	

END_FUNCTION

FUNCTION calcMultiplyTime

	calcMultiplyTime :=  REAL_TO_TIME(rFactor * TIME_TO_REAL(timBase));
	
END_FUNCTION

