
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	
	//Encoders
	encSupPos := encSupPos + myTickSup;
	encSupPos := encSupPos MOD Cfg.Config.EncSup.uiResolution;

	encDisPos := encDisPos + myTickDis;
	encDisPos := encDisPos MOD Cfg.Config.EncDis.uiResolution;
	
	hw.Com_.uiEncSup := (encSupPos);
	hw.Com_.uiEncDis := (encDisPos);


	
	gSimEnc := encSupPos;
	
	//s01 and s02 trigger
	uiS01 	:= uiS01 + myTickSup;
	uiS01	:= uiS01 MOD REAL_TO_UINT(Cfg.Config.EncSup.uiResolution / Cfg.Config.EncSup.rShakleAmount);
			
	uiS02 	:= uiS02 + myTickSup;
	uiS02	:= uiS02 MOD REAL_TO_UINT(Cfg.Config.EncDis.uiResolution / Cfg.Config.EncDis.rShakleAmount);

	
	//hw.Di_.xS01 := SEL(uiS01 < (0.2 * (Cfg.Config.EncSup.uiResolution / Cfg.Config.EncSup.rShakleAmount)), FALSE, TRUE);
	//hw.Di_.xS02 := SEL(uiS02 < (0.2 * (Cfg.Config.EncDis.uiResolution / Cfg.Config.EncDis.rShakleAmount)), FALSE, TRUE);
	
	

	
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

