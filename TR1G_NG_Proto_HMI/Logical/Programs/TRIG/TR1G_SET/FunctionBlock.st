
(* TODO: Add your comment here *)
FUNCTION_BLOCK Settings
	
	SetRehanger_Out	:= SetRehanger_in;
	SetRehanger_Out.rDisProdDet		:= SetRehanger_in.rDisProdDet 		* rEncSupPulsesMeter;
	SetRehanger_Out.rDisProdWindow	:= SetRehanger_in.rDisProdWindow	* rEncSupPulsesMeter;
	SetRehanger_Out.rDisPdsWindow	:= SetRehanger_in.rDisPdsWindow		* rEncSupPulsesMeter;
	SetRehanger_Out.rDisCarGo		:= SetRehanger_in.rDisCarGo			* rEncSupPulsesMeter;
	SetRehanger_Out.rDisCarGoWindow	:= SetRehanger_in.rDisCarGoWindow	* rEncSupPulsesMeter;
	SetRehanger_Out.aK1.rDisOut		:= SetRehanger_in.aK1.rDisOut		* rEncSupPulsesMeter;
	SetRehanger_Out.aK1.rDisIn		:= SetRehanger_in.aK1.rDisIn		* rEncSupPulsesMeter;
	SetRehanger_Out.aK2.rDisOut		:= SetRehanger_in.aK2.rDisOut		* rEncSupPulsesMeter;
	SetRehanger_Out.aK2.rDisIn		:= SetRehanger_in.aK2.rDisIn		* rEncSupPulsesMeter;
	SetRehanger_Out.aK1.rActWindow	:= SetRehanger_in.aK1.rActWindow 	* rEncSupPulsesMeter;
	SetRehanger_Out.aK2.rActWindow	:= SetRehanger_in.aK2.rActWindow 	* rEncSupPulsesMeter;
	SetRehanger_Out.rShakleLength	:= SetConfig_in.EncSup.rShakleSize	* rEncSupPulsesMeter;
	
	SetCarDisc_Out	:= SetCarDisc_in;
	
	SetRejector_Out	:= SetRejetor_in;
	SetRejector_Out.rRejectWindow 	:= SetRejetor_in.rRejectWindow 		* rEncDisPulsesMeter;
	SetRejector_Out.rDetWindow		:= SetRejetor_in.rDetWindow 		* rEncDisPulsesMeter;
	SetRejector_Out.rStartDelay		:= SetRejetor_in.rStartDelay		* rEncDisPulsesMeter;
	SetRejector_Out.rShakleLength	:= SetConfig_in.EncDis.rShakleSize	* rEncDisPulsesMeter;
	
	SetConfig_Out 					:= SetConfig_in;
	SetConfig_Out.CarDisc.Slip		:= LIMIT(0.0, (1.0 - SetConfig_in.CarDisc.Slip), 0.99);
	SetConfig_Out.CarDisc.SlipCom 	:= LIMIT(1.0, (1.0 + SetConfig_in.CarDisc.Slip), 1.99);
	 
END_FUNCTION_BLOCK
