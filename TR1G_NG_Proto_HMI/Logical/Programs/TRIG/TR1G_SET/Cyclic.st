
PROGRAM _CYCLIC
	
		//Calculate Shaklesize (INCH to M)
	Cfg.Config.EncSup.rShakleSize 	:= Cfg.Config.General.rShakleSizeSup * 0.0254;
	Cfg.Config.EncDis.rShakleSize 	:= Cfg.Config.General.rShakleSizeDis * 0.0254;
	
	
	Settings_0.SetRehanger_in 		:= Cfg.Rehanger;
	Settings_0.SetCarDisc_in 		:= Cfg.CarDisc;
	Settings_0.SetRejetor_in 		:= Cfg.Rejector;
	Settings_0.SetConfig_in			:= Cfg.Config;
	Settings_0.rEncSupPulsesMeter 	:= gEncSup.PulsesMeter;
	Settings_0.rEncDisPulsesMeter 	:= gEncDis.PulsesMeter;
	Settings_0();
	

	
	setTrolSys	:= Settings_0.SetCarDisc_Out;
	setGeneral	:= Settings_0.SetConfig_Out.General;
	setRehanger := Settings_0.SetRehanger_Out;
	setRejector := Settings_0.SetRejector_Out;
	 
END_PROGRAM
