
FUNCTION_BLOCK Settings
	VAR_INPUT
		SetRehanger_in : SetRehanger_typ;
		SetCarDisc_in : SetCarDisc_typ;
		SetRejetor_in : SetRejector_typ;
		SetConfig_in : Configuration_typ;
		rEncSupPulsesMeter : REAL;
		rEncDisPulsesMeter : REAL;
	END_VAR
	VAR_OUTPUT
		SetRehanger_Out : SetRehanger_typ;
		SetCarDisc_Out : SetCarDisc_typ;
		SetRejector_Out : SetRejector_typ;
		SetConfig_Out : Configuration_typ;
	END_VAR
END_FUNCTION_BLOCK
