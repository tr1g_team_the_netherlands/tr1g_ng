
PROGRAM _CYCLIC
	
	EM.Name 		:= 'Rejector';
	EM.Status		:= '';
	EM.xSC			:= FALSE;
	xStateComplete	:= FALSE;
	xEnaRejector	:= FALSE;
	xInit			:= FALSE;
	
	xModeMan		:= FALSE;
	xModeProd		:= FALSE;
	
	Enc 			:= gEncDis;
	
	xSensorLeft		:= hw.Di_.xS08;
	xSensorRight	:= hw.Di_.xS09;
	
	//Triggers Detection Sensors
	xReLeft(CLK := xSensorLeft);
	xReRight(CLK:= xSensorRight);
	
	
	//Settings 
	timCylOpenTime 		:= setRejector.Config.timCylResponseOpen + setRejector.Config.timValveResponseOpen;
	timCylCloseTime 	:= setRejector.Config.timCylResponseClose + setRejector.Config.timValveResponseClose;
	cfgEnableRej		:= Cfg.Rejector.Config.xEnableRej;
	
	
	CASE PackML_Mode OF
		Production:
			xModeProd	:= TRUE;
			CASE PackML_State OF	
								
				mpPACKML_STATE_RESETTING:
					xEnaRejector	:= TRUE;
					xInit			:= TRUE;
					xRejProd 		:= FALSE;
					xDetWindowEna	:= FALSE;
				
					IF (xTpCylIn.ET >= T#50ms AND xTpCylOut.ET >= T#50ms) THEN
						xStateComplete	:= TRUE;
					END_IF
				
				//mpPACKML_STATE_IDLE:
				//mpPACKML_STATE_STARTING:
				mpPACKML_STATE_EXECUTE:
					xEnaRejector	:= TRUE;
				
				mpPACKML_STATE_SUSPENDING:
					xEnaRejector	:= TRUE;
					xStateComplete	:= TRUE;
				
				//mpPACKML_STATE_SUSPENDED:
				//mpPACKML_STATE_UNSUSPENDING:
				//mpPACKML_STATE_STOPPING:
				
				mpPACKML_STATE_HOLDING:
					xEnaRejector	:= TRUE;
					xStateComplete	:= TRUE;
				
				mpPACKML_STATE_ABORTING:
					xInit			:= FALSE;
					xStateComplete	:= TRUE;
				
				mpPACKML_STATE_ABORTED:
					xInit			:= FALSE;
				
				ELSE
				xStateComplete	:= TRUE;
			END_CASE;
		
		Maintenance:
		
		Manual:
			
			CASE PackML_State OF	
				mpPACKML_STATE_EXECUTE:
					xModeMan	:= TRUE;
				
				ELSE
				xStateComplete	:= TRUE;
			END_CASE;
		
		Cleaning:
	
	END_CASE
	
	EM.xSC		:= xStateComplete;
	
		
	//calculate Cylinder open/close time in pulses
	rOpenCor  	:= (Enc.MeterSec * (TIME_TO_REAL(timCylOpenTime)/1000.0))  * Enc.PulsesMeter; 
	rCloseCor	:= (Enc.MeterSec * (TIME_TO_REAL(timCylCloseTime)/1000.0)) * Enc.PulsesMeter;
		

	
	// Detection Window 
	IF (xReRight.Q OR xReLeft.Q) AND NOT xDetWindowEna THEN
		xDetWindowEna 		:= TRUE;
		udiDetWindowEncPos	:= Enc.EncoderPosition;
		xRejProd			:= TRUE;
	END_IF;
	
	//Check if both legs are in the hook => only then reset reject signal
	IF xDetWindowEna AND (Enc.EncoderPosition <= (udiDetWindowEncPos + REAL_TO_UDINT(setRejector.rDetWindow)))	THEN
		IF xSensorLeft AND xSensorRight THEN
			xRejProd 		:= FALSE;
			xDetWindowEna	:= FALSE;
		END_IF
	END_IF
	
	
	//Reject Open
	xTpCylOut.IN := xInit OR (xRejProd AND Enc.EncoderPosition >= REAL_TO_UDINT(udiDetWindowEncPos + setRejector.rStartDelay - rOpenCor));
	xTpCylOut(PT := timCylOpenTime);
	
	
	//Reject Close
	xTpCylIn.IN := xInit OR (xRejProd AND Enc.EncoderPosition >= (udiDetWindowEncPos + REAL_TO_UDINT(setRejector.rStartDelay + setRejector.rRejectWindow - rCloseCor)));
	xTpCylIn(PT := timCylCloseTime);
	
	xCylRejOpen	:= cfgEnableRej AND ((xModeProd AND xTpCylOut.Q AND xEnaRejector) OR (xModeMan AND gMmiCmd.Man.xRejOpen));
	xCylRejClose:= cfgEnableRej AND ((xModeProd AND xTpCylIn.Q  AND xEnaRejector) OR (xModeMan AND gMmiCmd.Man.xRejClose));
	
	//reset Signal
	xReCylClose(CLK := xTpCylIn.Q);
	
	IF xReCylClose.Q THEN
		xRejProd 		:= FALSE;
		xDetWindowEna	:= FALSE;
	END_IF
	 
	
	(* ---------- //Connection to FastTask  ---------- *)
	Rejector.xCylRejOpen 	:= xCylRejOpen;
	Rejector.xCylRejClose	:= xCylRejClose;
	
	
END_PROGRAM
