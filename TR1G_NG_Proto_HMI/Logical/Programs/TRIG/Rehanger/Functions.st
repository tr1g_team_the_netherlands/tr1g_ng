
FUNCTION_BLOCK getTextFromId
	
	IF sTextID <> preTextID AND ArTextSysGetText_0.Busy = FALSE THEN
		ArTextSysGetSystemLanguage_0(Execute := TRUE );
		ArTextSysGetText_0.Execute 			:= TRUE; 		
	END_IF	
		
	IF ArTextSysGetText_0.Done THEN
		preTextID := sTextID;
		ArTextSysGetText_0.Execute 			:= FALSE; 
	END_IF;
			
	ArTextSysGetText_0.Namespace 		:= ADR('insideCodingNamespace'); 
	ArTextSysGetText_0.TextID 			:= ADR(sTextID); 
	ArTextSysGetText_0.LanguageCode 	:= ArTextSysGetSystemLanguage_0.LanguageCode; 
	ArTextSysGetText_0.TextBufferSize 	:= 255; 
	ArTextSysGetText_0.TextBuffer 		:= ADR(sResult);
	ArTextSysGetText_0();
	
END_FUNCTION_BLOCK



