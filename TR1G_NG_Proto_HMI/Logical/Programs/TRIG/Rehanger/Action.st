
ACTION StateTimer: 
	timState	:= timState + DINT_TO_TIME(RTInfo_0.cycle_time)/1000; 
END_ACTION

ACTION modeProduction:
	xModeProd	:= TRUE;
	CASE PackML_State OF
		mpPACKML_STATE_STOPPING:	
			IF DriveFB.uiMovSpd = 0 THEN
				xSC	:= TRUE;
			END_IF
				
		mpPACKML_STATE_STOPPED:
			timState			:= 0;
			ResetState			:= rrsInitStart;

		mpPACKML_STATE_RESETTING:
			CASE ResetState OF
				rrsInitStart:
					sEMStatus			:= 'Rehanger.ResetState.1';
					xInitSperStart		:= FALSE;
					xInitRoundPds		:= FALSE;
					xInitRoundMx		:= FALSE;
					xTpInitReset.IN		:= FALSE;
					xEnaFricDisc		:= FALSE;
					ResetState			:= rrsInitKickers;
						
				rrsWaitForRunSupLine:
					sEMStatus 			:= 'Rehanger.ResetState.2';
							
					IF xSupLineRun THEN
						ResetState		:= rrsWaitForRunDisLine;
					END_IF;
						
				rrsInitKickers:
					sEMStatus 			:= 'Rehanger.ResetState.3';
					StateTimer;
					IF timState > T#2s THEN
						ResetState		:= rrsWaitForRunSupLine;	 
					END_IF
						
				rrsWaitForRunDisLine:
					sEMStatus 			:= 'Rehanger.ResetState.4';
					IF xSupLineRun AND xDisLineRun THEN
						ResetState		:= rrsStartDrive;
					ELSIF NOT(xSupLineRun) THEN
						ResetState		:= rrsWaitForRunSupLine;
					END_IF
						
				rrsInitRejector:
					sEMStatus 			:= 'Rehanger.ResetState.5';
					StateTimer;
					IF timState > T#2s THEN
						IF xSupLineRun AND xDisLineRun THEN
							ResetState		:= rrsStartDrive;	 
						ELSE
						END_IF
					END_IF
						
				rrsStartDrive: 
					sEMStatus 			:= 'Rehanger.ResetState.6';
					IF DriveFB.rRpmAvg > 0 THEN
						timState 		:= 0;
						ResetState		:= rrsInitSper;
					END_IF;
						
				rrsInitSper:
					sEMStatus 			:= 'Rehanger.ResetState.7';
					StateTimer;
							
					IF xInitCarDone THEN
						ResetState	:= rrsInitFinished;
					ELSE
						ResetState	:= rrsWaitZeroCar;
					END_IF;
						
				rrsWaitZeroCar:
					sEMStatus 			:= 'Rehanger.ResetState.8';
					uiNrCarPres	:= 0;
					usiInitCarCnt(RESET	:= TRUE);
						
					IF xReDetNulCarPds.Q THEN
						xInitRoundPds	:= TRUE;
						ResetState		:= rrsInitSupStarted;
					ELSIF xReDetNulCarMx.Q THEN
						xInitRoundMx	:= TRUE;
						ResetState		:= rrsInitDisStarted;
					END_IF;
						
				rrsInitSupStarted:
					sEMStatus 			:= 'Rehanger.ResetState.9';
					usiInitCarCnt(CU:= xDetCarPds, RESET:= FALSE);
					IF xReDetNulCarMx.Q THEN
						usiCarCntPdsMx(PV:= usiInitCarCnt.CV, LOAD:= TRUE);
						usiCarCntPdsMx(LOAD:=FALSE);
						ResetState		:= rrsInitSupEnd;
					END_IF;
						
				rrsInitSupEnd:
					sEMStatus 			:= 'Rehanger.ResetState.10';
					usiInitCarCnt(CU:= xDetCarPds, RESET:= FALSE);
					IF xReDetNulCarPds.Q THEN
						usiCarCntMxPds(PV:= usiInitCarCnt.CV - usiCarCntPdsMx.CV, LOAD:= TRUE);
						usiCarCntMxPds(LOAD:= FALSE);
						xResetPdsTable	:= TRUE;
						ResetState		:= rrsInitFinished;
					END_IF;
						
				rrsInitDisStarted:
					sEMStatus 			:= 'Rehanger.ResetState.11';
					usiInitCarCnt(CU:= xDetCarMx, RESET:= FALSE);
					IF xReDetNulCarPds.Q THEN
						usiCarCntMxPds(PV:= usiInitCarCnt.CV, LOAD:= TRUE);
						usiCarCntMxPds(LOAD:=FALSE);
						ResetState		:= rrsInitDisEnd;
					END_IF;
						
				rrsInitDisEnd:
					sEMStatus 			:= 'Rehanger.ResetState.12';
					usiInitCarCnt(CU:= xDetCarMx, RESET:= FALSE);
					IF xReDetNulCarMx.Q THEN
						usiCarCntPdsMx(PV:= usiInitCarCnt.CV - usiCarCntMxPds.CV, LOAD:= TRUE);
						usiCarCntPdsMx(LOAD:= FALSE);
						xResetPdsTable	:= TRUE;
						ResetState		:= rrsInitFinished;
					END_IF;
						
				rrsInitFinished:
					sEMStatus 			:= 'Rehanger.ResetState.13';
					uiNrCarPres			:= 13; //usiInitCarCnt.CV;
					xEnaFricDisc		:= TRUE;
					xInitCarDone		:= TRUE;
					xSC					:= TRUE;
			END_CASE;	
								
		mpPACKML_STATE_EXECUTE:
			CTDSusLoop(LOAD := TRUE, PV := usiCarCntPdsMx.CV);
					
			//Manually release trolley 
			xRejCar := xOperRelTrol;
					
			//When empty shackles is > then setting, request remove products in Machine
			xReRejRemProds(CLK := (ShkEmpCnt.CV >= UINT_TO_INT(setGeneral.uiShkEmpty)));
			ProdRemCnt(CD := xSperSupOpen, LOAD := xReRejRemProds.Q , PV := SEL(usiProdInMach > 0, 0, usiCarCntPdsMx.CV));		
				
										
			//rotate nr of trolleys to empty TR1G
			IF ProdRemCnt.CV > 0 THEN
				xRsRejOneCar(SET := TRUE );
			END_IF
					
			//When stop shackles is reaches goto Suspend
			EM.Cmd.xHold 	:= ShkEmpCnt.CV >= UINT_TO_INT(setGeneral.uiShkStop) AND usiProdInMach = 0;
			EM.Cmd.xSuspend	:= NOT(xSupLineRun AND xDisLineRun);
				
					
						
												
		mpPACKML_STATE_SUSPENDING, mpPACKML_STATE_HOLDING:
			//Let disc run for 5 sec to position all trolleys
			xTonTrigWaitStop:= TRUE;
							
			xSC		:= xTonWaitStop.Q;
					
		mpPACKML_STATE_SUSPENDED:
			EM.Cmd.xUnSuspend := xSupLineRun AND xDisLineRun;
				
		mpPACKML_STATE_HELD:
			//Manually release trolley 
			xRejCar := xOperRelTrol;
					
			EM.Cmd.xUnHold := (xDetProd OR xRejCar) AND xSupLineRun AND xDisLineRun;
									
		mpPACKML_STATE_UNSUSPENDING, mpPACKML_STATE_UNHOLDING:
					
			//if the drive is running
			IF DriveFB.rRpmAvg > 0 THEN
				xSC	:= TRUE;
			END_IF
					
				
		mpPACKML_STATE_COMPLETING:
			CTDSusLoop(LOAD := FALSE);
			xRejCar := NOT xRsRejOneCar.Q1 AND NOT CTDSusLoop.Q AND xShkDisDet;		//request release carrier.					
			CTDSusLoop(CD := xSperDisClose);
					
			//Let disc run for 5 sec to position all trolleys
			xTonTrigWaitStop := CTDSusLoop.Q;
					
			xSC	:= xTonWaitStop.Q AND DriveFB.uiMovSpd = 0;
				
		mpPACKML_STATE_ABORTING:
			IF DriveFB.uiMovSpd = 0 THEN
				xSC	:= TRUE;
			END_IF
				
		mpPACKML_STATE_ABORTED:
			xInitCarDone	:= FALSE;	

		mpPACKML_STATE_CLEARING:
			xEnaFricDiscInit:= FALSE;
			xSC				:= TRUE;
				
		ELSE
		xEnaFricDiscInit	:= FALSE;
		xSC		:= TRUE;
	END_CASE;
			
	xResetInitKickers	:= PackML_State = mpPACKML_STATE_RESETTING AND ResetState = rrsInitKickers;
	xInitSperStart 		:= PackML_State = mpPACKML_STATE_RESETTING AND ResetState >= rrsInitSper AND ResetState < rrsInitFinished;
	xEnaFricDiscInit	:= PackML_State = mpPACKML_STATE_RESETTING AND ResetState >= rrsStartDrive AND ResetState <= rrsInitFinished;
END_ACTION

ACTION modeManual:
	CASE PackML_State OF
		mpPACKML_STATE_STOPPING:	
			IF DriveFB.uiMovSpd = 0 THEN
				xSC	:= TRUE;
			END_IF
				
								
		mpPACKML_STATE_EXECUTE:
			xModeMan := TRUE;				
				
		mpPACKML_STATE_ABORTING:
			IF DriveFB.uiMovSpd = 0 THEN
				xSC	:= TRUE;
			END_IF
				
		ELSE
		xSC		:= TRUE;

	END_CASE
END_ACTION

ACTION modeCleaning:
	CASE PackML_State OF
		mpPACKML_STATE_STOPPING:	
				
		mpPACKML_STATE_STOPPED:

		mpPACKML_STATE_RESETTING:
								
		mpPACKML_STATE_EXECUTE:
												
		mpPACKML_STATE_SUSPENDING:
				
		mpPACKML_STATE_UNSUSPENDING:
				
		mpPACKML_STATE_COMPLETING:
				
		mpPACKML_STATE_ABORTING:
				
		mpPACKML_STATE_ABORTED:

		mpPACKML_STATE_CLEARING:
				
		ELSE
		xSC		:= TRUE;

	END_CASE;
END_ACTION

ACTION modeMaintenance:
	CASE PackML_State OF
		mpPACKML_STATE_STOPPING:	
				
		mpPACKML_STATE_STOPPED:

		mpPACKML_STATE_RESETTING:
								
		mpPACKML_STATE_EXECUTE:
												
		mpPACKML_STATE_SUSPENDING:
				
		mpPACKML_STATE_UNSUSPENDING:
				
		mpPACKML_STATE_COMPLETING:
				
		mpPACKML_STATE_ABORTING:
				
		mpPACKML_STATE_ABORTED:

		mpPACKML_STATE_CLEARING:
				
		ELSE
		xSC		:= TRUE;

	END_CASE;

END_ACTION

ACTION kickerCalculations:
	(* ---------- kicker(s) ---------- *)	
	//calucute Cylinder open/close time in pulses	
	aDis.udiK1OutCor 	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(setRehanger.aK1.timCylResponseOpen  + setRehanger.aK1.timValveResponseOpen + calcMultiplyTime(setRehanger.aK1.rAirHoseLength , timAirHoseMeter))/1000.0) * Enc.PulsesMeter);
	aDis.udiK2OutCor	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(setRehanger.aK2.timCylResponseOpen  + setRehanger.aK2.timValveResponseOpen + calcMultiplyTime(setRehanger.aK2.rAirHoseLength, timAirHoseMeter))/1000.0) * Enc.PulsesMeter);
	aDis.udiK1InCor  	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(setRehanger.aK1.timCylResponseClose + setRehanger.aK1.timValveResponseClose + calcMultiplyTime(setRehanger.aK1.rAirHoseLength, timAirHoseMeter))/1000.0) * Enc.PulsesMeter);
	aDis.udiK2InCor 	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(setRehanger.aK2.timCylResponseClose + setRehanger.aK2.timValveResponseClose + calcMultiplyTime(setRehanger.aK2.rAirHoseLength, timAirHoseMeter))/1000.0) * Enc.PulsesMeter);

	aDis.udiSperSupCor	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(setTrolSys.aSupSlot.timCylResponseOpen + setTrolSys.aSupSlot.timValveResponseOpen + calcMultiplyTime(setTrolSys.aSupSlot.rAirHoseLength, timAirHoseMeter))/1000.0) * Enc.PulsesMeter);
	aDis.udiSperDisCor	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(setTrolSys.aDisSlot.timCylResponseOpen + setTrolSys.aDisSlot.timValveResponseOpen + calcMultiplyTime(setTrolSys.aDisSlot.rAirHoseLength, timAirHoseMeter))/1000.0) * Enc.PulsesMeter);
	aDis.udiRelCarCor	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(setTrolSys.aSupSlot.timCylResponseOpen + setTrolSys.aSupSlot.timValveResponseOpen + calcMultiplyTime(setRehanger.aK1.rAirHoseLength, timAirHoseMeter))/1000.0) * Enc.PulsesMeter);

	//calculate distance in pulses for the productdetection
	aDis.udiProdMin	:= Enc.EncoderPosition + REAL_TO_UDINT(setRehanger.rDisProdDet - (setRehanger.rDisProdWindow * 0.5 ));
	aDis.udiProdMax	:= Enc.EncoderPosition + REAL_TO_UDINT(setRehanger.rDisProdDet + (setRehanger.rDisProdWindow * 0.5));

	//calculate distance in pulses for the PDS signal
	aDis.udiPdsMin 	:= Enc.EncoderPosition + REAL_TO_UDINT(setRehanger.rDisProdDet - (setRehanger.rDisPdsWindow * 0.5));
	aDis.udiPdsMax	:= Enc.EncoderPosition + REAL_TO_UDINT(setRehanger.rDisProdDet + (setRehanger.rDisPdsWindow * 0.5));	

	aDis.udiCarGo	:= Enc.EncoderPosition + REAL_TO_UDINT(setRehanger.rDisCarGo);
	aDis.udiK1Out	:= Enc.EncoderPosition + REAL_TO_UDINT(setRehanger.aK1.rDisOut);
	aDis.udiK2Out	:= Enc.EncoderPosition + REAL_TO_UDINT(setRehanger.aK2.rDisOut);
	aDis.udiK1In	:= Enc.EncoderPosition + REAL_TO_UDINT(setRehanger.aK1.rDisIn );
	aDis.udiK2In	:= Enc.EncoderPosition + REAL_TO_UDINT(setRehanger.aK2.rDisIn );

	aDis.udiCarGoWindow			:= REAL_TO_UDINT(setRehanger.rDisCarGoWindow);
	aDis.udiKick1ActWindow		:= REAL_TO_UDINT(setRehanger.aK1.rActWindow);
	aDis.udiKick2ActWindow		:= REAL_TO_UDINT(setRehanger.aK2.rActWindow);
	aDis.udiShkLength			:= REAL_TO_UDINT(setRehanger.rShakleLength);
END_ACTION