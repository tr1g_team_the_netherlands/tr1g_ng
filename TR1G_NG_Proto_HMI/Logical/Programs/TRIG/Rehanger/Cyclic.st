
PROGRAM _CYCLIC
	
	
	sEMStatus			:= 'Rehanger.ResetState.0';
	EM.Status			:= '';
	EM.xSC				:= FALSE;
	EM.Cmd.xSuspend		:= FALSE;
	EM.Cmd.xUnSuspend	:= FALSE;
	EM.Cmd.xHold		:= FALSE;
	EM.Cmd.xUnHold		:= FALSE;
	xSC					:= FALSE;
	
	
	(* ---------- Inputs from FastTask ---------- *)
	xSperSupOpen 			:= FastTask.xSperSupOpen;
	xSperSupClose			:= FastTask.xSperSupClose;
	xSperDisOpen			:= FastTask.xSperSupClose;
	xSperDisClose			:= FastTask.xSperDisOpen;
	xTrolFrictSupEnaDiscDry	:= FastTask.xTrolFrictSupEnaDiscDry;
	xTrolFrictDisEnaDiscDry := FastTask.xTrolFrictDisEnaDiscDry;
	usiPdsVal				:= FastTask.TableActions.usiPdsVal;
	//	:= FastTask.xSperDisClose;
	
	
	(*---------- Redefinitions inputs ----------*)
	xShkSupDet		:= hw.Di_.xS01;
	xShkDisDet		:= hw.Di_.xS02;
	xDetProd		:= NOT(hw.Di_.xS03);
	xDetCarPds		:= hw.Di_.xS04;
	xDetNulCarPds	:= hw.Di_.xS05;
	xDetCarMx		:= hw.Di_.xS06;
	xDetNulCarMx	:= hw.Di_.xS07;
	Report.xReset	:= gMmiCmd.Operator.xResetPerformancePage;
	xOperRelTrol	:= hw.Di_.xCmdButRelCar OR gMmiCmd.xReleaseCar;
	
	//Movigear speed
	DriveFB.uiMovSpd:= ABS(gFeedBackFricDisc[0]);
	
	//Encoder
	Enc 			:= gEncSup;
	
	xSupLineRun		:= LineInfo.Sup.xRun;
	xDisLineRun		:= LineInfo.Dis.xRun;
	
	
	//Edge detections
	xReDetNulCarPds	(CLK := xDetNulCarPds);
	xFeDetNulCarPds	(CLK := xDetNulCarPds);
	xReDetNulCarMx	(CLK := xDetNulCarMx);
	xFeDetNulCarMx	(CLK := xDetNulCarMx);
	xReDetCarMx		(CLK := xDetCarMx);
	xFeDetCarMx		(CLK := xDetCarMx);
	xReDetCarPds	(CLK := xDetCarPds);
	xFeDetCarPds	(CLK := xDetCarPds);
	xReShkDet		(CLK := xShkSupDet);
	xReShkDisDet	(CLK := xShkDisDet);


	//calc MovigearSpd AND average Speed
	DriveFB.rRpm 		:= MoviGearSpd_to_RPM(DriveFB.uiMovSpd,  setTrolSys.config.aMoviGear.uiMaxCtrlLvl, setTrolSys.config.aMoviGear.uiMinCtrlLvl, setTrolSys.config.aMoviGear.rMaxRPM, setTrolSys.config.aMoviGear.rMinRPM);
	rAvgRpmDrive(Enable := TRUE, In	:= DriveFB.rRpm);
	DriveFB.rRpmAvg 	:= rAvgRpmDrive.Out; 
	
	
	//ACTION KICKER CALCULATION
	kickerCalculations;
	
	
	xModeProd	:= FALSE;
	xModeMan	:= FALSE;
	xTonTrigWaitStop:= FALSE;
	
	//PACKML MODE SELECTION
	CASE PackML_Mode OF
		Production:
			modeProduction;
		Manual:
			modeManual;		
		Cleaning:			
			modeCleaning;
		Maintenance:
			modeMaintenance;		
	END_CASE;
	
	xTonWaitStop(IN:= xTonTrigWaitStop, PT:= T#3s);
	
	//Get Text
	statusIDtoText(sTextID := sEMStatus);
	EM.Status 		:= statusIDtoText.sResult;
	EM.xSC			:= xSC;
	EM.Cmd.xStop	:= (PackML_Mode = Production) AND (PackML_State = mpPACKML_STATE_RESETTING) AND NOT(xSupLineRun AND xDisLineRun);
	
	
	//Reset INITstate. (When machine restarts the intitialisation will be executed)
	IF (PackML_State = mpPACKML_STATE_STOPPING OR PackML_State = mpPACKML_STATE_STOPPED) AND xInitCarDone AND
		(xReDetCarPds.Q OR xReDetNulCarPds.Q OR xReDetCarMx.Q OR xReDetNulCarMx.Q OR xFeDetCarPds.Q OR xFeDetNulCarPds.Q OR xFeDetCarMx.Q OR xFeDetNulCarMx.Q) THEN
		xInitCarDone := FALSE;
	END_IF
	
	
	
	(* ---------- Count number of products between SUP & DIS  ---------- *)	
	//Count empty Shakles 
	ShkEmpCnt.CU 	:= xShkSupDet AND (PackML_State = mpPACKML_STATE_EXECUTE) ;
	ShkEmpCnt.RESET := (PackML_State = mpPACKML_STATE_RESETTING) OR xDetProd OR xRejCar;
	ShkEmpCnt();
		
	
	(* ---------- Carrier Counter ---------- *)
//	IF PackML_State <> mpPACKML_STATE_RESETTING THEN
//		usiCarCntPdsMx(PV:= uiNrCarPres - usiCarCntMxPds.CV, LOAD:= ((xDetNulCarPds AND xDetCarPds) AND ((uiNrCarPres - usiCarCntMxPds.CV) <> usiCarCntPdsMx.CV) AND (PackML_State <> mpPACKML_STATE_RESETTING)));
//		usiCarCntMxPds(PV:= uiNrCarPres - usiCarCntPdsMx.CV, LOAD:= ((xDetNulCarMx AND xDetCarMx)   AND ((uiNrCarPres - usiCarCntPdsMx.CV) <> usiCarCntMxPds.CV) AND (PackML_State <> mpPACKML_STATE_RESETTING)));
//	END_IF
//	usiCarCntPdsMx(CD:= xDetCarMx,  CU:= xDetCarPds ,PV:= uiNrCarPres - usiCarCntMxPds.CV, LOAD:= ((xDetNulCarPds AND xDetCarPds) AND ((uiNrCarPres - usiCarCntMxPds.CV) <> usiCarCntPdsMx.CV)));
	//	usiCarCntMxPds(CD:= xDetCarPds, CU:= xDetCarMx, PV:= uiNrCarPres - usiCarCntPdsMx.CV, LOAD:= ((xDetNulCarMx AND xDetCarMx)   AND ((uiNrCarPres - usiCarCntPdsMx.CV) <> usiCarCntMxPds.CV)));
	
	usiCarCntPdsMx(CD:= xFeDetCarMx.Q,  CU:= xFeDetCarPds.Q ,PV:= uiNrCarPres - usiCarCntMxPds.CV, LOAD:= ((xDetNulCarPds AND xDetCarPds) AND ((uiNrCarPres - usiCarCntMxPds.CV) <> usiCarCntPdsMx.CV)));
	usiCarCntMxPds(CD:= xFeDetCarPds.Q, CU:= xFeDetCarMx.Q, PV:= uiNrCarPres - usiCarCntPdsMx.CV, LOAD:= ((xDetNulCarMx AND xDetCarMx)   AND ((uiNrCarPres - usiCarCntPdsMx.CV) <> usiCarCntMxPds.CV)));
	
//	usiCarCntPdsMx(CD:= FastTask.xSperSupClose,  CU:= FastTask.xSperDisClose);
//	usiCarCntMxPds(CD:= FastTask.xSperDisClose, CU:= FastTask.xSperSupClose);
	
	//Index counter @Supply position
	IF xFeDetCarPds.Q AND DriveFB.rRpmAvg > 0 THEN
		usiCarIndexSup := usiCarIndexSup + 1;	
	END_IF
	IF xDetCarPds AND xDetNulCarPds AND usiCarIndexSup <> 0 THEN
		usiCarIndexSup	:= 0;
	END_IF
	
	//Connect PDS value to Trolley
	xTofRelTrol(IN := xFeDetCarPds.Q, PT := UINT_TO_TIME(calcDisIntoTime(DriveFB.rRpmAvg, setTrolSys.config.rRadFricDisc, 0.1 )));
	IF xTofRelTrol.Q AND FastTask.xKicker1Out AND DriveFB.rRpmAvg > 0 THEN
		aPdsCarVal[usiCarIndexSup] := usiPdsVal;
	END_IF
	
	
	//Index counter @discharge position
	IF xFeDetCarMx.Q AND DriveFB.uiMovSpd > 0 THEN
		usiCarIndexDis 	:= usiCarIndexDis + 1;
		aPdsCarVal[usiCarIndexDis] := 0;					//reset Arrayvalue.
	END_IF
	IF xDetCarMx AND xDetNulCarMx AND usiCarIndexDis <> 0 THEN
		usiCarIndexDis	:= 0;
	END_IF;
	
	//Count filled position within ProductArray
	usiProdInMach := 0;
	FOR i:=0 TO (SIZEOF(aPdsCarVal)/ SIZEOF(aPdsCarVal[0]))-1 DO
		IF aPdsCarVal[i] > 0 THEN
			usiProdInMach := usiProdInMach + 1;
		END_IF
	END_FOR

	//Clear table
	IF xResetPdsTable THEN
		FOR i:=0 TO (SIZEOF(aPdsCarVal)/ SIZEOF(aPdsCarVal[0])) DO
			aPdsCarVal[i] := 0;
		END_FOR;
		xResetPdsTable:= FALSE;
	END_IF;
	
	
	//overkamDetectie
	IF NOT(xInitCarDone) THEN
		usiCarSupDis := 0;
		usiCarDisSup :=	0;
	ELSIF usiCarIndexSup > usiCarIndexDis THEN
		usiCarSupDis := (UINT_TO_USINT(uiNrCarPres) - usiCarIndexSup) + usiCarIndexDis;
		usiCarDisSup := usiCarIndexSup - usiCarIndexDis;
	ELSIF usiCarIndexSup < usiCarIndexDis  THEN
		usiCarSupDis := usiCarIndexSup - usiCarIndexDis;
		usiCarDisSup := (UINT_TO_USINT(uiNrCarPres) - usiCarIndexSup) + usiCarIndexDis;
	ELSIF usiCarIndexSup = usiCarIndexDis THEN
		usiCarSupDis := usiCarIndexSup - usiCarIndexDis;
		usiCarDisSup := usiCarIndexDis - usiCarIndexSup;
	END_IF
	
	xPdsMxFull := usiCarCntPdsMx.CV >= usiSetPdsMxFull;
	xMxPdsFull := usiCarCntMxPds.CV >= usiSetMxPdsFull;
	//xPdsMxFull := usiCarSupDis >= usiSetPdsMxFull;
	//xMxPdsFull := usiCarDisSup >= usiSetMxPdsFull;
		
		
	//Report
	IF EDGEPOS(xPdsMxFull) THEN
		Report.udiBufferFull	:= Report.udiBufferFull + 1;
	END_IF
	
	
	(* ---------- request Release one Carrier  ---------- *)		
	xRsRejOneCar(RESET1:= xSperSupClose, SET := xRejCar);
	
	//report
	IF EDGEPOS(xRejCar) THEN
		Report.udiManReleaseTrolley	:= Report.udiManReleaseTrolley + 1;
	END_IF
	
	(* ---------- Retract Kickers   ---------- *)
	xTofRetKickers.IN := (xReRejRemProds.Q OR NOT(LineInfo.Sup.xRun)) OR PackML_State = mpPACKML_STATE_STOPPING;
	xTofRetKickers.PT := MAX(setRehanger.aK1.timAct, setRehanger.aK2.timAct);
	xTofRetKickers();
	
	
	
	(* ---------- Speed control Drive  ---------- *)			
	xSpdCarDisc.Run 			:=  (xModeProd AND (
									(PackML_State = mpPACKML_STATE_RESETTING AND xEnaFricDiscInit) OR
									(PackML_State = mpPACKML_STATE_EXECUTE AND xInitCarDone AND xEnaFricDisc) OR
									(PackML_State = mpPACKML_STATE_SUSPENDING) OR
									(PackML_State = mpPACKML_STATE_UNSUSPENDING) OR
									(PackML_State = mpPACKML_STATE_HOLDING) OR
									(PackML_State = mpPACKML_STATE_UNHOLDING) OR
									(PackML_State = mpPACKML_STATE_COMPLETING AND NOT CTDSusLoop.Q)
									)) OR 
									(xModeMan AND gMmiCmd.Man.xDriveRun);
	
	xSpdCarDisc.Ramp			:= setTrolSys.config.aMoviGear.iRamp;
										
	MS_TO_RPM_0.rMeterSecond 	:= Enc.MeterSec;
	MS_TO_RPM_0.rRadius 		:= setTrolSys.config.rRadFricDisc;	
	MS_TO_RPM_0.rSpdFact		:= calcSpeedRatio(LineInfo.Sup.uiSpdMin, LineInfo.Dis.uiSpdMin);
	MS_TO_RPM_0.xEnaSpdFact		:= xModeProd;
	MS_TO_RPM_0();
	
	xSpdCarDisc.SpeedRPM 		:= MS_TO_RPM_0.rMS_TO_RPM;

	
	IF xModeProd THEN
		IF xInitCarDone  THEN
			rFricDiscSpeed 	:= xSpdCarDisc.SpeedRPM;
		ELSE
			rFricDiscSpeed	:= xSpdCarDisc.MinRPM;
		END_IF;
		
		RPM_to_MoviGearSpd_0.rRPM 			:= rFricDiscSpeed;
		RPM_to_MoviGearSpd_0.uiMaxCtrlLvl 	:= setTrolSys.config.aMoviGear.uiMaxCtrlLvl;
		RPM_to_MoviGearSpd_0.uiMinCtrlLvl 	:= setTrolSys.config.aMoviGear.uiMinCtrlLvl;
		RPM_to_MoviGearSpd_0.rMaxRPM 		:= setTrolSys.config.aMoviGear.rMaxRPM;
		RPM_to_MoviGearSpd_0.rMinRPM 		:= setTrolSys.config.aMoviGear.rMinRPM;
		RPM_to_MoviGearSpd_0.xReverse 		:= setTrolSys.config.aMoviGear.xReverse;
		RPM_to_MoviGearSpd_0();
		
		xSpdCarDisc.SpeedDrive 	:= RPM_to_MoviGearSpd_0.iRPM_to_MoviGearSpd;

	ELSIF xModeMan THEN
		xSpdCarDisc.SpeedDrive := Percent_to_MoviGearSpd(gMmiCmd.Man.usiSpdDrive, setTrolSys.config.aMoviGear);
	END_IF;	
	
	
	gFricDisc := xSpdCarDisc;
		
	(* ---------- Disc Drying  ---------- *)
	
	xDiscDrying	:= setGeneral.xDiscDrying AND PackML_State = mpPACKML_STATE_EXECUTE AND (xTrolFrictSupEnaDiscDry OR xTrolFrictDisEnaDiscDry);
	
	
//	(* ---------- Check sensors  ---------- *)

	
	
	(* ---------- Reset ES stop relay  ---------- *)
	xTofResetES(IN:= (PackML_State = mpPACKML_STATE_CLEARING), PT:= T#100ms);
	
		
	
	(* ---------- Indication light  ---------- *)
//	CASE IndicationLight OF
//	//	xReject	: xIndicationLight 	:= ShakleTable.FB.xReject;
//	//	xCarGo	: xIndicationLight	:= ShakleTable.xCarGo;
//	//	xGoOut	: xIndicationLight	:= ShakleTable.audiKickerPos.xGoOut;
//	//	xGoIn	: xIndicationLight	:= ShakleTable.audiKickerPos.xGoIn;
//	END_CASE
	
	
	(* ---------- Reset Report Counters  ---------- *)
	IF Report.xReset THEN
		Report.udiBufferFull		:= 0;
		Report.udiK1In				:= 0;
		Report.udiK1Out				:= 0;
		Report.udiK2In				:= 0;
		Report.udiK2Out				:= 0;
		Report.udiManReleaseTrolley	:= 0;
		Report.udiShk0DisEarly		:= 0;
		Report.udiShk0DisMissed		:= 0;
		Report.udiShk0SupEarly		:= 0;
		Report.udiShk0SupMissed		:= 0;
		Report.udiSperDisClose		:= 0;
		Report.udiSperDisOpen		:= 0;
		Report.udiSperSupClose		:= 0;
		Report.udiSperSupOpen		:= 0;
		Report.udiTrolleyRoundDisOK	:= 0;
		Report.udiTrolleyRoundSupOK	:= 0;
		Report.xReset				:= FALSE;
	END_IF
	
	(* ---------- //Connection to FastTask  ---------- *)
	Rehanger.aDis 				:= aDis;
	Rehanger.ResetState			:= ResetState;
	Rehanger.usiCarIndexSup		:= usiCarIndexSup;
	Rehanger.usiCarIndexDis 	:= usiCarIndexDis;
	Rehanger.xDiscDrying		:= xDiscDrying;
	Rehanger.xInitSperStart		:= xInitSperStart;
	Rehanger.xModeMan			:= xModeMan;
	Rehanger.xModeProd			:= xModeProd;
	Rehanger.xMxPdsFull			:= xMxPdsFull;
	Rehanger.xPdsMxFull			:= xPdsMxFull;
	Rehanger.xRejCar			:= xRejCar;
	Rehanger.xResetInitKickers	:= xResetInitKickers;
	Rehanger.xRsRejOneCar		:= xRsRejOneCar.Q1;	
	Rehanger.xRetractKickers	:= xTofRetKickers.Q;
	hw.Do_.xResetES				:= xTofResetES.Q;
	
	
	
	(* ---------- Warnings & Alarmen  ---------- *)
	Alm[1].xTrigger	:= PackML_Mode = 1 AND PackML_State = mpPACKML_STATE_EXECUTE AND NOT(xSupLineRun);
	Alm[1].Type		:= Warning;
	Alm[1].sName	:= 'Line Supply stop during production';
	Alm[1].AlarmCode:= 'W0100';
	
	Alm[2].xTrigger	:= PackML_Mode = 1 AND PackML_State = mpPACKML_STATE_EXECUTE AND NOT(xDisLineRun);
	Alm[2].Type		:= Warning;
	Alm[2].sName	:= 'Line Discharge stop during production';
	Alm[2].AlarmCode:= 'W0101';
	
	
	//TODO MOET DIT HIER OF KAN DIT OOK IN EEN LANGZAMERE TAAK
	
	Alm[3].xTrigger	:= FALSE;
	Alm[3].xOn		:= hw.IO_.Mod3.chnOk[4];
	Alm[3].xOff		:= NOT(hw.IO_.Mod3.chnOk[4]);
	Alm[3].Type		:= Alarm;
	Alm[3].sName	:= 'CDT-SUP Failure';
	Alm[3].AlarmCode:= 'A0004';
	
	Alm[4].xTrigger	:= FALSE;
	Alm[4].xOn		:= hw.IO_.Mod4.chnOk[1];
	Alm[4].xOff		:= NOT(hw.IO_.Mod4.chnOk[1]);
	Alm[4].Type		:= Alarm;
	Alm[4].sName	:= 'CDT-SUP-0 Failure';
	Alm[4].AlarmCode:= 'A0005';
	
	Alm[5].xTrigger	:= FALSE;
	Alm[5].xOn		:= hw.IO_.Mod4.chnOk[2];
	Alm[5].xOff		:= NOT(hw.IO_.Mod4.chnOk[2]);
	Alm[5].Type		:= Alarm;
	Alm[5].sName	:= 'CDT-DIS Failure';
	Alm[5].AlarmCode:= 'A0006';
	
	Alm[6].xTrigger	:= FALSE;
	Alm[6].xOn		:= hw.IO_.Mod4.chnOk[3];
	Alm[6].xOff		:= NOT(hw.IO_.Mod4.chnOk[3]);
	Alm[6].Type		:= Alarm;
	Alm[6].sName	:= 'CDT-DIS-0 Failure';
	Alm[6].AlarmCode:= 'A0007';	
	
	//	Alm[].xTrigger	:= FALSE;
	//	Alm[].xOn		:= FALSE;
	//	Alm[].xOff		:= FALSE;
	//	Alm[].Type		:= Warning OR Alarm;
	//	Alm[].sName		:= '';
	//	Alm[].AlarmCode:= 'W0000';
	
END_PROGRAM
