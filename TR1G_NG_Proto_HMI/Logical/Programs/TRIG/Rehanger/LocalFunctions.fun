
FUNCTION_BLOCK getTextFromId
	VAR_INPUT
		sTextID : STRING[255];
	END_VAR
	VAR_OUTPUT
		sResult : STRING[255];
	END_VAR
	VAR
		preTextID : STRING[255];
		ArTextSysGetSystemLanguage_0 : ArTextSysGetSystemLanguage;
		ArTextSysGetText_0 : ArTextSysGetText;
	END_VAR
END_FUNCTION_BLOCK
