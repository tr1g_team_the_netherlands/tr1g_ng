
PROGRAM _CYCLIC
	
	(* ---------- Inputs from rehanger ---------- *)
	aDis 				:= Rehanger.aDis;
	ResetState			:= Rehanger.ResetState;
	xInitSperStart		:= Rehanger.xInitSperStart;
	xModeMan			:= Rehanger.xModeMan;
	xModeProd			:= Rehanger.xModeProd;
	xPdsMxFull			:= Rehanger.xPdsMxFull;
	xMxPdsFull			:= Rehanger.xMxPdsFull;
	xRsRejOneCar		:= Rehanger.xRsRejOneCar;
	xRejCar 			:= Rehanger.xRejCar;
	usiCarIndexSup		:= Rehanger.usiCarIndexSup;
	usiCarIndexDis		:= Rehanger.usiCarIndexDis;
	xResetInitKickers	:= Rehanger.xResetInitKickers;
	xDiscDrying			:= Rehanger.xDiscDrying;
	xRetractKick		:= Rehanger.xRetractKickers;
	
	(* ---------- Inputs from rejector ---------- *)
	xCylRejOpen			:= Rejector.xCylRejOpen;
	xCylRejClose		:= Rejector.xCylRejClose;
	
	
	(*---------- Redefinitions inputs ----------*)
	xShkSupDet		:= hw.Di_.xS01;
	xShkDisDet		:= hw.Di_.xS02;
	xDetProd		:= NOT(hw.Di_.xS03);
	xDetCarPds		:= hw.Di_.xS04;
	xDetNulCarPds	:= hw.Di_.xS05;
	xDetCarMx		:= hw.Di_.xS06;
	xDetNulCarMx	:= hw.Di_.xS07;
	
	
	
	(* ---------- Encoder Calculations ---------- *)
	EncSup.EncSet 	:= Cfg.Config.EncSup; 
	EncSup.uiRawEnc := hw.Com_.uiEncSup;
	EncSup();
	
	EncDis.EncSet 	:= Cfg.Config.EncDis;
	EncDis.uiRawEnc := hw.Com_.uiEncDis;
	EncDis();
	
	(* ---------- S01 and S02 Generation based on ENC ---------- *)
	EncToPulsGeneration_Sup.iEncPos 	:= hw.Com_.uiEncSup;
	EncToPulsGeneration_Sup.Cfg			:= Cfg.Config.EncSup;
	EncToPulsGeneration_Sup.aiPulsLoc	:= ADR(Cfg.Config.EncSup.iSensorPulsPos);
	EncToPulsGeneration_Sup.xCalibrate	:= xCalZeroPosEncSup;
	EncToPulsGeneration_Sup();
	 
	EncToPulsGeneration_Dis.iEncPos 	:= hw.Com_.uiEncDis;
	EncToPulsGeneration_Dis.Cfg			:= Cfg.Config.EncDis;
	EncToPulsGeneration_Dis.aiPulsLoc	:= ADR(Cfg.Config.EncDis.iSensorPulsPos);
	EncToPulsGeneration_Dis.xCalibrate	:= xCalZeroPosEncDis;
	EncToPulsGeneration_Dis();
	
	xCalUnlocked:= (PackML_State = mpPACKML_STATE_ABORTED) AND (PackML_Mode = Maintenance);
	
	hw.Di_.xS01	:= EncToPulsGeneration_Sup.OUT;
	hw.Di_.xS02	:= EncToPulsGeneration_Dis.OUT;
	
	
	//OUTPUT - Connect to Global var
	gEncSup := EncSup.Enc;
	gEncDis	:= EncDis.Enc;
	
	
	(* ---------- Calculate LineSpeeds ---------- *)
	//change bufferlenght to get faster a stable result
	avgCalcSpdSup.xEnable		:= EncSup.Enc.MeterSec > 0;
	avgCalcSpdSup.rIn			:= (EncSup.Enc.RPM * Cfg.Config.EncSup.rShakleAmount);
	avgCalcSpdSup.uiStdBufsize	:= 3500;
	avgCalcSpdSup.uiChgBufsize	:= 250;
	avgCalcSpdSup.rDevFactor	:= 0.1;
	avgCalcSpdSup();
	
	avgCalcSpdDis.xEnable		:= EncDis.Enc.MeterSec > 0;
	avgCalcSpdDis.rIn			:= (EncDis.Enc.RPM * Cfg.Config.EncDis.rShakleAmount);
	avgCalcSpdDis.uiStdBufsize	:= 3500;
	avgCalcSpdDis.uiChgBufsize	:= 250;
	avgCalcSpdDis.rDevFactor	:= 0.1;
	avgCalcSpdDis();
	
	//OUTPUT 
	LineInfo.Sup.uiSpdMin	:= avgCalcSpdSup.uiOut;
	LineInfo.Dis.uiSpdMin	:= avgCalcSpdDis.uiOut;
	LineInfo.Sup.uiSpdHour	:= avgCalcSpdSup.uiOut * 60;
	LineInfo.Dis.uiSpdHour	:= avgCalcSpdDis.uiOut * 60;
	LineInfo.Sup.xRun		:= LineInfo.Sup.uiSpdMin > 10.0;
	LineInfo.Dis.xRun		:= LineInfo.Dis.uiSpdMin > 10.0;
	
	
	(* ---------- INPUT PDS Signal ---------- *)
	pdsDataInput_0.usiMode 		:= 0;
	pdsDataInput_0.xPdsWire1 	:= hw.Di_.xPdsSig1;
	pdsDataInput_0.xPdsWire2 	:= hw.Di_.xPdsSig2;
	pdsDataInput_0();
	
	
	//Check for broken wires / Relais
	PdsCheckBufSig_0.usiPdsNr	:= pdsDataInput_0.usiPdsNr;
	PdsCheckBufSig_0.xTrigger	:= pdsDataInput_0.xTrigger;
	PdsCheckBufSig_0.xPdt		:= xDetProd;
	PdsCheckBufSig_0();
		

	(* ---------- ShackleTable ---------- *)
	//calculate shackles inbetween kickerpos and xS01
	usiShkBtwKick_S01 := REAL_TO_USINT(floor(setRehanger.aK1.rDisOut / UDINT_TO_REAL(aDis.udiShkLength)));
	usiShkBtwKick_S01 := LIMIT(1, usiShkBtwKick_S01, 100);

	ShackleTab.udiEncPos		:= EncSup.Enc.EncoderPosition;
	ShackleTab.udiEncPosRes		:= EncSup.Enc.PulsesRevolution;
	ShackleTab.xAddNewRow	 	:= xShkSupDet;
	ShackleTab.xProdDet 		:= xDetProd; 
	ShackleTab.aPulsDis			:= aDis;
	ShackleTab.xCarLeft			:= NOT(xDetCarPds);
	ShackleTab.xPdsTrigger 		:= pdsDataInput_0.xTrigger;
	ShackleTab.usiPdsValue		:= pdsDataInput_0.usiPdsNr;
	ShackleTab.usiShkBtwKick_S01_input := usiShkBtwKick_S01;
	ShackleTab.xResetReport		:= gMmiCmd.Operator.xResetPerformancePage;
	//ShackleTab.xResetTable 	:= xResetTable;	
	ShackleTab();
	
	//OUTPUTS
	xShkTabCarGo	:= ShackleTab.xCarGo;
	TableActions	:= ShackleTab.audiKickerPos;
		
	Kick.xSetK1Out	:= TableActions.xGoOut1;
	Kick.xSetK2Out	:= TableActions.xGoOut2;
	Kick.xSetK1In 	:= TableActions.xGoIn1;
	Kick.xSetK2In	:= TableActions.xGoIn2;
	
		
	gDataLogging.xTrigger			:= ShackleTab.xDataLogTrigger;
	gDataLogging.xOverFlow			:= ShackleTab.xOverFlowTable;
	gDataLogging.PVReg.usiNrOfPvs	:= 17;
	gDataLogging.uiMaxSamp			:= Cfg.Config.General.uiMaxSampleLogger;
	
	
	(* ---------- OUTPUT PDS Signal ---------- *)
	pdsDataOutput_0(usiPdsValue := ShackleTab.audiKickerPos.usiPdsValPuls , timPdsPulse := T#100ms);	
	hw.Do_.xPdsSig1			:= pdsDataOutput_0.xPdsSig1;
	hw.Do_.xPdsSig2			:= pdsDataOutput_0.xPdsSig2;
	
	
	
	(* ---------- Gates / Spers  ---------- *)		
	SperSup.Set 			:= setTrolSys; 
	SperSup.Cfg				:= setTrolSys.config;
	SperSup.xDetHook	 	:= xShkSupDet; 
	SperSup.xDetHookInit	:= xShkDisDet;
	SperSup.Enc 			:= gEncSup; 
	SperSup.xInitRound		:= xInitSperStart;
	SperSup.xInitSper		:= xResetInitKickers;
	SperSup.rSpdRpmFricDisc := DriveFB.rRpmAvg;
	SperSup.xAfterSlotFull	:= xPdsMxFull;
	SperSup.xReqRelCar		:= (PackML_State = mpPACKML_STATE_EXECUTE 		AND xShkTabCarGo) OR
	(PackML_State = mpPACKML_STATE_EXECUTE		AND xRsRejOneCar) OR 
	(PackML_State = mpPACKML_STATE_SUSPENDING	AND xRsRejOneCar);
	SperSup();
	
	xSperSupOpen	:= 	(xModeProd AND (
	(PackML_State = mpPACKML_STATE_RESETTING 	AND SperSup.xSperOpen AND ResetState > rrsWaitForRunDisLine) OR
	(PackML_State = mpPACKML_STATE_EXECUTE  	AND SperSup.xSperOpen) OR 
	(PackML_State = mpPACKML_STATE_SUSPENDING  	AND SperSup.xSperOpen) OR
	(PackML_State = mpPACKML_STATE_COMPLETING  	AND SperSup.xSperOpen)
	)) OR
	(xModeMan AND gMmiCmd.Man.xSperSupOpen);		
		
												
	xSperSupClose	:= 	(xModeProd AND (
	(PackML_State = mpPACKML_STATE_RESETTING 	AND SperSup.xSperClose AND ResetState > rrsWaitForRunDisLine) OR
	(PackML_State = mpPACKML_STATE_EXECUTE 		AND SperSup.xSperClose) OR
	(PackML_State = mpPACKML_STATE_SUSPENDING 	AND SperSup.xSperClose) OR
	(PackML_State = mpPACKML_STATE_COMPLETING 	AND SperSup.xSperClose)
	)) OR
	(xModeMan AND gMmiCmd.Man.xSperSupClose);
	

	//when no PDS value, but product is taken out the line
	IF xKicker1OUT AND TableActions.usiPdsVal = 0 THEN
		TableActions.usiPdsVal := 10;
	END_IF
		

	//Friction Detection release trolley
	TrolFrictDet_Sup.xRelTrolley 		:= xSperSupOpen;
	TrolFrictDet_Sup.xDetTrolley 		:= xDetCarPds;
	TrolFrictDet_Sup.usiTrolNr 			:= usiCarIndexSup;
	TrolFrictDet_Sup.xReset 			:= xResetTrolFricTable;
	TrolFrictDet_Sup.uiBoundaryPerc 	:= USINT_TO_UINT(Cfg.Config.General.usiAlarmLvlTrol);
	TrolFrictDet_Sup.xEnable 			:= (PackML_State = mpPACKML_STATE_EXECUTE);
	TrolFrictDet_Sup();
	
	//report
	IF EDGEPOS(xSperSupOpen) THEN 
		Report.udiSperSupOpen	:= Report.udiSperSupOpen + 1;
	END_IF
	IF EDGEPOS(xSperSupClose) THEN 
		Report.udiSperSupClose	:= Report.udiSperSupClose + 1;
	END_IF
		
	
	
	(* ---------- MX Sper  ---------- *)
	//if there is a blokkage. Calculate the minimum time that the sensors could be off
	//xTonTrolErrDis(IN := (NOT(xDetCarMx OR xDetNulCarMx) OR (NOT(xDetCarMx) AND xDetNulCarMx)) , PT := UINT_TO_TIME(calcDisIntoTime(DriveFB.rRpmAvg, Cfg.CarDisc.config.rRadFricDisc ,0.07)));
			
	IF xInitSperStart THEN
		xPdsMxFull		:= FALSE;
		xMxPdsFull		:= FALSE;
	END_IF
			
	SperDis.Set 			:= setTrolSys;
	SperDis.Cfg				:= setTrolSys.config;
	SperDis.xDetHook		:= xShkDisDet;
	SperDis.xCarDet			:= xDetCarMx;
	SperDis.xCarDetNul		:= xDetNulCarMx;
	SperDis.xInitRound		:= xInitSperStart;
	SperDis.rSpdRpmFricDisc := DriveFB.rRpmAvg; 	
	SperDis.xAfterSlotFull	:= xMxPdsFull;
	SperDis.xDiAfterSlot	:= FALSE;
	SperDis.xRelTrol		:= xRejCar AND (PackML_State = mpPACKML_STATE_EXECUTE); 
	SperDis();
	
	xSperDisOpen	:= 	(xModeProd AND (
	(PackML_State = mpPACKML_STATE_RESETTING 	AND SperDis.xSperOpen AND ResetState > rrsWaitForRunDisLine) OR
	(PackML_State = mpPACKML_STATE_EXECUTE   	AND SperDis.xSperOpen) OR
	(PackML_State = mpPACKML_STATE_SUSPENDING 	AND SperDis.xSperOpen) OR
	(PackML_State = mpPACKML_STATE_COMPLETING 	AND SperDis.xSperOpen)
	)) OR 
	(xModeMan AND gMmiCmd.Man.xSperDisOpen);					
	
	xSperDisClose	:= 	(xModeProd AND (
	(PackML_State = mpPACKML_STATE_RESETTING 	AND SperDis.xSperClose AND ResetState > rrsWaitForRunDisLine) OR
	(PackML_State = mpPACKML_STATE_EXECUTE   	AND SperDis.xSperClose) OR
	(PackML_State = mpPACKML_STATE_SUSPENDING 	AND SperDis.xSperClose) OR	
	(PackML_State = mpPACKML_STATE_COMPLETING 	AND SperDis.xSperClose) 
	)) OR  
	(xModeMan AND gMmiCmd.Man.xSperDisClose);
	
	IF EDGEPOS(SperDis.xTrolJamReport AND PackML_State = mpPACKML_STATE_EXECUTE) THEN
		Report.udiTrolErrDis := Report.udiTrolErrDis + 1;
	END_IF	
	
	
	//Friction Detection release trolley
	TrolFrictDet_Dis.xRelTrolley 		:= xSperDisOpen;
	TrolFrictDet_Dis.xDetTrolley 		:= xDetCarMx;
	TrolFrictDet_Dis.usiTrolNr 			:= usiCarIndexDis;
	TrolFrictDet_Dis.xReset 			:= xResetTrolFricTable;
	TrolFrictDet_Dis.uiBoundaryPerc 	:= USINT_TO_UINT(Cfg.Config.General.usiAlarmLvlTrol);
	TrolFrictDet_Dis.xEnable 			:= (PackML_State = mpPACKML_STATE_EXECUTE);
	TrolFrictDet_Dis();
	
	
	//report
	IF EDGEPOS(xSperDisOpen) THEN 
		Report.udiSperDisOpen	:= Report.udiSperDisOpen + 1;
	END_IF
	IF EDGEPOS(xSperDisClose) THEN 
		Report.udiSperDisClose	:= Report.udiSperDisClose + 1;
	END_IF
	
	
	(* ---------- Kickers  ---------- *)
	Kick.xTpK1Out(IN := Kick.xSetK1Out, PT:= Cfg.Rehanger.aK1.timAct);
	Kick.xTpK2Out(IN := Kick.xSetK2Out, PT:= Cfg.Rehanger.aK2.timAct);
	Kick.xTpK1In( IN := Kick.xSetK1In,  PT:= Cfg.Rehanger.aK1.timAct);
	Kick.xTpK2In( IN :=	Kick.xSetK2In,  PT:= Cfg.Rehanger.aK2.timAct);
				
	xKicker1OUT :=  (xModeProd AND PackML_State = mpPACKML_STATE_EXECUTE	AND Kick.xTpK1Out.Q) OR
					(xModeMan AND  gMmiCmd.Man.xK1Out);				
	
	xKicker2OUT	:=  (xModeProd AND PackML_State = mpPACKML_STATE_EXECUTE AND Kick.xTpK2Out.Q) OR
					(xModeMan AND gMmiCmd.Man.xK2Out);
	
	xKicker1IN  :=  (xModeProd AND (
					(PackML_State = mpPACKML_STATE_RESETTING	AND xResetInitKickers) OR 
					(PackML_State = mpPACKML_STATE_EXECUTE 		AND Kick.xTpK1In.Q) OR
						(xRetractKick)
					)) OR
					(xModeMan AND gMmiCmd.Man.xK1In);
	
	xKicker2IN 	:= 	(xModeProd AND (
					(PackML_State = mpPACKML_STATE_RESETTING AND xResetInitKickers) OR 
					(PackML_State = mpPACKML_STATE_EXECUTE 		AND Kick.xTpK2In.Q) OR
						(xRetractKick)
					)) OR
					(xModeMan AND gMmiCmd.Man.xK2In);
	
	IF xKicker1OUT THEN	Kick.K1IsOut := TRUE;  Kick.K1IsIn := FALSE; END_IF
	IF xKicker2OUT THEN Kick.K2IsOut := TRUE;  Kick.K2IsIn := FALSE; END_IF
	IF xKicker1IN  THEN Kick.K1IsOut := FALSE; Kick.K1IsIn := TRUE;  END_IF
	IF xKicker2IN  THEN Kick.K2IsOut := FALSE; Kick.K2IsIn := TRUE;  END_IF
	
	//Report
	IF EDGEPOS(xKicker1OUT) THEN Report.udiK1Out	:= Report.udiK1Out 	+ 1; END_IF
	IF EDGEPOS(xKicker1IN) 	THEN Report.udiK1In 	:= Report.udiK1In 	+ 1; END_IF
	IF EDGEPOS(xKicker2OUT) THEN Report.udiK2Out	:= Report.udiK2Out 	+ 1; END_IF
	IF EDGEPOS(xKicker2IN) 	THEN Report.udiK2In 	:= Report.udiK2In 	+ 1; END_IF	
	
	(*********** ENABLE / DISABLE DISC BLOWING **********)
	xEnaFricDisc	:=(gMmiCmd.Operator.xEnaDiscDrying OR xDiscDrying) AND DriveFB.uiMovSpd > 800 AND ((PackML_State = mpPACKML_STATE_EXECUTE) OR (PackML_State = mpPACKML_STATE_RESETTING));
	
	
	//Festo Valves 
	FestoValves.aValve[0].A	:= xSperSupClose;
	FestoValves.aValve[0].B	:= xSperSupOpen;
	FestoValves.aValve[1].A	:= xSperDisClose;
	FestoValves.aValve[1].B	:= xSperDisOpen;
	FestoValves.aValve[2].A	:= xKicker1OUT;
	FestoValves.aValve[2].B	:= xKicker1IN;
	FestoValves.aValve[3].A	:= xKicker2OUT;
	FestoValves.aValve[3].B	:= xKicker2IN;
	FestoValves.aValve[4].A	:= xCylRejOpen;
	FestoValves.aValve[4].B	:= xCylRejClose;
	FestoValves.aValve[5].A	:= FALSE;
	FestoValves.aValve[5].B	:= xEnaFricDisc;
	FestoValves.aValve[6].A	:= xEnableAir AND NOT gMmiCmd.xReleaseAir;
	FestoValves.aValve[6].B	:= FALSE;
	FestoValves.aValve[7].B	:= FALSE;
	FestoValves.aValve[7].B	:= FALSE;
	
	FestoValves.uiPresIn[0]	:= hw.Com_.uiAirPres1;
	FestoValves.uiPresIn[1]	:= hw.Com_.uiAirPres2;
	FestoValves.uiPresIn[2]	:= hw.Com_.uiAirPres3;
	FestoValves.uiPresIn[3]	:= hw.Com_.uiAirPres4;
	
	FestoValves();
	
	hw.Com_.aValve1to4 		:= FestoValves.usiValve_1to4;
	hw.Com_.aValve5to8		:= FestoValves.usiValve_5to8;
	gFbAirPres.rAirPres1	:= FestoValves.rPresOut[0];
	gFbAirPres.rAirPres2	:= FestoValves.rPresOut[1];
	gFbAirPres.rAirPres3	:= FestoValves.rPresOut[2];
	gFbAirPres.rAirPres4	:= FestoValves.rPresOut[3];
	
	IF gPackML.StateCurrent = mpPACKML_STATE_EXECUTE AND gFbAirPres.rAirPres1 < gFbAirPres.minAirPres1 THEN
		gFbAirPres.minAirPres1 := gFbAirPres.rAirPres1;
	END_IF
	
	
	(* ---------- OUTPUTS  ---------- *)
	FastTask.TableActions 	:= TableActions;
	FastTask.xShkTabCarGo 	:= xShkTabCarGo;
	FastTask.xSperSupOpen 	:= xSperSupOpen;
	FastTask.xSperSupClose 	:= xSperSupClose;
	FastTask.xSperDisOpen 	:= xSperDisOpen;
	FastTask.xSperDisClose 	:= xSperDisClose;
	FastTask.xKicker1Out	:= xKicker1OUT;
	FastTask.xTrolFrictSupEnaDiscDry := TrolFrictDet_Sup.xEnaDrying;
	FastTask.xTrolFrictDisEnaDiscDry := TrolFrictDet_Dis.xEnaDrying;
	

	
	(* ---------- Warnings & Alarmen  ---------- *)
	Alm[0].xTrigger	:= SperDis.xTrolJamReport AND PackML_State = mpPACKML_STATE_EXECUTE;	
	Alm[0].Type		:= Alarm;
	Alm[0].sName	:= 'Trolley block ACM side';
	Alm[0].AlarmCode:= 'A0100';
	
	Alm[1].Type		:= Alarm;
	Alm[1].xOn		:= PdsCheckBufSig_0.xPdsBit0_Fault;
	Alm[1].xOff		:= NOT(PdsCheckBufSig_0.xPdsBit0_Fault);
	Alm[1].sName	:= 'Pds-Pulse Bit0 no signal';
	Alm[1].AlarmCode:= 'A0022';
	
	Alm[2].Type		:= Alarm;
	Alm[2].xOn		:= PdsCheckBufSig_0.xPdsBit1_Fault;
	Alm[2].xOff		:= NOT(PdsCheckBufSig_0.xPdsBit1_Fault);
	Alm[2].sName	:= 'Pds-Pulse Bit1 no signal';
	Alm[2].AlarmCode:= 'A0023';
	
	Alm[3].Type		:= Alarm;
	Alm[3].xOn		:= PdsCheckBufSig_0.xPDT_Fault;
	Alm[3].xOff		:= NOT(PdsCheckBufSig_0.xPDT_Fault);
	Alm[3].sName	:= 'PDS failure';
	Alm[3].AlarmCode:= 'A0030';
	
	Alm[4].Type		:= Alarm;
	Alm[4].xOn		:= (Cfg.Config.EncSup.iSensorPulsPos[0] = 0) AND (Cfg.Config.EncSup.iSensorPulsPos[1] = 0);
	Alm[4].xOff		:= NOT(Alm[4].xOn);
	Alm[4].sName	:= 'Encoder reference missing SUP';
	Alm[4].AlarmCode:= 'A0101';
	
	Alm[5].Type		:= Alarm;
	Alm[5].xOn		:= (Cfg.Config.EncDis.iSensorPulsPos[0] = 0) AND (Cfg.Config.EncDis.iSensorPulsPos[1] = 0);;
	Alm[5].xOff		:= NOT(Alm[5].xOn);
	Alm[5].sName	:= 'Encoder reference missing DIS';
	Alm[5].AlarmCode:= 'A0102';
	
	
	//	Alm[6].Type		:= Alarm;
	//	Alm[6].xOn		:= ;
	//	Alm[6].xOff		:= ;
	//	Alm[6].sName	:= '';
	//	Alm[6].AlarmCode:= '';

	
	//	Alm[7].Type		:= Alarm;
	//	Alm[7].xOn		:= ;
	//	Alm[7].xOff		:= ;
	//	Alm[7].sName	:= '';
	//	Alm[7].AlarmCode:= '';
END_PROGRAM
