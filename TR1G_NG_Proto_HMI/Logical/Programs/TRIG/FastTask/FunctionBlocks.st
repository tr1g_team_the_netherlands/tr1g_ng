FUNCTION_BLOCK EncoderCalculations
	RTInfo_0(enable := TRUE);
	
	Enc.Circumference		:= EncSet.rShakleAmount * EncSet.rShakleSize; 
	Enc.PulsesMeter			:= UINT_TO_REAL(EncSet.uiResolution) / Enc.Circumference;
	Enc.PulsesRevolution	:= EncSet.uiResolution;
	
	//Entend the range of the encoder.
	udiEncPos	:= UINT_TO_UDINT(uiRawEnc);
	udiEncExt(diEncVal := udiEncPos, diEncResolution := EncSet.uiResolution);
	
	rCycleTime	:= (UDINT_TO_REAL(RTInfo_0.cycle_time) / 1000000.0);
	
	//check windowLength
	IF MTmovingAvg.WindowLength = 0 THEN
		MTmovingAvg(WindowLength := 400, Update := TRUE);
		MTmovingAvg(Update := FALSE);
	END_IF;
	
	MTmovingAvg.Enable			:= TRUE;
	MTmovingAvg.In				:= DINT_TO_REAL(udiEncExt.diDiffEncVal);
	MTmovingAvg();
	
	rEncMS	:= (MTmovingAvg.Out / Enc.PulsesMeter) * (1.0 / rCycleTime);
	
	
	Enc.MeterSec			:= rEncMS; 
	Enc.RPM					:= (60.0 * rEncMS) / Enc.Circumference;
	Enc.EncoderPosition		:= udiEncExt.udiEncoder;	
	
		
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderExtender
	diCurrEncVal 	:= diEncVal;
	
	IF diCurrEncVal <> diOldEncVal THEN
		diDiffEncVal	:= encOverUnderFlow(diCurrEncVal - diOldEncVal, diEncResolution);
		diOldEncVal		:= diCurrEncVal;
	ELSE
		diDiffEncVal 	:= 0;
	END_IF
	
	udiEncoder := udiEncoder + diDiffEncVal;
		
END_FUNCTION_BLOCK

FUNCTION encOverUnderFlow
	IF 	diDiffEncoder >  diResolution / 3 THEN 
		diDiffEncoder := diDiffEncoder - diResolution; 
	END_IF
	IF 	diDiffEncoder < -diResolution / 3 THEN 
		diDiffEncoder := diDiffEncoder + diResolution; 
	END_IF
	
	encOverUnderFlow := diDiffEncoder;
END_FUNCTION 



FUNCTION_BLOCK pdsDataInput
	
	CASE usiMode OF
		0:
			usiPdsNr.0	:= xPdsWire1;
			usiPdsNr.1	:= xPdsWire2;		
			
			xTrigger 	:= 	usiPdsNr > 0;		
			
		1:
			usiPdsNr	:= usiPdsNrDataIn;
	END_CASE;
	
	
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK pdsDataOutput
	
	//Check Rising EDGE. only then generate pulse
	xPdsPulse(IN:= (usiPdsValue <> usiPdsValue_Prev ), PT:= timPdsPulse );
	
	xPdsSig1 := usiPdsValue.0 AND xPdsPulse.Q;
	xPdsSig2 := usiPdsValue.1 AND xPdsPulse.Q;
	
	//reset RisingEdge detection
	usiPdsValue_Prev := usiPdsValue;
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK ShackleTable
	
	//Array size calculation.
	usiArrSize	:= SIZEOF(ShakleTab) / SIZEOF(ShakleTab[0]);
	
	
	Report.xReset := xResetReport;
	
	(*---------- Insert New Row ----------*)
	xReAddNewRow(CLK := xAddNewRow);
	xFeCarLeft(CLK := xCarLeft);
	xReCarLeft(CLK := xCarLeft);
	
	IF xReAddNewRow.Q THEN
		
		//Fill New Row
		ShakleTab[siIndexTab].udiShkNr 		:= udiShkNr;
		ShakleTab[siIndexTab].udiDetShk		:= udiEncPos;
		ShakleTab[siIndexTab].udiDetProdMin	:= aPulsDis.udiProdMin;
		ShakleTab[siIndexTab].udiDetProd	:= 0;
		ShakleTab[siIndexTab].udiDetProdMax	:= aPulsDis.udiProdMax;
		ShakleTab[siIndexTab].ProdDetType	:= noProduct;
		ShakleTab[siIndexTab].udiDetPdsMin	:= aPulsDis.udiPdsMin;
		ShakleTab[siIndexTab].udiDetPds		:= 0;
		ShakleTab[siIndexTab].udiDetPdsMax	:= aPulsDis.udiPdsMax;
		ShakleTab[siIndexTab].usiPdsValue	:= 0;	
		ShakleTab[siIndexTab].udiCarGo		:= aPulsDis.udiCarGo;
		ShakleTab[siIndexTab].xCarLeft		:= FALSE;
		ShakleTab[siIndexTab].xReject		:= FALSE;
		ShakleTab[siIndexTab].udiK1In		:= aPulsDis.udiK1In;
		ShakleTab[siIndexTab].udiK1Out		:= aPulsDis.udiK1Out;
		ShakleTab[siIndexTab].udiK2In		:= aPulsDis.udiK2In;
		ShakleTab[siIndexTab].udiK2Out		:= aPulsDis.udiK2Out;
				
		//increase hook#
		udiShkNr := udiShkNr + 1;
		
		//increase index active row
		siIndexTab := siIndexTab + 1;
		
		//check boundries of index
		IF siIndexTab >= usiArrSize THEN siIndexTab := 0; END_IF
		
		//Increase performance counter
		Report.udiDetShackles := Report.udiDetShackles + 1;
	END_IF
	
	
	 
	(* ---------- INDEX locations ---------- *)	
	//Productdetection Index
	IF udiEncPos > (ShakleTab[usiIndexProdDet].udiDetProdMin + REAL_TO_UDINT(aPulsDis.udiShkLength * 0.95)) AND ShakleTab[usiIndexProdDet].udiDetProdMin > 0 THEN 
		usiIndexProdDet 	:= usiIndexProdDet	 + 1;	
	END_IF
	
	//SetReject or not Index
	IF udiEncPos > (ShakleTab[usiIndexSetRej].udiCarGo - REAL_TO_UDINT(aPulsDis.udiShkLength * 0.1)) AND ShakleTab[usiIndexSetRej].udiCarGo > 0 THEN 
		usiIndexSetRej 	:= usiIndexSetRej + 1;	
	END_IF
	
	//Carrier release Index
	IF udiEncPos > (ShakleTab[usiInexCarGo].udiCarGo + REAL_TO_UDINT(aPulsDis.udiShkLength * 0.95)) AND ShakleTab[usiIndexCarGo].udiCarGo > 0 THEN 
		usiIndexCarGo 		:= usiIndexCarGo 	 + 1;	
	END_IF
	
	//Kickers Actions Index
	IF udiEncPos >= (ShakleTab[usiIndexKickers].udiK1Out + REAL_TO_UDINT(aPulsDis.udiShkLength * rShkPerc)) AND ShakleTab[usiIndexKickers].udiK1Out > 0 THEN
		usiIndexKickers := usiIndexKickers + 1;
	END_IF
	

	//Start Shackle reference point Index
	IF udiEncPos >= (ShakleTab[usiIndexRefShk].udiDetShk + (REAL_TO_UDINT((USINT_TO_REAL(usiShkBtwKick_S01_input) + 0.95) * aPulsDis.udiShkLength))) AND ShakleTab[usiIndexRefShk].udiDetShk > 0 THEN
		usiIndexRefShk := usiIndexRefShk + 1;
	END_IF
	

	//Overflow corrections
	IF usiIndexProdDet 	>= usiArrSize 	THEN usiIndexProdDet 	:= 0; END_IF
	IF usiIndexSetRej>= usiArrSize 		THEN usiIndexSetRej 	:= 0; END_IF
	IF usiIndexCarGo 	>= usiArrSize 	THEN usiIndexCarGo 		:= 0; END_IF
	IF usiIndexKickers 	>= usiArrSize	THEN usiIndexKickers	:= 0; xOverFlowTable := TRUE;
	ELSE xOverFlowTable 	:= FALSE;
	END_IF
	IF usiIndexRefShk 	>= usiArrSize 	THEN usiIndexRefShk		:= 0; END_IF
	
	//Triggers When indexes changes
	xReIndexCarGo		:= usiIndexCarGo <> usiIndexCarGo_Prev;
	xReIndexSetRejChn 	:= usiIndexSetRej <> usiIndexSetRej_Prev;
	xReIndexKickChn		:= usiIndexKickers <> usiIndexKickers_Prev;
	
	
	
	(*---------- Product detection ----------*)
	xReProdDet(CLK := xProdDet);
	IF xReProdDet.Q AND
		udiEncPos >= ShakleTab[usiIndexProdDet].udiDetProdMin AND 
		udiEncPos <= ShakleTab[usiIndexProdDet].udiDetProdMax THEN
		
		//write encoder position on first edge detection
		IF ShakleTab[usiIndexProdDet].udiDetProd = 0 THEN
			ShakleTab[usiIndexProdDet].udiDetProd := udiEncPos;
		END_IF
					
		IF ShakleTab[usiIndexProdDet].ProdDetType = noProduct THEN
			ShakleTab[usiIndexProdDet].ProdDetType := oneLegDetected;
			//Report
			Report.udiDetProd := Report.udiDetProd + 1;
		ELSIF ShakleTab[usiIndexProdDet].ProdDetType = oneLegDetected THEN
			ShakleTab[usiIndexProdDet].ProdDetType := twoLegDetected;
		ELSIF ShakleTab[usiIndexProdDet].ProdDetType = twoLegDetected THEN
			ShakleTab[usiIndexProdDet].ProdDetType := moreLegDetected;
		END_IF
		
	END_IF
		
	xReTriggerPds(CLK := xPdsTrigger);
	

	(*---------- PDS signal detection ----------*)	
	IF udiEncPos > ShakleTab[usiIndexProdDet].udiDetPdsMin AND udiEncPos < ShakleTab[usiIndexProdDet].udiDetPdsMax AND xReTriggerPds.Q THEN			
		ShakleTab[usiIndexProdDet].udiDetPds 		:= udiEncPos;
		ShakleTab[usiIndexProdDet].usiPdsValue		:= usiPdsValue;
	END_IF
			
	//Report
	IF xReTriggerPds.Q THEN
		Report.udiDetPdsSig	:= Report.udiDetPdsSig + 1;
	END_IF
	
	FB.xPdsWindow := udiEncPos > ShakleTab[usiIndexProdDet].udiDetPdsMin AND udiEncPos < ShakleTab[usiIndexProdDet].udiDetPdsMax;
	FB.xPdsTrigger := xPdsTrigger;
	
	
	(*---------- Set REJECT Signal ----------*)
	IF RehangMode.xAuto THEN		
		IF  ShakleTab[usiIndexSetRej].usiPdsValue > 0 THEN
			ShakleTab[usiIndexSetRej].xReject := TRUE;
		END_IF
		
		IF xReIndexCarGo THEN	
			usiCnt := usiCnt + 1;
		END_IF
		
	ELSIF RehangMode.xPhotoCell THEN
		IF  ShakleTab[usiIndexSetRej].ProdDetType > 1 THEN// AND ShakleTab[usiIndexProdDet].xReject = FALSE THEN
			ShakleTab[usiIndexSetRej].xReject := TRUE;
		END_IF
		
	ELSIF RehangMode.xPdsPuls THEN
		IF  ShakleTab[usiIndexSetRej].usiPdsValue > 0 THEN// AND ShakleTab[usiIndexSetRej].xReject = FALSE THEN
			ShakleTab[usiIndexSetRej].xReject := TRUE;
		END_IF
		
	ELSIF RehangMode.xAlternating THEN
		//use alternating on products NOT ON HOOKS
		//use increment a USINT on each product detection.
		//Modulo used for alternating effect.
		
		IF ShakleTab[usiIndexSetRej].ProdDetType > 1 THEN 
			IF RehangMode.xOddEven THEN					
				ShakleTab[usiIndexSetRej].xReject		:= usiCnt MOD 2 = 0;
			ELSE
				ShakleTab[usiIndexSetRej].xReject		:= usiCnt MOD 2 = 1;
			END_IF	
			
			IF xReIndexSetRejChn THEN	
				usiCnt := usiCnt + 1;
			END_IF
		END_IF	
		
	ELSIF RehangMode.xTest THEN
		IF RehangMode.xOddEven THEN					
			ShakleTab[usiIndexSetRej].xReject		:= usiCnt MOD 2 = 0;
		ELSE
			ShakleTab[usiIndexSetRej].xReject		:= usiCnt MOD 2 = 1;
		END_IF	
		IF xReIndexSetRejChn THEN	
			usiCnt := usiCnt + 1;
		END_IF
	END_IF;
			
	(*---------- Release Carrier ----------*)
	
	xCarGo	:= 	udiEncPos >= (ShakleTab[usiIndexCarGo].udiCarGo - (aPulsDis.udiCarGoWindow / 2) - aPulsDis.udiRelCarCor) AND
				udiEncPos <= (ShakleTab[usiIndexCarGo].udiCarGo + (aPulsDis.udiCarGoWindow / 2)) AND
				(ShakleTab[usiIndexCarGo].xReject OR (RehangMode.xAuto AND (usiCnt MOD 4 = 0)));
	
	//Detect that the carrier (Supply side) has left it wait position.
	IF xReCarLeft.Q THEN 
		ShakleTab[usiIndexCarGo].xCarLeft := TRUE;
		
		//report
		Report.udiReleaseTrolleys	:= Report.udiReleaseTrolleys + 1;
	END_IF
	

	(*---------- Kickers ----------*)
		
	IF xReIndexKickChn THEN
		//Get previous row KickerIN ENC values
		IF (usiIndexKickers > 0) THEN
			audiKickerPos.udiK1In := (ShakleTab[usiIndexKickers-1].udiK1In - aPulsDis.udiK1InCor);
			audiKickerPos.udiK2In := (ShakleTab[usiIndexKickers-1].udiK2In - aPulsDis.udiK2InCor);
		ELSIF (usiIndexKickers = 0) THEN
			audiKickerPos.udiK1In := (ShakleTab[usiArrSize-1].udiK1In - aPulsDis.udiK1InCor);
			audiKickerPos.udiK2In := (ShakleTab[usiArrSize-1].udiK2In - aPulsDis.udiK2InCor);
		END_IF	
					
		audiKickerPos.udiK1Out		:= (ShakleTab[usiIndexKickers].udiK1Out - aPulsDis.udiK1OutCor);
		audiKickerPos.udiK2Out		:= (ShakleTab[usiIndexKickers].udiK2Out - aPulsDis.udiK2OutCor);
				
		//PDS value
		usiPdsVal					:= ShakleTab[usiIndexKickers].usiPdsValue;
		audiKickerPos.usiPdsVal		:= usiPdsVal;
		udiStartPosShk				:= udiEncPos;
		audiKickerPos.diProdDet		:= ShakleTab[usiIndexKickers].ProdDetType;
		
		//report
		IF  ShakleTab[usiIndexKickers].ProdDetType = oneLegDetected THEN
			Report.udiDetProd1Leg := Report.udiDetProd1Leg + 1;
		ELSIF ShakleTab[usiIndexKickers].ProdDetType = twoLegDetected THEN
			Report.udiDetProd2Leg := Report.udiDetProd2Leg + 1;
		ELSIF ShakleTab[usiIndexKickers].ProdDetType = moreLegDetected THEN
			Report.udiDetProdMoreLeg := Report.udiDetProdMoreLeg + 1;	
		END_IF;
				
	END_IF	
	
	
	audiKickerPos.xGoOut1	:= 	(udiEncPos >= audiKickerPos.udiK1Out) AND 	udiEncPos <= (audiKickerPos.udiK1Out + aPulsDis.udiKick1ActWindow)
								AND ShakleTab[usiIndexKickers].xReject AND ShakleTab[usiIndexKickers].xCarLeft AND ShakleTab[usiIndexKickers].ProdDetType > 0;
	audiKickerPos.xGoOut2	:= 	(udiEncPos >= audiKickerPos.udiK2Out) AND 	udiEncPos <= (audiKickerPos.udiK2Out + aPulsDis.udiKick2ActWindow)
								AND ShakleTab[usiIndexKickers].xReject AND ShakleTab[usiIndexKickers].xCarLeft AND ShakleTab[usiIndexKickers].ProdDetType > 0;
	FB.xReject := ShakleTab[usiIndexKickers].xReject;
	FB.xCarLeft:= ShakleTab[usiIndexKickers].xCarLeft;
	FB.usiProdType:= DINT_TO_USINT(ShakleTab[usiIndexKickers].ProdDetType);
	
	
	//Create a small pulse at the beginning of the Shackle (referenced to S01).
	rPdsSigShift := LIMIT(0, rPdsSigShift, 1);
	xTpRefS01(IN := udiEncPos >= (ShakleTab[usiIndexRefShk].udiDetShk + (usiShkBtwKick_S01_input * aPulsDis.udiShkLength) + (REAL_TO_UDINT(rPdsSigShift * aPulsDis.udiShkLength))), PT := T#100ms);
	
	myUdiPos 	   := ShakleTab[usiIndexRefShk].udiDetShk + (usiShkBtwKick_S01_input * aPulsDis.udiShkLength) + (REAL_TO_UDINT(rPdsSigShift * aPulsDis.udiShkLength));
	IF 	udiEncPos >= (ShakleTab[usiIndexRefShk].udiDetShk + (usiShkBtwKick_S01_input * aPulsDis.udiShkLength) + (REAL_TO_UDINT(rPdsSigShift * aPulsDis.udiShkLength))) THEN
		audiKickerPos.usiPdsValPuls := ShakleTab[usiIndexRefShk].usiPdsValue;
	END_IF;
	
	
	audiKickerPos.xGoIn1	:= 	udiEncPos >= audiKickerPos.udiK1In AND udiEncPos <= (audiKickerPos.udiK1In + aPulsDis.udiKick1ActWindow) AND
								(ShakleTab[usiIndexKickers].ProdDetType > 0) AND NOT(ShakleTab[usiIndexKickers].xReject);
	audiKickerPos.xGoIn2	:= 	udiEncPos >= audiKickerPos.udiK2In AND udiEncPos <= (audiKickerPos.udiK2In + aPulsDis.udiKick2ActWindow) AND
								(ShakleTab[usiIndexKickers].ProdDetType > 0) AND NOT(ShakleTab[usiIndexKickers].xReject);
		
	//report
	IF EDGEPOS(audiKickerPos.xGoOut1) THEN
		Report.udiRejectionsOut	:= Report.udiRejectionsOut + 1;
	END_IF
	IF EDGEPOS(audiKickerPos.xGoIn1) THEN
		Report.udiRejectionsIn	:= Report.udiRejectionsIn + 1;
	END_IF
	
	
	
	(* ---------- Reset Report Counters  ---------- *)
	IF Report.xReset THEN
		Report.udiDetPdsSig			:= 0;
		Report.udiDetProd			:= 0;
		Report.udiDetProd1Leg		:= 0;
		Report.udiDetProd2Leg		:= 0;
		Report.udiDetProdMoreLeg	:= 0;
		Report.udiDetShackles		:= 0;
		Report.udiRejectionsOut		:= 0;
		Report.udiRejectionsIn		:= 0;
		Report.udiReleaseTrolleys	:= 0;
		//Report.udiShk1Leg			:= 0;
		Report.xReset				:= FALSE;
	END_IF
				
	(* ---------- Reset Complete Table ---------- *)
	//RESET TABLE FOR TESTING 
	IF xResetTable THEN
		siIndexTab			:= 0;
		usiIndexProdDet 	:= 0;
		usiIndexSetRej		:= 0;
		usiIndexCarGo		:= 0;
		usiIndexKickers		:= 0;
		usiIndexRefShk		:= 0;
		
		FOR usiResetTabLoop := 0 TO usiArrSize - 1 DO 
			ShakleTab[usiResetTabLoop].udiShkNr 	:= 0;
			ShakleTab[usiResetTabLoop].udiDetShk	:= 0;
			ShakleTab[usiResetTabLoop].udiDetProdMin:= 0;
			ShakleTab[usiResetTabLoop].udiDetProd	:= 0;
			ShakleTab[usiResetTabLoop].udiDetProdMax:= 0;
			ShakleTab[usiResetTabLoop].ProdDetType	:= 0;
			ShakleTab[usiResetTabLoop].udiDetPdsMin	:= 0;
			ShakleTab[usiResetTabLoop].udiDetPds	:= 0;
			ShakleTab[usiResetTabLoop].udiDetPdsMax	:= 0;
			ShakleTab[usiResetTabLoop].usiPdsValue	:= 0;
			ShakleTab[usiResetTabLoop].udiCarGo		:= 0;
			ShakleTab[usiResetTabLoop].xCarLeft		:= FALSE;
			ShakleTab[usiResetTabLoop].xReject		:= FALSE;
			ShakleTab[usiResetTabLoop].udiK1In		:= 0;
			ShakleTab[usiResetTabLoop].udiK1Out		:= 0;
			ShakleTab[usiResetTabLoop].udiK2In		:= 0;
			ShakleTab[usiResetTabLoop].udiK2Out		:= 0;
		END_FOR
		xResetTable := FALSE;
	END_IF
	
	
	//update trigger indexes
	usiIndexCarGo_Prev 		:= usiIndexCarGo;
	usiIndexSetRej_Prev		:= usiIndexSetRej;
	usiIndexKickers_Prev	:= usiIndexKickers;
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK SperPDS
	//calculate the waittime before releasing a carrier.
	
	//Calculate RPM of the Synchrowheel	
	rRpsPickup			:= (Enc.MeterSec / (2.0 * PI * Cfg.rRadSynchroCar));
	rRpsSynchro			:= rRpsPickup / (5.0 / 6.0); 
	rMsSynchro			:= rRpsSynchro * (2.0 * PI * Cfg.rRadSynchro);

	timWaitBeforeOpen	:= (REAL_TO_TIME(((Set.aSupSlot.rDisBeforeOpen / rMsSynchro) * Cfg.SlipCom)* 1000.0))- (Set.aSupSlot.timCylResponseOpen - Set.aSupSlot.timValveResponseOpen);
	IF timWaitBeforeOpen < T#1ms THEN
		timWaitBeforeOpen	:= T#1ms;
	END_IF;
	
	//create a puls as long as the wanted waittime. And give a trigger on the Falling Edge
	xTpWaitTime(IN:= xDetHook, PT:= timWaitBeforeOpen); 
	xFeWait(CLK:= xTpWaitTime.Q);
	

	//INIT remember release puls from Discharge line
	xRSReleaseDis(SET := xDetHookInit , RESET1 := xSperOpen );
	
	//Calculate the open time of the Sper. Based on a distance
	timWaitOpen	:= REAL_TO_TIME((Set.aSupSlot.rDisOpen / (((PI * 2.0 * Cfg.rRadFricDisc) * rSpdRpmFricDisc) / 60.0))*1000.0);
	
	xTpShakleDetect(IN := xDetHook, PT := timWaitOpen);
	xReleasesper := xFeWait.Q AND			
					(   (xInitRound AND xRSReleaseDis.Q1 ) OR 
					(NOT(xInitRound OR xAfterSlotFull ) AND xReqRelCar));
	
	//Output OPEN
	xTpSperOpen(IN := xReleasesper OR xInitSper, PT := timWaitOpen );
	xSperOpen := xTpSperOpen.Q;	
	
	//If the sper has no activation signal for open, then close 
	xTpSperClose(IN:= NOT xSperOpen, PT:= timWaitOpen);  //Set.aSupSlot.timAct);
	
	//Output CLOSE
	xSperClose := xTpSperClose.Q;	
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK SperMX
	(*
	The Carrier sensor sees the carrier before it is stopped BY the sper. So the sper action has TO wait untill the carrier is at the 
	sper position. The time is calculated BY the TO travel distance.
	*)
	
	rMsFricDisc			:= (rSpdRpmFricDisc / 60.0) * (2.0 * PI * Cfg.rRadFricDisc);
	rPtMSFricDisc(Enable := TRUE, Gain := 1.0, In := rMsFricDisc, TimeConstant:= 0.25);

	//Check is trolley is jammed. 
	xTonTrolJammed.IN := NOT(xCarDet OR xCarDetNul) OR (NOT(xCarDet) AND xCarDetNul);
	//xTonTrolJammed.PT := UINT_TO_TIME(calcDisIntoTime(rMsFricDisc, Cfg.rRadFricDisc, (Set.aDisSlot.rDisBeforeOpen + Set.aDisSlot.rDisOpen)));
	xTonTrolJammed.PT := REAL_TO_TIME((Set.aDisSlot.rDisBeforeOpen + Set.aDisSlot.rDisOpen) / (rMsFricDisc / 1000.0));
	xTonTrolJammed();
	
	xTrolJamReport := xTonTrolJammed.Q;
	
	//if Carrier is detected, but not yet stopped by the sper.
	timWaitBeforeOpen	:= (REAL_TO_TIME(((Set.aDisSlot.rDisBeforeOpen / rPtMSFricDisc.Out)* Cfg.SlipCom)* 1000.0)) - (Set.aDisSlot.timCylResponseOpen + Set.aDisSlot.timValveResponseOpen);
	xTonBeforeWaitPos(IN := xCarDet , PT := timWaitBeforeOpen );
	
	
	//if jammed => retry to resolve the fault.
	xReTrolJammed(CLK := xTonTrolJammed.Q);
	ctdRetryTrols(CD := xSperOpen, LOAD := xReTrolJammed.Q, PV := Set.aDisSlot.usiJammedRetries);
	
	
	xReleaseSper	:= 	(xRelTrol AND xTonTrolJammed.Q ) OR
						(xInitRound AND xDetHook) OR
						(xDetHook AND
							NOT(xInitRound OR xAfterSlotFull OR xDiAfterSlot) AND 
							((xCarDet AND xTonBeforeWaitPos.Q AND NOT(xTonTrolJammed.Q)) OR (xTonTrolJammed.Q AND ctdRetryTrols.CV > 0)) AND
							NOT (xSperClose OR xTpSperClose.Q)
						);						
	
	timWaitOpen			:= REAL_TO_TIME(((Set.aDisSlot.rDisOpen / rPtMSFricDisc.Out) * Cfg.SlipCom)* 1000.0);
	timWaitBeforeClose	:= REAL_TO_TIME(((Set.aDisSlot.rDisWaitAfterClosed / rPtMSFricDisc.Out) *  Cfg.SlipCom)* 1000.0);
	
	xTpSperOpen(IN := xReleaseSper,	PT := timWaitOpen);
	xSperOpen 	:= xTpSperOpen.Q;
	
	xFeCloseAction(CLK := xTpSperOpen.Q);
	
	xtpWaitBeforeClose(IN:= xFeCloseAction.Q, PT:=timWaitBeforeClose );
	
	xTpSperClose(IN := xtpWaitBeforeClose.Q , PT := timWaitOpen);
	
	
	xSperClose	:= xTpSperClose.Q;

	
		
END_FUNCTION_BLOCK







FUNCTION_BLOCK TrolleyFrictionDetection
	
	RTInfo_0(enable :=	TRUE);			//Task cycle time
	
	xReRelTrol(CLK	:=	xRelTrolley);		//Rising edge release trolley
	xFeDetTrol(CLK	:=	xDetTrolley);		//Falling edge detect trolley
	
	uiUpperlimit	:=	REAL_TO_UINT(UINT_TO_REAL(uiTrolley[2, usiTrolNr]) * (1 + (UINT_TO_REAL(uiBoundaryPerc)/100)));
	uiLowerLimit	:=	REAL_TO_UINT(UINT_TO_REAL(uiTrolley[2, usiTrolNr]) * (1 - (UINT_TO_REAL(uiBoundaryPerc)/100)));
	
	//Start timemeasurement on release trolley signal
	IF xReRelTrol.Q THEN
		xActivate	:=	TRUE;
		rActTime 	:=	0;
	END_IF
	
	xTrigDrying := FALSE;
	
	IF xEnable THEN
	
		//count time (based on cylce time)
		IF xActivate THEN
			rActTime := rActTime + (UDINT_TO_REAL(RTInfo_0.cycle_time)/1000);
		END_IF
	
		//when trolley is not detected anymore stop measurement
		IF xFeDetTrol.Q THEN
			xActivate 	:= FALSE;
			rTime 		:= rActTime;
		
			(*
			Definition TrolleyIndex 
			0e col = index place 
			1e col = sample amount  
			2e col = average time 
			3e col = actual time
			4e col = out OF boundary
			*)
			//put measured time in the Average array based on trolleyNumber
			TrolleyAvg[usiTrolNr, uiTrolley[0,usiTrolNr]]	:= REAL_TO_UINT(rTime);
			//put measured time time in array (actual time)
			uiTrolley[3, usiTrolNr]	:= REAL_TO_UINT(rTime);
		
			//calculate average Time
			uiSumAvg := 0;
			FOR usiIndex := 0 TO uiTrolley[1,usiTrolNr] DO
				uiSumAvg := uiSumAvg + TrolleyAvg[usiTrolNr,usiIndex];
			END_FOR;
		
			//increase sampleIndex
			uiTrolley[0,usiTrolNr]	:= uiTrolley[0,usiTrolNr] + 1;
			//limit samples index
			uiTrolley[0,usiTrolNr]	:= uiTrolley[0,usiTrolNr] MOD usiTrolSize;
		
			//count samples (for calculating the avg, also when the array is not completely filled)
			uiTrolley[1,usiTrolNr]	:= MAX(uiTrolley[0,usiTrolNr], uiTrolley[1,usiTrolNr]);

			//Calculate Average values
			IF uiSumAvg > 0 THEN
				uiTrolley[2, usiTrolNr] := uiSumAvg / uiTrolley[1, usiTrolNr];
			END_IF
			
			//Calculate if measured time is out of boundary
			IF (REAL_TO_UINT(rTime) > uiUpperlimit OR REAL_TO_UINT(rTime) < uiLowerLimit) AND uiTrolley[1,usiTrolNr] > 1 THEN
				uiTrolley[4,usiTrolNr]	:=	uiTrolley[4,usiTrolNr] + 1;
				xTrigDrying	:= TRUE;
			END_IF
			
		END_IF
	ELSE
		xActivate 	:= FALSE;
		xTrigDrying	:= FALSE;
		rActTime 	:= 0;
		rTime 		:= 0;
	END_IF
	
	
	(* Trigger Drying function *)
	xTofDryingOn(IN := xTrigDrying, PT := T#2m);
	xTrigDrying	:= FALSE;
	xEnaDrying 	:= xEnable AND xTofDryingOn.Q;
	
	//RESET
	IF xReset THEN
		xActivate	:= FALSE;
		rActTime 	:= 0;
		rTime		:= 0;
		
		FOR usiIndex := 0 TO (usiTrolSize-1) DO
			FOR usiIndexAvg := 0 TO (usiTrolIndexSize-1) DO
				TrolleyAvg[usiIndexAvg ,usiIndex] := 0;
			END_FOR		
		END_FOR
		
		FOR usiIndex2 := 0 TO (usiTrolIndexSize-1) DO
			FOR usiIndex1 := 0 TO (uiTrolColums-1) DO
				uiTrolley[usiIndex1,usiIndex2] := 0;
			END_FOR
		END_FOR
		xReset	:= FALSE;
	END_IF;
		
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK ControlCPX_FB40
	
	usiValve_1to4.0	:= aValve[0].A;
	usiValve_1to4.1	:= aValve[0].B;
	usiValve_1to4.2	:= aValve[1].A;
	usiValve_1to4.3	:= aValve[1].B;
	usiValve_1to4.4	:= aValve[2].A;
	usiValve_1to4.5	:= aValve[2].B;
	usiValve_1to4.6	:= aValve[3].A;
	usiValve_1to4.7	:= aValve[3].B;
	
	usiValve_5to8.0	:= aValve[4].A;
	usiValve_5to8.1	:= aValve[4].B;
	usiValve_5to8.2	:= aValve[5].A;
	usiValve_5to8.3	:= aValve[5].B;
	usiValve_5to8.4	:= aValve[6].A;
	usiValve_5to8.5	:= aValve[6].B;
	usiValve_5to8.6	:= aValve[7].A;
	usiValve_5to8.7	:= aValve[7].B;
	
	rPresOut[0]	:= UINT_TO_REAL(uiPresIn[0]) / 1000;
	rPresOut[1]	:= UINT_TO_REAL(uiPresIn[1]) / 1000;
	rPresOut[2]	:= UINT_TO_REAL(uiPresIn[2]) / 1000;
	rPresOut[3]	:= UINT_TO_REAL(uiPresIn[3]) / 1000;
		
	MTDataStatistics_0.Enable 				:= TRUE;
	MTDataStatistics_0.MovingWindowLength 	:= 2080;
	//MTDataStatistics_0.Update 				:= ;
	MTDataStatistics_0.In 					:= rPresOut[0];
	//MTDataStatistics_0.Reset 				:= ;
	MTDataStatistics_0();
	
	
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncToPulsGeneration

	iEncRes			:= UINT_TO_INT(Cfg.uiResolution);
	iPulsPerHook 	:= REAL_TO_INT(UINT_TO_REAL(Cfg.uiResolution) / Cfg.rShakleAmount);
	iNrOfHooks 		:= REAL_TO_INT(floor(Cfg.rShakleAmount));
	iPulsWidth		:= iPulsPerHook / 5;
		
	
	
	//Determine PulsPositions
	IF xCalibrate THEN
		iEncStartPos := iEncPos;
		FOR i:=0 TO iNrOfHooks-1 DO
			iPulsPos := iEncStartPos + (i * iPulsPerHook);
			aiPulsLoc[i] := SEL(iPulsPos > iEncRes, iPulsPos, iPulsPos - iEncRes);
		END_FOR;
		xCalibrate := FALSE;
	END_IF
	
	
	//Generate Output puls
	//Take overflow into account
	FOR i:=0 TO iNrOfHooks-1 DO		
		IF (aiPulsLoc[i] + iPulsWidth) >= iEncRes THEN
			iMod := (aiPulsLoc[i] + iPulsWidth) MOD iEncRes;
			OUT := iEncPos >= aiPulsLoc[i] OR iEncPos <= iMod;
		ELSE
			OUT := iEncPos >= aiPulsLoc[i] AND iEncPos <= (aiPulsLoc[i] + iPulsWidth);
		END_IF	
			
		IF OUT THEN
			EXIT;
		END_IF
		
	END_FOR;
	
	
	
	
	
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK PdsCheckBufSig

	xReTrigger(CLK:= xTrigger AND (usiPdsNr > 0));
	xRePdtTrigger(CLK:= xPdt);
	
	//check PhotoCelDetector
	//for each PDS trigger must be minimal 1 PDS signal. (1 leg)
	IF xRePdtTrigger.Q THEN
		usiPdt		:= usiPdt + 1;
	END_IF
	
	IF xReTrigger.Q THEN
		usiPdsTrig := usiPdsTrig + 1;
	END_IF
	
	IF (usiPdt > usiLimit) OR (usiPdsTrig > usiLimit) THEN
		usiPdt		:= 0;
		usiPdsTrig	:= 0;
	END_IF
	
	
		
	//Check if both signals are detected for 5 cyles (1 - 2 - 3)
	IF xReTrigger.Q THEN		
		//Count number of recieved signals
		IF usiPdsNr = 1 THEN
			siBit0	:= siBit0 + 1;
		ELSIF usiPdsNr = 2 THEN
			siBit1	:= siBit1 + 1;	
		END_IF
		
		
		//if one of those reaches the limit, decrease all
		//Only check Bit 0 and Bit 1 => signal 1 and 2. Because 3 uses also bit 1
		IF siBit0 > usiLimit OR siBit1 > usiLimit THEN
			siBit0 	:= 0;
			siBit1	:= 0;
		END_IF
	END_IF
	
	
	
	xPdsBit0_Fault	:= (siBit1 - siBit0) > (siAllowMis+1);
	xPdsBit1_Fault	:= (siBit0 - siBit1) > (siAllowMis+1);
	
	
	xPDT_Fault		:= usiPdt <= (usiPdsTrig - SINT_TO_USINT(siAllowMis));
END_FUNCTION_BLOCK







