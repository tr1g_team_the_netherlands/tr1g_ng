
TYPE
	Performance_ShakleTab_typ : 	STRUCT 
		udiDetShackles : UDINT;
		udiDetProd : UDINT;
		udiDetProd1Leg : UDINT;
		udiDetProd2Leg : UDINT;
		udiDetProdMoreLeg : UDINT;
		udiDetPdsSig : UDINT;
		udiReleaseTrolleys : UDINT;
		udiRejectionsOut : UDINT;
		udiRejectionsIn : UDINT;
		xReset : BOOL;
	END_STRUCT;
	ProdDet_typ : 
		(
		noProduct := 0,
		oneLegDetected := 1,
		twoLegDetected := 2,
		moreLegDetected := 3
		);
	FestoValve : 	STRUCT 
		A : BOOL;
		B : BOOL;
	END_STRUCT;
	KickerStates_typ : 	STRUCT 
		xSetK1Out : BOOL;
		xSetK2Out : BOOL;
		xSetK1In : BOOL;
		xSetK2In : BOOL;
		K1IsIn : BOOL;
		K1IsOut : BOOL;
		K2IsIn : BOOL;
		K2IsOut : BOOL;
		xTpK1Out : TP;
		xTpK2Out : TP;
		xTpK1In : TP;
		xTpK2In : TP;
	END_STRUCT;
END_TYPE

(*Insert your comment here.*)

TYPE
	ShackleTableFeedback_typ : 	STRUCT 
		xPdsWindow : BOOL;
		xCarLeft : BOOL;
		xReject : BOOL;
		xCarGo : BOOL;
		xPdsTrigger : BOOL;
		usiProdType : USINT;
	END_STRUCT;
	RehangModes_typ : 	STRUCT 
		xAuto : BOOL := TRUE;
		xPhotoCell : BOOL;
		xPdsPuls : BOOL;
		xTest : BOOL;
		xAlternating : BOOL;
		usiShift : USINT;
		xOddEven : BOOL;
	END_STRUCT;
	ShakleRow_typ : 	STRUCT 
		udiShkNr : UDINT;
		udiDetShk : UDINT;
		udiDetProdMin : UDINT;
		udiDetProd : UDINT;
		udiDetProdMax : UDINT;
		ProdDetType : ProdDet_typ;
		udiDetPdsMin : UDINT;
		udiDetPds : UDINT;
		udiDetPdsMax : UDINT;
		usiPdsValue : USINT;
		udiCarGo : UDINT;
		xCarLeft : BOOL;
		xReject : BOOL;
		udiK1Out : UDINT;
		udiK2Out : UDINT;
		udiK1In : UDINT;
		udiK2In : UDINT;
	END_STRUCT;
END_TYPE
