
FUNCTION_BLOCK CylActTime_Dis
	//linked to the speed the open time of the cil with respect to the traveld distance.
	//check for minimum time. If 0ms the make 1
	RPM_TO_TimeDistance_0(rRPM:= rRPM, rRadius:= rRadius, rDistance:= rDistance);
	
	timWaitTime:= RPM_TO_TimeDistance_0.timRPM_TO_TimeDistance;
	
	IF timWaitTime < timMinTime THEN
		timWaitTime := timMinTime; //minimum Time
	END_IF;
	
	timCylActTime_Dis := timWaitTime;

END_FUNCTION_BLOCK








