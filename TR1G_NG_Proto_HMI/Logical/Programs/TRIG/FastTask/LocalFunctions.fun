
FUNCTION_BLOCK PdsCheckBufSig
	VAR_INPUT
		usiPdsNr : USINT;
		xTrigger : BOOL;
		xPdt : BOOL;
	END_VAR
	VAR_OUTPUT
		xPdsBit0_Fault : BOOL;
		xPdsBit1_Fault : BOOL;
		xPDT_Fault : BOOL;
	END_VAR
	VAR
		xReTrigger : R_TRIG;
		siBit0 : SINT;
		siBit1 : SINT;
	END_VAR
	VAR CONSTANT
		usiLimit : USINT := 20;
		siAllowMis : SINT := 4;
	END_VAR
	VAR
		xRePdtTrigger : R_TRIG;
		usiPdt : USINT;
		usiPdsTrig : USINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncToPulsGeneration
	VAR_INPUT
		iEncPos : INT;
		Cfg : EncSet_typ;
		aiPulsLoc : REFERENCE TO ARRAY[0..9] OF INT;
		xCalibrate : BOOL;
	END_VAR
	VAR_OUTPUT
		OUT : BOOL;
	END_VAR
	VAR
		iPulsWidth : INT;
		iPulsPerHook : INT;
		i : INT;
		iEncStartPos : INT;
		iPulsPos : INT;
		iMod : INT;
		iNrOfHooks : INT;
		iEncRes : INT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK ControlCPX_FB40
	VAR_INPUT
		aValve : ARRAY[0..7] OF FestoValve;
		uiPresIn : ARRAY[0..3] OF UINT;
	END_VAR
	VAR_OUTPUT
		usiValve_1to4 : USINT;
		usiValve_5to8 : USINT;
		rPresOut : ARRAY[0..3] OF REAL;
	END_VAR
	VAR
		MTDataStatistics_0 : MTDataStatistics;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK SperPDS
	VAR_INPUT
		Cfg : SetCarDiscConfig_typ;
		Set : SetCarDisc_typ;
		xDetHookInit : BOOL;
		xDetHook : BOOL;
		Enc : Encoder_typ;
		xInitRound : BOOL;
		rSpdRpmFricDisc : REAL;
		xAfterSlotFull : BOOL;
		xReqRelCar : BOOL;
		xInitSper : BOOL;
	END_VAR
	VAR_OUTPUT
		xSperOpen : BOOL;
		xSperClose : BOOL;
	END_VAR
	VAR
		xTpWaitTime : TP;
		timWaitOpen : TIME;
		xReleasesper : BOOL;
		xTpSperOpen : TP;
		xTpSperClose : TP;
		xRSReleaseDis : RS;
		xTpShakleDetect : TP;
		rRpsPickup : REAL;
		rRpsSynchro : REAL;
		rMsSynchro : REAL;
		timWaitBeforeOpen : TIME;
		xFeWait : F_TRIG;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK SperMX
	VAR_INPUT
		Set : SetCarDisc_typ;
		Cfg : SetCarDiscConfig_typ;
		xDetHook : BOOL;
		xCarDet : BOOL;
		xInitRound : BOOL;
		rSpdRpmFricDisc : REAL;
		xDiAfterSlot : BOOL;
		xAfterSlotFull : BOOL;
		xRelTrol : BOOL;
		xCarDetNul : BOOL;
	END_VAR
	VAR_OUTPUT
		xSperOpen : BOOL;
		xSperClose : BOOL;
		xTrolJamReport : BOOL;
	END_VAR
	VAR
		timWaitBeforeOpen : TIME;
		timWaitOpen : TIME;
		xReleaseSper : BOOL;
		xTpSperOpen : TP;
		xTpSperClose : TP;
		rMsFricDisc : REAL;
		xFeCloseAction : F_TRIG;
		xTonBeforeWaitPos : TON;
		rPtMSFricDisc : MTBasicsPT1;
		xtpWaitBeforeClose : TP;
		timWaitBeforeClose : TIME;
		xTonTrolJammed : TON;
		xReTrolJammed : R_TRIG;
		ctdRetryTrols : CTD;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderCalculations
	VAR_INPUT
		uiRawEnc : UINT;
		EncSet : EncSet_typ;
	END_VAR
	VAR_OUTPUT
		Enc : Encoder_typ;
	END_VAR
	VAR
		RTInfo_0 : RTInfo;
		udiEncExt : EncoderExtender;
		udiEncPos : UDINT;
		rCycleTime : REAL;
		rEncMS : REAL;
		MTmovingAvg : MTFilterMovingAverage := (WindowLength:=400);
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderExtender
	VAR_INPUT
		diEncVal : DINT;
		diEncResolution : DINT;
	END_VAR
	VAR_OUTPUT
		udiEncoder : UDINT;
		diDiffEncVal : DINT;
	END_VAR
	VAR
		diCurrEncVal : DINT;
		diOldEncVal : DINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION encOverUnderFlow : DINT
	VAR_INPUT
		diDiffEncoder : DINT;
		diResolution : DINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK pdsDataInput
	VAR_INPUT
		usiPdsNrDataIn : USINT;
		usiMode : INT; (*mode 0 = Wires / Mode 1 = number*)
		xPdsWire1 : BOOL;
		xPdsWire2 : BOOL;
	END_VAR
	VAR_OUTPUT
		usiPdsNr : USINT;
		xTrigger : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK pdsDataOutput
	VAR_INPUT
		usiPdsValue : USINT;
		timPdsPulse : TIME := T#20ms;
	END_VAR
	VAR_OUTPUT
		xPdsSig1 : BOOL;
		xPdsSig2 : BOOL;
	END_VAR
	VAR
		usiPdsValue_Prev : USINT;
		xPdsPulse : TP;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK ShackleTable
	VAR_INPUT
		udiEncPos : UDINT;
		udiEncPosRes : UDINT;
		aPulsDis : DistanceRehangerConversion_type;
		xAddNewRow : BOOL;
		xProdDet : BOOL;
		xResetTable : BOOL;
		xCarLeft : BOOL;
		usiPdsValue : USINT;
		xResetReport : BOOL;
		usiShkBtwKick_S01_input : USINT;
		rPdsSigShift : REAL := 0.2;
		xPdsTrigger : BOOL;
	END_VAR
	VAR_OUTPUT
		audiKickerPos : KickersPositions;
		xDataLogTrigger : BOOL;
		xOverFlowTable : BOOL;
		xCarGo : BOOL;
		ShakleTab : ARRAY[0..99] OF ShakleRow_typ;
		FB : ShackleTableFeedback_typ;
	END_VAR
	VAR
		RehangMode : RehangModes_typ;
		xReAddNewRow : R_TRIG;
		exportShakleRow : ShakleRow_typ;
		siIndexTab : SINT;
		udiShkNr : UDINT;
		udiProdDet : UDINT;
		usiProdType : USINT;
		udiPdsDet : UDINT;
		usiPdsVal : USINT;
		usiArrSize : USINT;
		usiIndexProdDet : USINT;
		usiIndexSetRej : USINT;
		usiIndexKickers : USINT;
		xReProdDet : R_TRIG;
		xRePdsSig : R_TRIG;
		usiResetTabLoop : USINT;
		usiIndexCarGo : USINT;
		xFeCarLeft : F_TRIG;
		udiActWindow : UDINT;
		xFbPdsSigWindow : BOOL;
		Report : Performance_ShakleTab_typ;
		zzEdge00000 : BOOL;
		zzEdge00001 : BOOL;
		usiCnt : USINT;
		xReIndexSetRejChn : BOOL;
		usiIndexSetRej_Prev : USINT;
		xReIndexKickChn : BOOL;
		usiIndexKickers_Prev : USINT;
		zzEdge00002 : BOOL;
		zzEdge00003 : BOOL;
		zzEdge00004 : BOOL;
		zzEdge00005 : BOOL;
		zzEdge00006 : BOOL;
		zzEdge00007 : BOOL;
		zzEdge00008 : BOOL;
		zzEdge00009 : BOOL;
		zzEdge00010 : BOOL;
		zzEdge00011 : BOOL;
		zzEdge00012 : BOOL;
		xReCarLeft : R_TRIG;
		xReIndexCarGo : BOOL;
		usiIndexCarGo_Prev : USINT;
	END_VAR
	VAR CONSTANT
		rShkPerc : REAL := 0.5;
	END_VAR
	VAR
		xReTriggerPds : R_TRIG;
		rMyShift : REAL := 0.08;
		udiStartPosShk : UDINT;
		xTpRefS01 : TP;
		usiIndexRefShk : USINT;
		myUdiPos : UDINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TrolleyFrictionDetection
	VAR_INPUT
		xRelTrolley : BOOL;
		xDetTrolley : BOOL;
		usiTrolNr : USINT;
		xReset : BOOL;
		uiBoundaryPerc : UINT;
		xEnable : BOOL;
	END_VAR
	VAR_OUTPUT
		xEnaDrying : BOOL;
	END_VAR
	VAR CONSTANT
		uiTrolColums : USINT := 5;
		usiTrolIndexSize : USINT := 15; (*Max number of Trolleys*)
		usiTrolSize : USINT := 50; (*Max sample amount*)
	END_VAR
	VAR
		xActivate : BOOL;
		RTInfo_0 : RTInfo;
		rActTime : REAL;
		rTime : REAL;
		TrolleyAvg : ARRAY[0..usiTrolIndexSize,0..usiTrolSize] OF UINT;
		uiTrolley : ARRAY[0..uiTrolColums,0..usiTrolIndexSize] OF UINT;
		usiIndex : INT;
		xReRelTrol : r_trig;
		xFeDetTrol : f_trig;
		uiSumAvg : UINT;
		uiUpperlimit : UINT;
		uiLowerLimit : UINT;
		usiIndexAvg : USINT;
		usiIndex1 : UINT;
		usiIndex2 : UINT;
		xTofDryingOn : TOF;
		xTrigDrying : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK CylActTime_Dis
	VAR_INPUT
		rDistance : {REDUND_UNREPLICABLE} REAL; (*distance in [m]*)
		rRPM : {REDUND_UNREPLICABLE} REAL;
		rRadius : {REDUND_UNREPLICABLE} REAL;
		timMinTime : {REDUND_UNREPLICABLE} TIME;
	END_VAR
	VAR_OUTPUT
		timCylActTime_Dis : TIME;
	END_VAR
	VAR
		timWaitTime : {REDUND_UNREPLICABLE} TIME;
		RPM_TO_TimeDistance_0 : RPM_TO_TimeDistance;
	END_VAR
END_FUNCTION_BLOCK
