
FUNCTION_BLOCK CylTimeMeasurement (* *) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		xActOut : BOOL;
		xFBout : BOOL;
	END_VAR
	VAR_OUTPUT
		xCylOut : CylCalc_type;
	END_VAR
	VAR CONSTANT
		usiArrPos : USINT := 100;
	END_VAR
	VAR
		RTInfo_0 : RTInfo;
		udiValOut : ARRAY[0..usiArrPos] OF UDINT;
		i : INT;
		xReAct : R_TRIG;
		xReFB : R_TRIG;
	END_VAR
	VAR_OUTPUT
		xTrigger : BOOL;
	END_VAR
END_FUNCTION_BLOCK
