
PROGRAM _CYCLIC
	(* Insert code here *)
	
	CylTimeMeasurement_0(xActOut := xKicker1OUT, 	xFBout := xFBKicker1OUT);
	CylTimeMeasurement_1(xActOut := xKicker2OUT, 	xFBout := xFBKicker2OUT);
	
	CylTimeMeasurement_2(xActOut := xKicker1IN, 	xFBout := xFBKicker1IN);
	CylTimeMeasurement_3(xActOut := xKicker2IN, 	xFBout := xFBKicker2IN);
	
	xTriggerKickers	:= CylTimeMeasurement_0.xTrigger OR CylTimeMeasurement_1.xTrigger OR CylTimeMeasurement_2.xTrigger OR CylTimeMeasurement_3.xTrigger;
	
	CylTimeMeasurement_4(xActOut := xSupOpen, 		xFBout := xFBxSupOpen);
	CylTimeMeasurement_5(xActOut := xSupClose,	 	xFBout := xFBxSupClose);
	
	CylTimeMeasurement_6(xActOut := xDisOpen,	 	xFBout := xFBxDisOpen);
	CylTimeMeasurement_7(xActOut := xDisClose, 		xFBout := xFBxDisClose);

	xTriggerSperren	:= CylTimeMeasurement_4.xTrigger OR CylTimeMeasurement_5.xTrigger OR CylTimeMeasurement_6.xTrigger OR CylTimeMeasurement_7.xTrigger;
	
	
	
	MpDataRecorder_1.MpLink 	:= ADR(gDataRecorderTimingKick);
	MpDataRecorder_1.DeviceName := ADR(sfileDevLoc);	
	MpDataRecorder_1.RecordMode := mpDATA_RECORD_MODE_TRIGGER;
	MpDataRecorder_1.Trigger 	:= xTriggerKickers;
	MpDataRecorder_1.Record 	:= MpDataRecorder_1.Active;
	MpDataRecorder_1.Enable 	:= TRUE; 
	
	IF MpDataRecorder_1.Error = ERR_OK THEN
		MpDataRegPar_1.MpLink 		:= ADR(gDataRecorderTimingKick);
		MpDataRegPar_1.PVName 		:= ADR('FB_Cyl:CylTimeMeasurement_0.xCylOut.rAvg');
		MpDataRegPar_1.Description	:= ADR('Kick1Out');
		MpDataRegPar_1.Enable 		:= TRUE;
		
		MpDataRegPar_2.MpLink 		:= ADR(gDataRecorderTimingKick);
		MpDataRegPar_2.PVName 		:= ADR('FB_Cyl:CylTimeMeasurement_1.xCylOut.rAvg');
		MpDataRegPar_2.Description	:= ADR('Kick2Out');
		MpDataRegPar_2.Enable 		:= TRUE;	
		
		MpDataRegPar_3.MpLink 		:= ADR(gDataRecorderTimingKick);
		MpDataRegPar_3.PVName 		:= ADR('FB_Cyl:CylTimeMeasurement_2.xCylOut.rAvg');
		MpDataRegPar_3.Description	:= ADR('Kick1In');
		MpDataRegPar_3.Enable 		:= TRUE;
		
		MpDataRegPar_4.MpLink 		:= ADR(gDataRecorderTimingKick);
		MpDataRegPar_4.PVName 		:= ADR('FB_Cyl:CylTimeMeasurement_3.xCylOut.rAvg');
		MpDataRegPar_4.Description	:= ADR('Kick2In');
		MpDataRegPar_4.Enable 		:= TRUE;
	ELSE
		MpDataRegPar_1.Enable 		:= FALSE;
		MpDataRegPar_2.Enable 		:= FALSE;
		MpDataRegPar_3.Enable 		:= FALSE;
		MpDataRegPar_4.Enable 		:= FALSE;
	END_IF
	
	MpDataRegPar_1();
	MpDataRegPar_2();
	MpDataRegPar_3();
	MpDataRegPar_4();
		
	MpDataRecorder_1();	
	
	
	MpDataRecorder_2.MpLink 	:= ADR(gDataRecorderTimingSper);
	MpDataRecorder_2.DeviceName := ADR(sfileDevLoc);	
	MpDataRecorder_2.RecordMode := mpDATA_RECORD_MODE_TRIGGER;
	MpDataRecorder_2.Trigger 	:= xTriggerSperren;
	MpDataRecorder_2.Record 	:= MpDataRecorder_2.Active;
	MpDataRecorder_2.Enable 	:= TRUE; 
	
	
	
	IF MpDataRecorder_2.Error = ERR_OK THEN
		MpDataRegPar_5.MpLink 		:= ADR(gDataRecorderTimingSper);
		MpDataRegPar_5.PVName 		:= ADR('FB_Cyl:CylTimeMeasurement_4.xCylOut.rAvg');
		MpDataRegPar_5.Description	:= ADR('SperSupOpen');
		MpDataRegPar_5.Enable 		:= TRUE;
		
		MpDataRegPar_6.MpLink 		:= ADR(gDataRecorderTimingSper);
		MpDataRegPar_6.PVName 		:= ADR('FB_Cyl:CylTimeMeasurement_5.xCylOut.rAvg');
		MpDataRegPar_6.Description	:= ADR('SperSupClose');
		MpDataRegPar_6.Enable 		:= TRUE;	
		
		MpDataRegPar_7.MpLink 		:= ADR(gDataRecorderTimingSper);
		MpDataRegPar_7.PVName 		:= ADR('FB_Cyl:CylTimeMeasurement_6.xCylOut.rAvg');
		MpDataRegPar_7.Description	:= ADR('SperDisOpen');
		MpDataRegPar_7.Enable 		:= TRUE;
		
		MpDataRegPar_8.MpLink 		:= ADR(gDataRecorderTimingSper);
		MpDataRegPar_8.PVName 		:= ADR('FB_Cyl:CylTimeMeasurement_7.xCylOut.rAvg');
		MpDataRegPar_8.Description	:= ADR('SperDisClose');
		MpDataRegPar_8.Enable 		:= TRUE;
	ELSE
		MpDataRegPar_5.Enable 		:= FALSE;
		MpDataRegPar_6.Enable 		:= FALSE;
		MpDataRegPar_7.Enable 		:= FALSE;
		MpDataRegPar_8.Enable 		:= FALSE;
	END_IF
	
	MpDataRegPar_5();
	MpDataRegPar_6();
	MpDataRegPar_7();
	MpDataRegPar_8();
	
	MpDataRecorder_2();	
	
	
END_PROGRAM
