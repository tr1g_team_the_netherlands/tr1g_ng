
(*  *)
FUNCTION_BLOCK CylTimeMeasurement
	RTInfo_0(enable := TRUE);
	xTrigger := FALSE;
	
	xReAct(CLK 	:= xActOut);
	xReFB(CLK 	:= xFBout);
	
	//start measurement
	IF xReAct.Q THEN 
		xCylOut.xBusy := TRUE;
		xCylOut.udTime:= 0;
	END_IF
	
	//increase time
	IF xCylOut.xBusy THEN
		xCylOut.udTime := xCylOut.udTime + (RTInfo_0.cycle_time / 1000);
	END_IF
	
	//end of signal
	IF xReFB.Q THEN
		//Ehd measurement
		xCylOut.xBusy 	:= FALSE;
		
		
		//save calc time
		udiValOut[xCylOut.usiIndex] := xCylOut.udTime;
		
		//increase and limit index
		xCylOut.usiIndex := xCylOut.usiIndex + 1;
		
		IF xCylOut.usiIndex >= usiArrPos THEN
			xCylOut.usiIndex 	:= 0;
			xTrigger			:= TRUE;
		END_IF
		
		//increase sample amount (only when array is not yet full)
		IF xCylOut.usiNrSamples < (usiArrPos-1) THEN
			xCylOut.usiNrSamples := xCylOut.usiNrSamples + 1;
		END_IF
	END_IF
	
	//calculate SUM and AVG
	xCylOut.diSum := 0;
	IF xCylOut.usiNrSamples > 0 THEN
		//SUM
		FOR i:=0 TO (xCylOut.usiNrSamples) DO
			xCylOut.diSum := xCylOut.diSum + udiValOut[i];
		END_FOR;
		
		//AVG
		xCylOut.rAvg := DINT_TO_REAL(xCylOut.diSum) / USINT_TO_REAL(xCylOut.usiNrSamples);
	END_IF
	
	
END_FUNCTION_BLOCK
