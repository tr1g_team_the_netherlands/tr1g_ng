
TYPE
	CylCalc_type : 	STRUCT 
		udTime : UDINT;
		xBusy : BOOL;
		rAvg : REAL;
		diSum : DINT;
		usiNrSamples : USINT;
		usiIndex : USINT;
	END_STRUCT;
END_TYPE
