
FUNCTION_BLOCK getTextFromId
	
	IF sTextID <> preTextID AND ArTextSysGetText_0.Busy = FALSE THEN
		ArTextSysGetSystemLanguage_0(Execute := TRUE );
		ArTextSysGetText_0.Execute 			:= TRUE; 		
	END_IF	
		
	IF ArTextSysGetText_0.Done THEN
		preTextID := sTextID;
		ArTextSysGetText_0.Execute 			:= FALSE; 
	END_IF;
			
	ArTextSysGetText_0.Namespace 		:= ADR('insideCodingNamespace'); 
	ArTextSysGetText_0.TextID 			:= ADR(sTextID); 
	ArTextSysGetText_0.LanguageCode 	:= ArTextSysGetSystemLanguage_0.LanguageCode; 
	ArTextSysGetText_0.TextBufferSize 	:= 255; 
	ArTextSysGetText_0.TextBuffer 		:= ADR(sResult);
	ArTextSysGetText_0();
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK getTextFromEnum
	sEnumUdint := '';
	sEnumUdint := UDINT_TO_STRING(udiInput);
	sEnumUdint := CONCAT(sTextID, sEnumUdint);
	
	
	IF (udiInput <> udiPreTextID AND GetText_0.Busy = FALSE) OR 
		(udiInput = udiPreTextID AND sResult = '' ) THEN
		ArTextSysGetSystemLanguage_0(Execute := TRUE );
		GetText_0.Execute 	:= TRUE; 		
	END_IF	
		
	IF GetText_0.Done THEN
		udiPreTextID := udiInput;
		GetText_0.Execute 			:= FALSE; 
	END_IF;
			
	GetText_0.Namespace 		:= ADR('insideCodingNamespace'); 
	GetText_0.TextID 			:= ADR(sEnumUdint); 
	GetText_0.LanguageCode 		:= ArTextSysGetSystemLanguage_0.LanguageCode; 
	GetText_0.TextBufferSize 	:= 255; 
	GetText_0.TextBuffer 		:= ADR(sResult);
	GetText_0();
	
END_FUNCTION_BLOCK