
PROGRAM _INIT

	gDataLogging.PVReg.tempPath				:= 'TR1G:Rehanger_0.ShakleTable.exportShakleRow.';
	gDataLogging.PVReg.description			:= 'udiShkNr';
	gDataLogging.PVReg.strVariableName		:= 'udiShkNr';
	gDataLogging.PVReg.strHookTabIdx		:= '0';	

END_PROGRAM

PROGRAM _CYCLIC
	
(*--------- USB ---------*)
	IF brdkUSBConnect_0.status = BRDK_USB_STATUS_FB_READY  THEN
		FOR i := 0 TO UINT_TO_UDINT(BRDK_USB_MAX_DEVICES) DO
			IF brdkUSBConnect_0.node[i].device <> '' THEN
				gUSB.deviceIdx 		:= UDINT_TO_USINT(i);
				gUSB.device 		:= brdkUSBConnect_0.node[i].device;
				gUSB.freeMemory 	:= brdkUSBConnect_0.node[i].freeMemory; 
				gUSB.totalMemory 	:= brdkUSBConnect_0.node[i].totalMemory;
				gUSB.USBMounted		:= TRUE;
			END_IF;
		END_FOR; 
	ELSE
		gUSB.deviceIdx 				:= 0;
		gUSB.device					:= '';
		gUSB.USBMounted				:= FALSE;
		xSelectUSB					:= FALSE;
	END_IF;
     
	brdkUSBConnect_0();
	gMmiCmd.Recipe.xUSBMounted 		:= gUSB.USBMounted;
	gMmiCmd.Recipe.sFbUsbDeviceName	:= gUSB.device;
	
	getTextFromEnum_4(sTextID :='FileIO.USB.ID.' , udiInput := brdkUSBConnect_0.status);
	gMmiCmd.Recipe.sFbUsbConnection		:= getTextFromEnum_4.sResult;
	
	
	//What fileDevice should be used!!!!
	IF gSimFilDevices THEN
		sfileDevLoc := 'Sim';
	ELSIF gUSB.USBMounted AND gMmiCmd.Recipe.xSelectUSB THEN
		sfileDevLoc := gUSB.device;
	ELSE
		sfileDevLoc	:= 'UserPart';
	END_IF
		
	
	
	
(*--------- Folderhandling ---------*)
	//When a file device changes, check folders again!
	CASE folderCheckState OF
		
		STATE_FOLDER_INIT:	
			folderCheckState		:= STATE_FOLDER_CHECK;
		
		STATE_FOLDER_IDLE:
			IF sOldFileDevLoc <> sfileDevLoc THEN
				folderCheckState 	:= STATE_FOLDER_CHECK;	
			END_IF;			
		
		STATE_FOLDER_CHECK:
			DirInfo_0.pDevice		:= ADR(sfileDevLoc);
			DirInfo_0.pPath			:= 0;
			DirInfo_0.enable		:= TRUE;
			DirInfo_0();
		
			IF DirInfo_0.status = ERR_OK THEN	
				folderCheckState:= STATE_CREATE_MAREL_FOLDER;
			ELSIF DirInfo_0.status = fiERR_DIR_NOT_EXIST THEN
				// No valid file device (perhaps simulation with no activation of USB sim)
			END_IF;
		
		STATE_CREATE_MAREL_FOLDER:
			DirCreate_0.pDevice		:= ADR(sfileDevLoc);
			DirCreate_0.pName		:= ADR('MachConfig');
			DirCreate_0.enable		:= TRUE;
			DirCreate_0();
			
			IF DirCreate_0.status = ERR_OK OR DirCreate_0.status = fiERR_DIR_ALREADY_EXIST THEN
				folderCheckState	:= STATE_CREATE_RECIPE_FOLDER;
			END_IF;
		
		STATE_CREATE_RECIPE_FOLDER:
			DirCreate_0.pDevice		:= ADR(sfileDevLoc);
			DirCreate_0.pName		:= ADR('MachSet');
			DirCreate_0.enable		:= TRUE;
			DirCreate_0();
			IF DirCreate_0.status = ERR_OK OR DirCreate_0.status = fiERR_DIR_ALREADY_EXIST THEN
				folderCheckState			:= STATE_FOLDER_IDLE;
			END_IF;		

	END_CASE;
	sOldFileDevLoc := sfileDevLoc;
	
	
	
(*---------- Save Recipe ---------*)
	Cfg;		//Call all settings before saving. Else an error will occure
	
	// LET OP: Global PV: <PV_name>		||	Local PV: <task_name>:<PV_name>
	MpRecipeRegPar_MachConfig(	MpLink := ADR(gRecipeXml_MachConfig), Enable := TRUE, PVName := ADR('Cfg.Config')	, Category := ADR('Config') );
	MpRecipeRegPar_MachSet_0(	MpLink := ADR(gRecipeXml_MachSet)	, Enable := TRUE, PVName := ADR('Cfg.Rejector')	, Category := ADR('Setting'));
	MpRecipeRegPar_MachSet_1(	MpLink := ADR(gRecipeXml_MachSet)	, Enable := TRUE, PVName := ADR('Cfg.CarDisc')  , Category := ADR('Setting'));
	MpRecipeRegPar_MachSet_2(	MpLink := ADR(gRecipeXml_MachSet)	, Enable := TRUE, PVName := ADR('Cfg.Rehanger')	, Category := ADR('Setting'));
	
	MpRecipeXml_MachConfig.Enable 		:= folderCheckState = STATE_FOLDER_IDLE;
	MpRecipeXml_MachConfig.MpLink 		:= ADR(gRecipeXml_MachConfig);
	MpRecipeXml_MachConfig.DeviceName 	:= ADR(sfileDevLoc);
	MpRecipeXml_MachConfig.FileName 	:= ADR('MachConfig\MachConfig.xml');
	MpRecipeXml_MachConfig.Category 	:= ADR('Config');
	MpRecipeXml_MachConfig.Save			:= gMmiCmd.Recipe.xSaveConfig;
	MpRecipeXml_MachConfig.Load			:= gMmiCmd.Recipe.xLoadConfig;
	MpRecipeXml_MachConfig();
	
	getTextFromEnum_0(sTextID :='FileIO.RecipeXML.ID.' , udiInput := MpRecipeXml_MachConfig.Info.Diag.StatusID.Code);
	gMmiCmd.Recipe.sFbConfigID		:= getTextFromEnum_0.sResult;
	
	getTextFromEnum_1(sTextID :='FileIO.RecipeXML.Servity.' , udiInput := MpRecipeXml_MachConfig.Info.Diag.StatusID.Severity);
	gMmiCmd.Recipe.sFbConfigServity	:= getTextFromEnum_1.sResult;
	

	MpRecipeXml_MachSet.Enable 			:= folderCheckState = STATE_FOLDER_IDLE;
	MpRecipeXml_MachSet.MpLink 			:= ADR(gRecipeXml_MachSet);
	MpRecipeXml_MachSet.DeviceName 		:= ADR(sfileDevLoc);
	MpRecipeXml_MachSet.FileName 		:= ADR('MachSet\MachSet.xml');
	MpRecipeXml_MachSet.Category 		:= ADR('Setting');
	MpRecipeXml_MachSet.Save			:= gMmiCmd.Recipe.xSaveMachSet;
	MpRecipeXml_MachSet.Load			:= gMmiCmd.Recipe.xLoadMachSet;
	MpRecipeXml_MachSet();
	
	getTextFromEnum_2(sTextID :='FileIO.RecipeXML.ID.' , udiInput := MpRecipeXml_MachSet.Info.Diag.StatusID.Code);
	gMmiCmd.Recipe.sFbMachSetID			:= getTextFromEnum_2.sResult;
	
	getTextFromEnum_3(sTextID :='FileIO.RecipeXML.Servity.' , udiInput := MpRecipeXml_MachSet.Info.Diag.StatusID.Severity);
	gMmiCmd.Recipe.sFbMachSetServity	:= getTextFromEnum_3.sResult;
	
		
	
	
	
(*---------- Save Table into CSV file ---------*)
	MpDataRecorder_0.MpLink := ADR(gDataRecorder);
	MpDataRecorder_0.DeviceName := ADR(sfileDevLoc);	
	MpDataRecorder_0.RecordMode := mpDATA_RECORD_MODE_TRIGGER;
	MpDataRecorder_0.Trigger := gDataLogging.xTrigger OR myTrig; 
	MpDataRecorder_0.Record := ((MpDataRecorder_0.Info.TotalSamples < gDataLogging.uiMaxSamp) AND gDataLogging.PVReg.allPVsRegistered) OR myRec;
	MpDataRecorder_0.Enable := folderCheckState = STATE_FOLDER_IDLE AND xEnaDataLogging;
	
		
	IF NOT gDataLogging.PVReg.allPVsRegistered THEN
		CASE gDataLogging.PVReg.state OF
			PV_REG_VARIABLE_INDEX_PATH:
				CASE gDataLogging.PVReg.pvIdx OF
					0:
						gDataLogging.PVReg.strVariableName 	:= 'udiShkNr';
						gDataLogging.PVReg.description		:= 'udiShkNr';
					1:
						gDataLogging.PVReg.strVariableName 	:= 'udiDetShk';
						gDataLogging.PVReg.description		:= 'udiDetShk';
					2:
						gDataLogging.PVReg.strVariableName 	:= 'udiDetProdMin';
						gDataLogging.PVReg.description		:= 'udiDetProdMin';
					3:
						gDataLogging.PVReg.strVariableName	:= 'udiDetProd';	
						gDataLogging.PVReg.description		:= 'udiDetProd';
					4:
						gDataLogging.PVReg.strVariableName	:= 'udiDetProdMax';
						gDataLogging.PVReg.description		:= 'udiDetProdMax';
					5:
						gDataLogging.PVReg.strVariableName	:= 'ProdDetType'; 
						gDataLogging.PVReg.description		:= 'ProdDetType';
					6:
						gDataLogging.PVReg.strVariableName	:= 'udiDetPdsMin';
						gDataLogging.PVReg.description		:= 'udiDetPdsMin';
					7:
						gDataLogging.PVReg.strVariableName 	:= 'udiDetPds';
						gDataLogging.PVReg.description 		:= 'udiDetPds';
					8:
						gDataLogging.PVReg.strVariableName	:= 'udiDetPdsMax';
						gDataLogging.PVReg.description 		:= 'udiDetPdsMax';
					9:
						gDataLogging.PVReg.strVariableName	:= 'usiPdsValue';
						gDataLogging.PVReg.description 		:= 'usiPdsValue';
					10:
						gDataLogging.PVReg.strVariableName 	:= 'udiCarGo';
						gDataLogging.PVReg.description 		:= 'udiCarGo';
					11:
						gDataLogging.PVReg.strVariableName 	:= 'xCarLeft';
						gDataLogging.PVReg.description 		:= 'xCarLeft';
					12:
						gDataLogging.PVReg.strVariableName	:= 'xReject';
						gDataLogging.PVReg.description 		:= 'xReject';
					13:
						gDataLogging.PVReg.strVariableName 	:= 'udiK1Out';
						gDataLogging.PVReg.description 		:= 'udiK1Out';
					14:
						gDataLogging.PVReg.strVariableName	:= 'udiK2Out';
						gDataLogging.PVReg.description 		:= 'udiK2Out';
					15:
						gDataLogging.PVReg.strVariableName 	:= 'udiK1In';
						gDataLogging.PVReg.description 		:= 'udiK1In';
					16:
						gDataLogging.PVReg.strVariableName 	:= 'udiK2In';
						gDataLogging.PVReg.description 		:= 'udiK2In';
					
					ELSE
					gDataLogging.PVReg.strVariableName 	:= 'udiShkNr';
					gDataLogging.PVReg.description		:= 'udiShkNr';
				 
				END_CASE;
			
				brdkStrCat(ADR(gDataLogging.PVReg.tempPath),ADR(gDataLogging.PVReg.strVariableName));	(* Add the variable to the table index path *)
		 
				gDataLogging.PVReg.state := PV_REG_BUSY;
			
			PV_REG_BUSY:
				IF MpDataRecorder_0.StatusID = ERR_OK THEN
					MpDataRegPar_0[gDataLogging.PVReg.pvIdx].MpLink := ADR(gDataRecorder);
					MpDataRegPar_0[gDataLogging.PVReg.pvIdx].PVName := ADR(gDataLogging.PVReg.tempPath);
					MpDataRegPar_0[gDataLogging.PVReg.pvIdx].Description := ADR(gDataLogging.PVReg.description); 
					MpDataRegPar_0[gDataLogging.PVReg.pvIdx].Enable := TRUE; 
				END_IF;
				
				
				IF MpDataRegPar_0[gDataLogging.PVReg.pvIdx].StatusID = ERR_OK AND MpDataRegPar_0[gDataLogging.PVReg.pvIdx].Active THEN
					IF gDataLogging.PVReg.pvIdx >= gDataLogging.PVReg.usiNrOfPvs -1 THEN	(* Good idea to check in MpDataRecorder if the number of registered PVs is correct also *)
						MpDataRecorder_0.Record := TRUE;
						gDataLogging.PVReg.state := PV_REG_DONE;	
					ELSE
						gDataLogging.PVReg.tempPath				:= 'TR1G:Rehanger_0.ShakleTable.exportShakleRow.';	(* Reset path *)
						gDataLogging.PVReg.pvIdx := gDataLogging.PVReg.pvIdx + 1; 
						gDataLogging.PVReg.state := PV_REG_VARIABLE_INDEX_PATH;	
					END_IF;
						
				END_IF;		

			PV_REG_DONE:
				gDataLogging.PVReg.allPVsRegistered 	:= TRUE;
				gDataLogging.PVReg.pvIdx				:= 0;
			
		END_CASE;
	
		FOR i:=0 TO (SIZEOF(MpDataRegPar_0) / SIZEOF(MpDataRegPar_0[0])) DO 
			MpDataRegPar_0[i]();	 
		END_FOR;
		
	END_IF;

	MpDataRecorder_0();
	
	

	
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

