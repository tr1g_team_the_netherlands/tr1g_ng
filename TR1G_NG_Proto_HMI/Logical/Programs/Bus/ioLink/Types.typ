
TYPE
	Cmd_typ : 	STRUCT 
		ReadDis : BOOL;
		ReadSup : BOOL;
		xBusy : BOOL;
	END_STRUCT;
	CaseHandlerIOLink_typ : 
		(
		IOLINK_CASE_IDLE,
		IOLINK_CASE_READ_INIT_ENC_SUP,
		IOLINK_CASE_READ_ENC_SUP,
		IOLINK_CASE_READ_INIT_ENC_DIS,
		IOLINK_CASE_READ_ENC_DIS,
		IOLINK_CASE_WRITE_ENC_SUP,
		IOLINK_CASE_WRITE_ENC_DIS
		);
	handler_typ : 
		(
		IOLINK_SENSOR_OFF := 0,
		IOLINK_IDLE := 1,
		IOLINK_INIT_SENSOR := 2,
		IOLINK_READ_SENSOR := 3,
		IOLINK_WRITE_SENSOR := 4
		);
	structIfmIoLink : 	STRUCT 
		strVendorName : STRING[80];
		strVendorText : STRING[80];
		strProductName : STRING[80];
		strProductId : STRING[80];
		strProductText : STRING[80];
		strHardwareVersion : STRING[80];
		strSerialNumber : STRING[80];
		strFirmwareVersion : STRING[80];
		strApplicationSpecificTag : STRING[80];
		uiDeviceStatus : UINT;
	END_STRUCT;
	structIoLinkReadData : 	STRUCT 
		pDeviceName : UDINT;
		uiIndex : UINT;
		usiSubIndex : USINT;
		usiDatatype : USINT;
		pData : UDINT;
		udiDataLenght : UDINT;
	END_STRUCT;
	RI360P0_QR24_typ : 
		(
		IOLINK_QR24_VendorName := 16,
		IOLINK_QR24_VendorText := 17,
		IOLINK_QR24_ProductName := 18,
		IOLINK_QR24_ProductID := 19,
		IOLINK_QR24_ProductText := 20,
		IOLINK_QR24_SerialNumber := 21,
		IOLINK_QR24_HardwareVersion := 22,
		IOLINK_QR24_FirmwareVersion := 23,
		IOLINK_QR24_AppSpecificTag := 24,
		IOLINK_QR24_Dir := 260
		);
	RV3500_RWoption_type : 
		(
		IOLINK_StandardCmd := 0,
		IOLINK_DevAccessLock := 1,
		IOLINK_VendorName := 2,
		IOLINK_VendorText := 3,
		IOLINK_ProductName := 4,
		IOLINK_ProductID := 5,
		IOLINK_ProductText := 6,
		IOLINK_SerialNumber := 7,
		IOLINK_HardwareVersion := 8,
		IOLINK_FirmwareVersion := 9,
		IOLINK_AppSpecificTag := 10,
		IOLINK_DeviceStatus := 11,
		IOLINK_DetDeviceStatus := 12,
		IOLINK_OperatingHours := 13,
		IOLINK_OutEnc := 14,
		IOLINK_EncResolution := 15,
		IOLINK_CountingDir := 16
		);
	EncoderRecieveData_typ : 	STRUCT 
		sVendorName : STRING[80];
		sVendorText : STRING[80];
		sProductName : STRING[80];
		sProductId : STRING[80];
		sProductText : STRING[80];
		sSerialNumber : STRING[80];
		sHardwareVersion : STRING[80];
		sFirmwareVersion : STRING[80];
		sApplicationSpecificTag : STRING[80];
		siReadItem : SINT := -1;
	END_STRUCT;
	IolinkResult_typ : 	STRUCT 
		strResult : STRING[80];
		uiResult : UINT;
		uiStatus : UINT;
		uiErrorCode : UINT;
		xBusy : BOOL;
		xRead : BOOL;
		xWrite : BOOL;
	END_STRUCT;
	OptionSet_RV3500_typ : 	STRUCT 
		Option : RV3500_RWoption_type;
		usiDataType : USINT;
		uiIndex : UINT;
		usiSubIndex : USINT;
	END_STRUCT;
	OptionSet_QR24_typ : 	STRUCT 
		Option : RI360P0_QR24_typ;
		usiDataType : USINT;
		uiIndex : UINT;
		usiSubIndex : USINT;
	END_STRUCT;
END_TYPE
