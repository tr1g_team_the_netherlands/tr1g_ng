FUNCTION_BLOCK IOlinkSensor

	strPDevName	:= ADR(pDeviceName);
	
	IF xRead AND handler = IOLINK_IDLE THEN
		handler 		:= IOLINK_READ_SENSOR;
		result.xRead	:= TRUE;
		result.xBusy 	:= TRUE;
	END_IF;
	
	IF xWrite AND handler = IOLINK_IDLE THEN
		handler 		:= IOLINK_WRITE_SENSOR;
		result.xWrite	:= TRUE;
		result.xBusy 	:= TRUE;
	END_IF;
	
	
	IF xEnable = FALSE THEN
		handler := IOLINK_SENSOR_OFF;
	END_IF
	IF EDGEPOS(xEnable) THEN
		handler := IOLINK_IDLE;
	END_IF 
	
	CASE handler OF
		IOLINK_SENSOR_OFF, IOLINK_IDLE:
//			ioLinkRead_0(enable := FALSE);
			result.xBusy 		:= FALSE;
			ioLinkRead_0.enable := FALSE;
		
		IOLINK_INIT_SENSOR:
		
		IOLINK_READ_SENSOR:
			
			result.uiResult 	:= 0;
			result.strResult	:= '';
			
			ioLinkRead_0.enable 		:= TRUE;
			ioLinkRead_0.pDeviceName	:= strPDevName;
			ioLinkRead_0.index			:= devSet_RV3500[usiOptionSelect].uiIndex;
			ioLinkRead_0.subIndex		:= devSet_RV3500[usiOptionSelect].usiSubIndex;
			ioLinkRead_0.datatype		:= devSet_RV3500[usiOptionSelect].usiDataType;
			IF devSet_RV3500[usiOptionSelect].usiDataType = ioLinkTYPE_UINT THEN
				ioLinkRead_0.pData			:= ADR(result.uiResult);
				ioLinkRead_0.dataLen		:= SIZEOF(result.uiResult);
			ELSIF devSet_RV3500[usiOptionSelect].usiDataType = ioLinkTYPE_STRING THEN
				ioLinkRead_0.pData			:= ADR(result.strResult);
				ioLinkRead_0.dataLen		:= SIZEOF(result.strResult);
			END_IF;
			ioLinkRead_0();
			
			result.uiErrorCode	:= ioLinkRead_0.errorCode;
			result.uiStatus		:= ioLinkRead_0.status;		
			
			
			IF ioLinkRead_0.status <> ERR_FUB_BUSY AND ioLinkRead_0.errorCode = ERR_OK THEN
				result.xRead	:= FALSE;
				handler 		:= IOLINK_IDLE;
			END_IF;
					
		
		IOLINK_WRITE_SENSOR:
		
			result.uiResult 	:= 0;
			result.strResult	:= '';
			
			ioLinkWrite_0.enable 		:= TRUE;
			ioLinkWrite_0.pDeviceName	:= strPDevName;
			ioLinkWrite_0.index			:= devSet_RV3500[usiOptionSelect].uiIndex;
			ioLinkWrite_0.subIndex		:= devSet_RV3500[usiOptionSelect].usiSubIndex;
			ioLinkWrite_0.datatype		:= devSet_RV3500[usiOptionSelect].usiDataType;
			IF devSet_RV3500[usiOptionSelect].usiDataType = ioLinkTYPE_UINT THEN
				ioLinkWrite_0.pData			:= ADR(uiWriteSet);
				ioLinkWrite_0.dataLen		:= SIZEOF(uiWriteSet);
			END_IF;
			ioLinkWrite_0();
			
			result.uiErrorCode	:= ioLinkWrite_0.errorCode;
			result.uiStatus		:= ioLinkWrite_0.status;		
			
			
			IF ioLinkWrite_0.status <> ERR_FUB_BUSY AND ioLinkWrite_0.errorCode = ERR_OK THEN
				result.xWrite	:= FALSE;
				handler 		:= IOLINK_IDLE;
			END_IF;

	END_CASE;
	
	
	
END_FUNCTION_BLOCK
