
PROGRAM _CYCLIC
	
	IOlinkSensor_0.pDeviceName 		:= 'IF6.ST7.IF3';
	//IOlinkSensor_0.usiOptionSelect 	:= IOLINK_SerialNumber;
	
	IOlinkSensor_1.pDeviceName 		:= 'IF6.ST7.IF4';
	//IOlinkSensor_1.usiOptionSelect 	:= IOLINK_SerialNumber;
	
	
	xReReadSup(CLK:= Cmd.ReadSup);
	xReReadDis(CLK:= Cmd.ReadDis);
	xFeReadSup(CLK:= IOlinkSensor_0.result.xRead);
	xFeReadDis(CLK:= IOlinkSensor_1.result.xRead);

	
	CASE handler OF
		IOLINK_CASE_IDLE:
		
			IF xReReadSup.Q THEN
				handler := IOLINK_CASE_READ_ENC_SUP;
				EncSupData.siReadItem := 1;
				xRead0	:= TRUE;
				//reset array
				EncSupData.sVendorName 				:= 'Unknown';
				EncSupData.sVendorText 				:= 'Unknown';
				EncSupData.sProductName 			:= 'Unknown';
				EncSupData.sProductId 				:= 'Unknown';
				EncSupData.sProductText 			:= 'Unknown';
				EncSupData.sSerialNumber 			:= 'Unknown';
				EncSupData.sHardwareVersion 		:= 'Unknown';
				EncSupData.sFirmwareVersion 		:= 'Unknown';
				EncSupData.sApplicationSpecificTag 	:= 'Unknown';
				
			ELSIF xReReadDis.Q THEN
				handler := IOLINK_CASE_READ_ENC_DIS;
				EncDisData.siReadItem := 1;
				xRead1	:= TRUE;
				//reset array
				EncDisData.sVendorName 				:= 'Unknown';
				EncDisData.sVendorText 				:= 'Unknown';
				EncDisData.sProductName 			:= 'Unknown';
				EncDisData.sProductId 				:= 'Unknown';
				EncDisData.sProductText 			:= 'Unknown';
				EncDisData.sSerialNumber 			:= 'Unknown';
				EncDisData.sHardwareVersion 		:= 'Unknown';
				EncDisData.sFirmwareVersion 		:= 'Unknown';
				EncDisData.sApplicationSpecificTag 	:= 'Unknown';
			ELSE
				xRead0 	:= FALSE;
				xRead1	:= FALSE;
				xEnable0:= FALSE;
				xEnable1:= FALSE;
			END_IF;
		
		IOLINK_CASE_READ_ENC_SUP:
			IF xFeReadSup.Q THEN
				EncSupData.siReadItem := EncSupData.siReadItem + 1;
				xRead0	:= TRUE;
			END_IF;

			xEnable0:= TRUE;
			IOlinkSensor_0.xEnable 			:= xEnable0;
			IOlinkSensor_0.xRead			:= xRead0;
			IOlinkSensor_0.xWrite			:= FALSE;
			IOlinkSensor_0.usiOptionSelect	:= EncSupData.siReadItem;
			IOlinkSensor_0();
			
			
			
			IF NOT IOlinkSensor_0.result.xRead AND (IOlinkSensor_0.result.uiResult <> 0 OR IOlinkSensor_0.result.strResult <> '' )THEN
				xRead0	:= FALSE;
				CASE EncSupData.siReadItem OF
					0: 
					1: 
					2: EncSupData.sVendorName 				:= IOlinkSensor_0.result.strResult;
					3: EncSupData.sVendorText 				:= IOlinkSensor_0.result.strResult;
					4: EncSupData.sProductName 				:= IOlinkSensor_0.result.strResult;
					5: EncSupData.sProductId 				:= IOlinkSensor_0.result.strResult;
					6: EncSupData.sProductText 				:= IOlinkSensor_0.result.strResult;
					7: EncSupData.sSerialNumber 			:= IOlinkSensor_0.result.strResult;
					8: EncSupData.sHardwareVersion 			:= IOlinkSensor_0.result.strResult;
					9: EncSupData.sFirmwareVersion 			:= IOlinkSensor_0.result.strResult;
					10:EncSupData.sApplicationSpecificTag 	:= IOlinkSensor_0.result.strResult;
					ELSE
				END_CASE;
			END_IF;
			
			// limit readactions
			IF EncSupData.siReadItem >= 11 THEN
				handler := IOLINK_CASE_IDLE;
			END_IF
			
			//Connect recieved Encoder Serialnumber to config settings
			IF Cfg.Config.EncSup.uiEncSerialNr <> EncSupData.sSerialNumber THEN
				Cfg.Config.EncSup.uiEncSerialNr := EncSupData.sSerialNumber;
			END_IF
							

		IOLINK_CASE_READ_ENC_DIS:
			
			IF xFeReadDis.Q THEN
				EncDisData.siReadItem := EncDisData.siReadItem + 1;
				xRead1	:= TRUE;
			END_IF;

			xEnable1:= TRUE;
			IOlinkSensor_1.xEnable 			:= xEnable1;
			IOlinkSensor_1.xRead			:= xRead1;
			IOlinkSensor_1.xWrite			:= FALSE;
			IOlinkSensor_1.usiOptionSelect	:= EncDisData.siReadItem;
			IOlinkSensor_1();
			
			
			IF NOT IOlinkSensor_1.result.xRead AND (IOlinkSensor_1.result.uiResult <> 0 OR IOlinkSensor_1.result.strResult <> '' )THEN
				xRead1	:= FALSE;
				CASE EncDisData.siReadItem OF
					0: 
					1: 
					2: EncDisData.sVendorName 				:= IOlinkSensor_1.result.strResult;
					3: EncDisData.sVendorText 				:= IOlinkSensor_1.result.strResult;
					4: EncDisData.sProductName 				:= IOlinkSensor_1.result.strResult;
					5: EncDisData.sProductId 				:= IOlinkSensor_1.result.strResult;
					6: EncDisData.sProductText 				:= IOlinkSensor_1.result.strResult;
					7: EncDisData.sSerialNumber 			:= IOlinkSensor_1.result.strResult;
					8: EncDisData.sHardwareVersion 			:= IOlinkSensor_1.result.strResult;
					9: EncDisData.sFirmwareVersion 			:= IOlinkSensor_1.result.strResult;
					10:EncDisData.sApplicationSpecificTag 	:= IOlinkSensor_1.result.strResult;
					ELSE
				END_CASE;
			END_IF;
		
			// limit readactions
			IF EncDisData.siReadItem >= 11 THEN
				handler := IOLINK_CASE_IDLE;
			END_IF
		
			//Connect recieved Encoder Serialnumber to config settings
			IF Cfg.Config.EncDis.uiEncSerialNr <> EncDisData.sSerialNumber THEN
				Cfg.Config.EncDis.uiEncSerialNr := EncDisData.sSerialNumber;
			END_IF
					
		IOLINK_CASE_WRITE_ENC_SUP:
		
		IOLINK_CASE_WRITE_ENC_DIS:

	END_CASE;
	
	Cmd.xBusy := NOT(handler <> IOLINK_CASE_IDLE);
	
	//check duration, and exit if duration is too long
	xTonUpdateDuration(IN := NOT(Cmd.xBusy) , PT:= T#10s);
	IF xTonUpdateDuration.Q THEN
		handler := IOLINK_CASE_IDLE;
	END_IF
	
	
	
	
	
	
END_PROGRAM


