
FUNCTION_BLOCK IOlinkSensor
	VAR_INPUT
		xEnable : BOOL;
		xRead : BOOL;
		xWrite : BOOL;
		pDeviceName : STRING[80];
		usiOptionSelect : RV3500_RWoption_type;
		uiWriteSet : UINT;
	END_VAR
	VAR_OUTPUT
		result : IolinkResult_typ;
	END_VAR
	VAR
		handler : handler_typ;
		ioLinkRead_0 : ioLinkRead;
		strPDevName : UDINT;
	END_VAR
	VAR CONSTANT
		devSet_QR24 : ARRAY[0..9] OF OptionSet_QR24_typ := [(Option:=IOLINK_QR24_VendorName,usiDataType:=9,uiIndex:=16,usiSubIndex:=0),(Option:=IOLINK_QR24_VendorText,usiDataType:=9,uiIndex:=17,usiSubIndex:=0),(Option:=IOLINK_QR24_ProductName,usiDataType:=9,uiIndex:=18,usiSubIndex:=0),(Option:=IOLINK_QR24_ProductID,usiDataType:=9,uiIndex:=19,usiSubIndex:=0),(Option:=IOLINK_QR24_ProductText,usiDataType:=9,uiIndex:=20,usiSubIndex:=0),(Option:=IOLINK_QR24_SerialNumber,usiDataType:=9,uiIndex:=21,usiSubIndex:=0),(Option:=IOLINK_QR24_HardwareVersion,usiDataType:=9,uiIndex:=22,usiSubIndex:=0),(Option:=IOLINK_QR24_FirmwareVersion,usiDataType:=9,uiIndex:=23,usiSubIndex:=0),(Option:=IOLINK_QR24_AppSpecificTag,usiDataType:=9,uiIndex:=24,usiSubIndex:=0),(Option:=IOLINK_QR24_Dir,usiDataType:=6,uiIndex:=260,usiSubIndex:=0)];
		devSet_RV3500 : ARRAY[0..16] OF OptionSet_RV3500_typ := [(Option:=0,usiDataType:=6,uiIndex:=2,usiSubIndex:=0),(Option:=1,usiDataType:=254,uiIndex:=12,usiSubIndex:=0),(Option:=2,usiDataType:=9,uiIndex:=16,usiSubIndex:=0),(Option:=3,usiDataType:=9,uiIndex:=17,usiSubIndex:=0),(Option:=4,usiDataType:=9,uiIndex:=18,usiSubIndex:=0),(Option:=5,usiDataType:=9,uiIndex:=19,usiSubIndex:=0),(Option:=6,usiDataType:=9,uiIndex:=20,usiSubIndex:=0),(Option:=7,usiDataType:=9,uiIndex:=21,usiSubIndex:=0),(Option:=8,usiDataType:=9,uiIndex:=22,usiSubIndex:=0),(Option:=9,usiDataType:=9,uiIndex:=23,usiSubIndex:=0),(Option:=10,usiDataType:=9,uiIndex:=24,usiSubIndex:=0),(Option:=11,usiDataType:=6,uiIndex:=36,usiSubIndex:=0),(Option:=12,usiDataType:=9,uiIndex:=37,usiSubIndex:=0),(Option:=13,usiDataType:=6,uiIndex:=542,usiSubIndex:=0),(Option:=14,usiDataType:=6,uiIndex:=4000,usiSubIndex:=0),(Option:=15,usiDataType:=6,uiIndex:=4004,usiSubIndex:=0),(Option:=16,usiDataType:=6,uiIndex:=4006,usiSubIndex:=0)];
	END_VAR
	VAR
		zzEdge00000 : BOOL;
		zzEdge00001 : BOOL;
		ioLinkWrite_0 : ioLinkWrite;
		zzEdge00002 : BOOL;
	END_VAR
END_FUNCTION_BLOCK
