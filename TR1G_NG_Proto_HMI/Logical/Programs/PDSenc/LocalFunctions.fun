
FUNCTION_BLOCK EncoderExtender
	VAR_INPUT
		diEncVal : DINT;
		diEncResolution : DINT;
	END_VAR
	VAR_OUTPUT
		diDiffEncVal : DINT;
	END_VAR
	VAR
		diCurrEncVal : DINT;
		diOldEncVal : DINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncToPdsPuls (* *) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		udiEncRes : UDINT;
		usiShackleAmount : USINT := 6;
		usiPulsAShackleAmount : USINT;
		rDutyCycle : REAL := 0.5;
		udiEncPos : UDINT;
	END_VAR
	VAR_OUTPUT
		xPuls : BOOL;
	END_VAR
	VAR
		udiPulsWith : UDINT;
		udiHalfPuls : UDINT;
		EncoderExtender_0 : EncoderExtender;
		udiPulsCnt : UDINT;
		uiPulsAmount : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION encOverUnderFlow : DINT
	VAR_INPUT
		diDiffEncoder : DINT;
		diResolution : DINT;
	END_VAR
END_FUNCTION
