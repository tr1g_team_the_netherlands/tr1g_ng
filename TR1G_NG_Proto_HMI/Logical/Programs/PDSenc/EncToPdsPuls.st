
(*  *)
FUNCTION_BLOCK EncToPdsPuls
	
	//Calculate EncoderPulses a PDS puls
	uiPulsAmount:= USINT_TO_UINT(usiPulsAShackleAmount * usiShackleAmount);
	udiPulsWith := udiEncRes / UINT_TO_UDINT(uiPulsAmount);
	udiHalfPuls := udiPulsWith / 2;
	
	EncoderExtender_0(diEncVal := udiEncPos , diEncResolution := udiEncRes);
	
	udiPulsCnt	:= udiPulsCnt + ABS(EncoderExtender_0.diDiffEncVal);
	udiPulsCnt	:= udiPulsCnt MOD udiPulsWith;
	
	xPuls		:= udiPulsCnt > 0 AND udiPulsCnt <= REAL_TO_UDINT(UDINT_TO_REAL(udiPulsWith) * rDutyCycle);
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderExtender
	diCurrEncVal 	:= diEncVal;
	
	IF diCurrEncVal <> diOldEncVal THEN
		diDiffEncVal	:= encOverUnderFlow(diCurrEncVal - diOldEncVal, diEncResolution);
		diOldEncVal		:= diCurrEncVal;
	ELSE
		diDiffEncVal 	:= 0;
	END_IF
	
	//udiEncoder := udiEncoder + diDiffEncVal;
		
END_FUNCTION_BLOCK

FUNCTION encOverUnderFlow
	IF 	diDiffEncoder >  diResolution / 3 THEN 
		diDiffEncoder := diDiffEncoder - diResolution; 
	END_IF
	IF 	diDiffEncoder < -diResolution / 3 THEN 
		diDiffEncoder := diDiffEncoder + diResolution; 
	END_IF
	
	encOverUnderFlow := diDiffEncoder;
END_FUNCTION 