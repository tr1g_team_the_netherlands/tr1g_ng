
PROGRAM _CYCLIC

//	udiSimCount 	:= udiSimCount + 5;
//	udiSimCount 	:= udiSimCount MOD 1000;

	
	EncToPdsPuls_0.udiEncRes 				:= 5000;
	EncToPdsPuls_0.usiShackleAmount			:= 6;
	EncToPdsPuls_0.usiPulsAShackleAmount	:= 12;
	EncToPdsPuls_0.rDutyCycle				:= 0.5;
	EncToPdsPuls_0.udiEncPos				:= UINT_TO_UDINT(hw.Com_.uiEncSup);
	EncToPdsPuls_0();
	
	EncToPdsPuls_1.udiEncRes 				:= 5000;
	EncToPdsPuls_1.usiShackleAmount			:= 5;
	EncToPdsPuls_1.usiPulsAShackleAmount	:= 28;
	EncToPdsPuls_1.rDutyCycle				:= 0.5;
	EncToPdsPuls_1.udiEncPos				:= UINT_TO_UDINT(hw.Com_.uiEncDis);
	EncToPdsPuls_1();
	
	
	xPulsPdsSup := EncToPdsPuls_0.xPuls;
	xPulsPdsDis	:= EncToPdsPuls_1.xPuls;

END_PROGRAM
