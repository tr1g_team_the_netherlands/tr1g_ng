
PROGRAM _INIT
	MpAlarmXCore_0.MpLink 		:= ADR(gAlarmXCore); 
	MpAlarmXCore_0.Enable 		:= TRUE;
	MpAlarmXCore_0.ErrorReset 	:= FALSE;
	MpAlarmXCore_0();
	 
	MpAlarmXHistory_0.MpLink 	:= ADR(gAlarmXHistory) ;
	MpAlarmXHistory_0.Enable 	:= TRUE;
	MpAlarmXHistory_0.ErrorReset := FALSE;
	MpAlarmXHistory_0.DeviceName := ADR('AlarmHistory');
	//MpAlarmXHistory_0.Language 	:= ; 
	//MpAlarmXHistory_0.Export 	:= ;
	MpAlarmXHistory_0();
	
	MpAlarmXListUI_0.MpLink 	:= ADR(gAlarmXCore) ; 
	MpAlarmXListUI_0.Enable 	:= TRUE; 
	//MpAlarmXListUI_0.ErrorReset := ; 
	//MpAlarmXListUI_0.UISetup 	:= ; 
	MpAlarmXListUI_0.UIConnect 	:= ADR(MpAlarmXListUIConnectType_0);
	MpAlarmXListUI_0();
	
	MpAlarmXHistoryUI_0.MpLink := ADR(gAlarmXHistory);
	MpAlarmXHistoryUI_0.Enable := TRUE;
	//MpAlarmXHistoryUI_0.ErrorReset := ; 
	//MpAlarmXHistoryUI_0.UISetup := ;
	MpAlarmXHistoryUI_0.UIConnect := ADR(MpAlarmXHistoryUIConnectType_0);
	MpAlarmXHistoryUI_0();
	
	
	

	
	
END_PROGRAM

PROGRAM _CYCLIC
	hw;
	
	
	//Get systemlanguage
	ArTextSysGetSystemLanguage_0();
	//MpAlarmXListUIConnectType_0.Language := ArTextSysGetSystemLanguage_0.LanguageCode;
	
	
	MpAlarmXCore_0();
	
	(* Get Alarm reactions *)
	gAlmReact.xAbortMach 	:= MpAlarmXCheckReaction(gAlarmXCore, 'AbortMach');
	gAlmReact.xStopMach 	:= MpAlarmXCheckReaction(gAlarmXCore, 'StopMach');
	
	IF MpAlarmXCore_0.Active THEN
		
		
		MpAlarmXHistory_0();
		MpAlarmXHistoryUI_0();
		
		MpAlarmXListUI_0();
		
		(* ALARM 0001 Fuse FrictionDisc *)
		//IF hw.Es_.xPowerFailure THEN	MpAlarmXSet(gAlarmXCore, 'A0001');
		//ELSIF  MpAlarmXCheckState(gAlarmXCore, 'A0001', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore, 'A0001'); END_IF
		
		(* ALARM 0002 NO AIRPRESSURE *)
		IF gFbAirPres.rAirPres1 < 0.5 THEN		MpAlarmXSet(gAlarmXCore, 'A0002');	
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0002', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore, 'A0002'); END_IF
					
		(* ALARM 0003 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0003');	END_IF		
				
		(* ALARM 0004 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0004');	END_IF
		
		(* ALARM 0005 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0005');	END_IF
		
		(* ALARM 0006 *)
		IF FALSE THEN 	MpAlarmXSet(gAlarmXCore, 'A0006');	END_IF
		
		(* ALARM 0007 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0007');	END_IF
		
		(* ALARM 0008 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0008');	END_IF
		
		(* ALARM 0009 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0009');	END_IF
		
		(* ALARM 0010 *)
		IF (NOT(hw.IO_.Mod1.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0010');	
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0010', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0010');	END_IF
		
		(* ALARM 0011 *)
		IF (NOT(hw.IO_.Mod2.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0011');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0011', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0011');	END_IF
		
		(* ALARM 0012 *)
		IF (NOT(hw.IO_.Mod3.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0012');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0012', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0012');	END_IF
		
		(* ALARM 0013 *)
		IF (NOT(hw.IO_.Mod4.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0013');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0013', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0013');	END_IF
		
		(* ALARM 0014 *)
		IF (NOT(hw.IO_.Mod5.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0014');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0014', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0014');	END_IF
		
		(* ALARM 0015 *)
		IF (NOT(hw.IO_.Mod6.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0015');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0015', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0015');	END_IF
		
		(* ALARM 0016 *)
		IF (NOT(hw.IO_.Mod7.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0016');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0016', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0016');	END_IF
		
		(* ALARM 0017 *)
		IF (NOT(hw.IO_.Mod8.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0017');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0017', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0017');	END_IF
		
		(* ALARM 0018 *)
		IF (NOT(hw.IO_.Mod9.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0018');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0018', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0018');	END_IF
		
		(* ALARM 0019 *)
		IF (NOT(hw.IO_.Mod10.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0019');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0019', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0019');	END_IF
		
		(* ALARM 0020 *)
		IF (NOT(hw.IO_.Mod11.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0020');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0020', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0020');	END_IF
		
		(* ALARM 0021 *)
//		IF (NOT(hw.IO_.Mod12.xOk)) THEN	MpAlarmXSet(gAlarmXCore, 'A0021');
//		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0021', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0021');	END_IF
		
		(* ALARM 0022 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0022');			
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0022', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0022');	END_IF
			
		(* ALARM 0023 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0023');
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0023', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0023');	END_IF
				
		(* ALARM 0024 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0024');	
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0024', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0024');	END_IF
				
		(* ALARM 0025 *)
		IF FALSE THEN	MpAlarmXSet(gAlarmXCore, 'A0025');	
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A0025', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A0025');	END_IF
		
		(* ALARM 1000 *)
		IF hw.Es_.xPowerFailure THEN	MpAlarmXSet(gAlarmXCore, 'A1000');	
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A1000', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A1000');	END_IF
		
		(* ALARM 1001 *)
		IF hw.Es_.xES THEN	MpAlarmXSet(gAlarmXCore, 'A1001');	
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A1001', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A1001');	END_IF
		
		(* ALARM 1002 *)
		IF hw.Es_.xESexternal THEN	MpAlarmXSet(gAlarmXCore, 'A1002');	
		ELSIF MpAlarmXCheckState(gAlarmXCore, 'A1002', mpALARMX_STATE_ACTIVE) THEN MpAlarmXReset(gAlarmXCore,  'A1002');	END_IF
		

		(* WARNING 0002 Operator release air button pressed *)
		IF gMmiCmd.xReleaseAir THEN	
			MpAlarmXSet(gAlarmXCore, 'W0002');		
		ELSIF gMmiCmd.Cmd.xClear AND MpAlarmXCheckState(gAlarmXCore, 'W0002', mpALARMX_STATE_ACTIVE) THEN 	
			MpAlarmXReset(gAlarmXCore,  'W0002');	
		END_IF
		
		(* WARNING 0006 Airpressure below Minimum *)
		IF (gFbAirPres.rAirPres2 < Cfg.Config.General.rMinAirPres) AND (gFbAirPres.rAirPres1 > Cfg.Config.General.rMinAirPres) THEN
			MpAlarmXSet(gAlarmXCore, 'W0006');
		ELSIF (gFbAirPres.rAirPres2 > Cfg.Config.General.rMinAirPres) AND MpAlarmXCheckState(gAlarmXCore, 'W0006', mpALARMX_STATE_ACTIVE) THEN 	
			MpAlarmXReset(gAlarmXCore,  'W0006');	
		END_IF
		
		
		
		
		FOR i:=0 TO (SIZEOF(Alm) / SIZEOF(Alm[0]))-1 DO		
			IF Alm[i].xTrigger OR Alm[i].xOn THEN	
				MpAlarmXSet(gAlarmXCore, Alm[i].AlarmCode); 
			END_IF	
			IF Alm[i].xOff THEN
				MpAlarmXReset(gAlarmXCore, Alm[i].AlarmCode); 
			END_IF
		END_FOR;
		
		
			
	ELSE
		MpAlarmXCore_0(Enable := TRUE, ErrorReset := TRUE );	
	END_IF;
	
	//FOR HMI TAB
	xAlarmActive := MpAlarmXCore_0.ActiveAlarms > 0 OR MpAlarmXCore_0.PendingAlarms > 0;					
	
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

