FUNCTION_BLOCK MMI_PilotLight
//VERSION HISTORY:
//	-----------------------------------------------------
//	Version:	Date:		Author:		Description:
//	V0.0		25-11-2016	X.Thissen	Initial Version	
//
	
	CASE PackMLMode OF
		Production:
		
		Manual:
			IF xManCmd AND currentState = mpPACKML_STATE_EXECUTE THEN
				uiCntMan := uiCntMan + 1;	
			ELSE
				uiCntMan := 0;
			END_IF;
			
			CASE uiCntMan OF
				0:
				1..10:		Color	:= Green;
				11..20:		Color	:= Red;
				21..30:		Color	:= Blue;
				31..40:		Color	:= White;
				41..50:		Color	:= Yellow;
				ELSE 
				uiCntMan := 1;
			END_CASE;
	END_CASE;
	
	
	
	(*RESET all wires to zero*)
	Wire.GreenWire := Wire.RedWire := Wire.YellowWire := Wire.BlueWire := Wire.WhiteWire := FALSE;
		
	CASE Color OF 
		OFF							:
		ON							:
		Test				 	 	: Wire.GreenWire 	:= Wire.RedWire	 	:= Wire.YellowWire 	:= Wire.BlueWire 	:= Wire.WhiteWire 	:= TRUE;
		Green				 	 	: Wire.GreenWire 	:= TRUE;		
		Green_Flash				 	: Wire.GreenWire	:= Wire.RedWire 	:= TRUE;
		Red				 	 	 	: Wire.RedWire 		:= TRUE;
		Red_Flash				 	: Wire.RedWire 		:= Wire.YellowWire 	:= TRUE;
		Blue				 	 	: Wire.BlueWire 	:= TRUE;
		Blue_Flash				 	: Wire.BlueWire 	:= Wire.WhiteWire 	:= TRUE;
		Yellow				 	 	: Wire.YellowWire 	:= TRUE;
		Yellow_Flash				: Wire.YellowWire 	:= Wire.BlueWire 	:= TRUE;
		White				 	 	: Wire.WhiteWire 	:= TRUE;
		White_Flash				 	: Wire.GreenWire 	:= Wire.WhiteWire 	:= TRUE;
		Green_Red_Flash				: Wire.GreenWire 	:= Wire.YellowWire 	:= TRUE;
		Green_Yellow_Flash			: Wire.GreenWire 	:= Wire.BlueWire 	:= TRUE;
		Green_Blue_Flash			: Wire.RedWire 		:= Wire.BlueWire 	:= TRUE;
		Green_White_Flash			: Wire.RedWire 		:= Wire.WhiteWire 	:= TRUE;
		Red_Yellow_Flash			: Wire.YellowWire 	:= Wire.WhiteWire 	:= TRUE;
		Red_Blue_Flash				: Wire.GreenWire 	:= Wire.RedWire 	:= Wire.WhiteWire 	:= TRUE;
		Red_White_Flash				: Wire.GreenWire 	:= Wire.RedWire 	:= Wire.BlueWire 	:= TRUE;
		Yellow_Blue_Flash			: Wire.GreenWire 	:= Wire.RedWire 	:= Wire.YellowWire 	:= TRUE;
		Yellow_White_Flash			: Wire.RedWire 		:= Wire.YellowWire 	:= Wire.WhiteWire 	:= TRUE;
		Blue_White_Flash			: Wire.GreenWire 	:= Wire.RedWire 	:= Wire.BlueWire 	:=  TRUE;
		Green_Red_Yellow_Flash		: Wire.GreenWire 	:= Wire.YellowWire 	:= Wire.WhiteWire 	:= TRUE;
		Green_Red_Blue_Flash		: Wire.RedWire 		:= Wire.BlueWire 	:= Wire.WhiteWire 	:= TRUE;
		Green_Red_White_Flash		: Wire.GreenWire 	:= Wire.BlueWire 	:= Wire.WhiteWire 	:= TRUE;
		Green_Yellow_Blue_Flash		: Wire.YellowWire 	:= Wire.BlueWire 	:= Wire.WhiteWire 	:= TRUE;
		Green_Yellow_White_Flash	: Wire.GreenWire 	:= Wire.YellowWire 	:= Wire.BlueWire 	:= TRUE;
		Green_Blue_White_Flash		: Wire.GreenWire 	:= Wire.YellowWire 	:= Wire.BlueWire 	:= Wire.WhiteWire	:= TRUE;
		Red_Yellow_Blue_Flash		: Wire.GreenWire 	:= Wire.RedWire 	:= Wire.YellowWire 	:= Wire.BlueWire 	:=  TRUE;
		Red_Yellow_White_Flash		: Wire.GreenWire 	:= Wire.RedWire 	:= Wire.YellowWire 	:= Wire.WhiteWire 	:= TRUE;
		Red_Blue_White_Flash		: Wire.GreenWire 	:= Wire.RedWire 	:=  Wire.BlueWire 	:= Wire.WhiteWire 	:= TRUE;
		Yellow_Blue_White_Flash		: Wire.RedWire 		:= Wire.YellowWire 	:= Wire.BlueWire 	:= Wire.WhiteWire 	:= TRUE;
		GRYBW_Flash					: Wire.GreenWire 	:= Wire.RedWire 	:= Wire.YellowWire 	:= Wire.BlueWire 	:= Wire.WhiteWire 	:= TRUE;
	END_CASE; 
	
END_FUNCTION_BLOCK


FUNCTION_BLOCK MMI_PackML_StateDisplay

	//enable & disable Packmlstates display
	CASE PackMLMode OF
		Production:
			PackMLStates.xAborted.xEnabled		:= TRUE;
			PackMLStates.xAborting.xEnabled 	:= TRUE;
			PackMLStates.xClearing.xEnabled 	:= TRUE;
			PackMLStates.xStopping.xEnabled 	:= TRUE;
			PackMLStates.xStopped.xEnabled 		:= TRUE;
			PackMLStates.xResetting.xEnabled 	:= TRUE;
			PackMLStates.xIdle.xEnabled 		:= TRUE;
			PackMLStates.xStarting.xEnabled 	:= TRUE;
			PackMLStates.xExecute.xEnabled 		:= TRUE;
			PackMLStates.xCompleting.xEnabled 	:= TRUE;
			PackMLStates.xComplete.xEnabled 	:= TRUE;
			PackMLStates.xSuspending.xEnabled 	:= TRUE;
			PackMLStates.xSuspended.xEnabled 	:= TRUE;
			PackMLStates.xUnSuspending.xEnabled := TRUE;
			PackMLStates.xHolding.xEnabled 		:= FALSE;
			PackMLStates.xHeld.xEnabled 		:= FALSE;
			PackMLStates.xUnHolding.xEnabled 	:= FALSE;
		
		Manual:
			PackMLStates.xAborted.xEnabled		:= TRUE;
			PackMLStates.xAborting.xEnabled		:= TRUE;
			PackMLStates.xClearing.xEnabled 	:= TRUE;
			PackMLStates.xStopping.xEnabled 	:= TRUE;
			PackMLStates.xStopped.xEnabled 		:= TRUE;
			PackMLStates.xResetting.xEnabled 	:= TRUE;
			PackMLStates.xIdle.xEnabled 		:= TRUE;
			PackMLStates.xStarting.xEnabled 	:= TRUE;
			PackMLStates.xExecute.xEnabled 		:= TRUE;
			PackMLStates.xCompleting.xEnabled 	:= FALSE;
			PackMLStates.xComplete.xEnabled 	:= FALSE;
			PackMLStates.xSuspending.xEnabled 	:= FALSE;
			PackMLStates.xSuspended.xEnabled 	:= FALSE;
			PackMLStates.xUnSuspending.xEnabled := FALSE;
			PackMLStates.xHolding.xEnabled 		:= FALSE;
			PackMLStates.xHeld.xEnabled 		:= FALSE;
			PackMLStates.xUnHolding.xEnabled 	:= FALSE;
		
		Cleaning:
		
		Maintenance:
		 
	END_CASE;
	
	//Active state AND BLINKING OR NOT
	PackMLStates.xAborting.xActive 		:= currentState = mpPACKML_STATE_ABORTING		AND xBlink;
	PackMLStates.xAborted.xActive 		:= currentState = mpPACKML_STATE_ABORTED;
	PackMLStates.xClearing.xActive 		:= currentState = mpPACKML_STATE_CLEARING		AND xBlink;
	PackMLStates.xStopping.xActive 		:= currentState = mpPACKML_STATE_STOPPING		AND xBlink;
	PackMLStates.xStopped.xActive 		:= currentState = mpPACKML_STATE_STOPPED;
	PackMLStates.xResetting.xActive 	:= currentState = mpPACKML_STATE_RESETTING		AND xBlink;
	PackMLStates.xIdle.xActive 			:= currentState = mpPACKML_STATE_IDLE;
	PackMLStates.xStarting.xActive 		:= currentState = mpPACKML_STATE_STARTING		AND xBlink;
	PackMLStates.xExecute.xActive 		:= currentState = mpPACKML_STATE_EXECUTE		AND xBlink;
	PackMLStates.xCompleting.xActive 	:= currentState = mpPACKML_STATE_COMPLETING		AND xBlink;
	PackMLStates.xComplete.xActive 		:= currentState = mpPACKML_STATE_COMPLETE;
	PackMLStates.xSuspending.xActive 	:= currentState = mpPACKML_STATE_SUSPENDING		AND xBlink;	
	PackMLStates.xSuspended.xActive 	:= currentState = mpPACKML_STATE_SUSPENDED;
	PackMLStates.xUnSuspending.xActive 	:= currentState = mpPACKML_STATE_UNSUSPENDING	AND xBlink;
	PackMLStates.xHolding.xActive 		:= currentState = mpPACKML_STATE_HOLDING		AND xBlink;
	PackMLStates.xHeld.xActive 			:= currentState = mpPACKML_STATE_HELD;
	PackMLStates.xUnHolding.xActive 	:= currentState = mpPACKML_STATE_UNHOLDING		AND xBlink;
	
	
END_FUNCTION_BLOCK


FUNCTION_BLOCK PackMLStatUi
	
	StatUITimes.Aborted 		:=  UDINT_TO_DT((StatUi.States.Aborted.CumulativeTime.Hours ) + (StatUi.States.Aborted.CumulativeTime.Minutes) + (StatUi.States.Aborted.CumulativeTime.Seconds));
	StatUITimes.Aborting		:=  UDINT_TO_DT((StatUi.States.Aborting.CumulativeTime.Hours ) + (StatUi.States.Aborting.CumulativeTime.Minutes) + (StatUi.States.Aborting.CumulativeTime.Seconds));
	StatUITimes.Clearing		:=  UDINT_TO_DT((StatUi.States.Clearing.CumulativeTime.Hours ) + (StatUi.States.Clearing.CumulativeTime.Minutes) + (StatUi.States.Clearing.CumulativeTime.Seconds));
	StatUITimes.Complete		:=  UDINT_TO_DT((StatUi.States.Complete.CumulativeTime.Hours ) + (StatUi.States.Complete.CumulativeTime.Minutes) + (StatUi.States.Complete.CumulativeTime.Seconds));
	StatUITimes.Completing		:=  UDINT_TO_DT((StatUi.States.Completing.CumulativeTime.Hours ) + (StatUi.States.Completing.CumulativeTime.Minutes) + (StatUi.States.Completing.CumulativeTime.Seconds));
	StatUITimes.Execute			:=  UDINT_TO_DT((StatUi.States.Execute.CumulativeTime.Hours ) + (StatUi.States.Execute.CumulativeTime.Minutes) + (StatUi.States.Execute.CumulativeTime.Seconds));
	StatUITimes.Held			:=  UDINT_TO_DT((StatUi.States.Held.CumulativeTime.Hours ) + (StatUi.States.Held.CumulativeTime.Minutes) + (StatUi.States.Held.CumulativeTime.Seconds));
	StatUITimes.Holding			:=  UDINT_TO_DT((StatUi.States.Holding.CumulativeTime.Hours ) + (StatUi.States.Holding.CumulativeTime.Minutes) + (StatUi.States.Holding.CumulativeTime.Seconds));
	StatUITimes.Idle			:=  UDINT_TO_DT((StatUi.States.Idle.CumulativeTime.Hours ) + (StatUi.States.Idle.CumulativeTime.Minutes) + (StatUi.States.Idle.CumulativeTime.Seconds));
	StatUITimes.Resetting		:=  UDINT_TO_DT((StatUi.States.Resetting.CumulativeTime.Hours ) + (StatUi.States.Resetting.CumulativeTime.Minutes) + (StatUi.States.Resetting.CumulativeTime.Seconds));
	StatUITimes.Starting		:=  UDINT_TO_DT((StatUi.States.Starting.CumulativeTime.Hours ) + (StatUi.States.Starting.CumulativeTime.Minutes) + (StatUi.States.Starting.CumulativeTime.Seconds));
	StatUITimes.Stopped			:=  UDINT_TO_DT((StatUi.States.Stopped.CumulativeTime.Hours ) + (StatUi.States.Stopped.CumulativeTime.Minutes) + (StatUi.States.Stopped.CumulativeTime.Seconds));
	StatUITimes.Stopping		:=  UDINT_TO_DT((StatUi.States.Stopping.CumulativeTime.Hours ) + (StatUi.States.Stopping.CumulativeTime.Minutes) + (StatUi.States.Stopping.CumulativeTime.Seconds));
	StatUITimes.Suspended		:=  UDINT_TO_DT((StatUi.States.Suspended.CumulativeTime.Hours ) + (StatUi.States.Suspended.CumulativeTime.Minutes) + (StatUi.States.Suspended.CumulativeTime.Seconds));
	StatUITimes.Suspending		:=  UDINT_TO_DT((StatUi.States.Suspending.CumulativeTime.Hours ) + (StatUi.States.Suspending.CumulativeTime.Minutes) + (StatUi.States.Suspending.CumulativeTime.Seconds));
	StatUITimes.Unholding		:=  UDINT_TO_DT((StatUi.States.Unholding.CumulativeTime.Hours ) + (StatUi.States.Unholding.CumulativeTime.Minutes) + (StatUi.States.Unholding.CumulativeTime.Seconds));
	StatUITimes.Unsuspending	:=  UDINT_TO_DT((StatUi.States.Unsuspending.CumulativeTime.Hours ) + (StatUi.States.Unsuspending.CumulativeTime.Minutes) + (StatUi.States.Unsuspending.CumulativeTime.Seconds));
	
	StatUITimes.TimeActive		:= UDINT_TO_DT((StatUi.Mode.CumulativeTime.Hours ) + (StatUi.Mode.CumulativeTime.Minutes) + (StatUi.Mode.CumulativeTime.Seconds));
	
	
END_FUNCTION_BLOCK

FUNCTION packMlTimeToTime
//	
//	strH 	:= UDINT_TO_STRING(udiHH);
//	strM	:= UDINT_TO_STRING(udiMM);
//	strS	:= UDINT_TO_STRING(udiSS);
//	
//	strcat(adrResult, ADR(strH));
//	strcat(adrResult, ADR(':'));
//	strcat(adrResult, ADR(strM));
//	strcat(adrResult, ADR(':'));
//	strcat(adrResult, ADR(strS));
	
	packMlTimeToTime := TRUE;
	
END_FUNCTION

