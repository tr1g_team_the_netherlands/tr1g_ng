
FUNCTION packMlTimeToTime : BOOL
	VAR_INPUT
		adrResult : UDINT;
		udiHH : UDINT;
		udiMM : UDINT;
		udiSS : UDINT;
	END_VAR
	VAR
		strH : STRING[10];
		strM : STRING[10];
		strS : STRING[10];
	END_VAR
END_FUNCTION

FUNCTION_BLOCK PackMLStatUi
	VAR_INPUT
		StatUi : MpPackMLStatisticsUIConnectType;
	END_VAR
	VAR_OUTPUT
		StatUITimes : packMLUIConvertTimes;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK MMI_PilotLight (*Marel PilotLight (5colors)*)
	VAR_INPUT
		PackMLMode : DINT;
		currentState : MpPackMLStateEnum;
		Color : DINT;
		xManCmd : BOOL;
	END_VAR
	VAR_OUTPUT
		Wire : MMI_PilotLight_Wires_typ;
	END_VAR
	VAR
		uiCntMan : UINT;
	END_VAR
	VAR CONSTANT
		uiStepInterval : UINT := 10;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK MMI_PackML_StateDisplay
	VAR_INPUT
		PackMLMode : DINT;
		currentState : MpPackMLStateEnum;
		xBlink : BOOL;
	END_VAR
	VAR_OUTPUT
		PackMLStates : MMI_PackMLStateDisplay_typ;
	END_VAR
END_FUNCTION_BLOCK
