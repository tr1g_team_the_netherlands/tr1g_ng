
PROGRAM _INIT
	sVersion	:= '4441622_P01';
	
	packML_UI.Enable 							:= TRUE;
	packML_UI.MpLink 							:= ADR(gPackMLCore);
	packML_UI.UIConnect 						:= ADR(gPackML);
	packML_UI();

	mpPackML.Enable := TRUE;
	mpPackML.MpLink := ADR(gPackMLCore);
	mpPackML();
	
	mpPackMLMode[0].Enable 	:= TRUE;
	mpPackMLMode[0].ModeID 	:= 1;
	mpPackMLMode[0].MpLink 	:= ADR(gPackMLCore);
	mpPackMLMode[0]();
	
	mpPackMLMode[1].Enable 	:= TRUE;
	mpPackMLMode[1].ModeID 	:= 2;
	mpPackMLMode[1].MpLink 	:= ADR(gPackMLCore);
	mpPackMLMode[1]();
	
	mpPackMLMode[2].Enable 	:= TRUE;
	mpPackMLMode[2].ModeID 	:= 3;
	mpPackMLMode[2].MpLink 	:= ADR(gPackMLCore);
	mpPackMLMode[2]();
	
	mpPackMLMode[3].Enable 	:= TRUE;
	mpPackMLMode[3].ModeID 	:= 4;
	mpPackMLMode[3].MpLink 	:= ADR(gPackMLCore);
	mpPackMLMode[3]();
	
	
	StatisticsUI.Enable		:= TRUE;
	StatisticsUI.MpLink		:= ADR(gPackMLCore);
	StatisticsUI.UIConnect	:= ADR(gPackMlStatUi);
	StatisticsUI();
	
END_PROGRAM

PROGRAM _CYCLIC
	
	//HMI PACKML page display states and blinking states
	packmlStates.PackMLMode 	:= mpPackML.ModeCurrent;
	packmlStates.currentState 	:= mpPackML.StateCurrent;
	packmlStates.xBlink			:= xBlinkState;
	packmlStates();
	gMmiCmd.StateDisplay		:= packmlStates.PackMLStates;
	
	
	
	xHmiModeActLocked 		:= NOT gPackML.ModeControl.LockActivation;
	xHmiModeProdActLock		:= NOT(gPackML.ModeCurrent = 1) AND (mpPackML.StateCurrent = mpPACKML_STATE_ABORTED OR mpPackML.StateCurrent = mpPACKML_STATE_STOPPED OR mpPackML.StateCurrent = mpPACKML_STATE_EXECUTE);
	xHmiModeMaintActLock	:= NOT(gPackML.ModeCurrent = 2) AND (mpPackML.StateCurrent = mpPACKML_STATE_ABORTED OR mpPackML.StateCurrent = mpPACKML_STATE_STOPPED);
	xHmiModeManActLock		:= NOT(gPackML.ModeCurrent = 3) AND (mpPackML.StateCurrent = mpPACKML_STATE_ABORTED OR mpPackML.StateCurrent = mpPACKML_STATE_STOPPED OR mpPackML.StateCurrent = mpPACKML_STATE_EXECUTE);
	xHmiModeCleanActLock	:= NOT(gPackML.ModeCurrent = 4) AND (mpPackML.StateCurrent = mpPACKML_STATE_ABORTED OR mpPackML.StateCurrent = mpPACKML_STATE_STOPPED);
	

	//bundle all EM info
	xSC					:= FALSE;
	EmCmd.xSC 			:= EM[0].xSC AND EM[1].xSC;
	EmCmd.Cmd.xSuspend 	:= EM[0].Cmd.xSuspend OR EM[1].Cmd.xSuspend;
	EmCmd.Cmd.xUnSuspend:= EM[0].Cmd.xUnSuspend OR EM[1].Cmd.xUnSuspend;
	EmCmd.Cmd.xHold		:= EM[0].Cmd.xHold OR EM[1].Cmd.xHold;
	EmCmd.Cmd.xUnHold	:= EM[0].Cmd.xUnHold OR EM[1].Cmd.xUnHold;
	EmCmd.Cmd.xStart	:= EM[0].Cmd.xStart OR EM[1].Cmd.xStart;
	EmCmd.Cmd.xStop		:= EM[0].Cmd.xStop OR EM[1].Cmd.xStop;
	xLinesRunning 		:= xLineSupRun AND xLineDisRun;
	

	
	
	//PackML StateControl by HMI Buttons
	gPackML.StateControl.Clear		:= gMmiCmd.Cmd.xClear;
	gPackML.StateControl.Reset		:= gMmiCmd.Cmd.xReset 	OR (mpPackML.StateCurrent = mpPACKML_STATE_STOPPED AND gMmiCmd.Cmd.xStart);
	gPackML.StateControl.Start		:= gMmiCmd.Cmd.xStart 	OR EmCmd.Cmd.xStart;
	gPackML.StateControl.Stop		:= gMmiCmd.Cmd.xStop 	OR EmCmd.Cmd.xStop;
	gPackML.StateControl.Suspend	:= gMmiCmd.Cmd.xPauze	OR EmCmd.Cmd.xSuspend;
	gPackML.StateControl.Unsuspend	:= EmCmd.Cmd.xUnSuspend;
	gPackML.StateControl.Hold		:= EmCmd.Cmd.xHold;
	gPackML.StateControl.Unhold		:= EmCmd.Cmd.xUnHold;
	
	//PACK ML
	CASE mpPackML.ModeCurrent OF
		Production: // #1 Mode Production
			CASE mpPackML.StateCurrent OF
				mpPACKML_STATE_STOPPED:		
					PilotLight.Color := Green;	
					xSC := EmCmd.xSC;
				
				mpPACKML_STATE_RESETTING:
					PilotLight.Color := Blue;
					xSC := EmCmd.xSC;
								
				mpPACKML_STATE_IDLE:
					PilotLight.Color := Blue;
					gPackML.StateControl.Start	:= TRUE;
				
				mpPACKML_STATE_STARTING:
					PilotLight.Color := Blue;
					xSC := EmCmd.xSC;
								
				mpPACKML_STATE_EXECUTE:
					PilotLight.Color := White;
					
				mpPACKML_STATE_SUSPENDING:
					PilotLight.Color := Blue;
					xSC := EmCmd.xSC;
								
				mpPACKML_STATE_SUSPENDED:
					PilotLight.Color := Blue_Flash;
				
				mpPACKML_STATE_UNSUSPENDING:
					PilotLight.Color := Blue;
					xSC := EmCmd.xSC;
								
				mpPACKML_STATE_HOLDING:
					PilotLight.Color := Blue;
					xSC := EmCmd.xSC;
				
				mpPACKML_STATE_HELD:
					PilotLight.Color := Blue_Flash;
				
				mpPACKML_STATE_UNHOLDING:
					PilotLight.Color := Blue;
					xSC := EmCmd.xSC;
				
				mpPACKML_STATE_STOPPING:
					PilotLight.Color := Blue;
					xSC := EmCmd.xSC;
								
				mpPACKML_STATE_ABORTING:
					PilotLight.Color := Yellow;
					xSC := gFbAirPres.rAirPres2 < 0.5 AND EmCmd.xSC;
				
				mpPACKML_STATE_COMPLETING:
					PilotLight.Color := Blue;
					xSC := EmCmd.xSC;
				
				mpPACKML_STATE_ABORTED:
					PilotLight.Color := Red;
								
				mpPACKML_STATE_CLEARING:
					StateTimer;
					PilotLight.Color := Yellow;
					xEnableAir		:= TRUE;	
					xSC := timState > T#2s AND gFbAirPres.rAirPres2 > Cfg.Config.General.rMinAirPres AND EmCmd.xSC;							//Give some time to reach stable airpressure.
			END_CASE
		Maintenance:
		Manual:
			CASE mpPackML.StateCurrent OF
				mpPACKML_STATE_STOPPED:		
					PilotLight.Color := Green;	
					xSC := TRUE;
						
				mpPACKML_STATE_IDLE:
					PilotLight.Color := Blue;
					gPackML.StateControl.Start	:= TRUE;
				
				mpPACKML_STATE_EXECUTE:
					PilotLight.Color := White;
						
				mpPACKML_STATE_ABORTING:
					PilotLight.Color := Yellow;
					xSC := gFbAirPres.rAirPres2 < 0.5 AND TRUE;
				
				mpPACKML_STATE_ABORTED:
					PilotLight.Color := Red;
								
				mpPACKML_STATE_CLEARING:
					StateTimer;
					PilotLight.Color := Yellow;
					xEnableAir		:= TRUE;	
				xSC := timState > T#2s AND gFbAirPres.rAirPres2 > Cfg.Config.General.rMinAirPres AND EmCmd.xSC;							//Give some time to reach stable airpressure.
			ELSE
				xSC := TRUE;	
			END_CASE
		Cleaning:
		
	END_CASE
	
	//PILOTLIGHT 
	PilotLight(currentState := mpPackML.StateCurrent, PackMLMode := mpPackML.ModeCurrent, xManCmd:= gMmiCmd.Man.xPilotLight);
	
	
	xPackMlStage1 	:=  mpPackML.StateCurrent = mpPACKML_STATE_ABORTING OR 
						mpPackML.StateCurrent = mpPACKML_STATE_ABORTED OR
						mpPackML.StateCurrent = mpPACKML_STATE_CLEARING;
	xPackMLStage2	:=  mpPackML.StateCurrent = mpPACKML_STATE_STOPPING OR
						mpPackML.StateCurrent = mpPACKML_STATE_STOPPED;
	xPackMLStage3	:=  mpPackML.StateCurrent = mpPACKML_STATE_RESETTING OR mpPackML.StateCurrent = mpPACKML_STATE_IDLE OR mpPackML.StateCurrent = mpPACKML_STATE_STARTING OR mpPackML.StateCurrent = mpPACKML_STATE_EXECUTE OR
						mpPackML.StateCurrent = mpPACKML_STATE_COMPLETING OR mpPackML.StateCurrent = mpPACKML_STATE_COMPLETE OR
						mpPackML.StateCurrent = mpPACKML_STATE_HOLDING OR mpPackML.StateCurrent = mpPACKML_STATE_HELD OR mpPackML.StateCurrent = mpPACKML_STATE_UNHOLDING OR
						mpPackML.StateCurrent = mpPACKML_STATE_SUSPENDING OR mpPackML.StateCurrent = mpPACKML_STATE_SUSPENDED OR mpPackML.StateCurrent = mpPACKML_STATE_UNSUSPENDING;
	
	
	gPackML.StateControl.StateComplete := xSC;
	//	gPackML.StateControl.Abort := (gFbAirPres.rAirPres1 < Cfg.Config.General.rMinAirPres) OR  gMmiCmd.xReleaseAir;// OR; 
	gPackML.StateControl.Abort := gAlmReact.xAbortMach OR gAlmReact.xStopMach OR  gMmiCmd.xReleaseAir;// OR; 
	
	
	packML_UI();
	mpPackML();
	StatisticsUI();
	
	packMlStatUI(StatUi := gPackMlStatUi);
	
	packMLStatUi	:= packMlStatUI.StatUITimes;
	
	mpPackMLMode[0];
	mpPackMLMode[1];
	mpPackMLMode[2];
	mpPackMLMode[3];
	
	PackML_Mode := mpPackML.ModeCurrent;
	PackML_State:= mpPackML.StateCurrent;
	
	hw.Do_.xLightBlue	:= PilotLight.Wire.BlueWire;
	hw.Do_.xLightGreen	:= PilotLight.Wire.GreenWire;
	hw.Do_.xLightRed	:= PilotLight.Wire.RedWire;
	hw.Do_.xLightWhite	:= PilotLight.Wire.WhiteWire;
	hw.Do_.xLightYellow	:= PilotLight.Wire.YellowWire;
		

	IF gPackML.StateCurrent = mpPACKML_STATE_ABORTING OR gPackML.StateCurrent = mpPACKML_STATE_ABORTED THEN
		xEnableAir := FALSE; 
	END_IF;
	

	RTInfo_0(enable := TRUE );
	IF gPackML.StateCurrent <> preState THEN
		timState	:= 0;
		preState	:= gPackML.StateCurrent;
	END_IF;
	
	StateBlickTimer;
	xBlinkState := timBlinkState >= T#0s AND timBlinkState <= T#750ms;
	IF timBlinkState > T#1s500ms THEN timBlinkState	:= 0;	END_IF;
	
				
	
	myText;
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

