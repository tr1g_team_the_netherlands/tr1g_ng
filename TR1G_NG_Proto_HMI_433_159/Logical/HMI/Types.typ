
TYPE
	hmi_advanced_typ : 	STRUCT 
		performance : hmi_performance_typ;
	END_STRUCT;
	hmi_supplyLine_typ : 	STRUCT 
		encoderResolution : UINT;
		distanceKicker2 : REAL;
		distanceKicker1 : REAL;
		distancePDSSignal : REAL;
		dps : REAL;
	END_STRUCT;
	hmi_authoritiesConfiguration_typ : 	STRUCT 
		shacklePitch : REAL;
		ramp : UINT;
		maxRPM : REAL;
		minRPM : REAL;
		maxControlLevel : UINT;
		minControlLevel : UINT;
		leftRight : BOOL;
	END_STRUCT;
	hmi_dischargeLine_typ : 	STRUCT 
		encoderResolution : UINT;
		rejectionWindow : REAL;
		rejectionOffset : REAL;
		detectionWindow : REAL;
	END_STRUCT;
	hmi_carrierControl_typ : 	STRUCT 
		dischargeDistance : REAL;
		dischargeDistanceBefore : REAL;
		supplyDistance : REAL;
		supplyDistanceBefore : REAL;
		radiusSynchroWheelCarrier : REAL;
		radiusSynchroWheel : REAL;
		startDelayCarrier : REAL;
		radiusFrictionDiscCarrier : REAL;
		radiusFrictionDisc : REAL;
	END_STRUCT;
	hmi_machineSettings_typ : 	STRUCT 
		discDrying : BOOL;
		emptyTr1g : DINT;
		stopTr1g : DINT;
	END_STRUCT;
	hmi_manualControl_typ : 	STRUCT 
		startStopMovigear : BOOL;
		rejectorClosed : BOOL;
		rejectorOpen : BOOL;
		sperPDSclosed : BOOL;
		sperPDSopen : BOOL;
		sperMXclosed : BOOL;
		sperMXopen : BOOL;
		kicker2out : BOOL;
		kicker2in : BOOL;
		kicker1out : BOOL;
		kicker1in : BOOL;
		checkPilotLight : BOOL;
		moviGearSpeed : USINT;
	END_STRUCT;
	hmi_plcIO_typ : 	STRUCT 
		dischargeEncoderRPM : REAL;
		supplyEncoderRPM : REAL;
		dischargeEncoderPos : UINT;
		supplyEncoderPos : UINT;
		airpressure : ARRAY[0..3]OF REAL;
		s : ARRAY[0..9]OF BOOL;
	END_STRUCT;
	hmi_mapp_typ : 	STRUCT 
		mpPackML : MpPackMLBasicUIConnectType;
		mpAlarmXHistory : MpAlarmXHistoryUIConnectType;
		mpAlarmX : MpAlarmXListUIConnectType;
	END_STRUCT;
	hmi_operatorPage_typ : 	STRUCT 
		uiSupLineSpdH : UINT;
		uiSupLineSpdMs : REAL;
		uiDisLineSpdH : UINT;
		uiDisLineSpdMs : REAL;
		selection : UINT;
		releaseCarrier : BOOL;
		enableMode : BOOL;
	END_STRUCT;
	hmi_performance_logPer_typ : 	STRUCT 
		assigned : REAL;
		loadMX : ARRAY[0..1]OF REAL;
		dropped : ARRAY[0..1]OF REAL;
	END_STRUCT;
	hmi_performance_log_typ : 	STRUCT 
		mxShackle : REAL;
		dropped : REAL;
		noTrolley : REAL;
		bufferFull : REAL;
		assigned : REAL;
		pdsShackle : REAL;
		actual : REAL;
	END_STRUCT;
	hmi_performance_trolley_typ : 	STRUCT 
		sh0missed : ARRAY[0..1]OF REAL;
		sh0early : ARRAY[0..1]OF REAL;
		roundsOK : ARRAY[0..1]OF REAL;
	END_STRUCT;
	hmi_performance_typ : 	STRUCT 
		resetCounter : BOOL;
		tests : BOOL;
		trolleyCounter : hmi_performance_trolley_typ;
		logisticsInPer : hmi_performance_logPer_typ;
		logistics : hmi_performance_log_typ;
	END_STRUCT;
	hmi_typ : 	STRUCT 
		numAlarms : UDINT;
		operatorPage : hmi_operatorPage_typ;
		mappUI : hmi_mapp_typ;
		authoritiesConfiguration : hmi_authoritiesConfiguration_typ;
		manualControl : hmi_manualControl_typ;
		plcIO : hmi_plcIO_typ;
		dischargeLine : hmi_dischargeLine_typ;
		supplyLine : hmi_supplyLine_typ;
		advanced : hmi_advanced_typ;
		carrierControl : hmi_carrierControl_typ;
		machineSettings : hmi_machineSettings_typ;
	END_STRUCT;
	mappUI_typ : 	STRUCT 
		mpPackMLMode : ARRAY[0..3]OF MpPackMLMode;
		mpAlarmXHistory : MpAlarmXHistoryUI;
		packML : MpPackMLBasicUI;
		mpAlarmX : MpAlarmXListUI;
	END_STRUCT;
END_TYPE
