
PROGRAM _INIT
	(* Insert code here *)
	
	mappUI.mpAlarmX.Enable 							:= TRUE;
	mappUI.mpAlarmX.MpLink 							:= ADR(gAlarmXCore);
	mappUI.mpAlarmX.UIConnect 						:= ADR(hmi.mappUI.mpAlarmX);
	mappUI.mpAlarmX.UISetup.AlarmListScrollWindow 	:= 1;
	mappUI.mpAlarmX.UISetup.AlarmListSize 			:= 20;
	mappUI.mpAlarmX();

	
	mappUI.mpAlarmXHistory.Enable 							:= TRUE;
	mappUI.mpAlarmXHistory.MpLink 							:= ADR(gAlarmXHistory);
	mappUI.mpAlarmXHistory.UIConnect 						:= ADR(hmi.mappUI.mpAlarmXHistory);
	mappUI.mpAlarmXHistory.UISetup.AlarmListScrollWindow 	:= 1;
	mappUI.mpAlarmXHistory.UISetup.AlarmListSize 			:= 20;
	mappUI.mpAlarmXHistory();

	mappUI.packML.Enable 							:= TRUE;
	mappUI.packML.MpLink 							:= ADR(gPackMLCore);
	mappUI.packML.UIConnect 						:= ADR(hmi.mappUI.mpPackML);
	mappUI.packML();
	

	FOR i:=0 TO 3 DO
		mappUI.mpPackMLMode[i].Enable := TRUE;
		mappUI.mpPackMLMode[i].ModeID := i+1;
		mappUI.mpPackMLMode[i].MpLink := ADR(gPackMLCore);
		mappUI.mpPackMLMode[i]();
	END_FOR

	
	//HMI
	gMmiCmd.StateDisplay.xAborting.xEnabled		:= TRUE;
	gMmiCmd.StateDisplay.xAborted.xEnabled 		:= TRUE;
	gMmiCmd.StateDisplay.xClearing.xEnabled 	:= TRUE;
	gMmiCmd.StateDisplay.xStopping.xEnabled 	:= TRUE;
	gMmiCmd.StateDisplay.xStopped.xEnabled 		:= TRUE;
	gMmiCmd.StateDisplay.xResetting.xEnabled 	:= TRUE;
	gMmiCmd.StateDisplay.xIdle.xEnabled 		:= TRUE;
	gMmiCmd.StateDisplay.xStarting.xEnabled 	:= TRUE;
	gMmiCmd.StateDisplay.xExecute.xEnabled 		:= TRUE;
	gMmiCmd.StateDisplay.xCompleting.xEnabled 	:= FALSE;
	gMmiCmd.StateDisplay.xComplete.xEnabled 	:= FALSE;
	gMmiCmd.StateDisplay.xSuspending.xEnabled 	:= TRUE;
	gMmiCmd.StateDisplay.xSuspended.xEnabled 	:= TRUE;
	gMmiCmd.StateDisplay.xUnSuspending.xEnabled := TRUE;
	gMmiCmd.StateDisplay.xHolding.xEnabled 		:= FALSE;
	gMmiCmd.StateDisplay.xHeld.xEnabled 		:= FALSE;
	gMmiCmd.StateDisplay.xUnHolding.xEnabled 	:= FALSE;
	
	
END_PROGRAM

PROGRAM _CYCLIC
	
	
	//HMI PackML page
	gMmiCmd.StateDisplay.xAborting.xActive 		:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_ABORTING		AND xBlinkState;
	gMmiCmd.StateDisplay.xAborted.xActive 		:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_ABORTED;
	gMmiCmd.StateDisplay.xClearing.xActive 		:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_CLEARING		AND xBlinkState;
	gMmiCmd.StateDisplay.xStopping.xActive 		:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_STOPPING		AND xBlinkState;
	gMmiCmd.StateDisplay.xStopped.xActive 		:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_STOPPED;
	gMmiCmd.StateDisplay.xResetting.xActive 	:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_RESETTING		AND xBlinkState;
	gMmiCmd.StateDisplay.xIdle.xActive 			:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_IDLE;
	gMmiCmd.StateDisplay.xStarting.xActive 		:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_STARTING		AND xBlinkState;
	gMmiCmd.StateDisplay.xExecute.xActive 		:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_EXECUTE		AND xBlinkState;
	gMmiCmd.StateDisplay.xCompleting.xActive 	:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_COMPLETING		AND xBlinkState;
	gMmiCmd.StateDisplay.xComplete.xActive 		:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_COMPLETE;
	gMmiCmd.StateDisplay.xSuspending.xActive 	:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_SUSPENDING		AND xBlinkState;	
	gMmiCmd.StateDisplay.xSuspended.xActive 	:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_SUSPENDED;
	gMmiCmd.StateDisplay.xUnSuspending.xActive 	:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_UNSUSPENDING	AND xBlinkState;
	gMmiCmd.StateDisplay.xHolding.xActive 		:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_HOLDING		AND xBlinkState;
	gMmiCmd.StateDisplay.xHeld.xActive 			:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_HELD;
	gMmiCmd.StateDisplay.xUnHolding.xActive 	:= hmi.mappUI.mpPackML.StateCurrent = mpPACKML_STATE_UNHOLDING		AND xBlinkState;
	
	
	hmi.operatorPage.enableMode := NOT hmi.mappUI.mpPackML.ModeControl.LockActivation;

	hmi.mappUI.mpPackML.StateControl			:= gPackML.StateControl;
	gPackML.ModeControl 	:= hmi.mappUI.mpPackML.ModeControl;	
	gPackML.ModeCurrent 	:= hmi.mappUI.mpPackML.ModeCurrent;	
	gPackML.StateCurrent 	:= hmi.mappUI.mpPackML.StateCurrent;	
	
	mappUI.mpAlarmX();
	mappUI.mpAlarmXHistory();
	mappUI.packML();

	FOR i:=0 TO 3 DO
		mappUI.mpPackMLMode[i]();
	END_FOR

	StateTimer;
	RTInfo_0(enable := TRUE );
	xBlinkState := timState >= T#0s AND timState <= T#750ms;
	IF timState > T#1s500ms THEN timState	:= 0;	END_IF;
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

