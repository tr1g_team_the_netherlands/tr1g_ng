
TYPE
	gUSB_typ : 	STRUCT 
		deviceIdx : USINT;
		device : STRING[5];
		USBMounted : BOOL;
		freeMemory : DINT;
		totalMemory : DINT;
	END_STRUCT;
END_TYPE
