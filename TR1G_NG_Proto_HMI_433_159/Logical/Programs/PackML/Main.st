
PROGRAM _INIT

	
	EM[0].Name	:= 'Rehanger';
	EM[1].Name	:= 'Rejector';
	
END_PROGRAM

PROGRAM _CYCLIC

	gPackML.StateControl.StateComplete := FALSE;
	xSC := (EM[0].xSC AND EM[1].xSC);
	
	//PACK ML
	CASE gPackML.ModeCurrent OF
		Production: // #1 Mode Production
			CASE gPackML.StateCurrent OF
				mpPACKML_STATE_STOPPED:		
					PilotLight(Color := Green);	
				
				mpPACKML_STATE_RESETTING:
					PilotLight(Color := Blue);
					gPackML.StateControl.StateComplete := xSC;
				
//					IF gFbAirPres.rAirPres1 >= Cfg.Config.General.rMinAirPres THEN
//						MpPackMLMode_0.Abort := TRUE;
//					END_IF;
					
				mpPACKML_STATE_IDLE:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start	:= TRUE;
				
				mpPACKML_STATE_STARTING:
					PilotLight(Color := Blue);
					gPackML.StateControl.StateComplete := xSC;
				
				mpPACKML_STATE_EXECUTE:
					PilotLight(Color := White);
					
				mpPACKML_STATE_SUSPENDING:
					PilotLight(Color := Blue);
					gPackML.StateControl.StateComplete := xSC;
				
				mpPACKML_STATE_SUSPENDED:
					PilotLight(Color := Blue_Flash);
				
				mpPACKML_STATE_UNSUSPENDING:
					PilotLight(Color := Blue);
					gPackML.StateControl.StateComplete := xSC;
				
				mpPACKML_STATE_STOPPING:
					PilotLight(Color := Blue);
					gPackML.StateControl.StateComplete := xSC;
				
				mpPACKML_STATE_ABORTING:
					PilotLight(Color := Yellow);
					IF gFbAirPres.rAirPres1 < 0.5 AND gFeedBackFricDisc[1] = 0 THEN
						gPackML.StateControl.StateComplete := TRUE;
					END_IF
				
				mpPACKML_STATE_ABORTED:
					PilotLight(Color := Red);
								
				mpPACKML_STATE_CLEARING:
					StateTimer;
					PilotLight(Color := Yellow);
					xEnableAir		:= TRUE;	
					
					IF timState > T#3s THEN								//Give some time to reach stable airpressure.
						gPackML.StateControl.StateComplete := TRUE;	
					END_IF;
					
					
				
			END_CASE
			
		Maintenance:	// #2 Mode Maintenance
			CASE gPackML.StateCurrent OF
				mpPACKML_STATE_STOPPED:		
					PilotLight(Color := Green);	
				
				mpPACKML_STATE_RESETTING:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
					
				mpPACKML_STATE_IDLE:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
				
				mpPACKML_STATE_STARTING:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
				
				mpPACKML_STATE_EXECUTE:
					PilotLight(Color := White);
					
				mpPACKML_STATE_SUSPENDING:
					PilotLight(Color := Blue);
				
				mpPACKML_STATE_SUSPENDED:
					PilotLight(Color := Blue_Flash);
				
				mpPACKML_STATE_UNSUSPENDING:
					PilotLight(Color := Blue);
				
				mpPACKML_STATE_STOPPING:
					PilotLight(Color := Blue);
				
				mpPACKML_STATE_ABORTING:
					PilotLight(Color := Yellow);
					IF gFbAirPres.rAirPres1 < 0.5 AND gFeedBackFricDisc[1] = 0 THEN
						gPackML.StateControl.StateComplete := TRUE;
					END_IF
				
				mpPACKML_STATE_ABORTED:
					PilotLight(Color := Red);
								
				mpPACKML_STATE_CLEARING:
					PilotLight(Color := Yellow);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
									
			END_CASE
		
		Manual:			// #3 Mode Manual
			CASE gPackML.StateCurrent OF
				mpPACKML_STATE_STOPPED:		
					PilotLight(Color := Green);	
				
				mpPACKML_STATE_RESETTING:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
					
				mpPACKML_STATE_IDLE:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
				
				mpPACKML_STATE_STARTING:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
				
				mpPACKML_STATE_EXECUTE:
					PilotLight(Color := White);
					
				mpPACKML_STATE_SUSPENDING:
					PilotLight(Color := Blue);
				
				mpPACKML_STATE_SUSPENDED:
					PilotLight(Color := Blue_Flash);
				
				mpPACKML_STATE_UNSUSPENDING:
					PilotLight(Color := Blue);
				
				mpPACKML_STATE_STOPPING:
					PilotLight(Color := Blue);
				
				mpPACKML_STATE_ABORTING:
					PilotLight(Color := Yellow);
					IF gFbAirPres.rAirPres1 < 0.5 AND gFeedBackFricDisc[1] = 0 THEN
						gPackML.StateControl.StateComplete := TRUE;
					END_IF
				
				mpPACKML_STATE_ABORTED:
					PilotLight(Color := Red);
								
				mpPACKML_STATE_CLEARING:
					PilotLight(Color := Yellow);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
									
			END_CASE
		
		Cleaning: 		// #4 Mode Cleaning
			CASE gPackML.StateCurrent OF
				mpPACKML_STATE_STOPPED:		
					PilotLight(Color := Green);	
				
				mpPACKML_STATE_RESETTING:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
					
				mpPACKML_STATE_IDLE:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
				
				mpPACKML_STATE_STARTING:
					PilotLight(Color := Blue);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
				
				mpPACKML_STATE_EXECUTE:
					PilotLight(Color := White);
					
				mpPACKML_STATE_SUSPENDING:
					PilotLight(Color := Blue);
				
				mpPACKML_STATE_SUSPENDED:
					PilotLight(Color := Blue_Flash);
				
				mpPACKML_STATE_UNSUSPENDING:
					PilotLight(Color := Blue);
				
				mpPACKML_STATE_STOPPING:
					PilotLight(Color := Blue);
				
				mpPACKML_STATE_ABORTING:
					PilotLight(Color := Yellow);
					IF gFbAirPres.rAirPres1 < 0.5 AND gFeedBackFricDisc[1] = 0 THEN
						gPackML.StateControl.StateComplete := TRUE;
					END_IF
				
				mpPACKML_STATE_ABORTED:
					PilotLight(Color := Red);
								
				mpPACKML_STATE_CLEARING:
					PilotLight(Color := Yellow);
					gPackML.StateControl.Start			:= gMmiCmd.Cmd.xStart;
									
			END_CASE
		
	END_CASE;
	
	hw.Do_.xLightBlue	:= PilotLight.Wire.BlueWire;
	hw.Do_.xLightGreen	:= PilotLight.Wire.GreenWire;
	hw.Do_.xLightRed	:= PilotLight.Wire.RedWire;
	hw.Do_.xLightWhite	:= PilotLight.Wire.WhiteWire;
	hw.Do_.xLightYellow	:= PilotLight.Wire.YellowWire;
	
	gPackML.StateControl.Abort := (gFbAirPres.rAirPres1 < Cfg.Config.General.rMinAirPres OR  gMmiCmd.xReleaseAir) AND 
									NOT (gPackML.StateCurrent = mpPACKML_STATE_STOPPED OR gPackML.StateCurrent = mpPACKML_STATE_STOPPING OR gPackML.StateCurrent = mpPACKML_STATE_CLEARING);
	

	IF gPackML.StateCurrent = mpPACKML_STATE_ABORTING OR gPackML.StateCurrent = mpPACKML_STATE_ABORTED THEN
		xEnableAir := FALSE; 
	END_IF;
	

	RTInfo_0(enable := TRUE );
	IF gPackML.StateCurrent <> preState THEN
		timState	:= 0;
		preState	:= gPackML.StateCurrent;
	END_IF;
	
				
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

