
FUNCTION_BLOCK MMI_PilotLight (*Marel PilotLight (5colors)*)
	VAR_INPUT
		Color : DINT;
	END_VAR
	VAR_OUTPUT
		Wire : MMI_PilotLight_Wires_typ;
	END_VAR
END_FUNCTION_BLOCK
