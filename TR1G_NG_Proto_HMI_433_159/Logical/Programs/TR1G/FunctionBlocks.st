
(* ---------- Encoder Stuff ---------- *)
FUNCTION_BLOCK SimulateEncoder
	RTInfo_0(enable := TRUE);
	
	(* Insert code here *)
	udiEncVal	:= udiEncVal + INT_TO_UDINT(ticks);
	udiEncVal	:= udiEncVal MOD (encValPeriode * 10);
	
	udiEncCounter	:= udiEncVal / 10;
		
	rRPM	:= (((1000.0 / (UDINT_TO_REAL(RTInfo_0.cycle_time) / 1000.0)) * INT_TO_REAL(ticks)) / UDINT_TO_REAL(encValPeriode * 10)) * 60.0;
	rMs		:= (rRPM * rCircumference) / 60.0; 
	
	diRawEnc := REAL_TO_DINT(rRPM);
	diRawEnc := SHL(REAL_TO_DINT(diRawEnc),16);
	diRawEnc := diRawEnc + (SHL(udiEncCounter,2) AND 65535);
	//diRawEnc := di
	
	
	udiEncPosTEST	:= (SHR(diRawEnc, 2) AND 16380);
	rEncRpmTEST			:= SHR(diRawEnc, 16);
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderCalculations
	
	//Extract encoder position from ENCODER RAW DATA
	udiEncPos	:= (SHR(diRawEnc, 2) AND 16380);
	
	//Extract RPM from ENCODER RAW DATA
	rEncRpm	:= SHR(diRawEnc, 16);

	//Entend the range of the encoder.
	udiEncExt(diEncVal := udiEncPos, diEncResolution := EncSet.uiResolution);
		
	//First order system to average signal RPM
	rAvgRpm(In := rEncRpm);

	//Set output
	Enc.EncoderPosition		:= udiEncExt.udiEncoder;
	Enc.PulsesRevolution	:= EncSet.uiResolution;
	Enc.Circumference		:= EncSet.rCircumFerrence;
	Enc.PulsesMeter			:= UINT_TO_REAL(EncSet.uiResolution) / EncSet.rCircumFerrence;
	Enc.RPM					:= ABS(rAvgRpm.Out);
	Enc.MeterSec			:= (Enc.RPM * EncSet.rCircumFerrence) / 60.0; 
		
END_FUNCTION_BLOCK

FUNCTION_BLOCK EncoderExtender
	diCurrEncVal 	:= diEncVal;
	
	IF diCurrEncVal <> diOldEncVal THEN
		diDiffEncVal	:= encOverUnderFlow(diCurrEncVal - diOldEncVal, diEncResolution);
		diOldEncVal		:= diCurrEncVal;
	ELSE
		diDiffEncVal 	:= 0;
	END_IF
	
	udiEncoder := udiEncoder + diDiffEncVal;
		
END_FUNCTION_BLOCK

FUNCTION encOverUnderFlow
	IF 	diDiffEncoder >  diResolution / 3 THEN 
		diDiffEncoder := diDiffEncoder - diResolution; 
	END_IF
	IF 	diDiffEncoder < -diResolution / 3 THEN 
		diDiffEncoder := diDiffEncoder + diResolution; 
	END_IF
	
	encOverUnderFlow := diDiffEncoder;
END_FUNCTION 


(* ---------- Settings Stuff ---------- *)
FUNCTION_BLOCK Settings
	
	SetRehanger_Out	:= SetRehanger_in;
	SetRehanger_Out.rDisProdDet		:= SetRehanger_in.rDisProdDet 		* rEncSupPulsesMeter;
	SetRehanger_Out.rDisProdWindow	:= SetRehanger_in.rDisProdWindow	* rEncSupPulsesMeter;
	SetRehanger_Out.rDisPdsDet		:= SetRehanger_in.rDisPdsDet		* rEncSupPulsesMeter;
	SetRehanger_Out.rDisPdsWindow	:= SetRehanger_in.rDisPdsWindow		* rEncSupPulsesMeter;
	SetRehanger_Out.rDisCarGo		:= SetRehanger_in.rDisCarGo			* rEncSupPulsesMeter;
	SetRehanger_Out.rDisCarGoWindow	:= SetRehanger_in.rDisCarGoWindow	* rEncSupPulsesMeter;
	SetRehanger_Out.aK1.rDisOut		:= SetRehanger_in.aK1.rDisOut		* rEncSupPulsesMeter;
	SetRehanger_Out.aK1.rDisIn		:= SetRehanger_in.aK1.rDisIn		* rEncSupPulsesMeter;
	SetRehanger_Out.aK2.rDisOut		:= SetRehanger_in.aK2.rDisOut		* rEncSupPulsesMeter;
	SetRehanger_Out.aK2.rDisIn		:= SetRehanger_in.aK2.rDisIn		* rEncSupPulsesMeter;
	SetRehanger_Out.aK1.rActWindow	:= SetRehanger_in.aK1.rActWindow 	* rEncSupPulsesMeter;
	SetRehanger_Out.aK2.rActWindow	:= SetRehanger_in.aK2.rActWindow 	* rEncSupPulsesMeter;
	
	
	SetCarDisc_Out	:= SetCarDisc_in;
	
	SetRejector_Out	:= SetRejetor_in;
	SetRejector_Out.rRejectWindow 	:= SetRejetor_in.rRejectWindow 		* rEncDisPulsesMeter;
	SetRejector_Out.rDetWindow		:= SetRejetor_in.rDetWindow 		* rEncDisPulsesMeter;
	SetRejector_Out.rStartDelay		:= SetRejetor_in.rStartDelay		* rEncDisPulsesMeter;
	
	SetConfig_Out 	:= SetConfig_in;
	
	 
END_FUNCTION_BLOCK

(* ---------- Output Stuff ---------- *)
FUNCTION_BLOCK ControlCPX_FB40
	
	usiValve_1to4.0	:= aValve[0].A;
	usiValve_1to4.1	:= aValve[0].B;
	usiValve_1to4.2	:= aValve[1].A;
	usiValve_1to4.3	:= aValve[1].B;
	usiValve_1to4.4	:= aValve[2].A;
	usiValve_1to4.5	:= aValve[2].B;
	usiValve_1to4.6	:= aValve[3].A;
	usiValve_1to4.7	:= aValve[3].B;
	
	usiValve_5to8.0	:= aValve[4].A;
	usiValve_5to8.1	:= aValve[4].B;
	usiValve_5to8.2	:= aValve[5].A;
	usiValve_5to8.3	:= aValve[5].B;
	usiValve_5to8.4	:= aValve[6].A;
	usiValve_5to8.5	:= aValve[6].B;
	usiValve_5to8.6	:= aValve[7].A;
	usiValve_5to8.7	:= aValve[7].B;
	
	rPresOut[0]	:= UINT_TO_REAL(uiPresIn[0]) / 1000;
	rPresOut[1]	:= UINT_TO_REAL(uiPresIn[1]) / 1000;
	rPresOut[2]	:= UINT_TO_REAL(uiPresIn[2]) / 1000;
	rPresOut[3]	:= UINT_TO_REAL(uiPresIn[3]) / 1000;
		
	rHistAir0[uiIndex0] := rPresOut[0];
	uiIndex0 := uiIndex0 + 1;
	IF uiIndex0 > 49999 THEN
		uiIndex0 := 0;	 
	END_IF;
	 
END_FUNCTION_BLOCK

(* ---------- TR1G Stuff ---------- *)
FUNCTION_BLOCK Rejector
	
	xEnaRejector	:= FALSE;
	xInit			:= FALSE;
	xStateComplete	:= FALSE;
	
	CASE PackMLMode OF
		Production:
			CASE PackMLState OF
				//mpPACKML_STATE_STOPPED:				
								
				mpPACKML_STATE_RESETTING:
					xEnaRejector	:= TRUE;
					xInit			:= TRUE;			
				
					IF (xTpCylIn.ET > T#50ms AND xTpCylOut.ET > T#50ms) THEN
						xStateComplete	:= TRUE;
					END_IF
				
				//mpPACKML_STATE_IDLE:
				//mpPACKML_STATE_STARTING:
				//mpPACKML_STATE_EXECUTE:
				//mpPACKML_STATE_SUSPENDING:
				//mpPACKML_STATE_SUSPENDED:
				//mpPACKML_STATE_UNSUSPENDING:
				//mpPACKML_STATE_STOPPING:
									
				mpPACKML_STATE_ABORTING:
					xEnaRejector	:= FALSE;
					xInit			:= FALSE;
					xStateComplete	:= TRUE;
				
				mpPACKML_STATE_ABORTED:
					xEnaRejector	:= FALSE;
					xInit			:= FALSE;
					xStateComplete	:= TRUE;
				
				//mpPACKML_STATE_CLEARING:
				ELSE
					xStateComplete	:= TRUE;
			END_CASE;
		
		Maintenance:
		
		Manual:
		
		Cleaning:
	
	END_CASE
	
	
	
	
	//calculate Cylinder open/close time in pulses
	rOpenCor  	:= (Enc.MeterSec * (TIME_TO_REAL(Cfg.timCylResponseOpen + Cfg.timValveResponseOpen)/1000.0))  * Enc.PulsesMeter; 
	rCloseCor	:= (Enc.MeterSec * (TIME_TO_REAL(Cfg.timCylResponseClose + Cfg.timValveResponseClose)/1000.0)) * Enc.PulsesMeter;
		
	//Triggers Detection Sensors
	xReLeft(CLK := xSensorLeft);
	xReRight(CLK:= xSensorRight);
	
	// Detection Window 
	IF (xReRight.Q OR xReLeft.Q) AND NOT xDetWindowEna THEN
		xDetWindowEna 		:= TRUE;
		udiDetWindowEncPos	:= Enc.EncoderPosition;
		xRejProd			:= TRUE;
	END_IF;
	
	//Check if both legs are in the hook => only then reset reject signal
	IF xDetWindowEna AND (Enc.EncoderPosition <= (udiDetWindowEncPos + REAL_TO_UDINT(Set.rDetWindow)))	THEN
		IF xSensorLeft AND xSensorRight THEN
			xRejProd 		:= FALSE;
			xDetWindowEna	:= FALSE;
		END_IF;
	END_IF;
	
	
	//Reject Open
	xTpCylOut.IN := xInit OR (xRejProd AND Enc.EncoderPosition >= REAL_TO_UDINT(udiDetWindowEncPos + Set.rStartDelay - rOpenCor));
	xTpCylOut(PT := T#80ms);
	
	//Reject Close
	xTpCylIn.IN := xInit OR (xRejProd AND Enc.EncoderPosition >= (udiDetWindowEncPos + REAL_TO_UDINT(Set.rStartDelay + Set.rRejectWindow - rCloseCor)));
	xTpCylIn(PT := T#80ms);
	
	
	xCylOpen	:= xTpCylOut.Q AND xEnaRejector;
	xCylClose	:= xTpCylIn.Q  AND xEnaRejector;
	
		
END_FUNCTION_BLOCK

FUNCTION_BLOCK ShakleTable
	
	//Array size calculation.
	usiArrSize:= SIZEOF(ShakleTab) / SIZEOF(ShakleTab[0]);
	
	
	(*---------- Insert New Row ----------*)
	xReAddNewRow(CLK := xAddNewRow);
	xFeCarLeft(CLK := xCarLeft);
	
	IF xReAddNewRow.Q THEN
		
		//Fill New Row
		ShakleTab[siIndexTab].udiShkNr 		:= udiShkNr;
		ShakleTab[siIndexTab].udiDetShk		:= udiEncPos;
		ShakleTab[siIndexTab].udiDetProdMin	:= aPulsDis.udiProdMin;
		ShakleTab[siIndexTab].udiDetProd	:= 0;
		ShakleTab[siIndexTab].udiDetProdMax	:= aPulsDis.udiProdMax;
		ShakleTab[siIndexTab].ProdDetType	:= noProduct;
		ShakleTab[siIndexTab].udiDetPdsMin	:= aPulsDis.udiPdsMin;
		ShakleTab[siIndexTab].udiDetPds		:= 0;
		ShakleTab[siIndexTab].udiDetPdsMax	:= aPulsDis.udiPdsMax;
		ShakleTab[siIndexTab].usiPdsValue	:= 0;	
		ShakleTab[siIndexTab].udiCarGo		:= aPulsDis.udiCarGo;
		ShakleTab[siIndexTab].xCarLeft		:= FALSE;
		ShakleTab[siIndexTab].xReject		:= FALSE;
		ShakleTab[siIndexTab].udiK1In		:= aPulsDis.udiK1In;
		ShakleTab[siIndexTab].udiK1Out		:= aPulsDis.udiK1Out;
		ShakleTab[siIndexTab].udiK2In		:= aPulsDis.udiK2In;
		ShakleTab[siIndexTab].udiK2Out		:= aPulsDis.udiK2Out;
		
		//increase hook#
		udiShkNr := udiShkNr + 1;
		
		//increase index active row
		siIndexTab := siIndexTab + 1;
		
		//check boundries of index
		IF siIndexTab >= usiArrSize THEN siIndexTab := 0; END_IF

	END_IF
	
	
	(* ---------- INDEX locations ---------- *)
	IF udiEncPos > ShakleTab[usiIndexProdDet].udiDetProdMax   AND ShakleTab[usiIndexProdDet].udiDetProdMax > 0 						THEN usiIndexProdDet 	:= usiIndexProdDet	 + 1;	END_IF
	IF udiEncPos > ShakleTab[usiIndexPdsSigDet].udiDetPdsMax  AND ShakleTab[usiIndexPdsSigDet].udiDetPdsMax > 0						THEN usiIndexPdsSigDet 	:= usiIndexPdsSigDet + 1;	END_IF
	IF udiEncPos > (ShakleTab[usiIndexCarGo].udiCarGo 	+ aPulsDis.udiCarGoWindow) 		AND ShakleTab[usiIndexCarGo].udiCarGo > 0	THEN usiIndexCarGo 		:= usiIndexCarGo 	 + 1;	END_IF
	IF udiEncPos > (ShakleTab[usiIndexKickOut].udiK2Out + aPulsDis.udiKickActWindow)  	AND ShakleTab[usiIndexKickOut].udiK2Out > 0 THEN usiIndexKickOut 	:= usiIndexKickOut 	 + 1;	END_IF
	IF udiEncPos > (ShakleTab[usiIndexKickIn].udiK2In 	+ aPulsDis.udiKickActWindow)   	AND ShakleTab[usiIndexKickIn].udiK2In > 0 	THEN 
		usiIndexKickIn := usiIndexKickIn + 1;	
		brsmemcpy(ADR(exportShakleRow),ADR(ShakleTab[usiIndexKickIn-1]),SIZEOF(ShakleTab[0]));
		xDataLogTrigger	:= TRUE;
	ELSE
		xDataLogTrigger := FALSE;
	END_IF
	
	//Overflow correction
	IF usiIndexProdDet 	>= usiArrSize 	THEN usiIndexProdDet 	:= 0; END_IF
	IF usiIndexPdsSigDet>= usiArrSize 	THEN usiIndexPdsSigDet 	:= 0; END_IF
	IF usiIndexCarGo 	>= usiArrSize 	THEN usiIndexCarGo 		:= 0; END_IF
	IF usiIndexKickOut 	>= usiArrSize  	THEN usiIndexKickOut 	:= 0; END_IF
	IF usiIndexKickIn 	>= usiArrSize  	THEN 
		usiIndexKickIn 	:= 0; 
		xOverFlowTable 	:= TRUE;
	ELSE
		xOverFlowTable 	:= FALSE;
	END_IF
	
	
	IF NOT xTestOn THEN
		ShakleTab[usiIndexCarGo].xReject :=  ShakleTab[usiIndexCarGo].udiDetProd <> 0 AND ShakleTab[usiIndexCarGo].udiDetPds <> 0; 	
	ELSE
		ShakleTab[usiIndexProdDet].xReject := (usiIndexProdDet MOD 2) = 0;
	END_IF
		
	xFbCarLeft 	:= ShakleTab[usiIndexKickOut].xCarLeft;
	xFbReject	:= ShakleTab[usiIndexKickOut].xReject;
	
	xCarGo	:= 	udiEncPos >= ShakleTab[usiIndexCarGo].udiCarGo - (aPulsDis.udiCarGoWindow / 2) AND 
	udiEncPos <= ShakleTab[usiIndexCarGo].udiCarGo + (aPulsDis.udiCarGoWindow / 2) AND 
	ShakleTab[usiIndexCarGo].xReject;
	
	//Detect that the carrier (Supply side) has left it wait position.
	IF xFeCarLeft.Q AND udiEncPos >= ShakleTab[usiIndexCarGo].udiCarGo AND udiEncPos < ShakleTab[usiIndexCarGo].udiK1Out THEN
		ShakleTab[usiIndexCarGo].xCarLeft := TRUE;
	END_IF
	
	
	audiKickerPos.xGoOut	:= ShakleTab[usiIndexKickOut].xReject AND ShakleTab[usiIndexKickOut].xCarLeft AND
	udiEncPos > (ShakleTab[usiIndexKickOut].udiK1Out - aPulsDis.udiK1OutCor )  AND 
	udiEncPos < (ShakleTab[usiIndexKickOut].udiK2Out + aPulsDis.udiKickActWindow);
	//	audiKickerPos.xGoIn		:= ShakleTab[usiIndexKickIn].xReject AND 
	//								(usiIndexKickIn < usiArrSize - 1 AND NOT ShakleTab[usiIndexKickIn + 1].xReject OR
	//								(usiIndexKickIn = usiArrSize - 1 AND NOT ShakleTab[0].xReject)) AND	
	//								udiEncPos > (ShakleTab[usiIndexKickIn].udiK1In - aPulsDis.udiK2OutCor) AND 
	//								udiEncPos < (ShakleTab[usiIndexKickIn].udiK1In + aPulsDis.udiKickActWindow);				
	audiKickerPos.xGoIn		:= ShakleTab[usiIndexKickIn].xReject AND NOT ShakleTab[usiIndexKickOut].xCarLeft AND
	udiEncPos > (ShakleTab[usiIndexKickIn].udiK1In - aPulsDis.udiK2OutCor) AND 
	udiEncPos < (ShakleTab[usiIndexKickIn].udiK1In + aPulsDis.udiKickActWindow);
	
	IF audiKickerPos.xGoOut THEN
		audiKickerPos.udiK1		:= ShakleTab[usiIndexKickOut].udiK1Out - aPulsDis.udiK1OutCor;
		audiKickerPos.udiK2		:= ShakleTab[usiIndexKickOut].udiK2Out - aPulsDis.udiK2OutCor;
		audiKickerPos.usiPdsVal := ShakleTab[usiIndexKickOut].usiPdsValue;
	ELSIF audiKickerPos.xGoIn THEN
		audiKickerPos.udiK1		:= ShakleTab[usiIndexKickIn].udiK1In - aPulsDis.udiK1InCor;
		audiKickerPos.udiK2		:= ShakleTab[usiIndexKickIn].udiK2In - aPulsDis.udiK2InCor;
	END_IF	

	
	(*---------- Product detection ----------*)
	xReProdDet(CLK := xProdDet);
	IF xReProdDet.Q AND
		udiEncPos >= ShakleTab[usiIndexProdDet].udiDetProdMin AND 
		udiEncPos <= ShakleTab[usiIndexProdDet].udiDetProdMax THEN
		
		//write encoder position on first edge detection
		IF ShakleTab[usiIndexProdDet].udiDetProd = 0 THEN
			ShakleTab[usiIndexProdDet].udiDetProd := udiEncPos;
		END_IF
					
		IF ShakleTab[usiIndexProdDet].ProdDetType = noProduct THEN
			ShakleTab[usiIndexProdDet].ProdDetType := oneLegDetected;
		ELSIF ShakleTab[usiIndexProdDet].ProdDetType = oneLegDetected THEN
			ShakleTab[usiIndexProdDet].ProdDetType := twoLegDetected;
		ELSIF ShakleTab[usiIndexProdDet].ProdDetType = twoLegDetected THEN
			ShakleTab[usiIndexProdDet].ProdDetType := moreLegDetected;
		END_IF
	END_IF
		
	//Offset indication between SETTING and ACTUAL
	IF ShakleTab[usiIndexProdDet].udiDetProd >= ShakleTab[usiIndexProdDet].udiDetProdMin AND ShakleTab[usiIndexProdDet].udiDetProd <= ShakleTab[usiIndexProdDet].udiDetProdMax THEN
		iProdIndicationWindow	:= UDINT_TO_INT(ShakleTab[usiIndexProdDet].udiDetProdMax - ShakleTab[usiIndexProdDet].udiDetProdMin);
		sProdIndication	:= REAL_TO_SINT(((ShakleTab[usiIndexProdDet].udiDetProd - (ShakleTab[usiIndexProdDet].udiDetProdMax - (iProdIndicationWindow / 2.0))) / (iProdIndicationWindow / 2.0)) * 100.0);
	END_IF;
	

	(*---------- PDS signal detection ----------*)	
	xRePdsSig(CLK:= xPdsSig);
	IF xRePdsSig.Q OR xPdsSig THEN
		ShakleTab[usiIndexPdsSigDet].udiDetPds := udiEncPos;
		ShakleTab[usiIndexPdsSigDet].usiPdsValue := usiPdsValue;
		ShakleTab[usiIndexPdsSigDet].xReject := TRUE;
	END_IF
	
	//Offset indication between SETTING and ACTUAL
	IF ShakleTab[usiIndexPdsSigDet].udiDetPds >= ShakleTab[usiIndexPdsSigDet].udiDetPdsMin AND ShakleTab[usiIndexPdsSigDet].udiDetPds <= ShakleTab[usiIndexPdsSigDet].udiDetPdsMax THEN
		iPdsIndicationWindow	:= UDINT_TO_INT(ShakleTab[usiIndexPdsSigDet].udiDetPdsMax - ShakleTab[usiIndexPdsSigDet].udiDetPdsMin);
		sPdsIndication	:= REAL_TO_SINT(((ShakleTab[usiIndexPdsSigDet].udiDetPds - (ShakleTab[usiIndexPdsSigDet].udiDetPdsMax - (iPdsIndicationWindow / 2.0))) / (iPdsIndicationWindow / 2.0)) * 100.0);
	END_IF
				
	(* ---------- Reset Complete Table ---------- *)
	//RESET TABLE FOR TESTING 
	IF xResetTable THEN
		siIndexTab			:= 0;
		usiIndexProdDet 	:= 0;
		usiIndexPdsSigDet	:= 0;
		usiIndexCarGo		:= 0;
		usiIndexKickIn		:= 0;
		usiIndexKickOut		:= 0;
		
		FOR usiResetTabLoop := 0 TO usiArrSize - 1 DO 
			ShakleTab[usiResetTabLoop].udiShkNr 	:= 0;
			ShakleTab[usiResetTabLoop].udiDetShk	:= 0;
			ShakleTab[usiResetTabLoop].udiDetProdMin:= 0;
			ShakleTab[usiResetTabLoop].udiDetProd	:= 0;
			ShakleTab[usiResetTabLoop].udiDetProdMax:= 0;
			ShakleTab[usiResetTabLoop].ProdDetType	:= 0;
			ShakleTab[usiResetTabLoop].udiDetPdsMin	:= 0;
			ShakleTab[usiResetTabLoop].udiDetPds	:= 0;
			ShakleTab[usiResetTabLoop].udiDetPdsMax	:= 0;
			ShakleTab[usiResetTabLoop].usiPdsValue	:= 0;
			ShakleTab[usiResetTabLoop].udiCarGo		:= 0;
			ShakleTab[usiResetTabLoop].xCarLeft		:= FALSE;
			ShakleTab[usiResetTabLoop].xReject		:= FALSE;
			ShakleTab[usiResetTabLoop].udiK1In		:= 0;
			ShakleTab[usiResetTabLoop].udiK1Out		:= 0;
			ShakleTab[usiResetTabLoop].udiK2In		:= 0;
			ShakleTab[usiResetTabLoop].udiK2Out		:= 0;
		END_FOR
		xResetTable := FALSE;
	END_IF
	
END_FUNCTION_BLOCK


FUNCTION_BLOCK Rehanger
	
	xStateComplete	:= FALSE;
	RTInfo_0(enable := TRUE);
	
	//redefine inputs
	xDetCarPds		:= Di_.xS04;
	xDetNulCarPds	:= Di_.xS05;
	xDetCarMx		:= Di_.xS06;
	xDetNulCarMx	:= Di_.xS07;
	
	//Edge detections
	xReDetNulCarPds(CLK := xDetNulCarPds);
	xFeDetNulCarPds(CLK := xDetNulCarPds);
	xReDetNulCarMx( CLK := xDetNulCarMx);
	xFeDetNulCarMx( CLK := xDetNulCarMx);
	xFeDetCarMx(	CLK := xDetCarMx);
	xFeDetCarPds(	CLK := xDetCarPds);
	xReShkDet(		CLK := Di_.xS01_Hook);
	
	
	
	(* ---------- kicker(s) ---------- *)	
	//calucute Cylinder open/close time in pulses
	aDis.udiK1OutCor 	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(SetRehang.aK1.timCylResponseOpen  + SetRehang.aK1.timValveResponseOpen)/1000.0) * Enc.PulsesMeter);
	aDis.udiK2OutCor	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(SetRehang.aK2.timCylResponseOpen  + SetRehang.aK2.timValveResponseOpen)/1000.0) * Enc.PulsesMeter);
	aDis.udiK1InCor  	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(SetRehang.aK1.timCylResponseClose + SetRehang.aK1.timValveResponseClose)/1000.0) * Enc.PulsesMeter);
	aDis.udiK2InCor 	:= REAL_TO_UDINT(Enc.MeterSec * (TIME_TO_REAL(SetRehang.aK2.timCylResponseClose + SetRehang.aK2.timValveResponseClose)/1000.0) * Enc.PulsesMeter);
	
	//calculate distance in pulses for the productdetection
	aDis.udiProdMin	:= Enc.EncoderPosition + REAL_TO_UDINT(SetRehang.rDisProdDet - (SetRehang.rDisProdWindow / 2.0));
	aDis.udiProdMax	:= Enc.EncoderPosition + REAL_TO_UDINT(SetRehang.rDisProdDet + (SetRehang.rDisProdWindow / 2.0));
	
	//calculate distance in pulses for the PDS signal
	aDis.udiPdsMin 	:= Enc.EncoderPosition + REAL_TO_UDINT(SetRehang.rDisPdsDet - (SetRehang.rDisPdsWindow / 2.0));
	aDis.udiPdsMax	:= Enc.EncoderPosition + REAL_TO_UDINT(SetRehang.rDisPdsDet + (SetRehang.rDisPdsWindow / 2.0));	
	
	aDis.udiCarGo	:= Enc.EncoderPosition + REAL_TO_UDINT(SetRehang.rDisCarGo);
	aDis.udiK1Out	:= Enc.EncoderPosition + REAL_TO_UDINT(SetRehang.aK1.rDisOut);
	aDis.udiK2Out	:= Enc.EncoderPosition + REAL_TO_UDINT(SetRehang.aK2.rDisOut);
	aDis.udiK1In	:= Enc.EncoderPosition + REAL_TO_UDINT(SetRehang.aK1.rDisIn );
	aDis.udiK2In	:= Enc.EncoderPosition + REAL_TO_UDINT(SetRehang.aK2.rDisIn );
	
	aDis.udiCarGoWindow			:= REAL_TO_UDINT(SetRehang.rDisCarGoWindow);
	aDis.udiKickActWindow		:= REAL_TO_UDINT(SetRehang.aK1.rActWindow);
	
	//SIMULATION PDS VALUE
	IF xTestOn AND xReShkDet.Q THEN
		usiSimPdsVal := usiSimPdsVal + 1;
		IF usiSimPdsVal > 3 THEN usiSimPdsVal := 0; END_IF
	ELSE
		usiSimPdsVal := 0;
	END_IF;
	
	
	
	//call functionblock 
	ShakleTable.udiEncPos		:= Enc.EncoderPosition;
	ShakleTable.udiEncPosRes	:= Enc.PulsesRevolution;
	ShakleTable.xAddNewRow 		:= Di_.xS01_Hook;
	ShakleTable.xProdDet 		:= Di_.xS03; 
	ShakleTable.aPulsDis		:= aDis;
	ShakleTable.xPdsSig 		:= xPdsReject;
	ShakleTable.xCarLeft		:= Di_.xS04;
	ShakleTable.usiPdsValue		:= usiSimPdsVal;
	//HookTable.xResetTable 		:= xResetTable;	
	
	ShakleTable;
		
	xShkTableCarGo		:= ShakleTable.xCarGo;
	
	//Used as trigger to make a CSV file
	xdataLogTrigger		:= ShakleTable.xDataLogTrigger;
	xOverFlowTable		:= ShakleTable.xOverFlowTable;
	
	
	Kick.xSetK1Out := (*Kick.K1IsIn AND*) ShakleTable.audiKickerPos.xGoOut AND  
	Enc.EncoderPosition >=  ShakleTable.audiKickerPos.udiK1 AND
	Enc.EncoderPosition <=  (ShakleTable.audiKickerPos.udiK1 + SetRehang.aK1.rActWindow); 
	
	Kick.xSetK2Out := (*Kick.K2IsIn AND*) ShakleTable.audiKickerPos.xGoOut AND 
	Enc.EncoderPosition >=  ShakleTable.audiKickerPos.udiK2 AND
	Enc.EncoderPosition <=  (ShakleTable.audiKickerPos.udiK2 + SetRehang.aK2.rActWindow);	
	
	Kick.xSetK1In := (*Kick.K1IsOut AND*) ShakleTable.audiKickerPos.xGoIn AND 
	Enc.EncoderPosition >=  ShakleTable.audiKickerPos.udiK1 AND
	Enc.EncoderPosition <=  (ShakleTable.audiKickerPos.udiK1 + SetRehang.aK1.rActWindow);

	Kick.xSetK2In := (*Kick.K2IsOut AND*) ShakleTable.audiKickerPos.xGoIn AND 
	Enc.EncoderPosition >=  ShakleTable.audiKickerPos.udiK2 AND
	Enc.EncoderPosition <=  (ShakleTable.audiKickerPos.udiK2 + SetRehang.aK2.rActWindow);

	
	CASE PackMLMode OF
		Production:
			CASE PackMLState OF
				mpPACKML_STATE_STOPPED:
					timState			:= 0;
					ResetState			:= rrsInitStart;

				mpPACKML_STATE_RESETTING:
					CASE ResetState OF
						rrsInitStart:
							xInitSperStart		:= FALSE;
							xInitRoundPds		:= FALSE;
							xInitRoundMx		:= FALSE;
							xTpInitReset.IN		:= FALSE;
							xEnaFricDisc		:= FALSE;
							ResetState			:= rrsWaitForRunSupLine;
						
						rrsWaitForRunSupLine:
							IF xSupLineRun THEN
								ResetState		:= rrsWaitForRunDisLine;
							END_IF;
						
						rrsWaitForRunDisLine:
							IF xDisLineRun THEN
								timState 		:= 0;
								ResetState		:= rrsInitKickers;
							END_IF
						
						rrsInitKickers: 
							StateTimer;
							IF timState > T#2s THEN
								ResetState		:= rrsStartDrive;	 
							END_IF
						
						rrsStartDrive: 
							IF uiSpdDrive > 0 THEN
								timState 		:= 0;
								ResetState		:= rrsInitSper;
							END_IF;
						
						rrsInitSper:
							StateTimer;
							IF timState > T#1s THEN
								IF xInitCarDone THEN
									ResetState	:= rrsInitFinished;
								ELSE
									ResetState	:= rrsWaitZeroCar;
								END_IF;
							END_IF;
						
						rrsWaitZeroCar:
							uiNrCarPres	:= 0;
							usiInitCarCnt(RESET	:= TRUE);
						
							IF xReDetNulCarPds.Q THEN
								xInitRoundPds	:= TRUE;
								ResetState		:= rrsInitSupStarted;
							ELSIF xReDetNulCarMx.Q THEN
								xInitRoundMx	:= TRUE;
								ResetState		:= rrsInitDisStarted;
							END_IF;
						
						rrsInitSupStarted:
							usiInitCarCnt(CU:= xDetCarPds, RESET:= FALSE);
							IF xReDetNulCarMx.Q THEN
								usiCarCntPdsMx(PV:= usiInitCarCnt.CV, LOAD:= TRUE);
								usiCarCntPdsMx(LOAD:=FALSE);
								ResetState		:= rrsInitSupEnd;
							END_IF;
						
						rrsInitSupEnd:
							usiInitCarCnt(CU:= xDetCarPds, RESET:= FALSE);
							IF xReDetNulCarPds.Q THEN
								usiCarCntMxPds(PV:= usiInitCarCnt.CV - usiCarCntPdsMx.CV, LOAD:= TRUE);
								usiCarCntMxPds(LOAD:= FALSE);
								ResetState		:= rrsInitFinished;
							END_IF;
						
						rrsInitDisStarted:
							usiInitCarCnt(CU:= xDetCarMx, RESET:= FALSE);
							IF xReDetNulCarPds.Q THEN
								usiCarCntMxPds(PV:= usiInitCarCnt.CV, LOAD:= TRUE);
								usiCarCntMxPds(LOAD:=FALSE);
								ResetState		:= rrsInitDisEnd;
							END_IF;
						
						rrsInitDisEnd:
							usiInitCarCnt(CU:= xDetCarMx, RESET:= FALSE);
							IF xReDetNulCarMx.Q THEN
								usiCarCntPdsMx(PV:= usiInitCarCnt.CV - usiCarCntMxPds.CV, LOAD:= TRUE);
								usiCarCntPdsMx(LOAD:= FALSE);
								ResetState		:= rrsInitFinished;
							END_IF;
						
						rrsInitFinished:
							uiNrCarPres			:= 13; //usiInitCarCnt.CV;
							xEnaFricDisc		:= TRUE;
							xInitCarDone		:= TRUE;
							xStateComplete		:= TRUE;
					END_CASE;

						
				//mpPACKML_STATE_IDLE:
					
				//mpPACKML_STATE_STARTING:
					
				mpPACKML_STATE_EXECUTE:
					CTDSusLoop(LOAD := TRUE, PV := usiCarCntPdsMx.CV);
				
				mpPACKML_STATE_SUSPENDING:
					CTDSusLoop(LOAD := FALSE);
					xRejCar := NOT xRsRejOneCar.Q1 AND NOT CTDSusLoop.Q AND Di_.xS02 ;		//request release carrier.					
					CTDSusLoop(CD := SperMX.xSperClose);
					xStateComplete	:= CTDSusLoop.Q;
				
				//mpPACKML_STATE_SUSPENDED:
					
				mpPACKML_STATE_UNSUSPENDING:
					//if the drive is running
					IF uiSpdDrive > 0 THEN
						xStateComplete	:= TRUE;
					END_IF
					
				mpPACKML_STATE_STOPPING:	
					CTDSusLoop(LOAD := FALSE);
					xRejCar := NOT xRsRejOneCar.Q1 AND NOT CTDSusLoop.Q AND Di_.xS02 ;		//request release carrier.					
					CTDSusLoop(CD := SperMX.xSperClose);
					
					xStateComplete	:= CTDSusLoop.Q AND uiSpdDrive = 0;
								
				//mpPACKML_STATE_ABORTING:
					
				mpPACKML_STATE_ABORTED:
					xInitCarDone	:= FALSE;	
					xStateComplete	:= TRUE;
				
				//mpPACKML_STATE_CLEARING:
				ELSE
				xEnaFricDiscInit	:= FALSE;
				xStateComplete		:= TRUE;
			END_CASE;
			
			xResetInitKickers	:= PackMLState = mpPACKML_STATE_RESETTING AND ResetState = rrsInitKickers;
			xInitSperStart 		:= PackMLState = mpPACKML_STATE_RESETTING AND ResetState >= rrsInitSper AND ResetState < rrsInitFinished;
			xEnaFricDiscInit	:= PackMLState = mpPACKML_STATE_RESETTING AND ResetState >= rrsStartDrive AND ResetState <= rrsInitFinished;
			
		
		
		
		Maintenance:
		
		Manual:
		
		Cleaning:
		
	END_CASE;
	
	
	
	
	
	(* ---------- Carrier Counter ---------- *)
	IF PackMLState <> mpPACKML_STATE_RESETTING THEN
		usiCarCntPdsMx(PV:= uiNrCarPres - usiCarCntMxPds.CV, LOAD:= xDetNulCarPds);
		usiCarCntMxPds(PV:= uiNrCarPres - usiCarCntPdsMx.CV, LOAD:= xDetNulCarMx );
	END_IF
	usiCarCntPdsMx(CD:= xDetCarMx,  CU:= xDetCarPds);
	usiCarCntMxPds(CD:= xDetCarPds, CU:= xDetCarMx);
	
	//Index counter @Supply position
	IF xFeDetCarPds.Q THEN
		usiCarIndexSup := usiCarIndexSup + 1;
	END_IF
	IF xDetCarPds AND xDetNulCarPds THEN
		usiCarIndexSup	:= 0;
	END_IF;
	
	//Index counter @discharge position
	IF xFeDetCarMx.Q THEN
		usiCarIndexDis := usiCarIndexDis + 1;
	END_IF
	IF xDetCarMx AND xDetNulCarMx THEN
		usiCarIndexDis	:= 0;
	END_IF;
	
	//Connect PDS value to Carrier
	aPdsCarVal[usiCarIndexSup] := ShakleTable.audiKickerPos.usiPdsVal;
	usiPdsValue	:= aPdsCarVal[usiCarIndexDis];
	
	
	//overkamDetectie
	xPdsMxFull := usiCarCntPdsMx.CV >= usiSetPdsMxFull;
	xMxPdsFull := usiCarCntMxPds.CV >= usiSetMxPdsFull;

	
	
	
	(* ---------- request Release one Carrier  ---------- *)		
	xRsRejOneCar(RESET1:= SperPDS.xSperClose, SET := xRejCar);
	
	
	
	
	(* ---------- Gates / Spers  ---------- *)		
	SperPDS.Set 			:= SetCarDisc; 
	SperPDS.Cfg				:= CfgCarDisc;
	SperPDS.xDetHook	 	:= Di_.xS01; 
	SperPDS.Enc 			:= Enc; 
	SperPDS.xInitRound		:= xInitSperStart;
	SperPDS.rSpdRpmFricDisc := MoviGearSpd_to_RPM(uiSpdDrive, CfgCarDisc.aMoviGear.uiMaxCtrlLvl, CfgCarDisc.aMoviGear.uiMinCtrlLvl, CfgCarDisc.aMoviGear.rMaxRPM, CfgCarDisc.aMoviGear.rMinRPM); //xSpdCarDisc.SpeedRPM;
	SperPDS.xAfterSlotFull	:= xPdsMxFull;
	SperPDS.xReqRelCar		:= (PackMLState = mpPACKML_STATE_EXECUTE 	AND xShkTableCarGo) OR
	(PackMLState = mpPACKML_STATE_SUSPENDING	AND xRsRejOneCar.Q1);
	SperPDS();
	
	xSperPdsOpen	:= 	(PackMLState = mpPACKML_STATE_RESETTING 	AND xInitSperStart AND SperPDS.xSperOpen) OR
	(PackMLState = mpPACKML_STATE_EXECUTE  		AND SperPDS.xSperOpen) OR 
	(PackMLState = mpPACKML_STATE_SUSPENDING  	AND SperPDS.xSperOpen) OR
	(PackMLState = mpPACKML_STATE_STOPPING  	AND SperPDS.xSperOpen);
		
	xSperPdsClose	:= 	(PackMLState = mpPACKML_STATE_RESETTING 	AND SperPDS.xSperClose) OR
	(PackMLState = mpPACKML_STATE_EXECUTE 		AND SperPDS.xSperClose) OR
	(PackMLState = mpPACKML_STATE_SUSPENDING 	AND SperPDS.xSperClose) OR
	(PackMLState = mpPACKML_STATE_STOPPING 		AND SperPDS.xSperClose);
	
	(* ---------- MX Sper  ---------- *)
	IF xInitSperStart THEN
		xSelectDetCarMx	:= Di_.xS01;
		xPdsMxFull		:= FALSE;
		xMxPdsFull		:= FALSE;
	ELSE
		xSelectDetCarMx	:= xDetCarMx;
	END_IF
	
	SperMX.Set 				:= SetCarDisc;
	SperMX.Cfg				:= CfgCarDisc;
	SperMX.xInitCarDet		:= Di_.xS01;
	SperMX.xDetHook			:= Di_.xS02;
	SperMX.xCarDet			:= xDetCarMx;
	SperMX.xInitRound		:= xInitSperStart;
	SperMX.rSpdRpmFricDisc 	:= MoviGearSpd_to_RPM(uiSpdDrive, CfgCarDisc.aMoviGear.uiMaxCtrlLvl, CfgCarDisc.aMoviGear.uiMinCtrlLvl, CfgCarDisc.aMoviGear.rMaxRPM, CfgCarDisc.aMoviGear.rMinRPM); //xSpdCarDisc.SpeedRPM;
	SperMX.xAfterSlotFull	:= xMxPdsFull;
	SperMX();
	
	xSperMxOpen		:= 	(PackMLState = mpPACKML_STATE_RESETTING 	AND xInitSperStart AND SperPDS.xSperOpen) OR
	(PackMLState = mpPACKML_STATE_EXECUTE   	AND SperMX.xSperOpen) OR
	(PackMLState = mpPACKML_STATE_SUSPENDING 	AND SperMX.xSperOpen) OR
	(PackMLState = mpPACKML_STATE_STOPPING 		AND SperMX.xSperOpen);
	
	xSperMxClose	:= 	(PackMLState = mpPACKML_STATE_RESETTING 	AND SperMX.xSperClose) OR
	(PackMLState = mpPACKML_STATE_EXECUTE   	AND SperMX.xSperClose) OR
	(PackMLState = mpPACKML_STATE_SUSPENDING 	AND SperMX.xSperClose) OR	
	(PackMLState = mpPACKML_STATE_STOPPING 		AND SperMX.xSperClose);
	
	(* ---------- Speed control Drive  ---------- *)	
	xSpdCarDisc.Run 			:=  (PackMLState = mpPACKML_STATE_RESETTING AND xEnaFricDiscInit) OR
	(PackMLState = mpPACKML_STATE_EXECUTE AND xInitCarDone AND xEnaFricDisc) OR
	(PackMLState = mpPACKML_STATE_SUSPENDING) OR
	(PackMLState = mpPACKML_STATE_UNSUSPENDING) OR
	(PackMLState = mpPACKML_STATE_STOPPING AND NOT CTDSusLoop.Q);
	xSpdCarDisc.Ramp			:= CfgCarDisc.aMoviGear.iRamp;
		
	MS_TO_RPM_0(rMeterSecond 	:= Enc.MeterSec, rRadius := CfgCarDisc.rRadFricDisc);	
	xSpdCarDisc.SpeedRPM 		:= INT_TO_REAL(MS_TO_RPM_0.iMS_TO_RPM);

	
		
	IF xInitCarDone  THEN
		rFricDiscSpeed 	:= xSpdCarDisc.SpeedRPM;
	ELSE
		rFricDiscSpeed	:= xSpdCarDisc.MinRPM;
	END_IF;
	
	RPM_to_MoviGearSpd_0(rRPM := rFricDiscSpeed, uiMaxCtrlLvl := CfgCarDisc.aMoviGear.uiMaxCtrlLvl, uiMinCtrlLvl := CfgCarDisc.aMoviGear.uiMinCtrlLvl, rMaxRPM := CfgCarDisc.aMoviGear.rMaxRPM, rMinRPM := CfgCarDisc.aMoviGear.rMinRPM, xReverse := TRUE);
	
	xSpdCarDisc.SpeedDrive 	:= RPM_to_MoviGearSpd_0.iRPM_to_MoviGearSpd;
	
	(* ---------- Kickers  ---------- *)
	Kick.xTpK1Out(IN := Kick.xSetK1Out, PT:= SetRehang.aK1.timAct);
	Kick.xTpK2Out(IN := Kick.xSetK2Out, PT:= SetRehang.aK2.timAct);
	Kick.xTpK1In( IN := Kick.xSetK1In,  PT:= SetRehang.aK1.timAct);
	Kick.xTpK2In( IN :=	Kick.xSetK2In,  PT:= SetRehang.aK2.timAct);
				
	xKicker1OUT :=  PackMLState = mpPACKML_STATE_EXECUTE	AND Kick.xTpK1Out.Q;
	xKicker2OUT	:=  PackMLState = mpPACKML_STATE_EXECUTE 	AND Kick.xTpK2Out.Q; 		
	xKicker1IN  := (PackMLState = mpPACKML_STATE_RESETTING	AND xResetInitKickers) OR 
	(PackMLState = mpPACKML_STATE_EXECUTE 	AND Kick.xTpK1In.Q);
	xKicker2IN 	:= (PackMLState = mpPACKML_STATE_RESETTING 	AND xResetInitKickers) OR 
	(PackMLState = mpPACKML_STATE_EXECUTE 	AND Kick.xTpK2In.Q);

	IF xKicker1OUT THEN	Kick.K1IsOut := TRUE;  Kick.K1IsIn := FALSE; END_IF
	IF xKicker2OUT THEN Kick.K2IsOut := TRUE;  Kick.K2IsIn := FALSE; END_IF
	IF xKicker1IN  THEN Kick.K1IsOut := FALSE; Kick.K1IsIn := TRUE;  END_IF
	IF xKicker2IN  THEN Kick.K2IsOut := FALSE; Kick.K2IsIn := TRUE;  END_IF
		
	(* ---------- Indication light  ---------- *)
	CASE IndicationLight OF
		xReject	: xIndicationLight 	:= ShakleTable.xFbReject;
		xCarGo	: xIndicationLight	:= ShakleTable.xCarGo;
		xGoOut	: xIndicationLight	:= ShakleTable.audiKickerPos.xGoOut;
		xGoIn	: xIndicationLight	:= ShakleTable.audiKickerPos.xGoIn;
		 
	END_CASE;
	

END_FUNCTION_BLOCK


FUNCTION_BLOCK SperPDS

	(*
	calculate the time needed FOR the synchrowheel TO travel the given distance.
	When the sensor detects a hook, it can NOT immediately release a carrier. because the opening OF the 
	synchrowheel is NOT on the correct position. This distance between de hookdetection sensor AND the final position is given
	as an setting.
	*)
	
	//calculate the waittime before releasing a carrier.
	
	//Calculate RPM of the Synchrowheel	
	rRpsPickup			:= (Enc.MeterSec / (2.0 * PI * Cfg.rRadSynchroCar));
	rRpsSynchro			:= rRpsPickup / (5.0 / 6.0); 
	rMsSynchro			:= rRpsSynchro * (2.0 * PI * Cfg.rRadSynchro);
	
	timWaitSynchro		:= REAL_TO_TIME((Set.aSupSlot.rDisBeforeOpen / rMsSynchro)* 1000.0);
	timWaitFricDisc		:= REAL_TO_TIME((0.02 / (((PI * 2.0 * Cfg.rRadFricDisc) * rSpdRpmFricDisc) / 60.0))*1000.0);
	timWaitBeforeOpen	:= timWaitSynchro;// - timWaitFricDisc;
	IF timWaitBeforeOpen < T#1ms THEN
		timWaitBeforeOpen	:= T#1ms;
	END_IF;
	
	//create a puls as long as the wanted waittime. And give a trigger on the Falling Edge
	xTpWaitTime(IN:= xDetHook, PT:= timWaitBeforeOpen); 
	xFeWait(CLK:= xTpWaitTime.Q);
		
	
	//Calculate the open time of the Sper. Based on a distance
	timWaitOpen	:= REAL_TO_TIME((Set.aSupSlot.rDisOpen / (((PI * 2.0 * Cfg.rRadFricDisc) * rSpdRpmFricDisc) / 60.0))*1000.0);
	
	xReleasesper := xFeWait.Q AND
					(   (xInitRound ) OR 
					(NOT(xInitRound OR xAfterSlotFull ) AND xReqRelCar));
	
	//Output OPEN
	xTpSperOpen(IN := xReleasesper, PT := timWaitOpen );
	xSperOpen := xTpSperOpen.Q;	
	
	//If the sper has no activation signal for open, then close 
	xTpSperClose(IN:= NOT xSperOpen, PT:=  Set.aSupSlot.timAct);
	
	//Output CLOSE
	xSperClose := xTpSperClose.Q;	
	
END_FUNCTION_BLOCK

FUNCTION_BLOCK SperMX
	(*
	The Carrier sensor is sees the carrier before it is stopped BY the sper. So the sper action has TO wait untill the carrier is at the 
	sper position. The time is calculated BY the TO travel distance.
	*)
	
	rMsFricDisc			:= (rSpdRpmFricDisc / 60.0) * (2.0 * PI * Cfg.rRadFricDisc);

	timWaitBeforeOpen	:= REAL_TO_TIME((Set.aDisSlot.rDisBeforeOpen / rMsFricDisc)* 1000.0);

	xTpWaitTime(IN := xCarDet , PT := timWaitBeforeOpen);
	xFeWaitTime(CLK:= xTpWaitTime.Q);
	
	//When space after slot is full don't release a new carrier. 
	xFeMxPdsFull(CLK:= xAfterSlotFull);
	
	timWaitOpen			:= REAL_TO_TIME((Set.aDisSlot.rDisOpen / rMsFricDisc)* 1000.0);
	
	xReleaseSper		:= 	 (	 xInitRound AND xInitCarDet) OR
							(NOT (xInitRound OR xAfterSlotFull OR xDiAfterSlot) AND xCarDet AND xDetHook AND NOT xTpWaitTillPosition.Q);				//hook MX detected
	
	xTpSperOpen(IN := xReleaseSper AND NOT xSperClose , PT := timWaitOpen);
	xSperOpen			:= xTpSperOpen.Q;
	
	//If the sper has no activation signal for open, then close 
	xTpSperClose(IN:= NOT xSperOpen, PT:= timWaitOpen);
	
	//wait Till Carrier has reached final position.
	timWaitAfterClose	:= REAL_TO_TIME((Set.aDisSlot.rDisWaitAfterClosed / rMsFricDisc)* 1000.0);
	xFeCloseAction(CLK := xTpSperClose.Q);
	xTpWaitTillPosition(IN := xFeCloseAction.Q, PT := timWaitAfterClose);
	
	//Output CLOSE
	xSperClose := xTpSperClose.Q;
		
END_FUNCTION_BLOCK






