
PROGRAM _INIT
	(* ---------- xxxxxxxxxxx ---------- *)
	 
END_PROGRAM

PROGRAM _CYCLIC
		
	//HMI encoder Table
	IF uiStartTab < 0 THEN
		uiStartTab	:= 0;
	ELSIF uiStartTab > 79  THEN
		uiStartTab	:= 79;
	END_IF
	
	uiSizeTab	:= SIZEOF(shkTab) / SIZEOF(shkTab[0])-1;

	FOR iLoop := 0  TO uiSizeTab DO
		shkTab[iLoop] := Rehanger_0.ShakleTable.ShakleTab[iLoop + uiStartTab];
	END_FOR
	
	
	(* ---------- Encoders ---------- *)
	IF NOT gSimulate THEN
		EncSup(diRawEnc := hw.Com_.diEncSup, EncSet := Cfg.Config.EncSup);
		EncDis(diRawEnc := hw.Com_.diEncDis, EncSet := Cfg.Config.EncDis);	
	ELSE
		EncSupSim(ticks	:= iSimEncSupTicks, rCircumference := Cfg.Config.EncSup.rCircumFerrence, encValPeriode := UINT_TO_UDINT(Cfg.Config.EncSup.uiResolution));
		EncDisSim(ticks	:= iSimEncDisTicks, rCircumference := Cfg.Config.EncDis.rCircumFerrence, encValPeriode := UINT_TO_UDINT(Cfg.Config.EncDis.uiResolution));
		EncSup(diRawEnc := EncSupSim.diRawEnc, EncSet := Cfg.Config.EncSup);
		EncDis(diRawEnc := EncDisSim.diRawEnc, EncSet := Cfg.Config.EncDis);
	END_IF;
	
	
	
(* ---------- Settings ---------- *)
	Settings_0.SetRehanger_in 		:= Cfg.Rehanger;
	Settings_0.SetCarDisc_in 		:= Cfg.CarDisc;
	Settings_0.SetRejetor_in 		:= Cfg.Rejector;
	Settings_0.SetConfig_in			:= Cfg.Config;
	Settings_0.rEncSupPulsesMeter 	:= EncSup.Enc.PulsesMeter;
	Settings_0.rEncDisPulsesMeter 	:= EncDis.Enc.PulsesMeter;
	Settings_0;
		
	
(* ---------- Calculate LineSpeeds ---------- *)
	uiSupSpd_h	:= REAL_TO_UINT(EncSup.Enc.RPM * Cfg.Config.EncSup.rShakleAmount * 60.0);
	uiDisSpd_h	:= REAL_TO_UINT(EncDis.Enc.RPM * Cfg.Config.EncDis.rShakleAmount * 60.0);
	uiSupSpd_m	:= REAL_TO_UINT(EncSup.Enc.RPM * Cfg.Config.EncSup.rShakleAmount);
	uiDisSpd_m	:= REAL_TO_UINT(EncDis.Enc.RPM * Cfg.Config.EncDis.rShakleAmount);
	xSupLineRun := uiSupSpd_m > 1.0;
	xDisLineRun := uiDisSpd_m > 1.0;
	
	
(* ---------- Rehanger & CarrierDisc ---------- *)
	Rehanger_0.PackMLMode	:= gPackML.ModeCurrent;
	Rehanger_0.PackMLState	:= gPackML.StateCurrent;
	Rehanger_0.Di_			:= hw.Di_;
	Rehanger_0.Enc			:= EncSup.Enc;
	Rehanger_0.SetCarDisc	:= Settings_0.SetCarDisc_Out;
	Rehanger_0.SetRehang	:= Settings_0.SetRehanger_Out;
	Rehanger_0.CfgCarDisc	:= Settings_0.SetConfig_Out.CarDisc;
	Rehanger_0.xPdsReject	:= hw.Di_.xPdsReleaseCar; //FALSE;
	Rehanger_0.xRejCar		:= gMmiCmd.xReleaseCar OR hw.Di_.xCmdButRelCar;
	Rehanger_0.uiSpdDrive	:= ABS(gFeedBackFricDisc[0]);
	Rehanger_0.xSupLineRun	:= xSupLineRun;
	Rehanger_0.xDisLineRun	:= xDisLineRun;
	Rehanger_0.IndicationLight := udiIndicationLight;
	
	Rehanger_0;
	
	hw.Do_.xIndicationLight	:= Rehanger_0.xIndicationLight;
	EM[0].xSC				:= Rehanger_0.xStateComplete;
	gDataLogging.xTrigger	:= Rehanger_0.xdataLogTrigger;
	gDataLogging.xOverFlow	:= Rehanger_0.xOverFlowTable;
	gDataLogging.PVReg.usiNrOfPvs	:= 17;
	gDataLogging.uiMaxSamp	:= Cfg.Config.General.uiMaxSampleLogger;
	
	
(* ---------- Rejector ---------- *)
	
	Rejector_0.PackMLMode 	:= gPackML.ModeCurrent;
	Rejector_0.PackMLState	:= gPackML.StateCurrent;
	Rejector_0.Enc 			:= EncDis.Enc; 
	Rejector_0.Set 			:= Settings_0.SetRejector_Out;
	Rejector_0.xSensorLeft 	:= hw.Di_.xS08;
	Rejector_0.xSensorRight := hw.Di_.xS09;
	
	Rejector_0;
	EM[1].xSC	:=  Rejector_0.xStateComplete;
	
	
(* ---------- OUTPUTS ---------- *)	
	
	gFricDisc	:= Rehanger_0.xSpdCarDisc;
	
	
	//Festo Valves 
	FestoValves.aValve[0].A	:= Rehanger_0.xSperPdsClose;
	FestoValves.aValve[0].B	:= Rehanger_0.xSperPdsOpen;
	FestoValves.aValve[1].A	:= Rehanger_0.xSperMxClose;
	FestoValves.aValve[1].B	:= Rehanger_0.xSperMxOpen;
	FestoValves.aValve[2].A	:= Rehanger_0.xKicker1OUT;
	FestoValves.aValve[2].B	:= Rehanger_0.xKicker1IN;
	FestoValves.aValve[3].A	:= Rehanger_0.xKicker2OUT;
	FestoValves.aValve[3].B	:= Rehanger_0.xKicker2IN;
	FestoValves.aValve[4].A	:= Rejector_0.xCylOpen; 
	FestoValves.aValve[4].B	:= Rejector_0.xCylClose;	
	FestoValves.aValve[5].A	:= FALSE;
	FestoValves.aValve[5].B	:= FALSE;
	FestoValves.aValve[6].A	:= xEnableAir AND NOT gMmiCmd.xReleaseAir;
	FestoValves.aValve[6].B	:= FALSE;
	FestoValves.aValve[7].B	:= FALSE;
	FestoValves.aValve[7].B	:= FALSE;
	
	FestoValves.uiPresIn[0]	:= hw.Com_.uiAirPres1;
	FestoValves.uiPresIn[1]	:= hw.Com_.uiAirPres2;
	FestoValves.uiPresIn[2]	:= hw.Com_.uiAirPres3;
	FestoValves.uiPresIn[3]	:= hw.Com_.uiAirPres4;
	
	FestoValves;
	
	hw.Com_.aValve1to4 		:= FestoValves.usiValve_1to4;
	hw.Com_.aValve5to8		:= FestoValves.usiValve_5to8;
	gFbAirPres.rAirPres1	:= FestoValves.rPresOut[0];
	gFbAirPres.rAirPres2	:= FestoValves.rPresOut[1];
	gFbAirPres.rAirPres3	:= FestoValves.rPresOut[2];
	gFbAirPres.rAirPres4	:= FestoValves.rPresOut[3];
	
	IF gPackML.StateCurrent = mpPACKML_STATE_EXECUTE AND gFbAirPres.rAirPres1 < gFbAirPres.minAirPres1 THEN
		gFbAirPres.minAirPres1 := gFbAirPres.rAirPres1;
	END_IF
	
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

