(*Insert your comment here.*)

TYPE
	RehangResetState : 
		( (*Rehanger Reset State (rrs)*)
		rrsInitStart := 1,
		rrsWaitForRunSupLine := 2,
		rrsWaitForRunDisLine := 3,
		rrsInitKickers := 4,
		rrsStartDrive := 5,
		rrsInitSper := 6,
		rrsWaitZeroCar := 7,
		rrsInitSupStarted := 8,
		rrsInitSupEnd := 9,
		rrsInitDisStarted := 10,
		rrsInitDisEnd := 11,
		rrsInitFinished := 12
		);
	KickerStates_typ : 	STRUCT 
		xSetK1Out : BOOL;
		xSetK2Out : BOOL;
		xSetK1In : BOOL;
		xSetK2In : BOOL;
		K1IsIn : BOOL;
		K1IsOut : BOOL;
		K2IsIn : BOOL;
		K2IsOut : BOOL;
		xTpK1Out : TP;
		xTpK2Out : TP;
		xTpK1In : TP;
		xTpK2In : TP;
	END_STRUCT;
	FestoValve : 	STRUCT 
		A : BOOL;
		B : BOOL;
	END_STRUCT;
	Encoder_typ : 	STRUCT 
		EncoderPosition : {REDUND_UNREPLICABLE} UDINT;
		PulsesRevolution : {REDUND_UNREPLICABLE} INT;
		Circumference : {REDUND_UNREPLICABLE} REAL;
		PulsesMeter : {REDUND_UNREPLICABLE} REAL;
		RPM : REAL;
		MeterSec : REAL;
	END_STRUCT;
END_TYPE

(*PROGRAM*)

TYPE
	ShakleRow_typ : 	STRUCT 
		udiShkNr : UDINT;
		udiDetShk : UDINT;
		udiDetProdMin : UDINT;
		udiDetProd : UDINT;
		udiDetProdMax : UDINT;
		ProdDetType : ProdDet_typ;
		udiDetPdsMin : UDINT;
		udiDetPds : UDINT;
		udiDetPdsMax : UDINT;
		usiPdsValue : USINT;
		udiCarGo : UDINT;
		xCarLeft : BOOL;
		xReject : BOOL;
		udiK1Out : UDINT;
		udiK2Out : UDINT;
		udiK1In : UDINT;
		udiK2In : UDINT;
	END_STRUCT;
	ProdDet_typ : 
		(
		noProduct := 0,
		oneLegDetected := 1,
		twoLegDetected := 2,
		moreLegDetected := 3
		);
	KickersPositions : 	STRUCT 
		xGoOut : BOOL;
		xGoIn : BOOL;
		udiK1 : UDINT;
		udiK2 : UDINT;
		usiPdsVal : USINT;
	END_STRUCT;
	KickersPulsDistances_typ : 	STRUCT 
		udiProdMin : UDINT;
		udiProdMax : UDINT;
		udiPdsMin : UDINT;
		udiPdsMax : UDINT;
		udiCarGo : UDINT;
		udiCarGoWindow : UDINT;
		udiK1Out : UDINT;
		udiK2Out : UDINT;
		udiK1In : UDINT;
		udiK2In : UDINT;
		udiKickActWindow : UDINT;
		udiK1OutCor : UDINT;
		udiK2OutCor : UDINT;
		udiK1InCor : UDINT;
		udiK2InCor : UDINT;
	END_STRUCT;
END_TYPE
