
PROGRAM _CYCLIC
	(* Insert code here *)
	mpAlarmX();
	mpAlarmXHistory();
	mpPackML();

FOR i:=0 TO MAX_ALARMS DO
 onAlarm_TON[i].IN := MpAlarmXCheckState(gAlarmXCore,alarmNames[i],mpALARMX_STATE_NONE);
 offAlarm_TON[i].IN := MpAlarmXCheckState(gAlarmXCore,alarmNames[i],mpALARMX_STATE_ACKNOWLEDGED);

onAlarm_TON[i]();
offAlarm_TON[i]();

 IF onAlarm_TON[i].Q  THEN
 	MpAlarmXSet(gAlarmXCore,alarmNames[i]);
 END_IF

 IF offAlarm_TON[i].Q THEN
 	MpAlarmXReset(gAlarmXCore,alarmNames[i]);
 END_IF
END_FOR



END_PROGRAM
