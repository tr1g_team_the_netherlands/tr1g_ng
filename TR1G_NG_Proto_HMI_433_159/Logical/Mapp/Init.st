

PROGRAM _INIT
	(* Insert code here *)
	 
	mpAlarmX.Enable := TRUE;
	mpAlarmX.MpLink := ADR(gAlarmXCore);
	mpAlarmX();

	mpAlarmXHistory.Enable 		:= TRUE;
	mpAlarmXHistory.DeviceName 	:= ADR("EXPORT");
	mpAlarmXHistory.MpLink 		:= ADR(gAlarmXHistory);
	mpAlarmXHistory.Language 	:= ADR("en");
	mpAlarmXHistory();

	mpPackML.Enable := TRUE;
	mpPackML.MpLink := ADR(gPackMLCore);
	mpPackML();

	FOR i:=0 TO 1 DO
		onAlarm_TON[0].PT := T#10s;
		offAlarm_TON[0].PT := T#5s;
	END_FOR

END_PROGRAM