(*Hardware*)

TYPE
	MMI_manual_typ : 	STRUCT 
		xK1In : BOOL;
		xK2In : BOOL;
		xK1Out : BOOL;
		xK2Out : BOOL;
		xRejOpen : BOOL;
		xRejClose : BOOL;
		xSperSupOpen : BOOL;
		xSperSupClose : BOOL;
		xSperDisOpen : BOOL;
		xSperDisClose : BOOL;
		xPilotLight : BOOL;
		xDriveRun : BOOL;
		usiSpdDrive : USINT;
	END_STRUCT;
	IndicatorLight_typ : 
		(
		xReject := 0,
		xCarGo := 1,
		xGoOut := 2,
		xGoIn := 3,
		xProdDet := 4,
		xPdsDet := 5,
		xK1Out := 6,
		xK2Out := 7,
		xK1In := 8,
		xK2In := 9
		);
	AirPres_typ : 	STRUCT 
		rAirPres1 : REAL;
		rAirPres2 : REAL;
		rAirPres3 : REAL;
		rAirPres4 : REAL;
		minAirPres1 : REAL;
		maxAirPres1 : REAL;
	END_STRUCT;
	MMI_typ : 	STRUCT 
		Cmd : MMI_PackMLCmd_typ;
		Recipe : MMI_Recipe_typ;
		xReleaseAir : BOOL;
		xReleaseCar : BOOL;
		Man : MMI_manual_typ;
		StateDisplay : MMI_PackMLStateDisplay_typ;
	END_STRUCT;
	MMI_PackMLStateDisplay_typ : 	STRUCT 
		xAborting : MMI_PackMLStateGroup_typ;
		xAborted : MMI_PackMLStateGroup_typ;
		xClearing : MMI_PackMLStateGroup_typ;
		xStopping : MMI_PackMLStateGroup_typ;
		xStopped : MMI_PackMLStateGroup_typ;
		xResetting : MMI_PackMLStateGroup_typ;
		xIdle : MMI_PackMLStateGroup_typ;
		xStarting : MMI_PackMLStateGroup_typ;
		xExecute : MMI_PackMLStateGroup_typ;
		xCompleting : MMI_PackMLStateGroup_typ;
		xComplete : MMI_PackMLStateGroup_typ;
		xHolding : MMI_PackMLStateGroup_typ;
		xHeld : MMI_PackMLStateGroup_typ;
		xUnHolding : MMI_PackMLStateGroup_typ;
		xSuspending : MMI_PackMLStateGroup_typ;
		xSuspended : MMI_PackMLStateGroup_typ;
		xUnSuspending : MMI_PackMLStateGroup_typ;
	END_STRUCT;
	MMI_Recipe_typ : 	STRUCT 
		xLoadConfig : BOOL;
		xSaveConfig : BOOL;
		sFbConfig : STRING[80];
		xLoadMachSet : BOOL;
		xSaveMachSet : BOOL;
		sFbMachSet : STRING[80];
		xSelectUSB : BOOL;
		xUSBMounted : BOOL;
	END_STRUCT;
	MMI_PackMLStateGroup_typ : 	STRUCT 
		xEnabled : BOOL;
		xActive : BOOL;
	END_STRUCT;
	MMI_PackMLCmd_typ : 	STRUCT 
		xStart : BOOL;
		xStop : BOOL;
		xClear : BOOL;
		xReset : BOOL;
		xPauze : BOOL;
	END_STRUCT;
	PackMLMode : 
		(
		Production := 1,
		Maintenance := 2,
		Manual := 3,
		Cleaning := 4
		);
	hw_Typ : 	STRUCT 
		Ao_ : Ao_Typ;
		Ai_ : Ai_Typ;
		Do_ : Do_Typ;
		Di_ : Di_Typ;
		Com_ : Com_Typ;
	END_STRUCT;
	Com_Typ : 	STRUCT 
		diEncSup : DINT;
		diEncDis : DINT;
		uiAirPres1 : UINT;
		uiAirPres2 : UINT;
		uiAirPres3 : UINT;
		uiAirPres4 : UINT;
		aValve1to4 : USINT;
		aValve5to8 : USINT;
	END_STRUCT;
	Ao_Typ : 	STRUCT 
		Spare : USINT;
	END_STRUCT;
	Ai_Typ : 	STRUCT 
		Spare : USINT;
	END_STRUCT;
	Do_Typ : 	STRUCT 
		xLightGreen : BOOL;
		xLightRed : BOOL;
		xLightYellow : BOOL;
		xLightBlue : BOOL;
		xLightWhite : BOOL;
		xIndicationLight : BOOL;
	END_STRUCT;
	Di_Typ : 	STRUCT 
		xS01_Hook : BOOL;
		xS01 : BOOL;
		xS02 : BOOL;
		xS03 : BOOL;
		xS04 : BOOL;
		xS05 : BOOL;
		xS06 : BOOL;
		xS07 : BOOL;
		xS08 : BOOL;
		xS09 : BOOL;
		xS10 : BOOL;
		xFbK1out : BOOL;
		xFbK1In : BOOL;
		xFbK2out : BOOL;
		xFbK2In : BOOL;
		xFbPdsClosed : BOOL;
		xFbPdsOpen : BOOL;
		xFbMxClosed : BOOL;
		xFbMxOpen : BOOL;
		xPdsReleaseCar : BOOL;
		xCmdButRelCar : BOOL;
		Test1 : BOOL;
	END_STRUCT;
END_TYPE

(*Settings*)

TYPE
	Config_typ : 	STRUCT 
		Rejector : SetRejector_typ;
		CarDisc : SetCarDisc_typ;
		Rehanger : SetRehanger_typ;
		Config : Configuration_typ;
	END_STRUCT;
	Configuration_typ : 	STRUCT 
		EncSup : EncSet_typ;
		EncDis : EncSet_typ;
		CarDisc : SetCarDiscConfig_typ;
		Rejector : SetRejectorConfig_typ;
		General : SetGeneral_typ;
	END_STRUCT;
	SetRejectorConfig_typ : 	STRUCT 
		timCylResponseClose : TIME := T#60ms;
		timCylResponseOpen : TIME := T#70ms;
		timValveResponseOpen : TIME := T#20ms;
		timValveResponseClose : TIME := T#20ms;
	END_STRUCT;
	SetRejector_typ : 	STRUCT 
		rStartDelay : REAL := 0.11;
		rRejectWindow : REAL := 0.10;
		rDetWindow : REAL := 0.02;
	END_STRUCT;
	SetCarDiscConfig_typ : 	STRUCT 
		rRadFricDisc : REAL := 0.24;
		rRadFricDiscCar : REAL := 0.53;
		rRadSynchro : REAL := 0.11;
		rRadSynchroCar : REAL := 0.33;
		aMoviGear : MoviGearSettings_typ;
	END_STRUCT;
	SetCarDisc_typ : 	STRUCT 
		aSupSlot : AirCyl_typ := (rDisBeforeOpen:=0.05,rDisOpen:=0.03,timCylResponseClose:=T#60ms,timCylResponseOpen:=T#70ms,timValveResponseOpen:=T#20ms,timValveResponseClose:=T#20ms,timAct:=T#80ms);
		aDisSlot : AirCyl_typ := (rDisBeforeOpen:=0.03,rDisOpen:=0.07,rDisWaitAfterClosed:=0.03,timCylResponseClose:=T#60ms,timCylResponseOpen:=T#70ms,timValveResponseOpen:=T#20ms,timValveResponseClose:=T#20ms,timAct:=T#80ms);
		usiCarAmount : USINT := 13;
	END_STRUCT;
	SetRehanger_typ : 	STRUCT 
		rDisProdDet : REAL := 0.62;
		rDisProdWindow : REAL := 0.1;
		rDisPdsDet : REAL := 0.80;
		rDisPdsWindow : REAL := 0.1;
		rDisCarGo : REAL := 1.04;
		rDisCarGoWindow : REAL := 0.1;
		aK1 : AirCyl_typ := (rDisOut:=1.10,rDisIn:=1.27,timAct:=T#80ms);
		aK2 : AirCyl_typ := (rDisOut:=1.13,rDisIn:=1.31,timAct:=T#80ms);
	END_STRUCT;
	SetGeneral_typ : 	STRUCT 
		timAirHoseTimeDelay : TIME := T#6ms;
		rMinAirPres : REAL := 5.0;
		rWorkAirPres : REAL := 6.0;
		uiMaxSampleLogger : UINT := 1000;
		uiShkEmpty : UINT := 50;
		uiShkStop : UINT := 100;
		xDiscDrying : BOOL := FALSE;
	END_STRUCT;
	AirCyl_typ : 	STRUCT 
		rDisOut : REAL;
		rDisIn : REAL;
		rDisBeforeOpen : REAL;
		rDisOpen : REAL;
		rDisWaitAfterClosed : REAL;
		timCylResponseClose : TIME := T#70ms;
		timCylResponseOpen : TIME := T#60ms;
		timValveResponseOpen : TIME := T#20ms;
		timValveResponseClose : TIME := T#20ms;
		rAirHoseLength : REAL := 1.0;
		rActWindow : REAL := 0.02;
		timAct : TIME := T#80ms;
	END_STRUCT;
	MoviGearSettings_typ : 	STRUCT 
		uiMinCtrlLvl : UINT := 1000;
		uiMaxCtrlLvl : UINT := 10000;
		rMinRPM : REAL := 11.5;
		rMaxRPM : REAL := 115.4;
		iRamp : INT := 2500;
	END_STRUCT;
	EncSet_typ : 	STRUCT 
		uiResolution : UINT := 5000;
		rCircumFerrence : REAL;
		rShakleAmount : REAL;
	END_STRUCT;
	CarDiscCtrl_typ : 	STRUCT 
		Run : BOOL := 0;
		SpeedRPM : REAL := 0.0;
		SpeedDrive : INT;
		Ramp : INT := 2500;
		DriveNumber : USINT := 1;
		MinRPM : REAL := 11.5;
		MaxRPM : REAL := 115.4;
		MinCtrlLevel : UINT := 1000;
		MaxCtrlLevel : UINT := 10000;
	END_STRUCT;
	pvReg_typ : 	STRUCT 
		allPVsRegistered : BOOL;
		usiNrOfPvs : USINT;
		tempPath : STRING[100];
		unit : STRING[20];
		description : STRING[50];
		scaleFactor : REAL;
		strVariableName : STRING[15];
		strHookTabIdx : STRING[2];
		path : STRING[100];
		state : pvreg_enum_typ;
		pvIdx : UINT;
	END_STRUCT;
	pvreg_enum_typ : 
		(
		PV_REG_VARIABLE_INDEX_PATH := 0,
		PV_REG_BUSY := 1,
		PV_REG_DONE := 2
		);
	dataLogging_typ : 	STRUCT 
		xTrigger : BOOL;
		xOverFlow : BOOL;
		xRecord : BOOL;
		uiMaxSamp : UINT := 1000;
		PVReg : pvReg_typ;
	END_STRUCT;
	EM_typ : 	STRUCT  (*Equipment Module*)
		Name : STRING[50];
		xSC : BOOL;
	END_STRUCT;
END_TYPE
