
PROGRAM _CYCLIC

	struct_SessionConnectInfo.SecurityMsgMode 							:= UASecurityMsgMode_None;
	struct_SessionConnectInfo.SecurityPolicy 							:= UASecurityPolicy_None;
	struct_SessionConnectInfo.TransportProfile 							:= UATP_UATcp;
	struct_SessionConnectInfo.UserIdentityToken.UserIdentityTokenType 	:= UAUITT_Username;
	struct_SessionConnectInfo.UserIdentityToken.TokenParam1 			:= 'OpcMarel';
	struct_SessionConnectInfo.UserIdentityToken.TokenParam2 			:= 'OpcMarel';
	struct_SessionConnectInfo.SessionTimeout 							:= T#20s;
	struct_SessionConnectInfo.MonitorConnection 						:= T#100s;
	
	(*-------------------UA_Connect - establish connection to OPC-UA Server --------------------*)
	CASE opc_state OF
		OPC_UA_CONNECT:
			fb_UA_Connect.Execute := TRUE;
			fb_UA_Connect.ServerEndpointUrl := 'opc.tcp://10.1.25.56:4840';
			fb_UA_Connect.SessionConnectInfo := struct_SessionConnectInfo;
			fb_UA_Connect.Timeout := tConnectionTimeout;
			fb_UA_Connect();
			
		
			IF (fb_UA_Connect.Busy = 0) THEN
				IF (fb_UA_Connect.Done = 1) THEN
					ErrorID := 0;
					opc_state := OPC_UA_GETNAMESPACE;	
				END_IF
			
				IF (fb_UA_Connect.Error = 1) THEN
					ErrorID := fb_UA_Connect.ErrorID;
					opc_state := OPC_UA_DISCONNECT;
				END_IF
			
			END_IF
		
			
		(* --------------------UA_GetNamespaceIndex - read index of required namespace for PVs -------*)
		OPC_UA_GETNAMESPACE:
			fb_UA_GetNamespaceIndex.Execute 		:= TRUE;
			fb_UA_GetNamespaceIndex.ConnectionHdl 	:= fb_UA_Connect.ConnectionHdl;
			fb_UA_GetNamespaceIndex.NamespaceUri 	:= 'urn:B&R/pv/';
			fb_UA_GetNamespaceIndex.Timeout 		:= tConnectionTimeout;
			fb_UA_GetNamespaceIndex();
			
			IF (fb_UA_GetNamespaceIndex.Busy = 0) THEN
				IF (fb_UA_GetNamespaceIndex.Done = 1) THEN
					ErrorID:= 0;	
					FOR i := 0 TO usiREAD_ITEMS DO
						NodeIDs_0[i].NamespaceIndex := fb_UA_GetNamespaceIndex.NamespaceIndex;
						NodeIDs_0[i].IdentifierType := UAIdentifierType_String;
					END_FOR
				
					NodeIDs_0[0].Identifier 	:= '::Com:toPds.usiPosNr';
					NodeIDs_0[1].Identifier 	:= '::Com:toPds.xS01';
					NodeIDs_0[2].Identifier 	:= '::Com:toPds.xS02';
					NodeIDs_0[3].Identifier 	:= '::Com:toPds.xS04';
					NodeIDs_0[4].Identifier 	:= '::Com:toPds.xS05';
					NodeIDs_0[5].Identifier 	:= '::Com:toPds.xS06';
					NodeIDs_0[6].Identifier 	:= '::Com:toPds.xS07';
					opc_state 					:= OPC_UA_GET_SERVER_STATUS;		
				END_IF
			
				IF (fb_UA_GetNamespaceIndex.Error = 1) THEN
					ErrorID:= fb_UA_GetNamespaceIndex.ErrorID;
					NamespaceIndex := 0;
					opc_state:=OPC_UA_DISCONNECT;
				END_IF
			END_IF
		
		(* --------------------Get server Status  ------------*)
		OPC_UA_GET_SERVER_STATUS:
			
			
			
			fb_UA_ConnectionGetStatus.ConnectionHdl	:= fb_UA_Connect.ConnectionHdl;
			fb_UA_ConnectionGetStatus.Execute		:= TRUE;
			fb_UA_ConnectionGetStatus.Timeout		:= tConnectionTimeout;
			fb_UA_ConnectionGetStatus();
		
			IF (fb_UA_ConnectionGetStatus.Busy = 0) THEN
				IF (fb_UA_ConnectionGetStatus.Done = 1) THEN
					ErrorID := 0;
					opc_state := OPC_UA_GET_NODEHANDLE_LIST;	
				END_IF
			
				IF (fb_UA_ConnectionGetStatus.Error = 1) THEN
					ErrorID := fb_UA_ConnectionGetStatus.ErrorID;
					opc_state:=OPC_UA_DISCONNECT;
				END_IF
			
			END_IF
		
		(* ----------------------UA_NodeGetHandleList - get handles for required nodes---------------- *)
		OPC_UA_GET_NODEHANDLE_LIST:
			
			fb_UA_NodeGetHandleList.Execute 		:= TRUE;
			fb_UA_NodeGetHandleList.ConnectionHdl 	:= fb_UA_Connect.ConnectionHdl;
			fb_UA_NodeGetHandleList.NodeIDCount 	:= usiREAD_ITEMS;
			fb_UA_NodeGetHandleList.NodeIDs 		:= NodeIDs_0;
			fb_UA_NodeGetHandleList.Timeout 		:= tConnectionTimeout;
			fb_UA_NodeGetHandleList();
			
			IF (fb_UA_NodeGetHandleList.Busy = 0) THEN
				IF (fb_UA_NodeGetHandleList.Done = 1) THEN
					
					Variables_Rd0[0] 			:= '::OPC_UA_C:structFromTr1G.usiPosNr';
					Variables_Rd0[1] 			:= '::OPC_UA_C:structFromTr1G.xS01';
					Variables_Rd0[2] 			:= '::OPC_UA_C:structFromTr1G.xS02';
					Variables_Rd0[3] 			:= '::OPC_UA_C:structFromTr1G.xS04';
					Variables_Rd0[4] 			:= '::OPC_UA_C:structFromTr1G.xS05';
					Variables_Rd0[5] 			:= '::OPC_UA_C:structFromTr1G.xS06';
					Variables_Rd0[6] 			:= '::OPC_UA_C:structFromTr1G.xS07';
			
					
					fb_UA_ReadList.Execute		:=FALSE;
					fb_UA_ReadList(Variables 	:= Variables_Rd0);
					
					ErrorID						:= 0;
					opc_state 					:= OPC_UA_READ_LIST;
				END_IF
				IF (fb_UA_NodeGetHandleList.Error = 1) THEN
					ErrorID:= fb_UA_NodeGetHandleList.ErrorID;
					NodeHdls[0] := 0;
					opc_state:=OPC_UA_DISCONNECT;
				END_IF
			END_IF			
		
		(* UA_ReadList - read required nodes from OPC-UA Server and write it in local plc variables *)
		OPC_UA_READ_LIST:				
			fb_UA_ReadList.Execute 			:= TRUE;
			fb_UA_ReadList.ConnectionHdl 	:= fb_UA_Connect.ConnectionHdl;
			fb_UA_ReadList.NodeHdlCount 	:= usiREAD_ITEMS;
			fb_UA_ReadList.NodeHdls 		:= fb_UA_NodeGetHandleList.NodeHdls;
			fb_UA_ReadList.NodeAddInfo 		:= NodeAddInfo_0;
			fb_UA_ReadList.Timeout 			:= tConnectionTimeout;
			fb_UA_ReadList(Variables 		:= Variables_Rd0);
			
			
			IF (fb_UA_ReadList.Busy = 0) THEN
				IF (fb_UA_ReadList.Done OR fb_UA_ReadList.Error) THEN
					ErrorID								:= 	fb_UA_ReadList.ErrorID;
					structFromTr1G.dtTimeStampUsiPosNr	:=	fb_UA_ReadList.TimeStamps[0];
					structFromTr1G.dtTimeStampXs01		:=	fb_UA_ReadList.TimeStamps[1];
					structFromTr1G.dtTimeStampXs02		:=	fb_UA_ReadList.TimeStamps[2];
					structFromTr1G.dtTimeStampXs04		:=	fb_UA_ReadList.TimeStamps[3];
					structFromTr1G.dtTimeStampXs05		:=	fb_UA_ReadList.TimeStamps[4];
					structFromTr1G.dtTimeStampXs06		:=	fb_UA_ReadList.TimeStamps[5];
					structFromTr1G.dtTimeStampXs07		:=	fb_UA_ReadList.TimeStamps[6];
					fb_UA_NodeGetHandleList(Execute 	:= FALSE);
					opc_state 							:= 	OPC_UA_RELEASE_NODELIST_HANDLES;	
				END_IF
			END_IF
		
		(* -----------------------Write client data to the OPC server-----------------------------*)
		OPC_UA_WRITE_LIST:
			fb_UA_WriteList.Execute	:= TRUE;
			fb_UA_WriteList.ConnectionHdl	:= fb_UA_Connect.ConnectionHdl;
			fb_UA_WriteList.NodeHdlCount	:= 1;
		//	fb_UA_WriteList.NodeHdls[0]		:=
		//	fb_UA_WriteList.NodeAddInfo		:=
		//	fb_UA_WriteList.NodeAddInfo[0]	:=
		//	fb_UA_WriteList.NodeHdls		:= 
		
		
		(* -------------------UA_NodeReleaseHandleList - release the handles for the nodes------ *)
		OPC_UA_RELEASE_NODELIST_HANDLES:	
			fb_UA_NodeReleaseHandleList.Execute 		:= TRUE;
			fb_UA_NodeReleaseHandleList.ConnectionHdl 	:= fb_UA_Connect.ConnectionHdl;
			fb_UA_NodeReleaseHandleList.NodeHdlCount 	:= usiREAD_ITEMS;
			fb_UA_NodeReleaseHandleList.NodeHdls 		:= NodeHdls;
			fb_UA_NodeReleaseHandleList.Timeout 		:= tConnectionTimeout;
			fb_UA_NodeReleaseHandleList();
			
			IF (fb_UA_NodeReleaseHandleList.Busy = 0) THEN
				IF (fb_UA_NodeReleaseHandleList.Done) THEN
					ErrorID:= 0;
					NodeHdls[0] := 0;
					NodeHdls[1] := 0;
					NodeHdls[2] := 0;
					NodeHdls[3] := 0;
					NodeHdls[4] := 0;
					NodeHdls[5] := 0;
					NodeHdls[6] := 0;
					fb_UA_NodeGetHandleList(Execute := FALSE);
					opc_state := OPC_UA_GET_NODEHANDLE_LIST;
					diSessionCounter := diSessionCounter +1;
				END_IF
				IF (fb_UA_NodeReleaseHandleList.Error = 1) THEN
					ErrorID:= fb_UA_NodeReleaseHandleList.ErrorID;
					opc_state:=OPC_UA_DISCONNECT;
				END_IF
			END_IF
			
			
		(* -----------------------UA_Disconnect - disconnect from OPC-UA Server *)
		OPC_UA_DISCONNECT:	
			fb_UA_Disconnect.Execute 		:= TRUE;
			fb_UA_Disconnect.ConnectionHdl 	:= fb_UA_Connect.ConnectionHdl;
			fb_UA_Disconnect.Timeout 		:= tConnectionTimeout;
			fb_UA_Disconnect();
			
			IF (fb_UA_Disconnect.Busy = 0) THEN
				IF (fb_UA_Disconnect.Done) THEN
					fb_UA_NodeReleaseHandleList(Execute:=FALSE);
					fb_UA_ReadList(Execute := FALSE, Variables := Variables_Rd0);
					fb_UA_NodeGetHandleList(Execute := FALSE);
					fb_UA_GetNamespaceIndex(Execute := FALSE);
					fb_UA_Connect(Execute := FALSE);
					fb_UA_ConnectionGetStatus(Execute := FALSE);
					opc_state := OPC_UA_CONNECT;
				END_IF
			END_IF
	END_CASE;
	
	
	structFromTr1G;
END_PROGRAM
