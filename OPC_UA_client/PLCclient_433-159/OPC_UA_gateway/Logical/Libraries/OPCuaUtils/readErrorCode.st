
(* TODO: Add your comment here *)
FUNCTION_BLOCK readErrorCode
	(* 	read out the selected system language	*)
	fb_ArTextSysGetSystemLanguage(Execute := TRUE );
	
	(*	convert the error ID code to the hex representation string *)
	brdkStrUdintToA(DWORD_TO_UDINT(dwErrorNumber), ADR(strTextId),16);
	brdkStrInsertAtPos(ADR(strTextId),ADR('0x'),0);
	
	
	(* find the corresponding text in the textlist.	*)
	IF dwErrorNumber <> dwErrorNumberHist THEN
		fb_ArTextSysGetText.LanguageCode 	:= fb_ArTextSysGetSystemLanguage.LanguageCode;
		fb_ArTextSysGetText.Namespace		:= ADR('OPC_UA');
		fb_ArTextSysGetText.TextBuffer		:= ADR(strResult);
		fb_ArTextSysGetText.TextBufferSize  := SIZEOF(strResult);
		fb_ArTextSysGetText.TextID			:= ADR(strTextId);
		fb_ArTextSysGetText(Execute := TRUE);
		dwErrorNumberHist := dwErrorNumber;
	ELSE
		fb_ArTextSysGetText(Execute := FALSE);
	END_IF;
	
	
END_FUNCTION_BLOCK
