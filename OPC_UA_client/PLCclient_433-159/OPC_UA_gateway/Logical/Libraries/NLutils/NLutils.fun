
FUNCTION INT_TO_HEX : BOOL (*convert an INT to a HEX notation string*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		uiDecIn : UINT;
	END_VAR
	VAR_IN_OUT
		strValue : STRING[80];
	END_VAR
	VAR
		uiDecHist : UINT;
		aiRem : ARRAY[0..50] OF INT;
		iI : INT := 0;
		iLenght : INT := 0;
		uiNum : UINT;
		iX : INT;
		strElement : ARRAY[0..4] OF STRING[80];
		strHexOut : STRING[255];
	END_VAR
END_FUNCTION
