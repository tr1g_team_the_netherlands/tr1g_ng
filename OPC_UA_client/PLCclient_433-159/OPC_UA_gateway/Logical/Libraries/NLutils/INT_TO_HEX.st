
(* convert an INT to a HEX notation string *)
FUNCTION INT_TO_HEX
	IF (uiDecIn <> uiDecHist) THEN
		strElement[0]:='';
		strElement[1]:='';
		strElement[2]:='';
		strElement[3]:='';
		strElement[4]:='';
		uiDecHist:=uiDecIn;
	END_IF;
	
	uiNum:=uiDecIn;
	
	WHILE (uiNum>0) DO
		aiRem[iI] := uiNum MOD 16;
		uiNum := uiNum / 16;
		iI := iI+1;
		iLenght := iLenght+1;
	END_WHILE

	FOR iX:= iLenght-1 TO 0 BY -1 DO
		CASE aiRem[iX] OF
			10 : strElement[iX] := 'A';
			11 : strElement[iX] := 'B';
			12 : strElement[iX] := 'C';
			13 : strElement[iX] := 'D';
			
			
			14 : strElement[iX] := 'E';
			15 : strElement[iX] := 'F';
			ELSE
			strElement[iX] := INT_TO_STRING(aiRem[iX]);
		END_CASE
	END_FOR

	IF uiDecIn = 0 THEN
		strHexOut:= '00';
	ELSE
		strHexOut := CONCAT ( CONCAT(strElement[3],strElement[2]),CONCAT(strElement[1],strElement[0]));
	END_IF
	iI:=iLenght:=uiNum:=iX:=0;
	strValue := strHexOut;

	
	
END_FUNCTION
