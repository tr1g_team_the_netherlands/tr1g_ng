
TYPE
	structDataFromTr1g : 	STRUCT 
		usiPosNr : USINT;
		xS07 : BOOL;
		xS06 : BOOL;
		xS05 : BOOL;
		xS04 : BOOL;
		xS02 : BOOL;
		xS01 : BOOL;
	END_STRUCT;
	structDataToTr1g : 	STRUCT 
		usiPosNr : USINT;
	END_STRUCT;
END_TYPE
