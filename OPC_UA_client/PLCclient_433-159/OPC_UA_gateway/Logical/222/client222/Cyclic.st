
PROGRAM _INIT
	(* Insert code here *)
	diSessionCounter := 0;
	tConnectionTimeout:=T#50s;
	
	
	fb_UA_Connect.ServerEndpointUrl := 'opc.tcp://10.1.25.56:4840';
	
	
	
	
	
END_PROGRAM

PROGRAM _CYCLIC
	
	struct_SessionConnectInfo.SecurityMsgMode 							:= UASecurityMsgMode_None;
	struct_SessionConnectInfo.SecurityPolicy 							:= UASecurityPolicy_None;
	struct_SessionConnectInfo.TransportProfile 							:= UATP_UATcp;
	struct_SessionConnectInfo.UserIdentityToken.UserIdentityTokenType 	:= UAUITT_Username;
	struct_SessionConnectInfo.UserIdentityToken.TokenParam1 			:= 'OpcMarel';
	struct_SessionConnectInfo.UserIdentityToken.TokenParam2 			:= 'OpcMarel';
	struct_SessionConnectInfo.SessionTimeout 							:= T#20s;
	struct_SessionConnectInfo.MonitorConnection 						:= T#100s;
	
	// map data from hw inputs to a variable
	structToTr1G.usiPosNr.0 := di_usiPosNr1;
	structToTr1G.usiPosNr.1 := di_usiPosNr2;
	structToTr1G.usiPosNr.2 := di_usiPosNr3;

	(*-------------------UA_Connect - establish connection to OPC-UA Server --------------------*)
	CASE opc_state OF
		OPC_UA_CONNECT:
			fb_UA_Connect.Execute := TRUE;
			fb_UA_Connect.SessionConnectInfo := struct_SessionConnectInfo;
			fb_UA_Connect.Timeout := tConnectionTimeout;
			fb_UA_Connect();
		
			IF (fb_UA_Connect.Busy = 0) THEN
				IF (fb_UA_Connect.Done = 1) THEN
					ErrorID := 0;
					strMessage := '';
					fb_UA_Connect(Execute := FALSE);	
					opc_state := OPC_UA_GETNAMESPACE;	
				END_IF
			
				IF (fb_UA_Connect.Error = 1) THEN
					fb_UA_Connect(Execute := FALSE);

					CASE DWORD_TO_UDINT(fb_UA_Connect.ErrorID) OF
					
						16#80AD_0000: // not able to connect to server
							strMessage := 'not able to connect to server';

						ELSE
							strMessage := '';
							opc_state := OPC_UA_DISCONNECT;	
						
						
					END_CASE
					
				END_IF
			
			END_IF
		
			
		(* --------------------UA_GetNamespaceIndex - read index of required namespace for PVs -------*)
		OPC_UA_GETNAMESPACE:
			fb_UA_GetNamespaceIndex.Execute 		:= TRUE;
			fb_UA_GetNamespaceIndex.ConnectionHdl 	:= fb_UA_Connect.ConnectionHdl;
			fb_UA_GetNamespaceIndex.NamespaceUri 	:= 'urn:B&R/pv/';
			fb_UA_GetNamespaceIndex.Timeout 		:= tConnectionTimeout;
			fb_UA_GetNamespaceIndex();
			
			IF (fb_UA_GetNamespaceIndex.Busy = 0) THEN
				IF (fb_UA_GetNamespaceIndex.Done = 1) THEN
					ErrorID:= 0;	
					fb_UA_GetNamespaceIndex(Execute := FALSE);
					FOR i := 0 TO usiREAD_ITEMS_ARR DO
						NodeIDs_0[i].NamespaceIndex := fb_UA_GetNamespaceIndex.NamespaceIndex;
						NodeIDs_0[i].IdentifierType := UAIdentifierType_String;
						NodeAddInfo_0[i].AttributeId := UAAI_Value;
						NodeAddInfo_0[i].IndexRangeCount := 0;
					END_FOR

					NodeIDs_0[0].Identifier 	:= '::Com:toPds.usiPosNr';
					NodeIDs_0[1].Identifier 	:= '::Com:toPds.xS01';
					NodeIDs_0[2].Identifier 	:= '::Com:toPds.xS02';
					NodeIDs_0[3].Identifier 	:= '::Com:toPds.xS04';
					NodeIDs_0[4].Identifier 	:= '::Com:toPds.xS05';
					NodeIDs_0[5].Identifier 	:= '::Com:toPds.xS06';
					NodeIDs_0[6].Identifier 	:= '::Com:toPds.xS07';
				//	NodeIDs_0[7].Identifier 	:= '::Com:toPds.dintArray';
					
					Variables_Rd0[0] 			:= '::client222:structFromTr1G.usiPosNr';
					Variables_Rd0[1] 			:= '::client222:structFromTr1G.xS01';
					Variables_Rd0[2] 			:= '::client222:structFromTr1G.xS02';
					Variables_Rd0[3] 			:= '::client222:structFromTr1G.xS04';
					Variables_Rd0[4] 			:= '::client222:structFromTr1G.xS05';
					Variables_Rd0[5] 			:= '::client222:structFromTr1G.xS06';
					Variables_Rd0[6] 			:= '::client222:structFromTr1G.xS07';
				//	Variables_Rd0[7] 			:= '::client222:structFromTr1G.dintArray';
					
					opc_state 					:= OPC_UA_READ_BULK;		
				END_IF
			
				IF (fb_UA_GetNamespaceIndex.Error = 1) THEN
					fb_UA_GetNamespaceIndex(Execute := FALSE);
					ErrorID:= fb_UA_GetNamespaceIndex.ErrorID;
					NamespaceIndex := 0;
					opc_state:=OPC_UA_DISCONNECT;
				END_IF
			END_IF
		
		(* UA_ReadList - read required nodes from OPC-UA Server and write it in local plc variables *)
		OPC_UA_READ_BULK:
			fb_UaClt_ReadBulk.Execute 		:= TRUE;
			fb_UaClt_ReadBulk.ConnectionHdl := fb_UA_Connect.ConnectionHdl;
			fb_UaClt_ReadBulk.NodeIDCount	:= usiREAD_ITEMS;
			fb_UaClt_ReadBulk.NodeIDs		:= ADR(NodeIDs_0);
			fb_UaClt_ReadBulk.NodeAddInfo	:= ADR(NodeAddInfo_0);
			fb_UaClt_ReadBulk.Variables		:= ADR(Variables_Rd0);
			fb_UaClt_ReadBulk.NodeErrorIDs	:= ADR(NodeErrorIDs_0);
			fb_UaClt_ReadBulk.TimeStamps	:= ADR(TimeStamps_0);
			fb_UaClt_ReadBulk.Timeout		:= T#10s;		
			fb_UaClt_ReadBulk();
		
			IF (fb_UaClt_ReadBulk.Busy = 0) THEN
				
				IF (fb_UaClt_ReadBulk.Done = 1) THEN
					fb_UaClt_ReadBulk(Execute := FALSE);
					structFromTr1G.dtTimeStampUsiPosNr := TimeStamps_0[0];
					structFromTr1G.dtTimeStampXs01 := TimeStamps_0[1];
					structFromTr1G.dtTimeStampXs02 := TimeStamps_0[2];
					structFromTr1G.dtTimeStampXs04 := TimeStamps_0[3];
					structFromTr1G.dtTimeStampXs05 := TimeStamps_0[4];
					structFromTr1G.dtTimeStampXs06 := TimeStamps_0[5];
					structFromTr1G.dtTimeStampXs07 := TimeStamps_0[6];
					
					

					NodeIDs_0[0].Identifier 	:= '::Com:fromPds.usiPosNr';
					
					Variables_Wr0[0] := '::client222:structToTr1G.usiPosNr';
					opc_state 					:= OPC_UA_WRITE_BULK;
					ErrorID:= 0;
				END_IF
				IF (fb_UaClt_ReadBulk.Error = 1) THEN
					fb_UaClt_ReadBulk(Execute := FALSE);
					
					ErrorID:= fb_UaClt_ReadBulk.ErrorID;
					opc_state 					:= OPC_UA_DISCONNECT;
					
				END_IF
			END_IF
		
		(* -----------------------Write client data to the OPC server-----------------------------*)
		OPC_UA_WRITE_BULK:
			fb_UaClt_WriteBulk.Execute 			:= TRUE;
			fb_UaClt_WriteBulk.ConnectionHdl 	:= fb_UA_Connect.ConnectionHdl;
			fb_UaClt_WriteBulk.NodeIDCount		:= usiWRITE_ITEMS;
			fb_UaClt_WriteBulk.NodeIDs 			:= ADR(NodeIDs_0);
			fb_UaClt_WriteBulk.NodeAddInfo 		:= ADR(NodeAddInfo_0);
			fb_UaClt_WriteBulk.Variables 		:= ADR(Variables_Wr0);
			fb_UaClt_WriteBulk.NodeErrorIDs 	:= ADR(NodeErrorIDs_0);
			fb_UaClt_WriteBulk.Timeout 			:= T#10s;
			fb_UaClt_WriteBulk();
			
			IF NOT fb_UaClt_WriteBulk.Busy THEN
				
				IF fb_UaClt_WriteBulk.Done THEN
					fb_UaClt_WriteBulk(Execute := FALSE);
					
					structToTr1G.dtTimeStampUsiPosNr := TimeStamps_0[0];
					
					ErrorID:= 0;
					NodeIDs_0[0].Identifier 	:= '::Com:toPds.usiPosNr';
					opc_state 					:= SEL(stopComm,OPC_UA_READ_BULK,OPC_UA_DISCONNECT);
				END_IF
				IF fb_UaClt_WriteBulk.Error THEN
					fb_UaClt_WriteBulk(Execute := FALSE);
					
					ErrorID:= fb_UaClt_WriteBulk.ErrorID;
					opc_state 					:= OPC_UA_DISCONNECT;
				END_IF
			END_IF		
			
			
		(* -----------------------UA_Disconnect - disconnect from OPC-UA Server *)
		OPC_UA_DISCONNECT:	
			fb_UA_Disconnect.Execute 		:= TRUE;
			fb_UA_Disconnect.ConnectionHdl 	:= fb_UA_Connect.ConnectionHdl;
			fb_UA_Disconnect.Timeout 		:= tConnectionTimeout;
			fb_UA_Disconnect();
			
			IF NOT fb_UA_Disconnect.Busy THEN
				IF fb_UA_Disconnect.Done THEN
					fb_UA_GetNamespaceIndex(Execute := FALSE);
					fb_UA_Connect(Execute := FALSE);
					opc_state 					:= SEL(stopComm,OPC_UA_CONNECT,OPC_UA_STOP);
				END_IF
				
				IF fb_UA_Disconnect.Error THEN
					fb_UA_Disconnect(Execute := FALSE);
					opc_state := OPC_UA_CONNECT;
				END_IF
				
			END_IF
		
		OPC_UA_STOP:
			opc_state 					:= SEL(stopComm,OPC_UA_CONNECT,OPC_UA_STOP);
		
		
	END_CASE;
	
	// map data from variable to hw outputs
	do_usiPosNr1 := structFromTr1G.usiPosNr.0;
	do_usiPosNr2 := structFromTr1G.usiPosNr.1;
	do_usiPosNr3 := structFromTr1G.usiPosNr.2;

END_PROGRAM
